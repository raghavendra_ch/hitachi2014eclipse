4-hydroxy-cyclophosphamide diffuses
4-hydroxycyclophosphamide
aberrant egfr gene copy
abnormal egfr expression
active cyclophosphamide metabolites
active egfr
adenocarcinoma lung
adjuvant cyclophosphamide
advanced lung cancer
advanced non-small-cell lung cancer
advanced nsclc
advanced stage lung cancer
advanced stage nsclc
advanced-stage lung cancer
advanced-stage nsclc
advanced/metastatic nsclc
afatinib
afatinib monotherapy
agr2-induced lung metastases
andunphosphorylated egfr
angiogenesis-related genes vegfr2
anti egfr
anti-egfr agents
anti-egfr antibodies
anti-egfr antibody therapy
anti-egfr mab cetuximab
anti-egfr monoclonal antibody
anti-egfr monoclonal antibody cetuximab
anti-egfr monocolonal antibody
anti-egfr targeted therapies
anti-egfr targeted therapy
anti-egfr therapies
anti-egfr treatment
anti-egfr treatments
anti-flt-1 (vegfr-1
anti-pegfr antibody
anti-vegfr2
apical lung tumor
autocrine egfr activation
autocrine egfr activity
autocrine egfr signaling
autocrine ligand-induced egfr activity
biopsy-proven interstitial lung disease
biotinylatedsecond egfr
bronchoscopic lung biopsy
cancer lung
carcinoma ex pleomorphic adenoma
carcinoma lung
carcinoma lung patients
carcinoma lung wrongly
carcinoma of lung
cavitary lung cancer
cell lung cancer (sclc
cell lung carcinoma (sclc
centrally localized lung tumor
centrally located nsclc
centrally located nsclc tumors
century lung cancer
ceruminous pleomorphic adenoma
characterize lung cancer
chemotherapy egfr
chemotherapy nsclc histology
clung metastasis
cmf cyclophosphamide
common egfr truncation mutation
conclusive egfrviii
ct-guided lung biopsies
cyclophosphamide
cyclophosphamide account
cyclophosphamide dme snps
cyclophosphamide metabolism
cyclophosphamide metabolism independently
cyclophosphamide-based adjuvant chemotherapy
cyclophosphamide-based chemotherapy
cyclophosphamide-dme snps
cyclophosphamide-metabolizing enzyme cyp3a4 independently
cyclophosphamide-metabolizing enzyme polymorphisms
cyclophosphamide-methotrexate
cytoplasmic egfr immunostaining
decorin antibody co-precipitated egfr
decorin-egfr relation
decreased p-egfr level
development of lung metastases
diagnosed lung cancer
diagnosing lung cancer
differentiated nsclc
distant metastatic lung cancer
distant stage lung cancer
doxorubicin/cyclophosphamide
dual egfr kinase
early lung cancer
early lung cancer patients
early stage lung cancer
early-stage lung cancer
egf-egfr pathway
egf-induced egfr downregulation process
egfr
egfr activating-mutations
egfr activation
egfr activity
egfr acts
egfr ag
egfr amplification
egfr anti-rabbit
egfr antibodies
egfr antibody
egfr assessment
egfr association
egfr autocrine pathway
egfr biomarkers
egfr blockade
egfr coexpression
egfr content
egfr contribution
egfr copy
egfr correlation
egfr dimers
egfr downregulation process
egfr dynamics
egfr evaluation
egfr exon
egfr expression
egfr expression data
egfr expression extent
egfr expression median scores
egfr extent
egfr family
egfr family members
egfr fragments
egfr gcn
egfr gene
egfr gene amplification
egfr gene copy
egfr generally
egfr high-resolution melting assays
egfr homodimers
egfr ihc
egfr immunoexpression
egfr immunopositivity
egfr immunoreactivity
egfr immunostain
egfr immunostaining
egfr inhibition
egfr inhibitor
egfr inhibitor erlotinib
egfr inhibitor therapy
egfr inhibitors
egfr intensity
egfr internalization/degradation
egfr inthe malignant tissue
egfr ip
egfr kinase
egfr kinases
egfr l858r
egfr lead
egfr ligand binding
egfr ligands
egfr missense mutations
egfr monoclonal antibody
egfr monoclonal antibody cetuximab
egfr mrna expression
egfr mutation
egfr mutation test kit
egfr mutation testing
egfr mutation types
egfr mutational analysis
egfr mutational profile
egfr mutations
egfr negative
egfr negative cases
egfr negative staining
egfr over-expression
egfr overexpression
egfr overexpression accounts
egfr pathway
egfr pathways
egfr pharmdx
egfr phenotype
egfr phospho-tyrosine levels
egfr phosphorylation
egfr positive
egfr positive cells
egfr positive staining
egfr positivity
egfr protein
egfr protein expression
egfr receptors
egfr regulation
egfr resistance
egfr sequences
egfr signal transduction
egfr signaling
egfr signaling pathway
egfr signalling
egfr silent mutations
egfr staining
egfr targeted antibodies
egfr targeted therapies
egfr targeting quinazolines
egfr thatco-immunoprecipitated
egfr tki
egfr tki erlotinib
egfr tki therapy
egfr transactivation
egfr transmembrane precursor
egfr tyrosine kinase activity
egfr tyrosine kinase domain
egfr tyrosine kinase inhibition
egfr tyrosine kinase inhibitor
egfr tyrosine kinase inhibitors
egfr variant
egfr wild-type patients
egfr(dako clone
egfr) pathway
egfr) tyrosine kinase
egfr)-inhibitor cetuximab
egfr)-targeting monoclonal antibody
egfr)-tyrosine
egfr+ subtypes
egfr-1
egfr-2
egfr-activating mutation
egfr-activating mutations
egfr-activation status
egfr-amplified sporadic breast tumours
egfr-blocking antibody
egfr-dependent signaling pathways
egfr-dependent signalling cascades
egfr-expressing ctcs
egfr-inhibitors
egfr-initiated signaling
egfr-mediated signaling
egfr-negative
egfr-negative primary tumor
egfr-overexpressing population
egfr-positive
egfr-positive cells
egfr-positive colon carcinomas
egfr-positive ctcs
egfr-positive/pegfr-negative carcinomas
egfr-positive/pegfr-negative tumours
egfr-positive/pegfr-positive tumours
egfr-resistant cell
egfr-resistant cell lines
egfr-targeted therapies
egfr-targeted treatments
egfr-targeting approaches
egfr-tki
egfr-tki therapy
egfr/ck expression
egfr/erk1/2 activation
egfr/erk1/2 pathway
egfr/pegfr expression
egfr/pegfr phenotype
egfrs
egfrviii
egfrviii detection
egfrviii expression
egfrviii mutation
egfrviii mutation negative
egfrviii mutation positive
egfrviii negative group
egfrviii positive group
egfrviii response
egfrviii-transfected scchn cells
egfrvlll cell line (u373flagegfrvlll
egfr–tki
egfr–tki subgroup
egfr–tki therapy
egfr–tki use
endobronchial lung biopsy
endobronchial lung cancer
endogenous egfr
enhancement of lung metastases
epithelial cell egfr
epithelial egfr
erlotinib
erlotinib blocks
erlotinib exposure
erlotinib group
erlotinib stabilization
erlotinib therapy
ex pleomorphic adenoma
express egfr
fibrosarcoma lung
flt-1egfr
fragmented egfr family members
frequent lung cancer
full human anti-egfr antibody
gpcr ligand-induced egfr transactivation
gpcr-egfr cross-talk
gpcr-induced egfr transactivation
gpcr-mediated egfr transactivation
high egfr expression
high egfr gcn
high egfr gene copy
high-dose cyclophosphamide
higher parent cyclophosphamide levels
his/her lung cancer diagnosis
histologically-confirmed bilateral lung metastasis
human anti-egfr antibody
human anti-egfr monoclonal antibody
human nonsmall-cell lung carcinomas
human nsclc cell lines
humanized anti-egfr igg1 antibody
immunostains egfr
incidence of lung cancer
incidence of lung metastases
increased lung cancer
inhibitor bibw-2992
ins316
intratumoral neovasculature
invasive lung cancer
invasive lung metastases
iressa
irreversible inhibition bibw-2992
left lung
left lung fields
left lung mass
left upper lobe lung
left upper lung fields
lewis lung carcinoma
ligand-activatable egfr
limited-disease small-cell lung cancer
localized stage lung cancer
locally advanced lung cancer
long-standing pleomorphic adenoma
low egfr expression groups
low egfr gcn
lung adenocarcinoma
lung adenocarcinomas
lung biopsies
lung biopsy
lung biopsy etc
lung biopsy specimen
lung biopsy specimens
lung cancer
lung cancer (sclc
lung cancer biology
lung cancer cases
lung cancer cases patients
lung cancer cells
lung cancer concluded
lung cancer data
lung cancer diagnosis
lung cancer metastasis
lung cancer patients
lung cancer prevalence
lung cancer risk
lung cancer screening
lung cancer screening trials
lung cancer signals incurability
lung cancer smear positive pulmonary
lung cancer solely
lung cancer staging
lung cancer subjects
lung cancer therapy
lung cancer tp53 mutations
lung cancer treatment
lung cancer type nsclc
lung cancer worldwide
lung cancer16
lung cancer19
lung cancers
lung carcinoma
lung carcinoma (sclc
lung carcinoma calu
lung carcinomas
lung lesion-only resection group
lung malignancy
lung metastases
lung metastases groups
lung metastases signature
lung metastases sooner
lung metastasis
lung metastatic
lung metastatic cells
lung metastatic colonies
lung neoplasms
lung resection
lung squamous cell carcinomas
lung tissue carcinomas
lung tumor
lung tumor progression
lung tumor suppressors
lung tumors
lung-metastatic 4175-lm2 population
lung-metastatic breast cancer cells
lungs malignancy
major egfr inactivation pathway
male lung cancer
male lung cancer patients
malignancy specially lung cancer
malignant lung
malignant lung carcinomas
malignant lung tumor
markedly pleomorphic tumor cells
markers egfr
median egfrviii fold
median egfrviii fold change
melanoma lung colonization
metastasis-associated lung adenocarcinoma transcript
metastasizing lung carcinoma
metastasizing pleomorphic adenoma
metastasizing pleomorphic adenoma carcinoma
metastatic egfr-positive patients
metastatic lung cancer
metastatic lung disease
metastatic lung tumors
metastatic non-small-cell lung cancer
metastatic nsclc
metastatic nsclc patients
metastatic/advanced non-small-cell lung cancer
metronomic cyclophosphamide
mimic lung carcinoma
mimic pleomorphic adenoma
mimic pleomorphic adenoma grossly
mimicked lung cancer
monoclonal egfr rabbit antibody
monophasic synovial sarcoma lung
multifocal lung carcinoma
multiple egfr
mutant egfr
neoplasm of lung
neoplastic lung disease
nlung cancer
nm_000141 vegfr1
nm_002019 vegfr2
nm_002253 vegfr3
non-egfr targeted agents
non-erlotinib group
non-erlotinib treated patients
non-small cell lung cancer
non-small cell lung cancers
non-small cell lung carcinogenesis
non-small cell lung carcinoma
non-small lung cancer
non-small lung cancer patients
non-small lung carcinoma
non-small lung tumor progression
non-small-cell lung cancer
non-small-cell lung cancer patients
non-small-cell lung cancers
non-small-cell lung carcinoma
non-small-cell lung carcinomas
nonactivated membrane-bound egfr
nonsmall cell lung cancer
nonsmall-cell lung carcinoma
non–small cell carcinoma lung
normal left lung fields
nsclc
nsclc accounts
nsclc cancer
nsclc cases
nsclc cell lines
nsclc cells
nsclc component
nsclc disease
nsclc histology
nsclc pathology
nsclc patients
nsclc subsets
nsclc therapy
nsclc tumor
nsclc tumors
nsclc typically
nsclc-nos
nuclear egfr
nuclear localized egfr
older lung cancer patients
open lung biopsies
open lung biopsy
open lung biopsy specimens
oral cyclophosphamide
paramalignant effusion
parotid pleomorphic adenoma
pcr-based egfr mutation testing
pegfr
pegfr association
pegfr coexpression
pegfr differential expression
pegfr expression
pegfr immunoreactivity
pegfr phenotype
pegfr play
pegfr positive
pegfr proteins
pegfr relationship
pegfr-positive ctcs
pegfr-positive patients
pemetrexed disodium
percutaneous lung biopsy
phospho-egfr
phosphorylated egfr
phosphorylated egfr family members
pleomorphic adenoma
pleomorphic adenoma component
pleomorphic adenoma radiologically
pleomorphic adenomas
pleomorphic carcinoma
pleomorphic high grade carcinomas
pleomorphic highly atypical adenocarcinoma
pleomorphic sarcoma
pleomorphic sarcomas
pleomorphic tumor cells
pleomorphic tumor foci
polyclonal egfr anti-rabbit
polyclonal pegfr anti-rabbit
positive egfr immunoreactivity
positive egfr immunostaining
post lung cancer diagnosis
potent dual egfr/erbb2 kinase inhibitor
practice of lung cancer
pre-egfr-testing era
prevalence of lung cancer
primary lung cancer
primary lung cancer patients
primary lung cancers
primary lung carcinoma
primary lung carcinomas
primary lung malignancy
primary lung tumors
primary synovial sarcoma lung
primer sequencesaaegfr
profile of lung cancer
proteins egfr
pulse intravenous cyclophosphamide
rabbit anti-egfr
radiologically suspicious lung malignancy
rare benign lung tumor
receptors vegfr-1
recurrent lung cancer
recurrent nsclc
reduction of lung metastases
reversible competitive egfr-selective tkis
select nsclc patients
selective egfr inhibitor
separate metastatic lung disease
shorter egfr intron
small cell lung cancer
small cell lung cancers
small cell lung carcinoma
small-cell lung cancer
solely egfr
spontaneous lung metastases
spontaneous lung metastasis
spontaneous lung metastasis burden
squamous cell lung cancer
stage iii/iv nsclc
stage lung cancer
staging lung cancer
staging of lung cancer
standard-dose cyclophosphamide
strong egfr
studied egfr
study lung malignancy
study pegfr
subsequent egfr dependent activation
subtype nsclc
suspected lung cancer
suspicion of lung cancer
synovial sarcoma lung
tarceva
the egfr exon
therapies targeting egfr
thetotal egfr
tn egfr
total egfr
total egfr expression
total egfr protein
trachea-bronchuslung cancer
transactive egfr
transbronchial lung biopsy
transbronchial lung biopsy specimen
transthoracic lung biopsy
tumour neovasculature
tumour progression egfr
tyrosine kinase receptors vegfr1
ultimately lung cancer
ultrasound-guided lung biopsy
upar-mediated egfr transactivation pathway
vinorelbine ditartrate
wild-type egfr
wild-type egfr control
wnt-induced egfr transactivation
wnt-mediated egfr transactivation
wnt1-driven egfr transactivation
wnt1-mediated egfr transactivation
worldwide lung cancer
younger nsclc subjects
