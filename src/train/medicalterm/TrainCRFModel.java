package train.medicalterm;

import java.io.IOException;

import edu.iiit.cde.r.crf.CRFModelGenerator;

/**
 * Class to train CRF model.
 * @author Raghavendra Ch
 *
 */
public class TrainCRFModel {

	
	/**
	 * Trains CRF model
	 * Usage : CRFModelGenerator <discharge_summary_dir> <pipedoutput_dir> <output_features_file> <output_crf_model>
	 * @param args
	 * @throws IOException
	 */
	public static void main (String[] args) throws IOException{
		CRFModelGenerator crfModelGenerator = new CRFModelGenerator();
		
		//train file without UMLS
		crfModelGenerator.generateFeaturesAndTrainWithOutUMLS(args[0],
				args[1],
				args[2],
				args[3]);
	
		//train file with umls
		/*crfModelGenerator.generateFeaturesAndTrainWithUMLS(args[0],
				args[1],
				args[2],
				args[3]);*/
				
	}
	
}
