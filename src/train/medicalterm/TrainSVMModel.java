package train.medicalterm;

import java.io.IOException;

import edu.iiit.cde.r.svm.SVMModelGenerator;

/**
 * Class to train SVM model
 * @author Raghavendra Ch
 *
 */
public class TrainSVMModel {
	
	/**
	 * Trains SVM model
	 * Usage : SVMModelGenerator <discharge_summary_dir> <pipedoutput_dir> <output_features_file> <output_svm_model>
	 * @param args
	 * @throws IOException
	 */
	public static void main (String[] args) throws IOException{
		SVMModelGenerator svmModelGenerator = new SVMModelGenerator();
		/*svmModelGenerator.generateFeaturesAndTrainSVM(args[0],
				args[1],
				args[2],
				args[3]);*/
	}

}
