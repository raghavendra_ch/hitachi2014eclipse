package com.cde.mihir;
/**
 * Contains all the parameters
 * @author mihirshekhar
 *
 */
public class GlobalVariable {
	// Folder where SVM-predict result for all the bi-classifier are kept.
	public static String SVMTestOutputFolder = "Test/temp";
	// path to svm-predict exe
	public static String PathToTestLIBSVMexe = "svm-predict";
	//url of UMLS
	public static String url="jdbc:mysql://localhost:3306/";
	//dbname of UMLS
	public static String dbName="umls2014aa";
	//driver name of UMLS
	public static String driver= "com.mysql.jdbc.Driver";
	//UMLS user name
	public static String userName="rag";
	//umls password
	public static String password="datacopy";
	// Path where all the disease dictionaries are kept. Only containing root words.
	public static String dictionaryFolderPath="dictionary";
	// Path to  semantic type vocabiulary
	public static String semanticVocabPath="vocabulary/sem.txt";
	// Path to  word type vocabiulary
	public static String wordVocabPath="vocabulary/term.txt";
	// Path to  umls type vocabiulary
	public static String umlsVocabPath="vocabulary/umls.txt";
	//Folder where piped output for training are kept
	public static String inputFolderTrain="DiseaseIdentification/trainingdata";
	//Path to Stop word file
	public static String stopwordPath="stopword/StopWord";
	//Path to folderwhere all svm bi-classifier models are kept 
	public static String modelFolder="model";
	//Path where features files generated for creating training models are kept
	public static String featureFolderTrain="Train/featurefile";
	//Path to svm-train exe
	public static String PathToSVMTrain="svm-train";
	//Path containing output by 1st module 
	public static String testInputFolder="Test/input";
	//Path contaijnig features generated for each file for testing phase.
	public static String testFeatureFolder="Test/featurefile";
	//Path containing output result for test module
	public static String testOutputFolder="output";
	
	public static String semanticTypes = "conf/clefSemanticTypes.txt";

}
