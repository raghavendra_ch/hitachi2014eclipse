package test.medicalterm;

import java.io.IOException;

import edu.iiit.cde.r.svm.SVMModelGenerator;

/**
 * Class used for testing SVM Model
 * @author raghavendra
 *
 */
public class TestSVMModel {
	
	/**
	 * Test SVM model
	 * Usage : SVMModelGenerator <discharge_summary_dir> <pipedoutput_dir> <output_SVM_model>
	 * It generated piped output files and stores in given <pipedoutput_dir>
	 * @param args
	 * @throws IOException
	 */
	public static void main (String[] args) throws IOException{
		SVMModelGenerator svmModelGenerator = new SVMModelGenerator();
		svmModelGenerator.svmTestBNER(args[0],
				args[1],
				args[2]);
	}
	
}
