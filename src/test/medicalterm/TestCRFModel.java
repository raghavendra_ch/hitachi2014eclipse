package test.medicalterm;

import java.io.File;
import java.io.IOException;

import edu.iiit.cde.r.crf.CRFModelGenerator;

/**
 * Class used for testing CRF model
 * @author raghavendra
 *
 */
public class TestCRFModel {

	
	
	
	/**
	 * Test CRF model
	 * Usage : CRFModelGenerator <discharge_summary_dir> <pipedoutput_dir> <output_crf_model>
	 * It generated piped output files and stores in given <pipedoutput_dir>
	 * @param args
	 * @throws IOException
	 */
	public static void main (String[] args) throws IOException{
		CRFModelGenerator crfModelGenerator = new CRFModelGenerator();
		
		boolean includeUMLS = true;
		//test file without UMLS and with PP. Uncomment this if you want to create piped output with ost processing.
		/*crfModelGenerator.crfTestBNERWithPP(args[0],
				args[1],
				args[2],
				includeUMLS);*/
		
		//test file without UMLS and with out PP
		/*crfModelGenerator.crfTestBNERWithOutPP(args[0],
				args[1],
				args[2],
				includeUMLS);*/
		
		if(args.length!=3) {
			System.err.println("Usage: crfModelGenerator <DocumentDirectory> <PipedOutputDirectory> <CRFModelPath>");
			/*"finaldata/pubmedDataset_phase4/test/DOCS",
			"finaldata/pubmedDataset_phase4/test/dec8_PIPED",
			"finaldata/pubmedDataset_phase4/model_dump/dec8_train_features.model",*/
		} else {
			new File(args[2]).mkdir();
			crfModelGenerator.crfTestBNERWithPP(args[0],
					args[1],
					args[2],
					true);
		}
	}
}
