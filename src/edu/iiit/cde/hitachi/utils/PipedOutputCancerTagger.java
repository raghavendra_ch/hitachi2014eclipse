package edu.iiit.cde.hitachi.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class PipedOutputCancerTagger {

	static HashMap<String, Integer> wordToCancerNumber = new HashMap<String, Integer>();
	
	static void loadMap() throws IOException {
		List<String> lines = FileUtils.readLines(new File("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/cancerDictionaries_phase1/brainterms_1_withDefs_grepBrain_uniq_phase1"));
		for (String line : lines) {
			wordToCancerNumber.put(line, 6);
		}
		
		lines = FileUtils.readLines(new File("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/cancerDictionaries_phase1/breastterms_1_withDefs_grepBreastDefs_phase1"));
		for (String line : lines) {
			wordToCancerNumber.put(line, 7);
		}
		lines = FileUtils.readLines(new File("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/cancerDictionaries_phase1/headandneckcancer_CUIs_Defs_checkedCUIs_terms_phase1"));
		for (String line : lines) {
			wordToCancerNumber.put(line, 8);
		}
		lines = FileUtils.readLines(new File("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/cancerDictionaries_phase1/lungterms_1_withDefs_greplungDefs_phase1"));
		for (String line : lines) {
			wordToCancerNumber.put(line, 9);
		}
		lines = FileUtils.readLines(new File("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/cancerDictionaries_phase1/prostateterms_1_withDefs_grepprostateDefs_phase1"));
		for (String line : lines) {
			wordToCancerNumber.put(line, 10);
		}
	}
	
	void tagPipedOutputsWithCancer (File pipedoutput, File pipedoutputWithCancer) throws IOException {
		List<String> lines = FileUtils.readLines(pipedoutput);
		List<String> outputLines = new ArrayList<String>();
		for (String line : lines) {
			String[] split = line.split("\\|");
			if (wordToCancerNumber.containsKey(split[2])) {
				outputLines.add(line+"|"+wordToCancerNumber.get(split[2].toLowerCase()));
				System.out.println(pipedoutput);
			} else {
				outputLines.add(line+"|0");
			}
		}
		//System.out.println(pipedoutputWithCancer);
		FileUtils.writeLines(pipedoutputWithCancer, outputLines);
	}
	
	void tagAllFiles () {
		File dir = new File ("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/brainCancerDocs_pipedoutput_merged");
		File opdir = new File ("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/brainCancerDocs_pipedoutput_cancerTagged");
		File[] files = dir.listFiles();
		for (File file : files) {
			try {
				tagPipedOutputsWithCancer(file, new File(opdir.getPath()+"/"+file.getName()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		dir = new File ("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/breastCancerDocs_pipedoutput_merged");
		opdir = new File ("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/breastCancerDocs_pipedoutput_cancerTagged");
		files = dir.listFiles();
		for (File file : files) {
			try {
				tagPipedOutputsWithCancer(file,new File(opdir.getPath()+"/"+file.getName()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		dir = new File ("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/headAndNeckCancerDocs_pipedoutput_merged");
		opdir = new File ("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/headAndNeckCancerDocs_pipedoutput_cancerTagged");
		files = dir.listFiles();
		for (File file : files) {
			try {
				tagPipedOutputsWithCancer(file, new File(opdir.getPath()+"/"+file.getName()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
		dir = new File ("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/lungCancerDocs_pipedoutput_merged");
		opdir = new File ("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/lungCancerDocs_pipedoutput_cancerTagged");
		files = dir.listFiles();
		for (File file : files) {
			try {
				tagPipedOutputsWithCancer(file, new File(opdir.getPath()+"/"+file.getName()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		dir = new File ("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/prostateCancerDocs_pipedoutput_merged");
		opdir = new File ("/home/raghavendra/BackUP/MyWorks/Hitachi/Hitachi2015/pubmedDataset/prostateCancerDocs_pipedoutput_cancerTagged");
		files = dir.listFiles();
		for (File file : files) {
			try {
				tagPipedOutputsWithCancer(file, new File(opdir.getPath()+"/"+file.getName()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Tagged All files!");
		//System.out.println(wordToCancerNumber);
	}
	
	public static void main (String[] args) throws IOException {
		loadMap();
		PipedOutputCancerTagger pipedOutputCancerTagger = new PipedOutputCancerTagger();
		pipedOutputCancerTagger.tagAllFiles();
	}
}
