package edu.iiit.cde.r.metamap;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import edu.iiit.cde.r.mysql.Java2MySql;
import edu.iiit.cde.r.umls.MedicalDictionary;
import edu.iiit.cde.r.umls.UMLSTermDiseaseIndexer;
import edu.iiit.cde.r.utils.HashMapUtilsCustom;

/**
 * MetaMapOutputProcessor extracts the attributes related to medical entities from MetaMap output.
 * Feew attributes are CUI (Concept Unique Idenitfier, concept Name, preferred name, Matched words and semantic type) 
 * @author raghavendra
 *
 */
public class MetaMapOutputProcessor {
	
	Java2MySql java2MySql; 
	
	UMLSTermDiseaseIndexer umlsTermDiseaseIndexer = new UMLSTermDiseaseIndexer();
	MedicalDictionary medicalDictionary = new MedicalDictionary();
	
	/**
	 * Constructor
	 * @throws IOException
	 */
	public MetaMapOutputProcessor() throws IOException {
		readSemanticTypes();
		
		try {
			java2MySql = new Java2MySql();
			
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	List<String> semanticTypes = new ArrayList<String> ();
	/**
	 * Loads UMLS Semantic types from configuration file.
	 * @throws IOException
	 */
	void readSemanticTypes() throws IOException {
		List<String> lines = FileUtils.readLines(new File("conf/clefSemanticTypes.txt"));
		for (String line : lines) {
			semanticTypes.add(StringUtils.substringBefore(line, "|"));
		}
	}
	
	/**
	 * Reads metamap output from the given input directory.
	 * Format of Input Directory :
	 * 	InputDir / DiseaseDirectory / DiseaseFiles
	 * @param inputDir Contains disease directories which in turn contains all files related to the disease
	 * @param outputDir 
	 */
	public void readMetaMapOP (File inputDir, String outputDir) {
		File[] diseaseDirs = inputDir.listFiles();
		for (File diseaseDir : diseaseDirs) {
			//for word - doc count
			readFilesOfDiseaseDocCount(diseaseDir, outputDir);
			//for word - word frequency
//			readFilesOfDisease(diseaseDir);
			
			//for wordWithPhrase - word frequency
//			readFilesOfDiseaseWithPhraseLookup(diseaseDir);
		}
	}
	
	/**
	 * Read files of each Disease Directory and dump its attributes into a file.
	 * Build HashMap of term and word Count for each disease 
	 * @param diseaseDir Contains all files related to the disease
	 */
	public void readFilesOfDisease(File diseaseDir) {
		
		File[] abstracts = diseaseDir.listFiles();
		HashMap<String, Integer> medicalTermCountMap = new HashMap<String, Integer>();
		//For each file
		for (File diseaseAbstractOPFile : abstracts) {
			try {
				String fileText = FileUtils.readFileToString(diseaseAbstractOPFile);
				String[] lines = fileText.split("\n\n");

				for(String line : lines) {
					String cui = StringUtils.substringBetween(line, "cui : ", "\n");
					String medicalTerm = StringUtils.substringBetween(line, "MatchedWords : [", "]");
					String semanticType = StringUtils.substringBetween(line, "SemanticType : [", "]");
					System.out.println(cui + "\t" + medicalTerm + "\t" + semanticType);
					
					String term = cui + "_" + StringUtils.replaceChars(medicalTerm, ",", "") + "_" + semanticType;
					
					if(semanticTypes.contains(semanticType)) {
						HashMapUtilsCustom.incrementKeyFreqCount(medicalTermCountMap, term);
					}
					
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		HashMapUtilsCustom.dumpHashMap(medicalTermCountMap, "/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/OPs/" + diseaseDir.getName()+".txt");
	}
	
	/**
	 * Read files of each Disease Directory and dump its attributes into a file
	 * Build HashMap of term and word Count for each disease 
	 * @param diseaseDir Contains all files related to the disease
	 */
	public void readFilesOfDiseaseUseTfIdf(File diseaseDir) {
		
		File[] abstracts = diseaseDir.listFiles();
		HashMap<String, Integer> medicalTermCountMap = new HashMap<String, Integer>();
		//For each file
		for (File diseaseAbstractOPFile : abstracts) {
			try {
				String fileText = FileUtils.readFileToString(diseaseAbstractOPFile);
				String[] sentences = fileText.split("Input Text: ");
				
				for (String sentenceMetamapOP : sentences) {
					
					String[] phrasesOp = sentenceMetamapOP.split(" phrase ");
					
					for (String phraseOp : phrasesOp) {
						
						String phrase = StringUtils.substringBefore(phraseOp, "\n");
						String[] lines = phraseOp.split("\n\n");
						
						System.out.println(phrase);
						//each input text
						for(String line : lines) {
							String cui = StringUtils.substringBetween(line, "cui : ", "\n");
							String medicalTerm = StringUtils.substringBetween(line, "MatchedWords : [", "]");
							String semanticType = StringUtils.substringBetween(line, "SemanticType : [", "]");
							
							String matchedPhrase = getMatchedPhrase (medicalTerm,phrase);
							
							System.out.println(cui + "\t" + medicalTerm + "\t" + semanticType + "\t"
									+ matchedPhrase);
							
							String term = cui + "_" + StringUtils.replaceChars(medicalTerm, ",", "") + "_" + semanticType;
//									+ "_" + matchedPhrase;
							
							if(semanticTypes.contains(semanticType)) {
								HashMapUtilsCustom.incrementKeyFreqCount(medicalTermCountMap, term);
							}
							
						}
					}
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		HashMapUtilsCustom.dumpHashMap(medicalTermCountMap, "/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/OPs/" + diseaseDir.getName()+".txt");
	}
	
	/**
	 * Read files of each Disease Directory and dump its attributes into a file
	 * Build HashMap of term and Document Count for each disease 
	 * @param diseaseDir Contains all files related to the disease
	 * @throws IOException 
	 */
	public void readFilesOfDiseaseDocCount(File diseaseDir, String outputDir) {
		
		File[] abstracts = diseaseDir.listFiles();
		int numberOfDocs = abstracts.length;
		HashMap<String, Integer> medicalTermCountMap = new HashMap<String, Integer>();
		for (File diseaseAbstractOPFile : abstracts) {
			try {
				String fileText = FileUtils.readFileToString(diseaseAbstractOPFile);
				String[] lines = fileText.split("\n\n");
				
				HashSet<String> uniqueWordsOfDoc = new HashSet<String>(); 
				//For each file
				for(String line : lines) {
					String cui = StringUtils.substringBetween(line, "cui : ", "\n");
					String medicalTerm = StringUtils.substringBetween(line, "MatchedWords : [", "]");
					String semanticType = StringUtils.substringBetween(line, "SemanticType : [", "]");
					System.out.println(cui + "\t" + medicalTerm + "\t" + semanticType);
					
					
					
					if(semanticTypes.contains(semanticType)) {
						String medicalString = StringUtils.replaceChars(medicalTerm, ",", "");
						String[] split = medicalString.split(" ");
						int spCount = StringUtils.countMatches(medicalString, " ");
						
						for (int i=0;i<=spCount;i++) {
							for (String token : HashMapUtilsCustom.getNGrams(split, i+1)) {
								String term = cui + "_" + token + "_" + semanticType;
								uniqueWordsOfDoc.add(term);
							}
						}
					}
					
				}
				
				for(String term : uniqueWordsOfDoc){
					HashMapUtilsCustom.incrementKeyFreqCount(medicalTermCountMap, term);
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		List<String> list = new ArrayList<String>();
    	for (Entry<String, Integer> entry : medicalTermCountMap.entrySet()) {
    		list.add(entry.getValue() + "\t" + numberOfDocs + "\t" + entry.getKey() );
    	}
		
    	try {
			FileUtils.writeLines(new File( outputDir +"/"
			+ diseaseDir.getName()+".txt"), list );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Read files of each Disease Directory and dump its attributes into a file
	 * Build HashMap of term and word Count for each disease 
	 * @param diseaseDir Contains all files related to the disease
	 */
	public void readFilesOfDiseaseWithPhraseLookup(File diseaseDir) {
		
		File[] abstracts = diseaseDir.listFiles();
		HashMap<String, Integer> medicalTermCountMap = new HashMap<String, Integer>();
		//For each file
		for (File diseaseAbstractOPFile : abstracts) {
			try {
				String fileText = FileUtils.readFileToString(diseaseAbstractOPFile);
				String[] sentences = fileText.split(" phrase ");
				
				for (String sentenceMetamapOP : sentences) {
					
					String[] lines = sentenceMetamapOP.split("\n\n");
					String phrase = StringUtils.substringBefore(sentenceMetamapOP, "\n");
					System.out.println(phrase);
					//each input text
					for(String line : lines) {
						String cui = StringUtils.substringBetween(line, "cui : ", "\n");
						String medicalTerm = StringUtils.substringBetween(line, "MatchedWords : [", "]");
						String semanticType = StringUtils.substringBetween(line, "SemanticType : [", "]");
						
						String matchedPhrase = getMatchedPhrase (medicalTerm,phrase);
						
						System.out.println(cui + "\t" + medicalTerm + "\t" + semanticType + "\t"
								+ matchedPhrase);
						
						String term = cui + "_" + StringUtils.replaceChars(medicalTerm, ",", "") + "_" + semanticType
								+ "_" + matchedPhrase;
						
						if(semanticTypes.contains(semanticType)) {
							HashMapUtilsCustom.incrementKeyFreqCount(medicalTermCountMap, term);
						}
						
					}
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		HashMapUtilsCustom.dumpHashMap(medicalTermCountMap, "/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/OPsWithPhrase/" + diseaseDir.getName()+".txt");
	}
	
	
	private String getMatchedPhrase(String phrase, String medicalTerm) {
		
		String[] splitWords = medicalTerm.split(", ");
		TreeMap<Integer, String> tm = new TreeMap<Integer, String>();
		
		for (String word : splitWords) {
			int index = StringUtils.indexOfIgnoreCase(phrase,medicalTerm);
			if(index!=-1) {
				tm.put(index, word.trim());
			} else {
				return null;
			}
		}
		
		
		int count = 0 ;
		int endindex = 0;
		StringBuffer sb = new StringBuffer();
		for (Entry<Integer, String> entry : tm.entrySet()) {
			if(count ==0) {
				sb.append(entry.getValue());
				//startindex+length of the string
				endindex = entry.getKey()+entry.getValue().length();
				count++;
			} else {
				
				if(Math.abs(entry.getKey()-endindex)<5) {
					sb.append(" " + entry.getValue());
				} else {
					sb.append("..."  + entry.getValue());
				}
				endindex = entry.getKey()+entry.getValue().length();
			}
		}
		
		return sb.toString();
	}

	/**
	 * Read MetaMap OP file
	 * @param mmOPfile
	 */
	public HashMap<String, Integer> readMetaMapOPFile (File mmOPfile) {
		HashMap<String, Integer> medicalTermCountMap = new HashMap<String, Integer>();
		try {
			String fileText = FileUtils.readFileToString(mmOPfile);
			String[] lines = fileText.split("\n\n");
			
			for(String line : lines) {
				String cui = StringUtils.substringBetween(line, "cui : ", "\n");
				String medicalTerm = StringUtils.substringBetween(line, "MatchedWords : [", "]");
				String semanticType = StringUtils.substringBetween(line, "SemanticType : [", "]");
				System.out.println(cui + "\t" + medicalTerm + "\t" + semanticType);
				
				String term = cui + "_" + StringUtils.replaceChars(medicalTerm, ",", "") + "_" + semanticType;
				
				if(semanticTypes.contains(semanticType)) {
					HashMapUtilsCustom.incrementKeyFreqCount(medicalTermCountMap, term);
				}
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(medicalTermCountMap);
		System.out.println(semanticTypes.size());
		return medicalTermCountMap;
	}
	
	
	public void annotateMedicalTermsOld (File diseaseDir, File dsDir, File outputFile) {

		File[] abstracts = diseaseDir.listFiles();
		//For each file
		for (File diseaseAbstractOPFile : abstracts) {
			try {
				TreeMap<String, HashMap<String,Integer>> spanToCUIsCount = new TreeMap<String, HashMap<String,Integer>>();
				String mmOPfileText = FileUtils.readFileToString(diseaseAbstractOPFile);
				String dsFileText = FileUtils.readFileToString(new File(dsDir+"/"+diseaseAbstractOPFile.getName()));
				String dsFileTextNoNewLine = StringUtils.replace(dsFileText, "\n", "");
				String[] sentences = mmOPfileText.split("Input Text: \n");
				
				for (String sentenceMetamapOP : sentences) {
					
					
					String inputText = StringUtils.substringBefore(sentenceMetamapOP, "\n");
					System.out.println("Input Text : " + inputText);
					
					
					int inputTextStartIndex = StringUtils.indexOf(dsFileTextNoNewLine, inputText);
					
					if(inputTextStartIndex == -1) 
						System.err.println("Wrong index");
					
					String[] phrasesOp = sentenceMetamapOP.split("\n phrase ");
					
					for (String phraseOp : phrasesOp) {
				
						String[] lines = phraseOp.split("\n\n");
						String phrase = StringUtils.substringBefore(phraseOp, "\n");
						//System.out.println("Each Phrase : " +phrase);
						
							//each input text
						for(String line : lines) {
							if(line.contains("cui :")) {
								String cui = StringUtils.substringBetween(line, "cui : ", "\n");
								String medicalTerm = StringUtils.substringBetween(line, "MatchedWords : [", "]");
								String semanticType = StringUtils.substringBetween(line, "SemanticType : [", "]");
								String conceptName = StringUtils.substringBetween(line, "Concept Name : ", "\n");
								String preferredName = StringUtils.substringBetween(line, "Preferred Name : ", "\n");
								
								String positionalInfo = StringUtils.substringBetween(line, "Positional Info : [(", ")]");
								
								String spanPair = getSpanPairOld(positionalInfo, inputTextStartIndex, dsFileText);
								
								//System.out.println(spanPair + "\t" + cui + "\t" + conceptName + "\t" + semanticType);
								
								String term = cui + "_" + conceptName + "_" + preferredName + "_" + semanticType;	
								boolean flag = false;
								if (semanticType.contains(",")) {
									String[] sts = semanticType.split(",");
									for (String st : sts) {
										if (semanticTypes.contains(st.trim())) {
											System.out.println(st.trim());
											flag = true;
											break;
										}
									}
								} else {
									flag = semanticTypes.contains(semanticType);
								}
								
								if(flag) {
									
									HashMap<String,Integer> cuisCount;
									
									if (spanToCUIsCount.containsKey(spanPair)) {
										cuisCount = spanToCUIsCount.get(spanPair);
									} else {
										cuisCount = new HashMap<String, Integer>();
									}
									HashMapUtilsCustom.incrementKeyFreqCount(cuisCount, term);
									spanToCUIsCount.put(spanPair, cuisCount);
								}
								
							}
						}
					}
					
					dumpAnnotatedFile(spanToCUIsCount,outputFile,diseaseAbstractOPFile.getName());
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//HashMapUtils.dumpHashMap(, "/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/Annotations/OPs/" + diseaseDir.getName()+".txt");
		
	}
	
	public void annotateMedicalTerms (File diseaseDir, File dsDir) {

		File[] abstracts = diseaseDir.listFiles();
		//For each file
		for (File diseaseAbstractOPFile : abstracts) {
			try {
				TreeMap<String, HashMap<String,Integer>> spanToCUIsCount = new TreeMap<String, HashMap<String,Integer>>();
				String mmOPfileText = FileUtils.readFileToString(diseaseAbstractOPFile);
				String dsFileText = FileUtils.readFileToString(new File(dsDir+"/"+diseaseAbstractOPFile.getName()));
				String dsFileTextNoNewLine = StringUtils.replace(dsFileText, "\n", "");
				String[] sentences = mmOPfileText.split("Input Text: \n");
				
				String[] OriginalSentencesList = dsFileText.split("\\.");
				HashMap<String, String> orgSentToReplacedSentMap = new HashMap<String, String>();
				for (String orgSen : OriginalSentencesList) {
					orgSentToReplacedSentMap.put(StringUtils.replace(orgSen, "\n", ""), orgSen);
				}
				
				
				for (String sentenceMetamapOP : sentences) {
					
					
					String inputText = StringUtils.substringBefore(sentenceMetamapOP, "\n").trim();
					System.out.println(inputText);
					
					
					
					/*int inputTextStartIndex = StringUtils.indexOf(dsFileTextNoNewLine, inputText);
					System.out.println("inputTextStartIndexInitial = " + inputTextStartIndex);
					int countOfNNToTextindex1 = inputTextStartIndex + StringUtils.countMatches(
							StringUtils.substring(dsFileText, 0, inputTextStartIndex), "\n");
					
					int countOfNNToTextindex2 = inputTextStartIndex + StringUtils.countMatches(
							StringUtils.substring(dsFileText, 0, countOfNNToTextindex1), "\n");
					
					System.out.println(countOfNNToTextindex1 + " == " + countOfNNToTextindex2);
					
					inputTextStartIndex = countOfNNToTextindex2;
					System.out.println("inputTextStartIndex = " + inputTextStartIndex);*/
					
					int inputTextStartIndex = -1;
					if(orgSentToReplacedSentMap.containsKey(inputText)) {
						inputTextStartIndex = StringUtils.indexOf(dsFileText, orgSentToReplacedSentMap.get(inputText));
					
					}
					
					if(inputTextStartIndex == -1) { 
						System.err.println("Wrong index");
						continue;
					}
					
					String[] phrasesOp = sentenceMetamapOP.split("\n phrase ");
					
					for (String phraseOp : phrasesOp) {
				
						String[] lines = phraseOp.split("\n\n");
						String phrase = StringUtils.substringBefore(phraseOp, "\n");
						//System.out.println(phrase);
						
							//each input text
						for(String line : lines) {
							if(line.contains("cui :")) {
								String cui = StringUtils.substringBetween(line, "cui : ", "\n");
								String medicalTerm = StringUtils.substringBetween(line, "MatchedWords : [", "]");
								String semanticType = StringUtils.substringBetween(line, "SemanticType : [", "]");
								String conceptName = StringUtils.substringBetween(line, "Concept Name : ", "\n");
								String preferredName = StringUtils.substringBetween(line, "Preferred Name : ", "\n");
								
								String positionalInfo = StringUtils.substringBetween(line, "Positional Info : [(", ")]");
								
								String spanPair = getSpanPair(positionalInfo, inputTextStartIndex, dsFileText);
								
								System.out.println(spanPair + "\t" + cui + "\t" + conceptName + "\t" + semanticType);
								
								String term = cui + "_" + conceptName + "_" + preferredName + "_" + semanticType;	
								
								if(semanticTypes.contains(semanticType)) {
									
									HashMap<String,Integer> cuisCount;
									
									if (spanToCUIsCount.containsKey(spanPair)) {
										cuisCount = spanToCUIsCount.get(spanPair);
									} else {
										cuisCount = new HashMap<String, Integer>();
									}
									HashMapUtilsCustom.incrementKeyFreqCount(cuisCount, term);
									spanToCUIsCount.put(spanPair, cuisCount);
								}
								
							}
						}
					}
					
//					dumpAnnotatedFile(spanToCUIsCount,diseaseAbstractOPFile.getName());
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//HashMapUtils.dumpHashMap(, "/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/Annotations/OPs/" + diseaseDir.getName()+".txt");
		
	}
	
	private String getSpanPair(String positionalInfo, int index, String dsFileText) {
		//System.out.println(positionalInfo);
		
		
		if (!positionalInfo.contains(",")){
			return null;
		}
		StringBuffer spanString = new StringBuffer();
		//49, 8), (85, 4), (98, 7
		if (positionalInfo.contains(")")) {
			
			String[] spansSplit = positionalInfo.split("\\), \\(");
			
			for (int i=0; i < spansSplit.length ; i++) {
				
				String eachSpan = spansSplit[i];				
				
				int termIndex = index+Integer.parseInt(eachSpan.split(",")[0]);
				
				int finalIndex1 = index + termIndex + StringUtils.countMatches(
						StringUtils.substring(dsFileText, index, index+termIndex), "\n");
					
				int finalIndex2 = finalIndex1 + StringUtils.countMatches(
						StringUtils.substring(dsFileText, finalIndex1, StringUtils.countMatches(
								StringUtils.substring(dsFileText, index, index+termIndex), "\n")), "\n");
				
				System.out.println("FinalIndex " + finalIndex1 +" = "+ finalIndex2);
				
				Pair<Integer, Integer> pair = Pair.of(finalIndex2,
						Integer.parseInt(eachSpan.split(", ")[1]));
				spanString.append(String.valueOf(pair.getLeft())+"-"+String.valueOf(pair.getLeft()+pair.getRight()));
				if(i != spansSplit.length)
					spanString.append(",");
			}
		} else {
			
			int termIndex = index+Integer.parseInt(positionalInfo.split(",")[0]);
			
			int finalIndex1 = index + termIndex + StringUtils.countMatches(
					StringUtils.substring(dsFileText, index, index+termIndex), "\n");
				
			int finalIndex2 = finalIndex1 + StringUtils.countMatches(
					StringUtils.substring(dsFileText, finalIndex1, StringUtils.countMatches(
							StringUtils.substring(dsFileText, index, index+termIndex), "\n")), "\n");
			
			System.out.println("FinalIndex2 " + finalIndex1 +" = "+ finalIndex2);
			Pair<Integer, Integer> pair = Pair.of(finalIndex2,
					Integer.parseInt(positionalInfo.split(", ")[1]));
				spanString.append(String.valueOf(pair.getLeft())+"-"+String.valueOf(pair.getLeft()+pair.getRight()));
		}
		
		return spanString.toString();
		
	}
	
	private void dumpAnnotatedFile(TreeMap<String, HashMap<String, Integer>> spanToCUIsCount, File outputDir,String filename) {
		List<String> list = new ArrayList<String>();
		StringBuffer sb;
		for (Entry<String, HashMap<String, Integer>> entry : spanToCUIsCount.entrySet()) {
    		sb = new StringBuffer();
    		
    		sb.append(filename+"|"+entry.getKey() + "|");
    		
    		
    		HashMap<String, Integer> cuisCount = entry.getValue();
    		
    		//appending all CUIs one by one
    		for (Entry<String, Integer> cuisEntry : cuisCount.entrySet()) {
    			sb.append(cuisEntry.getKey().replace("_", "|") + "|");
    		}
    		
//    		appending diseases for each CUI
    		for (Entry<String, Integer> cuisEntry : cuisCount.entrySet()) {
    			
    			String cui = StringUtils.substringBefore(cuisEntry.getKey(), "_");
    			
    			if(medicalDictionary.isCancer(cui)) {
    				System.out.println("cancer1");
    				sb.append("cancer1|");
    			} else if (medicalDictionary.isContextCancer(cui)) {
    				System.out.println("cancer2");
    				sb.append("cancer2|");
    			} else if (medicalDictionary.isNotCancer(cui)) {
    				System.out.println("notcancer");
    				sb.append("generic|");
    			}else {
    				System.out.println("generic");
    				sb.append("generic|");
    			}
    			
    			/*sb.append(cuisEntry.getKey().replace("_", "|") + "|" + 
    					java2MySql.getUMLSDefinitionsOfCUI(StringUtils.substringBefore(cuisEntry.getKey(), "_")) + 
    					"|"+ cuisEntry.getValue() +"|");*/
    		}
    		
    		list.add(sb.toString());
    	}
    	
    	try {
			FileUtils.writeLines(new File(outputDir.getPath() +"/"
					+filename.replace(".txt", ".pipe.txt")), list);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getSpanPairOld(String positionalInfo, int index, String dsFileText) {
		//System.out.println(positionalInfo);
		
		
		if (!positionalInfo.contains(",")){
			return null;
		}
		StringBuffer spanString = new StringBuffer();
		//49, 8), (85, 4), (98, 7
		if (positionalInfo.contains(")")) {
			
			String[] spansSplit = positionalInfo.split("\\), \\(");
			
			for (int i=0; i < spansSplit.length ; i++) {
				
				String eachSpan = spansSplit[i];				
				
				int termIndex = index+Integer.parseInt(eachSpan.split(",")[0]);
				
				Pair<Integer, Integer> pair = Pair.of(termIndex,
						Integer.parseInt(eachSpan.split(", ")[1]));
				spanString.append(String.valueOf(pair.getLeft())+"-"+String.valueOf(pair.getLeft()+pair.getRight()));
				if(i != spansSplit.length)
					spanString.append(",");
			}
		} else {
			
			int termIndex = index+Integer.parseInt(positionalInfo.split(",")[0]);
			
			Pair<Integer, Integer> pair = Pair.of(termIndex,
					Integer.parseInt(positionalInfo.split(", ")[1]));
				spanString.append(String.valueOf(pair.getLeft())+"-"+String.valueOf(pair.getLeft()+pair.getRight()));
		}
		
		return spanString.toString();
		
	}
	
	/**
	 * String of the span . abc of 1-3
	 * @param positionalInfo span value of the medical term
	 * @param inputText Input sentence
	 * @return
	 */
	private String getSpanStringFromInputText (String positionalInfo, String inputText) {
		//System.out.println(positionalInfo);
		
		
		if (!positionalInfo.contains(",")){
			return null;
		}
		StringBuffer spanString = new StringBuffer();
		//49, 8), (85, 4), (98, 7
		if (positionalInfo.contains(")")) {
			
			String[] spansSplit = positionalInfo.split("\\), \\(");
			
			for (int i=0; i < spansSplit.length ; i++) {
				
				String eachSpan = spansSplit[i];				
				
				int termIndex = Integer.parseInt(eachSpan.split(",")[0]);
				Pair<Integer, Integer> pair = Pair.of(termIndex,
						Integer.parseInt(eachSpan.split(", ")[1]));
				spanString.append(inputText.substring(pair.getLeft()
						,pair.getLeft()+pair.getRight()));
				if(i != spansSplit.length)
					spanString.append(" ");
			}
		} else {
			
			int termIndex = Integer.parseInt(positionalInfo.split(",")[0]);
			Pair<Integer, Integer> pair = Pair.of(termIndex,
					Integer.parseInt(positionalInfo.split(", ")[1]));
				spanString.append(inputText.substring(pair.getLeft()
						,pair.getLeft()+pair.getRight()));
		}
		
		return spanString.toString();
		
	}
	
	/**
	 * Extracts medical terms from metamap op.
	 * @param diseaseDir Contains all files related to the disease
	 * @param outputFile Stores all medical terms extracted.
	 * @throws IOException 
	 */
	public void getTermsFromDischargeSummaryMetamapOP (File diseaseDir, File outputFile) throws IOException {

		File[] dsMMop = diseaseDir.listFiles();
		TreeSet<String> termsSet = new TreeSet<String>();
		//For each file
		for (File diseaseAbstractOPFile : dsMMop) {
			try {
				String mmOPfileText = FileUtils.readFileToString(diseaseAbstractOPFile);
				String[] sentences = mmOPfileText.split("Input Text: \n");
				
				for (String sentenceMetamapOP : sentences) {
					
					String inputText = StringUtils.substringBefore(sentenceMetamapOP, "\n");
					//System.out.println("Input Text : " + inputText);
					
					String[] phrasesOp = sentenceMetamapOP.split("\n phrase ");
					
					for (String phraseOp : phrasesOp) {
				
						String[] lines = phraseOp.split("\n\n");
						String phrase = StringUtils.substringBefore(phraseOp, "\n");
						//System.out.println("Each Phrase : " +phrase);
						
							//each input text
						for(String line : lines) {
							if(line.contains("cui :")) {
								String cui = StringUtils.substringBetween(line, "cui : ", "\n");
								String medicalTerm = StringUtils.substringBetween(line, "MatchedWords : [", "]");
								String semanticType = StringUtils.substringBetween(line, "SemanticType : [", "]");
								String conceptName = StringUtils.substringBetween(line, "Concept Name : ", "\n").replace("|", "");
								String preferredName = StringUtils.substringBetween(line, "Preferred Name : ", "\n").replace("|", "");
								
								String positionalInfo = StringUtils.substringBetween(line, "Positional Info : [(", ")]");
								
								//System.out.println(spanPair + "\t" + cui + "\t" + conceptName + "\t" + semanticType);
								
								boolean flag = false;
								if (semanticType.contains(",")) {
									String[] sts = semanticType.split(",");
									for (String st : sts) {
										if (semanticTypes.contains(st.trim())) {
											//System.out.println(st.trim());
											flag = true;
											break;
										}
									}
								} else {
									flag = semanticTypes.contains(semanticType);
								}
								
								String term = cui + "_" + getSpanStringFromInputText(positionalInfo,inputText)
										+ "_" + semanticType + "_" +
										conceptName + "_" + preferredName;	
								if(flag) {
									
									//System.out.println(term);
									termsSet.add(term);
									
								}
								
							}
						}
					}
					
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		for (String term : termsSet) {
			FileUtils.write(outputFile, term +"\n" , true);
		}
		//HashMapUtils.dumpHashMap(, "/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/Annotations/OPs/" + diseaseDir.getName()+".txt");
		
	}
	
	public static void main (String[] args) throws IOException {
		MetaMapOutputProcessor diseaseAttributeExtractor = new MetaMapOutputProcessor();
		long st= System.currentTimeMillis();
//		diseaseAttributeExtractor.readInputDir(new File("/home/raghavendra/BackUP/MyWorks/PubMed/TaggedOutput/articles.C-H/"));
		if(args.length !=2) {
			System.err.println("Usage : <diseaseMMOPdir> <OUTPUTFile>");
			System.exit(0);
		}
		FileUtils.deleteQuietly(new File(args[1]));
		diseaseAttributeExtractor.getTermsFromDischargeSummaryMetamapOP(new File(args[0]), new File(args[1]));
//		diseaseAttributeExtractor.getTermsDefsAndCancerFromDischargeSummaryMetamapOP(new File(args[0]), new File(args[1]));
		
		
		//Annotate discharge summaries
		/*if(args.length !=3) {
			System.err.println("Usage : <mmOPdir> <DSdir> <OUTPUTFILE>");
			System.exit(0);
		}
		FileUtils.deleteQuietly(new File(args[2]));
		diseaseAttributeExtractor.annotateMedicalTermsOld(new File(args[0]),
				new File(args[1]), new File(args[2]));*/
		
		System.out.println(System.currentTimeMillis()-st);
//		diseaseAttributeExtractor.readFilesOfDiseaseWithPhraseLookup(
////				new File("/home/raghavendra/BackUP/MyWorks/MedicalParser/TaggedOutput/articles.C-H/Cancer"));
//				new File("/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/mmTaggedIP/Cancer"));
	}
}
