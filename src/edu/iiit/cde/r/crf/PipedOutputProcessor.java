package edu.iiit.cde.r.crf;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import opennlp.tools.util.Span;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import edu.iiit.cde.r.mysql.Java2MySql;
import edu.iiit.cde.r.utils.CancerDiseaseDictionary;
import edu.iiit.cde.r.utils.ClefPipedLineParser;
import edu.iiit.cde.r.utils.ClinicalTokenizer;
import edu.iiit.cde.r.utils.HashMapUtilsCustom;
import edu.iiit.nlp.OpenNLPTokenizer;

/**
 * PipedOutputProcessor contains the methods to perform various functions on piped output. 
 * @author raghavendra
 *
 */
public class PipedOutputProcessor {

	Java2MySql java2MySql; 
	ClefPipedLineParser clefPipedLineParser = new ClefPipedLineParser();
	ClinicalTokenizer cli = new ClinicalTokenizer();

	public PipedOutputProcessor() {
		java2MySql = Java2MySql.getInstance();
	}

	/**
	 * Compares the gold standard(piped output) and the predicted (piped output) file of CRF model and 
	 * returns the predicted medical terms which are overlapped with gold standard. 
	 * @param corpusDir
	 * @param goldDir
	 * @param crfopDir
	 * @param relaxedWithDefs
	 * @throws IOException
	 */
	void getRelaxedWords (String corpusDir, String goldDir, String crfopDir, String relaxedWithDefs) throws IOException {

		FileUtils.deleteQuietly(new File(relaxedWithDefs));

		File[] corpusFiles = new File(corpusDir).listFiles();
		for(File corpusFile : corpusFiles) {

			File goldFile = new File(goldDir + "/" + corpusFile.getName().replace(".txt", ".pipe.txt")); 
			File crfopFile = new File(crfopDir + "/" + corpusFile.getName().replace(".txt", ".pipe.txt"));
			String DStext = FileUtils.readFileToString(corpusFile, "utf-8");
			List<String> goldLines = FileUtils.readLines(goldFile);
			List<String> predictedLines = FileUtils.readLines(crfopFile);
			String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize(DStext);
			TreeMap<Integer, Integer> spanToTokenNumberMap = getStartSpansForTokens(corpusFile, tokens);
			//		System.out.println(spanToTokenNumberMap.size());
			for (String predictedLine : predictedLines) {
				//overlap (predictedLine, goldLines);

				if(StringUtils.countMatches(predictedLine, "|")!=2)
					continue;

				//System.out.println(predictedLine);
				String spanPredicted = predictedLine.split("\\|")[1];
				String[] pair1Str = spanPredicted.split("\\-");
				//System.out.println(pair1Str[0]);
				int[] pair1 = new int[]{Integer.parseInt(pair1Str[0]),Integer.parseInt(pair1Str[1])};

				for (String goldLine : goldLines) {

					if(StringUtils.countMatches(goldLine, "|")!=2)
						continue;

					String spanGold = goldLine.split("\\|")[1];
					/*String[] spanString = slots[1].split(",");

					int[] spanCUI = new int[spanString.length * 2];

					//Iterate on each span of each line.
					for (int i = 0; i < spanString.length; i++) {

						spanCUI[i * 2] = Integer.parseInt(spanString[i].split("-")[0]);
						spanCUI[i * 2 + 1] = Integer.parseInt(spanString[i].split("-")[1]);
					}*/
					/*System.out.println( "spanddddd = " + spanGold);
					System.out.println("span gold " + spanGold.split(",")[0]);*/
					String[] pair2Str = spanGold.split(",")[0].split("-");
					String goldTerm = goldLine.split("\\|")[2];
					String predictedTerm = predictedLine.split("\\|")[2];


					int[] pair2 = new int[]{Integer.parseInt(pair2Str[0]),Integer.parseInt(pair2Str[1])};

					//System.out.println("===" + pair1[0] + " \t "+ pair1[1] +"\t"+ pair2[0] +"\t"+pair2[1]);
					//spans not matching
					if ((pair2[0] >= pair1[1])||(pair1[0]>=pair2[1])) {
						continue;
					} // spans equal 
					else if ((pair1[0] == pair2[0])&&(pair1[1]==pair2[1])){
						break;
					} // spans overlapping 
					else  {
						int spanStartFirstWordPredictedTerm  = pair1[0];
						int spanStartLastWordPredictedTerm = spanStartFirstWordPredictedTerm;
						//if there is space in predicted term(bi/trigram).. get last token start index 
						if (!StringUtils.substringAfterLast(predictedTerm, " ").isEmpty()){
							spanStartLastWordPredictedTerm = pair1[1]-StringUtils.substringAfterLast(predictedTerm, " ").length()+1;
						}

						System.out.println("predicted Line = " + predictedLine);
						System.out.println("spanstart firstword pred =" + spanStartFirstWordPredictedTerm);
						//get the token number of the first word predicted line
						int firsttokenNumber =0 ;
						int lasttokenNumber =0 ; 
						if(spanToTokenNumberMap.containsKey(spanStartFirstWordPredictedTerm)) {
							firsttokenNumber = spanToTokenNumberMap.get(spanStartFirstWordPredictedTerm);
							System.out.println("spanstart lastword pred =" + spanStartLastWordPredictedTerm);
							if(spanToTokenNumberMap.containsKey(spanStartLastWordPredictedTerm)) {
								lasttokenNumber = spanToTokenNumberMap.get(spanStartLastWordPredictedTerm);
							}
						}


						String nearByWords = getNearByTokens(tokens, firsttokenNumber+1, 2+StringUtils.countMatches(DStext.substring(pair1[0], pair1[1]), " "));
						System.out.println("near by = " + nearByWords);

						FileUtils.write(new File(relaxedWithDefs), "Gold = "+  goldTerm +" \t predicted = "
								+ predictedTerm  + "\tNear by words = " + nearByWords + "\n", true );

						boolean flagGranted = false;
						String leftWord = getPreviousToken(tokens, firsttokenNumber, 1);
						String rightWord = getLaterToken(tokens, lasttokenNumber, 1);
						//check X0 X
						if(!leftWord.isEmpty() && !flagGranted) {
							String extendedPredictedTerm = leftWord + " " + predictedTerm;
							FileUtils.write(new File(relaxedWithDefs), "Checking X0 X= " + extendedPredictedTerm + "\n", true );
							flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm, relaxedWithDefs);
						}
						//check X X1
						if(!flagGranted && !rightWord.isEmpty()) {
							String extendedPredictedTerm = predictedTerm + " " + rightWord ;
							FileUtils.write(new File(relaxedWithDefs), "Checking X X1 = " + extendedPredictedTerm + "\n", true );
							flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm, relaxedWithDefs);
						}
						String extendedPredictedTerm = "";
						//four grams
						if (StringUtils.countMatches(predictedTerm, " ") >= 3) {
							//If X is the predicted term
							//							if(!leftWord.isEmpty() || !rightWord.isEmpty()) {
							extendedPredictedTerm = leftWord + " " + predictedTerm + " " + rightWord ;
							extendedPredictedTerm = extendedPredictedTerm.trim();
							//							} 

						}

						//tri grams
						if (StringUtils.countMatches(predictedTerm, " ") == 2) {
							//							if(!leftWord.isEmpty() || !rightWord.isEmpty()) {
							extendedPredictedTerm = leftWord + " " + predictedTerm + " " + rightWord ;
							extendedPredictedTerm = extendedPredictedTerm.trim();
							//							} 

						}

						//bigrams
						if (StringUtils.countMatches(predictedTerm, " ") == 1) {
							//							if(!leftWord.isEmpty() || !rightWord.isEmpty()) {
							extendedPredictedTerm = leftWord + " " + predictedTerm + " " + rightWord ;
							extendedPredictedTerm = extendedPredictedTerm.trim();
							//							} 

						}

						//unigrams
						if (StringUtils.countMatches(predictedTerm, " ") == 0) {
							leftWord = getPreviousToken(tokens, firsttokenNumber, 2);
							rightWord = getLaterToken(tokens, lasttokenNumber, 2);
							//							if(!leftWord.isEmpty() || !rightWord.isEmpty()) {
							extendedPredictedTerm = leftWord + " " + predictedTerm + " " + rightWord ;
							extendedPredictedTerm = extendedPredictedTerm.trim();
							//							} 
						}

						FileUtils.write(new File(relaxedWithDefs), "Checking extended term = " + extendedPredictedTerm + "\t", true );
						flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm, relaxedWithDefs);
						checkAllNGramsForMedicalTerms(extendedPredictedTerm, relaxedWithDefs, 3);
						checkAllNGramsForMedicalTerms(extendedPredictedTerm, relaxedWithDefs, 2);
						checkAllNGramsForMedicalTerms(extendedPredictedTerm, relaxedWithDefs, 1);

						//dont delete
						/*//trigram check all combinations 
						if (StringUtils.countMatches(predictedTerm, " ") >= 2) {
							//If X is the predicted term
							// check for X0 X X1
							if(!leftWord.isEmpty() || !rightWord.isEmpty()) {
								String extendedPredictedTerm = leftWord + " " + predictedTerm + " " + rightWord ;
								extendedPredictedTerm = extendedPredictedTerm.trim();
								FileUtils.write(new File(relaxedWithDefs), "Checking X0 X X1 = " + extendedPredictedTerm + "\n", true );
								flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm, relaxedWithDefs);
							} 
							List<String> trigrams = ClinicalTokenizer.getNGrams(predictedTerm, 3);
							for (String trigram : trigrams) {
								trigram = trigram.replace("_", " ");
								FileUtils.write(new File(relaxedWithDefs), "Checking trigrams = " + trigram + "\n" , true );
								flagGranted = checkForExtendedWordInUMLS(trigram, relaxedWithDefs);
								if(flagGranted)
									break;
							}
						}

						//check X
						if(!flagGranted) {
							FileUtils.write(new File(relaxedWithDefs), "Checking X = " + predictedTerm +"\n" , true );
							flagGranted = checkForExtendedWordInUMLS(predictedTerm, relaxedWithDefs);
						}
						// if X is > 3 chop X into bigrams. get first bigram found in UMLS
						if(!flagGranted) {

						}
						// if X is trigram chop X into bigrams. get first bigram found in UMLS
						if(!flagGranted) {
							if (StringUtils.countMatches(predictedTerm, " ") == 2) {
								List<String> bigrams = ClinicalTokenizer.getNGrams(predictedTerm, 2);
								for (String bigram : bigrams) {
									bigram = bigram.replace("_", " ");
									FileUtils.write(new File(relaxedWithDefs), "Checking bigram = " + bigram + "\n" , true );
									flagGranted = checkForExtendedWordInUMLS(bigram, relaxedWithDefs);
									if(flagGranted)
										break;
								}
							}
						}*/
						FileUtils.write(new File(relaxedWithDefs), "\n\n" , true );
					}

				}

			}
		}
	}


	void getOverlappedTerms (String posDir, String crfop1Dir, String crfop2_2014Dir, String overlappedTerms) throws IOException {

		FileUtils.deleteQuietly(new File(overlappedTerms));

		File[] posFiles = new File(posDir).listFiles();
		for(File posFile : posFiles) {

			File crfop1File = new File(crfop1Dir + "/" + posFile.getName().replace(".txt", ".pipe.txt"));
			File crfop2_2014File = new File(crfop2_2014Dir + "/" + posFile.getName().replace(".txt", ".pipe.txt"));

			//checking for only subset of brain output files for 75% data
			if(!crfop1File.exists())
				continue;


			List<String> pos_Lines = FileUtils.readLines(posFile);
			List<String> crfop1_Lines = FileUtils.readLines(crfop1File);
			List<String> crfop2_Lines = FileUtils.readLines(crfop2_2014File);

			System.out.println("pos lines = "+pos_Lines.size());
			System.out.println("crfop1 lines = "+crfop1_Lines.size());
			System.out.println("crfop2 lines = "+crfop2_Lines.size());
			//		System.out.println(spanToTokenNumberMap.size());
			//			Iterate on POS lines and check the overlapped words of crfop1 and crfop2 
			for (String pos_Line : pos_Lines) {
				//overlap (predictedLine, goldLines);

				/*if(StringUtils.countMatches(predictedLine, "|")!=2)
					continue;*/

				//System.out.println(predictedLine);
				String spanPos = pos_Line.split("\\|")[1];
				String[] pos_pair1Str = spanPos.split("\\-");
				String pos_Term = pos_Line.split("\\|")[2];
				//System.out.println(pair1Str[0]);
				int[] pos_pair = new int[]{Integer.parseInt(pos_pair1Str[0]),Integer.parseInt(pos_pair1Str[1])};
				boolean overlapped = false;
				StringBuffer sb_crfop1 = new StringBuffer();
				for (String crfop1_Line : crfop1_Lines) {

					if(StringUtils.countMatches(crfop1_Line, "|")!=2)
						continue;

					String spanCrf1 = crfop1_Line.split("\\|")[1];
					String[] crfop1_pairStr = spanCrf1.split(",")[0].split("-");
					String crfop1_Term = crfop1_Line.split("\\|")[2];

					//adding 1 for the right 
					int[] crfop1_pair = new int[]{Integer.parseInt(crfop1_pairStr[0]),Integer.parseInt(crfop1_pairStr[1])+1};

					//System.out.println("===" + pair1[0] + " \t "+ pair1[1] +"\t"+ pair2[0] +"\t"+pair2[1]);
					//spans not matching
					if ((crfop1_pair[0] >= pos_pair[1])||(pos_pair[0]>=crfop1_pair[1])) {
						continue;
					} // spans equal 
					else if ((pos_pair[0] == crfop1_pair[0])&&(pos_pair[1]==crfop1_pair[1])){
						break;
					} // spans overlapping 
					else  {
						sb_crfop1.append(crfop1_Term);
						overlapped = true;

					}

				}

				StringBuffer sb_crfop2 = new StringBuffer();
				for (String crfop2_Line : crfop2_Lines) {

					if(StringUtils.countMatches(crfop2_Line, "|")!=2)
						continue;

					String spanCrf2 = crfop2_Line.split("\\|")[1];
					String[] crfop2_pairStr = spanCrf2.split(",")[0].split("-");
					String crfop2_Term = crfop2_Line.split("\\|")[2];

					int[] crfop2_pair = new int[]{Integer.parseInt(crfop2_pairStr[0]),Integer.parseInt(crfop2_pairStr[1])+1};

					//System.out.println("===" + pair1[0] + " \t "+ pair1[1] +"\t"+ pair2[0] +"\t"+pair2[1]);
					//spans not matching
					if ((crfop2_pair[0] >= pos_pair[1])||(pos_pair[0]>=crfop2_pair[1])) {
						continue;
					} // spans equal 
					else if ((pos_pair[0] == crfop2_pair[0])&&(pos_pair[1]==crfop2_pair[1])){
						break;
					} // spans overlapping 
					else  {
						sb_crfop2.append(crfop2_Term);
						overlapped = true;
					}

				}
				if(overlapped) {
					FileUtils.write(new File(overlappedTerms), 
							posFile.getName() +" - "+ spanPos +" - pos = "+ pos_Term +
							",\tcrfop1 = " + sb_crfop1+ 
							",\tcrfop2 = " + sb_crfop2+ "\n", true );

				}
			}
		}
	}

	/**
	 * Merge pos and crf old outputs for medical term annotation using google api.
	 * @param pmaPipedOutput
	 * @param crfOldPipedOutput
	 * @param mergedPipedOutput
	 * @throws IOException 
	 */
	void mergePmaAndCRFoldUsingGoogleApiWorkingFinal (String corpusDir, String pmaPipedOutput, String crfOldPipedOutput, String mergedPipedOutput) throws IOException {

		new File(mergedPipedOutput).mkdir();
		File[] posFiles = new File(pmaPipedOutput).listFiles();
		for(File posFile : posFiles) {
			
			TreeMap<Span, String> spanToTextMap1 = new TreeMap<>();
			TreeMap<Span, String> spanToTextMap2 = new TreeMap<>();
			TreeMap<Span, String> spanToTextMap1_uniq = new TreeMap<>();
			TreeMap<Span, String> spanToTextMap2_uniq = new TreeMap<>();
			TreeMap<Span, String> equalSpansMap = new TreeMap<>();
			//Key is first list, value is second list
			TreeMap<Span, Span> overlappedSpansMap = new TreeMap<>();
			TreeMap<Span, String> mergedspanToTextMap = new TreeMap<>();

			File corpusFile = new File(corpusDir + "/" + posFile.getName());
			File crfop1File = new File(crfOldPipedOutput + "/" + posFile.getName().replace(".txt", ".pipe.txt"));
			File mergedFile = new File(mergedPipedOutput + "/" + posFile.getName().replace(".txt", ".pipe.txt"));
			FileUtils.deleteQuietly(mergedFile);
			//FileUtils.deleteQuietly(new File("overlappedTerms_pos_crf"));

			List<String> pos_Lines = FileUtils.readLines(posFile);
			//System.out.println("pos lines = "+pos_Lines.size());
			//checking for only subset of brain output files for 75% data
			if(!crfop1File.exists()) {
				System.err.println(crfop1File.getPath() + " File doesnot exists! So creating empty file!");
				//continue;
				crfop1File.createNewFile();
			}
			System.out.println("Processing .. " + posFile.getName());
			List<String> crfop1_Lines = FileUtils.readLines(crfop1File);
			String docText = FileUtils.readFileToString(corpusFile);

			
			//System.out.println("crfop1 lines = "+crfop1_Lines.size());
			//		System.out.println(spanToTokenNumberMap.size());

			spanToTextMap1 = readSpanAndTextFromHitachiFile(posFile, 0);
			spanToTextMap2 = readSpanAndTextFromHitachiFile(crfop1File, 1);

			createUniqAndOverLappedSpans(spanToTextMap1, spanToTextMap2, spanToTextMap1_uniq, 
					spanToTextMap2_uniq, equalSpansMap, overlappedSpansMap);

			/*System.out.println("spanToTextMap1 = " + spanToTextMap1.size());
			System.out.println("spanToTextMap2 = " + spanToTextMap2.size());
			System.out.println("spanToTextMap1_uniq = " + spanToTextMap1_uniq.size());
			System.out.println("spanToTextMap2_uniq = " + spanToTextMap2_uniq.size());
			System.out.println("equalSpansMap = " + equalSpansMap.size());
			System.out.println("overlappedSpansMap = " + overlappedSpansMap.size());*/
			/*String line = posFile.getName()+"|"+span.getStart+"-"+span.getEnd()+"|"+text;
			FileUtils.write(mergedFile, line +"\n", true);*/


			for (Entry<Span, String> entry1 : spanToTextMap1_uniq.entrySet()) {
//				System.out.println("uniq_term1 = " + entry1.getValue());
				putTermFromPOS_CRF (entry1.getKey(), entry1.getValue(), docText, posFile, mergedspanToTextMap);
			}
			for (Entry<Span, String> entry2 : spanToTextMap2_uniq.entrySet()) {
//				System.out.println("uniq_term2 = " + entry2.getValue());
				putTermFromPOS_CRF (entry2.getKey(), entry2.getValue(), docText, posFile, mergedspanToTextMap);
			}
			for (Entry<Span, String> entry1 : equalSpansMap.entrySet()) {
//				System.out.println("equal_term = " + entry1.getValue());
				putTermFromPOS_CRF (entry1.getKey(), entry1.getValue(), docText, posFile, mergedspanToTextMap);
			}
			//first span represents span of first output
			//second span represents span of second output
			for (Entry<Span, Span> entry1 : overlappedSpansMap.entrySet()) {
				String term1 = spanToTextMap1.get(entry1.getKey());
				String term2 = spanToTextMap2.get(entry1.getValue());
				//choosing the span with larger length
				if(entry1.getKey().length() >= entry1.getValue().length() || 
						(term2.contains("[") || term2.contains("]") || term2.contains("\"") || term2.contains("<") || 
								term2.contains(">") || term2.startsWith("of ") || term2.startsWith("in ")) 
						) {
					//System.out.println("term1 = " + term1);
					putTermFromPOS_CRF (entry1.getKey(), spanToTextMap1.get(entry1.getKey()), docText, posFile, mergedspanToTextMap);
				} else {
//					System.out.println("term2 = " + term2);
					putTermFromPOS_CRF (entry1.getValue(), spanToTextMap2.get(entry1.getValue()), docText, posFile, mergedspanToTextMap);
				}
			}
			
			for (Entry<Span, String> entry1 : mergedspanToTextMap.entrySet()) {
				String line = posFile.getName()+"|"+entry1.getKey().getStart()+"-"+entry1.getKey().getEnd()
						+"|"+entry1.getValue();
				FileUtils.write(mergedFile, line +"\n", true);
			}

			//clear all for each file
			spanToTextMap1.clear();
			spanToTextMap2.clear();
			spanToTextMap1_uniq.clear();
			spanToTextMap2_uniq.clear();
			equalSpansMap.clear();
			overlappedSpansMap.clear();
			mergedspanToTextMap.clear();

		}
	}

	/**
	 * For merging the CRF and pma
	 * @param correctSpan
	 * @param term1
	 * @param docText
	 * @param posFile
	 * @param mergedspanToTextMap
	 */
	private void putTermFromPOS_CRF(Span correctSpan, String term1, String docText, File posFile,
			TreeMap<Span, String> mergedspanToTextMap) {
		
		if(ClinicalTokenizer.isGarbageTerm(term1)) {
			return;
		}
		
		if ((term1.matches(".*[A-Z|a-z|)|]| ]\\.[A-Z|a-z|(|]| ].*") && term1.length() > 10) || 
				term1.contains("cancer.") || term1.contains("PCa.") ) {
			String left_term1 = StringUtils.substringBefore(term1, ".");
			Span left_correctSpan = new Span(correctSpan.getStart(), correctSpan.getStart()+term1.length());
			//System.out.println(posFile.getName() + " left-- " + correctSpan + " " +term1);
			/*if(!ClinicalTokenizer.isGarbageTerm(left_term1)) {
				mergedspanToTextMap.put(left_correctSpan, left_term1);
			}*/
			
			if (!checkForMultipleTerms(left_correctSpan, left_term1, docText, mergedspanToTextMap)
					) {
				putTerm(left_correctSpan, left_term1, docText, mergedspanToTextMap);
			} 
			
			String right_term1 = StringUtils.substringAfterLast(term1, ".");
			Span right_correctSpan = new Span(correctSpan.getEnd() - right_term1.length(), correctSpan.getEnd());
			//System.out.println(posFile.getName() + " right-- " + correctSpan + " " +term1);
			/*if(!ClinicalTokenizer.isGarbageTerm(right_term1)) {
				mergedspanToTextMap.put(right_correctSpan, right_term1);
			}*/
			if(!checkForMultipleTerms(right_correctSpan, right_term1, docText, mergedspanToTextMap)) {
				putTerm(right_correctSpan, right_term1, docText, mergedspanToTextMap);
			} 

		} else  {
			if(!checkForMultipleTerms(correctSpan, term1, docText, mergedspanToTextMap)) {
				//System.out.println(posFile.getName() + "full-- " + term1);
				putTerm(correctSpan, term1, docText, mergedspanToTextMap);
			} 
			
			/*String prunedTerm = ClinicalTokenizer.getPrunedMedicalTerm(term1);
			correctSpan = ClinicalTokenizer.getPrunedSpan (correctSpan, term1, prunedTerm);
			term1 = prunedTerm;
			
			if(!ClinicalTokenizer.isGarbageTerm(term1))
				mergedspanToTextMap.put(correctSpan, term1);*/
		}
	}
	
	/**
	 * For getting terms greater than 5
	 * @param correctSpan
	 * @param term1
	 * @param docText
	 * @param posFile
	 * @param mergedspanToTextMap
	 */
	private void putTermFromPOS_CRF_gt5(Span correctSpan, String term1, String docText, File posFile,
			TreeMap<Span, String> mergedspanToTextMap) {
		
		if(ClinicalTokenizer.isGarbageTerm(term1)) {
			return;
		}
		
		if (term1.contains(".") && term1.contains(" ") && term1.length() > 10) {
			String left_term1 = StringUtils.substringBefore(term1, ".");
			Span left_correctSpan = new Span(correctSpan.getStart(), correctSpan.getStart()+term1.length());
			//System.out.println(posFile.getName() + " -- " + correctSpan + " " +term1);
			/*if(!ClinicalTokenizer.isGarbageTerm(left_term1)) {
				mergedspanToTextMap.put(left_correctSpan, left_term1);
			}*/
			
			if (StringUtils.countMatches(left_term1, " ") >= 4 && !ClinicalTokenizer.checkForInstituteTerms(left_term1) && !checkForMultipleTerms(left_correctSpan, left_term1, docText, mergedspanToTextMap)) {
				putTerm(left_correctSpan, left_term1, docText, mergedspanToTextMap);
			}
			
			String right_term1 = StringUtils.substringAfter(term1, ".");
			Span right_correctSpan = new Span(correctSpan.getEnd() - right_term1.length(), correctSpan.getEnd());
			//System.out.println(posFile.getName() + " -- " + correctSpan + " " +term1);
			/*if(!ClinicalTokenizer.isGarbageTerm(right_term1)) {
				mergedspanToTextMap.put(right_correctSpan, right_term1);
			}*/
			if(StringUtils.countMatches(right_term1, " ") >= 4  && !ClinicalTokenizer.checkForInstituteTerms(right_term1) && !checkForMultipleTerms(right_correctSpan, right_term1, docText, mergedspanToTextMap)) {
				putTerm(right_correctSpan, right_term1, docText, mergedspanToTextMap);
			}

		} else  {
			if(StringUtils.countMatches(term1, " ") >= 4  && !ClinicalTokenizer.checkForInstituteTerms(term1) && !checkForMultipleTerms(correctSpan, term1, docText, mergedspanToTextMap)) {
				putTerm(correctSpan, term1, docText, mergedspanToTextMap);
			}
			
			/*String prunedTerm = ClinicalTokenizer.getPrunedMedicalTerm(term1);
			correctSpan = ClinicalTokenizer.getPrunedSpan (correctSpan, term1, prunedTerm);
			term1 = prunedTerm;
			
			if(!ClinicalTokenizer.isGarbageTerm(term1))
				mergedspanToTextMap.put(correctSpan, term1);*/
		}
	}

	private boolean checkForMultipleTerms(Span span, String term,
			String docText,
			TreeMap<Span, String> mergedspanToTextMap) {

		boolean flag = false;
		
		if (StringUtils.countMatches(term," ") <= 1 ) {
			return flag;
		}
			
//		flag = splitTermusingRegex(span, term, docText, mergedspanToTextMap, "(?=\\p{Upper}\\p{Lower}\\p{Lower})|\\s(?=\\p{Upper})");
		if(StringUtils.isAllUpperCase(term.replace(" ", "")))
			flag = splitTermusingRegex(span, term, docText, mergedspanToTextMap, "\\s(?=\\p{Upper}\\p{Upper}\\p{Upper})", false);
		if(!flag) {
			flag = splitTermusingRegex(span, term, docText, mergedspanToTextMap, "\\p{Lower}(?=\\p{Upper}\\p{Lower}\\p{Lower})", true);
		}
		if(!flag) {
			flag = splitTermusingRegex(span, term, docText, mergedspanToTextMap, " is ", false);
		}
		if(!flag) {
			flag = splitTermusingRegex(span, term, docText, mergedspanToTextMap, " of the ", false);
		}
		if(!flag) {
			flag = splitTermusingRegex(span, term, docText, mergedspanToTextMap, " of ", false);
		}
		if(!flag) {
			flag = splitTermusingRegex(span, term, docText, mergedspanToTextMap, "^of ", false);
			System.out.println(flag);
		}
		if(!flag) {
			flag = splitTermusingRegex(span, term, docText, mergedspanToTextMap, " for ", false);
		}
		if(!flag) {
			flag = splitTermusingRegex(span, term, docText, mergedspanToTextMap, " in ", false);
		} 
		if(!flag) {
			//uncomment to get actual terms and changed terms because of stop words
			/*System.out.print("Actual Term = " + term + "\t");
			System.out.println("Changed = " + term);*/
		}
		
		return flag;
	}

	private boolean splitTermusingRegex(Span span, String term, String docText,
			TreeMap<Span, String> mergedspanToTextMap, String regex, boolean regexCaps) {
		boolean flag = false;
		String[] subTerms = term.split(regex);
		if(subTerms.length==1)
			return flag;
		
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < subTerms.length; i++) {
			String subterm = subTerms[i].trim();
			
			if(subterm.isEmpty())
				continue;
			Span subSpan;
			if (i!=subTerms.length-1 && regexCaps) {
				subSpan = new Span(span.getStart()+StringUtils.indexOf(term, subterm), 
						span.getStart()+StringUtils.indexOf(term, subterm)+subterm.length()+1);
				subterm = StringUtils.substring(docText, subSpan.getStart(), subSpan.getEnd());
				sb.append(subterm + ", ");
			} else {
				subSpan = new Span(span.getStart()+StringUtils.indexOf(term, subterm), 
						span.getStart()+StringUtils.lastIndexOf(term, subterm)+subterm.length());			
				sb.append(subterm + ", ");
			}
			
			if(subTerms.length>1){
				putTerm(subSpan, subterm, docText, mergedspanToTextMap);
				flag = true;
			}
		}
		if(flag) {
			System.out.print("Actual Term = " + term + "\t");
			System.out.println("Changed = " + sb.toString());
		}
		return flag;
	}

	private void putTerm (Span correctSpan, String term2, String docText, 
			TreeMap<Span, String> mergedspanToTextMap) {
		
		if(ClinicalTokenizer.isGarbageTerm(term2)) {
			return;
		}
	
		if(term2.contains("[") || term2.contains("]")) {
			if(term2.contains("[") && term2.contains("]")) {
				String term2_trimmed = StringUtils.replacePattern(term2, "\\[.*\\]$", "").trim();
				term2_trimmed = StringUtils.replacePattern(term2_trimmed, "^\\[.*\\]", "").trim();
				correctSpan = new Span(correctSpan.getStart() + StringUtils.indexOf(term2, term2_trimmed),
						correctSpan.getStart() + StringUtils.lastIndexOf(term2, term2_trimmed));
				term2 = term2_trimmed;
			} else if (term2.contains("[")) {
				
				String term2_trimmed = StringUtils.replacePattern(term2, "\\[.*", "").trim();
				correctSpan = new Span(correctSpan.getStart() + StringUtils.indexOf(term2, term2_trimmed),
						correctSpan.getStart() + StringUtils.lastIndexOf(term2, term2_trimmed));
				term2 = term2_trimmed;
			} else if (term2.contains("]")) {
				
				String term2_trimmed = StringUtils.replacePattern(term2, ".*\\]", "").trim();
				correctSpan = new Span(correctSpan.getStart() + StringUtils.indexOf(term2, term2_trimmed),
						correctSpan.getStart() + StringUtils.lastIndexOf(term2, term2_trimmed));
				term2 = term2_trimmed;
			}
		}
		
		if(term2.contains("(") || term2.contains("}")) {
			if((term2.contains("(") && term2.contains(")")) &&
					(term2.indexOf("(") < term2.indexOf(")")) 
					) {
				/*String term2_trimmed = StringUtils.replacePattern(term2, "\\[.*\\]", "").trim();
				correctSpan = new Span(correctSpan.getStart() + StringUtils.indexOf(term2, term2_trimmed),
						correctSpan.getStart() + StringUtils.lastIndexOf(term2, term2_trimmed));*/
			} else {
				//return;
			}
		}

		String prunedTerm = ClinicalTokenizer.getPrunedMedicalTerm(term2);
		correctSpan = ClinicalTokenizer.getPrunedSpan (correctSpan, term2, prunedTerm);
		term2 = prunedTerm;
		
		if(!ClinicalTokenizer.isGarbageTerm(term2))
			mergedspanToTextMap.put(correctSpan, term2);
		
	}

	/*private Span getCorrectSpanForPOS(Span span, String term1, String docText) throws Exception{
	
		if (span.getStart()!=0 && docText.substring(span.getStart()-1, span.getEnd()-1).equals(term1)){
			span = new Span(span.getStart()-1, span.getEnd()-1);
		}
		
		return span;
	}*/

	/**
	 * Checks which of the both would suits better with google api
	 * @param span1
	 * @param span1Value
	 * @param span2
	 * @param span2Value
	 * @return
	 */
	private int checkUsingGoogleApi(Span span1, String span1Value, Span span2,
			String span2Value) {
		int i = 1;
		/*PythonInterpreter interpreter = new PythonInterpreter();
		interpreter.exec("import sys\nsys.path.append('pathToModiles if they're not there by default')\nimport yourModule");
		// execute a function that takes a string and returns a string
		PyObject someFunc = interpreter.get("funcName");
		PyObject result = someFunc.__call__(new PyString("Test!"));
		String realResult = (String) result.__tojava__(String.class);*/
		return i;
	}

	/**
	 * Creates uniq and overlapped spans from the set of 2 spans list.
	 * @param spanToTextMap1
	 * @param spanToTextMap2
	 * @param spanToTextMap1_uniq
	 * @param spanToTextMap2_uniq
	 * @param overlappedSpansMap
	 */
	private void createUniqAndOverLappedSpans(
			TreeMap<Span, String> spanToTextMap1,
			TreeMap<Span, String> spanToTextMap2,
			TreeMap<Span, String> spanToTextMap1_uniq,
			TreeMap<Span, String> spanToTextMap2_uniq,
			TreeMap<Span, String> equalSpansMap,
			TreeMap<Span, Span> overlappedSpansMap) {

		Set<Span> completedCRFSpans = new HashSet();
		for (Entry<Span, String> entry1 : spanToTextMap1.entrySet()) {
			boolean matching = false;
			for (Entry<Span, String> entry2 : spanToTextMap2.entrySet()) {

				//System.out.println("===" + pair1[0] + " \t "+ pair1[1] +"\t"+ pair2[0] +"\t"+pair2[1]);
				//spans not matching
				if ((entry2.getKey().getStart() >= entry1.getKey().getEnd())
						||(entry1.getKey().getStart() >=entry2.getKey().getEnd())) {
					/*spanToTextMap1_uniq.put(entry1.getKey(), entry1.getValue());
					break;*/
				} // spans equal 
				else if ((entry1.getKey().getStart() == entry2.getKey().getStart()) 
						&&(entry1.getKey().getEnd() == entry2.getKey().getEnd())){
					equalSpansMap.put(entry1.getKey(), entry1.getValue());
					matching = true;
					break;
				} // spans overlapping 
				else  {
					matching = true;
					if (entry1.getKey().contains(entry2.getKey())) {
						spanToTextMap1_uniq.put(entry1.getKey(), entry1.getValue());
						completedCRFSpans.add(entry2.getKey());
						System.out.println(entry1.getKey()+","+entry2.getKey());
						break;
						//
					} else if (entry2.getKey().contains(entry1.getKey())) {
						//spanToTextMap2_uniq.put(entry2.getKey(), entry2.getValue());
					} else {
						
						overlappedSpansMap.put(entry1.getKey(), entry2.getKey());
						String line = "pos = " + entry1.getValue() +"\t crf = "+ entry2.getValue();
						try {
							FileUtils.write(new File("overlappedTerms_pos_crf"), line +"\n", true);
						} catch (IOException e) {
							e.printStackTrace();
						}
						break;
					}
				}
			}
			if(!matching) {
				spanToTextMap1_uniq.put(entry1.getKey(), entry1.getValue());
			}
		}

		for (Entry<Span, String> entry2 : spanToTextMap2.entrySet()) {
			if(!spanToTextMap1_uniq.containsKey(entry2.getKey()) &&
					!spanToTextMap2_uniq.containsKey(entry2.getKey())
					&& !equalSpansMap.containsKey(entry2.getKey()) 
					&& !overlappedSpansMap.containsKey(entry2.getKey())
					&& !overlappedSpansMap.containsValue(entry2.getKey())
					&& !completedCRFSpans.contains(entry2.getKey())
					&& noIntersetionSpans(spanToTextMap1, spanToTextMap2, equalSpansMap, overlappedSpansMap, completedCRFSpans, entry2.getKey())
					) {
				spanToTextMap2_uniq.put(entry2.getKey(), entry2.getValue());
			} 
		}

	}

	private boolean noIntersetionSpans(TreeMap<Span, String> spanToTextMap1,
			TreeMap<Span, String> spanToTextMap2,
			TreeMap<Span, String> equalSpansMap,
			TreeMap<Span, Span> overlappedSpansMap, Set<Span> completedCRFSpans, Span span) {
		
		for (Span span1 : spanToTextMap1.keySet()) {
			if (span1.intersects(span))
				return false;
		}
		
		for (Span span1 : spanToTextMap2.keySet()) {
			if (span1.intersects(span))
				return false;
		}
		
		for (Span span1 : equalSpansMap.keySet()) {
			if (span1.intersects(span))
				return false;
		}
		
		for (Span span1 : overlappedSpansMap.keySet()) {
			if (span1.intersects(span))
				return false;
		}
		
		for (Span span1 : completedCRFSpans) {
			if (span1.intersects(span))
				return false;
		}
		
		return true;
	}

	private TreeMap<Span, String> readSpanAndTextFromHitachiFile(File posFile, int increment) {
		List<String> pos_Lines = null;
		TreeMap<Span, String> spanToTextMap = new TreeMap<>();

		try {
			pos_Lines = FileUtils.readLines(posFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//Iterate on POS lines and put spans and text into the map
		for (String pos_Line : pos_Lines) {
			/*if(StringUtils.countMatches(predictedLine, "|")!=2)
				continue;*/

			//System.out.println(predictedLine);
			String spanPos = pos_Line.split("\\|")[1];
			String[] pos_pair1Str = spanPos.split("\\-");
			String pos_Term = pos_Line.split("\\|")[2];
			//System.out.println(pair1Str[0]);
			try {
				Span span = new Span(Integer.parseInt(pos_pair1Str[0]),
						Integer.parseInt(StringUtils.substringBefore(pos_pair1Str[1],","))+increment);
				//System.out.println(span.toString());
				spanToTextMap.put(span, pos_Term);
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Error at " + posFile.getName());
			}

		}
		return spanToTextMap;
	}

	/**
	 * Extends the term predicted by the CRF model on either sides, and searches in UMLS 
	 * dictionary to get the correct medical term.
	 * This method is used to improve the identification of the correct medical term. 
	 * If the given medical term is not present in UMLS dictionary, it splits the medical term and gets the medical term
	 * present in UMLS.
	 * @param startSpan
	 * @param lastSpan
	 * @param predictedTerm
	 * @param tokens
	 * @param spanToTokenNumberMap
	 * @return 
	 * @throws IOException
	 */
	List<String> extendMedicalTermBothSides (int startSpan, int lastSpan, String predictedTerm, String[] tokens, TreeMap<Integer, Integer> spanToTokenNumberMap ) throws IOException {


		Map<Integer, Integer> tokenNumberToSpanMap = new HashMap<>();
		for(Entry<Integer, Integer> entry : spanToTokenNumberMap.entrySet()){
			tokenNumberToSpanMap.put(entry.getValue(), entry.getKey());
		}
		//span|term list
		List<String> finalMedicalTerms = new ArrayList<String>();
		int spanStartFirstWordPredictedTerm  = startSpan;
		int spanStartLastWordPredictedTerm = spanStartFirstWordPredictedTerm;
		//if there is space in predicted term(bi/trigram).. get last token start index 
		if (!StringUtils.substringAfterLast(predictedTerm, " ").isEmpty()){
			spanStartLastWordPredictedTerm = lastSpan-StringUtils.substringAfterLast(predictedTerm, " ").length()+1;
		}

		//System.out.println("predicted Line = " + predictedLine);
		System.out.println("spanstart firstword pred =" + spanStartFirstWordPredictedTerm);
		//get the token number of the first word predicted line
		int firsttokenNumber =0 ;
		int lasttokenNumber =0 ; 

		if(spanToTokenNumberMap.containsKey(spanStartFirstWordPredictedTerm)) {
			firsttokenNumber = spanToTokenNumberMap.get(spanStartFirstWordPredictedTerm);
			System.out.println("spanstart lastword pred =" + spanStartLastWordPredictedTerm);
			if(spanToTokenNumberMap.containsKey(spanStartLastWordPredictedTerm)) {
				lasttokenNumber = spanToTokenNumberMap.get(spanStartLastWordPredictedTerm);
			}
		}

		String nearByWords = getNearByTokens(tokens, firsttokenNumber+1, 2);
		System.out.println("near by = " + nearByWords);

		/*FileUtils.write(new File(relaxedWithDefs), "Gold = "+  goldTerm +" \t predicted = "
				+ predictedTerm  + "\tNear by words = " + nearByWords + "\n", true );*/

		boolean flagGranted = false;
		String leftWord = getPreviousToken(tokens, firsttokenNumber, 1);
		String rightWord = getLaterToken(tokens, lasttokenNumber, 1);

		String extendedPredictedTerm = predictedTerm;
		//four grams
		if (StringUtils.countMatches(predictedTerm, " ") >= 1) {
			//If X0 X X1 is the predicted term
			if(!leftWord.isEmpty() && !rightWord.isEmpty()) {
				extendedPredictedTerm = leftWord + " " + predictedTerm + " " + rightWord ;
				extendedPredictedTerm = extendedPredictedTerm.trim();
				flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm);
				if(flagGranted) {
					finalMedicalTerms.add(tokenNumberToSpanMap.get(firsttokenNumber-1)+"-"
							+(tokenNumberToSpanMap.get(lasttokenNumber+1)+tokens[lasttokenNumber+1].length()-1)
							+"|"+extendedPredictedTerm
							);
					return finalMedicalTerms;
				}
			}
			//check X0 X
			if(!leftWord.isEmpty() && !flagGranted) {
				extendedPredictedTerm = leftWord + " " + predictedTerm;
				flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm);
				if(flagGranted) {
					finalMedicalTerms.add(tokenNumberToSpanMap.get(firsttokenNumber-1)+"-"
							+(tokenNumberToSpanMap.get(lasttokenNumber)+tokens[lasttokenNumber].length()-1)
							+"|"+extendedPredictedTerm
							);
					return finalMedicalTerms;
				}
			}
			//check X X1
			if(!flagGranted && !rightWord.isEmpty()) {
				extendedPredictedTerm = predictedTerm + " " + rightWord ;
				flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm);
				if(flagGranted) {
					finalMedicalTerms.add(tokenNumberToSpanMap.get(firsttokenNumber)+"-"
							+(tokenNumberToSpanMap.get(lasttokenNumber+1)+tokens[lasttokenNumber+1].length()-1)
							+"|"+extendedPredictedTerm
							);
					return finalMedicalTerms;
				}
			}

			//check X
			if(leftWord.isEmpty() && rightWord.isEmpty()) {/*
				extendedPredictedTerm = predictedTerm;
				extendedPredictedTerm = extendedPredictedTerm.trim();

				String[] split = extendedPredictedTerm.split(".");

				flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm);
				if(flagGranted) {
					finalMedicalTerms.add(tokenNumberToSpanMap.get(firsttokenNumber-1)+"-"
						+(tokenNumberToSpanMap.get(lasttokenNumber+1)+tokens[lasttokenNumber+1].length()-1)
						+"|"+extendedPredictedTerm
						);

				}
				if(flagGranted)
					return finalMedicalTerms;
			 */}

			int firsttokenNumberForExtended = firsttokenNumber;
			int lasttokenNumberForExtended = lasttokenNumber;
			if(!leftWord.isEmpty()) {
				firsttokenNumberForExtended = firsttokenNumber - 1;
			}
			if(!rightWord.isEmpty()) {
				lasttokenNumberForExtended = lasttokenNumber + 1;
			}

			finalMedicalTerms = checkAllNGramsForMedicalTerms(extendedPredictedTerm, firsttokenNumberForExtended,
					lasttokenNumberForExtended, tokens, tokenNumberToSpanMap, 3);
			if(finalMedicalTerms.size() > 0)
				return finalMedicalTerms;
			finalMedicalTerms = checkAllNGramsForMedicalTerms(extendedPredictedTerm, firsttokenNumberForExtended,
					lasttokenNumberForExtended, tokens, tokenNumberToSpanMap, 2);
			if(finalMedicalTerms.size() > 0)
				return finalMedicalTerms;
			finalMedicalTerms = getAllUniMedicalTerms(predictedTerm, firsttokenNumber, lasttokenNumber, tokens, tokenNumberToSpanMap, 1);
			if(finalMedicalTerms.size() > 0)
				return finalMedicalTerms;
		}

		//unigrams
		else if (StringUtils.countMatches(predictedTerm, " ") == 0) {
			leftWord = getPreviousToken(tokens, firsttokenNumber, 2);
			rightWord = getLaterToken(tokens, lasttokenNumber, 2);

			//If X0 X X1 is the predicted term
			if(!leftWord.isEmpty() && !rightWord.isEmpty()) {
				extendedPredictedTerm = leftWord + " " + predictedTerm + " " + rightWord ;
				extendedPredictedTerm = extendedPredictedTerm.trim();
				flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm);
				if(flagGranted) {
					finalMedicalTerms.add(tokenNumberToSpanMap.get(firsttokenNumber-2)+"-"
							+(tokenNumberToSpanMap.get(lasttokenNumber+2)+tokens[lasttokenNumber+2].length()-1)
							+"|"+extendedPredictedTerm
							);
					return finalMedicalTerms;
				}
			}
			//check X0 X
			if(!leftWord.isEmpty() && !flagGranted) {
				extendedPredictedTerm = leftWord + " " + predictedTerm;
				flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm);
				if(flagGranted) {
					finalMedicalTerms.add(tokenNumberToSpanMap.get(firsttokenNumber-2)+"-"
							+(tokenNumberToSpanMap.get(lasttokenNumber)+tokens[lasttokenNumber].length()-1)
							+"|"+extendedPredictedTerm
							);
					return finalMedicalTerms;
				}
			}
			//check X X1
			if(!flagGranted && !rightWord.isEmpty()) {
				extendedPredictedTerm = predictedTerm + " " + rightWord ;
				flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm);
				if(flagGranted) {
					finalMedicalTerms.add(tokenNumberToSpanMap.get(firsttokenNumber)+"-"
							+(tokenNumberToSpanMap.get(lasttokenNumber+2)+tokens[lasttokenNumber+2].length()-1)
							+"|"+extendedPredictedTerm
							);
					return finalMedicalTerms;
				}
			}


			leftWord = getPreviousToken(tokens, firsttokenNumber, 1);
			rightWord = getLaterToken(tokens, lasttokenNumber, 1);

			//If X0 X X1 is the predicted term
			if(!leftWord.isEmpty() && !rightWord.isEmpty()) {
				extendedPredictedTerm = leftWord + " " + predictedTerm + " " + rightWord ;
				extendedPredictedTerm = extendedPredictedTerm.trim();
				flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm);
				if(flagGranted) {
					finalMedicalTerms.add(tokenNumberToSpanMap.get(firsttokenNumber-1)+"-"
							+(tokenNumberToSpanMap.get(lasttokenNumber+1)+tokens[lasttokenNumber+1].length()-1)
							+"|"+extendedPredictedTerm
							);
					return finalMedicalTerms;
				}
			}
			//check X0 X
			if(!leftWord.isEmpty() && !flagGranted) {
				extendedPredictedTerm = leftWord + " " + predictedTerm;
				flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm);
				if(flagGranted) {
					finalMedicalTerms.add(tokenNumberToSpanMap.get(firsttokenNumber-1)+"-"
							+(tokenNumberToSpanMap.get(lasttokenNumber)+tokens[lasttokenNumber].length()-1)
							+"|"+extendedPredictedTerm
							);
					return finalMedicalTerms;
				}
			}
			//check X X1
			if(!flagGranted && !rightWord.isEmpty()) {
				extendedPredictedTerm = predictedTerm + " " + rightWord ;
				flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm);
				if(flagGranted) {
					finalMedicalTerms.add(tokenNumberToSpanMap.get(firsttokenNumber)+"-"
							+(tokenNumberToSpanMap.get(lasttokenNumber+1)+tokens[lasttokenNumber+1].length()-1)
							+"|"+extendedPredictedTerm
							);
					return finalMedicalTerms;
				}
			}

			//check X
			if(!flagGranted ) {
				extendedPredictedTerm = predictedTerm ;
				flagGranted = checkForExtendedWordInUMLS(extendedPredictedTerm);
				if(flagGranted) {
					finalMedicalTerms.add(tokenNumberToSpanMap.get(firsttokenNumber)+"-"
							+(tokenNumberToSpanMap.get(lasttokenNumber)+tokens[lasttokenNumber].length()-1)
							+"|"+extendedPredictedTerm
							);
					return finalMedicalTerms;
				}
			}

		}

		return finalMedicalTerms;
	}


	/**
	 * Checks whether the token is in UMLS dictionary.
	 * Returns true if the token is in UMLS, else false.
	 * @param token
	 * @return true/false
	 */
	public boolean isInUMLS(String token, String[] tokens, int k) {
		if(token.length()<3)
			return false;

		String cui = java2MySql.getCUIOfMedicalTerm(token);
		//System.out.println("1term and cui  =" + token + " \t "+ cui);
		if(cui !=null) {
			if(!cui.isEmpty()) {
				return true;
			}
		}

		String temp = "";
		temp = getPreviousToken(tokens, k, 1) + " " + token;
		cui = java2MySql.getCUIOfMedicalTerm(temp);
		//System.out.println("1term and cui  =" + token + " \t "+ cui);
		if(cui !=null) {
			if(!cui.isEmpty()) {
				return true;
			}
		}

		temp = getPreviousToken(tokens, k, 1) + " " + token + " " + getLaterToken(tokens, k, 1);
		cui = java2MySql.getCUIOfMedicalTerm(temp);
		//System.out.println("1term and cui  =" + token + " \t "+ cui);
		if(cui !=null) {
			if(!cui.isEmpty()) {
				return true;
			}
		}

		temp = token + " " + getLaterToken(tokens, k, 1);
		cui = java2MySql.getCUIOfMedicalTerm(temp);
		//System.out.println("1term and cui  =" + token + " \t "+ cui);
		if(cui !=null) {
			if(!cui.isEmpty()) {
				return true;
			}
		}

		temp = getPreviousToken(tokens, k, 2) + " " + token;
		cui = java2MySql.getCUIOfMedicalTerm(temp);
		//System.out.println("1term and cui  =" + token + " \t "+ cui);
		if(cui !=null) {
			if(!cui.isEmpty()) {
				return true;
			}
		}

		temp = getPreviousToken(tokens, k, 2) + " " + token + " " + getLaterToken(tokens, k, 2);
		cui = java2MySql.getCUIOfMedicalTerm(temp);
		//System.out.println("1term and cui  =" + token + " \t "+ cui);
		if(cui !=null) {
			if(!cui.isEmpty()) {
				return true;
			}
		}

		temp = token + " " + getLaterToken(tokens, k, 2);
		cui = java2MySql.getCUIOfMedicalTerm(temp);
		//System.out.println("1term and cui  =" + token + " \t "+ cui);
		if(cui !=null) {
			if(!cui.isEmpty()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Searches all ngrams in UMLS dictionary and returns the ngrams that are present in UMLS.
	 * @param predictedTerm
	 * @param firsttokenNumber
	 * @param lasttokenNumber
	 * @param tokens
	 * @param tokenNumberToSpanMap
	 * @param ngramNumber
	 * @return correct medical terms 
	 * @throws IOException
	 */
	List<String> checkAllNGramsForMedicalTerms (String predictedTerm, int firsttokenNumber, int lasttokenNumber, String[] tokens, Map<Integer, Integer> tokenNumberToSpanMap, int ngramNumber) throws IOException {
		//as tokens[] contains . as separate token
		predictedTerm = predictedTerm.replace(".", " .");
		boolean flagGranted = false;
		List<String> ngrams = ClinicalTokenizer.getNGrams(predictedTerm, ngramNumber);
		List<String> UMLSGrantedUnigrams = new ArrayList<String>();

		for (int i =0; i < ngrams.size(); i++) {
			String ngram = ngrams.get(i).replace("_", " ");
			flagGranted = checkForExtendedWordInUMLS(ngram);
			if(flagGranted) {
				//firstTokenNumber - lastTokenNumberofNgram+lastTokenLength
				UMLSGrantedUnigrams.add(tokenNumberToSpanMap.get(firsttokenNumber+i)+"-"+
						(tokenNumberToSpanMap.get(firsttokenNumber+i+ngramNumber-1)+tokens[firsttokenNumber+i+ngramNumber-1].length()-1)
						+"|"+ngram);
				//return UMLSGrantedUnigrams;
				flagGranted = false;
				i = i+ngramNumber;
			}
		}
		return UMLSGrantedUnigrams;
	}

	/**
	 * Returns unigrams of predicted term that are present in UMLS.
	 * @param predictedTerm
	 * @return
	 * @throws IOException
	 */
	List<String> getAllUniMedicalTerms (String predictedTerm, int firsttokenNumber, int lasttokenNumber, String[] tokens, Map<Integer, Integer> tokenNumberToSpanMap, int ngramNumber) throws IOException {
		boolean flagGranted = false;
		List<String> UMLSGrantedUnigrams = new ArrayList<String>();
		List<String> unigrams = ClinicalTokenizer.getNGrams(predictedTerm, 1);
		for (int i =0; i < unigrams.size(); i++) {
			flagGranted = checkForExtendedWordInUMLS(unigrams.get(i));
			if(flagGranted) {
				////firstTokenNumber - firstTokenNumberofNgram+lastTokenLength
				UMLSGrantedUnigrams.add(tokenNumberToSpanMap.get(firsttokenNumber+i)+"-"+
						(tokenNumberToSpanMap.get(firsttokenNumber+i)+unigrams.get(i).length()-1)+"|"+unigrams.get(i));
			}
		}
		return UMLSGrantedUnigrams;
	}

	/**
	 * 
	 * @param predictedTerm
	 * @param relaxedWithDefs
	 * @param ngram
	 * @return
	 * @throws IOException
	 */
	boolean checkAllNGramsForMedicalTerms (String predictedTerm, String relaxedWithDefs, int ngram) throws IOException {
		boolean flagGranted = false;
		List<String> unigrams = ClinicalTokenizer.getNGrams(predictedTerm, ngram);
		for (String unigram : unigrams) {
			unigram = unigram.replace("_", " ");
			FileUtils.write(new File(relaxedWithDefs), "Checking ngram = " + unigram + "\t\t", true );
			flagGranted = checkForExtendedWordInUMLS(unigram, relaxedWithDefs);
			if(flagGranted)
				break;
		}
		return flagGranted;
	}

	boolean checkForExtendedWordInUMLS (String extendedWord) throws IOException {
		extendedWord = extendedWord.replace("_", " ");
		System.out.println("Checking = " + extendedWord);
		String cui = java2MySql.getCUIOfMedicalTerm(extendedWord.trim());
		if(!StringUtils.isEmpty(cui)) {
			return true;
		}
		return false;
	}

	boolean checkForExtendedWordInUMLS (String extendedWord, String relaxedWithDefs) throws IOException {
		extendedWord = extendedWord.replace("_", " ");
		System.out.println("bigram = " + extendedWord);
		String cui = java2MySql.getCUIOfMedicalTerm(extendedWord.trim());

		if(!StringUtils.isEmpty(cui)) {
			//List<String> defs = java2MySql.getUMLSDefinitionsOfCUI(cui);

			FileUtils.write(new File(relaxedWithDefs), "final = " + extendedWord +" _ "+ cui , true );
			/*for (int i = 0 ; i < defs.size(); i++) {
				if(i!= 0)
					FileUtils.write(new File(relaxedWithDefs), "\t\t,\t\t", true);
				FileUtils.write(new File(relaxedWithDefs), "\t"+ defs.get(i), true );
			}*/
			FileUtils.write(new File(relaxedWithDefs), "\n" , true );
			return true;
		}
		FileUtils.write(new File(relaxedWithDefs), "\n" , true );
		return false;
	}


	private String getNearByTokens(String[] tokens, int tokenNumber, int i) {

		System.out.println( tokens[tokenNumber] + "-- "+ tokenNumber + "-- "+ i);
		String term = "";
		for (int j = 1; j<=i; j++) {
			if (tokenNumber+j < tokens.length) {
				term = term + " " + tokens[tokenNumber+j];
			}
		}
		term = tokens[tokenNumber]+" "+term.trim();

		for (int j = 1; j<=i; j++) {
			if (tokenNumber-j >= 0) {
				term = tokens[tokenNumber-j] + " " + term ;
			}
		}

		return term.trim();
	}

	private String getLaterToken(String[] tokens, int tokenNumber, int i) {


		String term = "";
		for (int j = 1; j<=i; j++) {
			if (tokenNumber+j < tokens.length) {
				String currentToken = tokens[tokenNumber+j];
				if(StringUtils.isAlpha(currentToken) && 
						(currentToken.length() > 2 || StringUtils.isAllUpperCase(currentToken))) {
					term =  term + " " +  currentToken;
				}else {
					return "";
				}
			}
		}

		return term.trim();

	}

	/**
	 * Returns the number of tokens to the left of the term(token number)
	 * @param tokens
	 * @param tokenNumber
	 * @param i
	 * @return
	 */
	private String getPreviousToken(String[] tokens, int tokenNumber, int i) {
		String term = "";
		for (int j = 1; j<=i; j++) {
			if (tokenNumber-j >= 0) {
				String currentToken  = tokens[tokenNumber-j];
				//checks for numeric, alpha numeric, puntuation, word less than 3 letters
				// if only alpha and gt 2
				if(StringUtils.isAlpha(currentToken) && 
						(currentToken.length() > 2 || StringUtils.isAllUpperCase(currentToken))) {
					term = currentToken + " " + term ;
				} else {
					return "";
				}
			}
		}

		return term.trim();
	}

	/**
	 * Returns span start of token and token position (number of the token)
	 * @param tokens
	 * @return
	 */
	TreeMap<Integer, Integer> getStartSpansForTokens (File corpusFile, String[] tokens) {
		String fileText="";
		try {
			fileText = FileUtils.readFileToString(corpusFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		TreeMap<Integer, Integer> tm = new TreeMap<Integer, Integer>();
		int span = 0;
		for (int i = 0 ;i < tokens.length ; i++) {

			int noOfSpaces = StringUtils.indexOf(fileText, tokens[i]);
			//space + length of string
			//at medical term start
			span += noOfSpaces;
			System.out.println(span  + tokens[i]);
			tm.put(span, i);
			span += tokens[i].length();
			fileText = StringUtils.substring(fileText, (tokens[i].length()+ noOfSpaces));			
		}
		return tm;
	}

	void removeLinesLT3(String DSPIPED, String DSPIPED_OP) throws IOException {
		File DSPIPED_DIR = new File(DSPIPED);

		File[] files = DSPIPED_DIR.listFiles();
		for(File file : files) {
			File newFile = new File(DSPIPED_OP+"/"+file.getName());
			FileUtils.deleteQuietly(newFile);
			List<String> lines = FileUtils.readLines(file);
			for (String line  : lines) {
				String[] split = line.split("\\|");
				if(split.length == 3) {
					if(split[2].length()>3 ) {
						FileUtils.write(newFile, line + "\n", true);
					} else if(split[2].length()==3 && StringUtils.isAllUpperCase(split[2]) ) {
						System.out.println(split[2]);
						FileUtils.write(newFile, line + "\n", true);
					} 
				} else {
					FileUtils.write(newFile, line + "\n", true);
				}
			}
		}

	}

	/**
	 * Extract medical terms from the spans.
	 * Checks the count of the templates that a medical term occurs.
	 * @param corpusDir
	 * @param pipeDir
	 * @throws IOException
	 */
	void extractTermsOfSpan (String corpusDir, String pipeDir) throws IOException {

		File[] corpusFiles = new File(corpusDir).listFiles();
		for(File corpusFile : corpusFiles) {
			List<String> pipeDelimitedLines = null;
			String corpuslines = "";
			try {
				//read the piped files of the discharge summary files
				corpuslines  = FileUtils.readFileToString(corpusFile);
				File pipeFile = new File(pipeDir + "/" + corpusFile.getName()); 
				//+ StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));

				if (corpusFile.getName().contains(".txt"))
					pipeFile = new File(pipeDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));

				if(!pipeFile.exists()) {
					System.out.println("No pipe file found = " + pipeFile);
					continue;
				}

				pipeDelimitedLines = FileUtils.readLines(pipeFile);
			} catch (IOException e) {
				e.printStackTrace();
			}

			//For each line of the piped file. Each line has a medical term with spans.
			for (String pipeDelimitedLine : pipeDelimitedLines) {
				//CLEF2014
				String medicalterm = "";

				String[] slots = pipeDelimitedLine.split("\\|");

				//comment below to get all medical terms.
				if(slots[slots.length-1].equals("0"))
					continue;

				String[] spanString = slots[1].split(",");

				int[] spanCUI = new int[spanString.length * 2];
				int diseaseNo = Integer.parseInt(slots[slots.length-1]);


				//Iterate on each span of each line.
				for (int i = 0; i < spanString.length; i++) {

					spanCUI[i * 2] = Integer.parseInt(spanString[i].split("-")[0]);
					spanCUI[i * 2 + 1] = Integer.parseInt(spanString[i].split("-")[1]);
					//System.out.println(spanCUI[i * 2] +"-"+ spanCUI[i * 2 + 1]);
					medicalterm = medicalterm +" "+ StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]+1);

					//uncomment for only disease tokens 
					if (diseaseNo!=0)
						//for hitachi train medical tokens
						System.out.println(StringUtils.substring(corpuslines, spanCUI[i * 2], (spanCUI[i * 2 + 1]+1)).trim().replace(" ", "\n"));
				}
				//for train dict terms
				//				System.out.println(medicalterm.trim());

				//CLEF2013
				/*int[] spanCUI = clefPipedLineParser.parseCLEF2013PipedFile(pipeDelimitedLine);
				String medicalterm = "";
				for (int i = 0; i < spanCUI.length/2; i++) {
//					medicalterm = medicalterm + " " + StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]);
					System.out.println(StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]).trim());
				}*/

			}
		}
	}

	public void getStatOfDataset(String corpusDir, String goldDir) throws IOException {

		int disease1 = 0;
		int disease2 = 0;
		int disease3 = 0;
		int disease4 = 0;
		int disease5 = 0;

		int medicalNo = 0;
		int diseaseNo = 0;
		int noOfDocs = 0;
		File[] corpusFiles = new File(corpusDir).listFiles();
		for(File corpusFile : corpusFiles) {
			//			if(corpusFile.getName().endsWith("txt")) {
			List<String> goldpipeDelimitedLines = null;
			try {
				//read the piped files of the discharge summary files
				File goldpipeFile = new File(goldDir + "/" + corpusFile.getName());

				if (corpusFile.getName().contains(".txt")) {
					goldpipeFile = new File(goldDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));
				}

				goldpipeDelimitedLines = FileUtils.readLines(goldpipeFile);
				noOfDocs++;
				for (String goldpipeDelimitedLine : goldpipeDelimitedLines) {
					if(!goldpipeDelimitedLine.endsWith("0")) {
						diseaseNo++;
					}
					if(goldpipeDelimitedLine.endsWith("1")) {
						disease1++;
					}
					if(goldpipeDelimitedLine.endsWith("2")) {
						disease2++;
					}
					if(goldpipeDelimitedLine.endsWith("3")) {
						disease3++;
					}
					if(goldpipeDelimitedLine.endsWith("4")) {
						disease4++;
					}
					if(goldpipeDelimitedLine.endsWith("5")) {
						disease5++;
					}
					medicalNo++;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			//			}
		}
		System.out.println("disease1 = "+disease1);
		System.out.println("disease2 = "+disease2);
		System.out.println("disease3 = "+disease3);
		System.out.println("disease4 = "+disease4);
		System.out.println("disease5 = "+disease5);

		System.out.println("medicalNo = "+medicalNo);
		System.out.println("diseaseNo = "+diseaseNo);
		System.out.println("noOfDocs = "+noOfDocs);
	}

	/**
	 * Gold medical terms that are not predicted by out model.
	 * @param corpusDir
	 * @param goldDir
	 * @param crfopDir
	 * @throws IOException
	 */
	public void getWordsNotCaught(String corpusDir, String goldDir,
			String crfopDir, String outputFile) throws IOException {

		int correct = 0;
		int wrong = 0;
		int countGold = 0;
		int countpredicted = 0;

		File[] corpusFiles = new File(corpusDir).listFiles();
		FileUtils.deleteQuietly(new File(outputFile));
		for(File corpusFile : corpusFiles) {
			//			if(corpusFile.getName().endsWith("txt")) {
			List<String> goldpipeDelimitedLines = null;
			List<String> predictedpipeDelimitedLines = null;
			String corpuslines = "";
			List<Span> predictedSpanList = new ArrayList<Span>();
			List<Span> goldSpanList = new ArrayList<Span>();
			try {
				//read the piped files of the discharge summary files
				corpuslines  = FileUtils.readFileToString(corpusFile);
				File goldpipeFile = new File(goldDir + "/" + corpusFile.getName());
				File predictedpipeFile = new File(crfopDir + "/" + corpusFile.getName());
				//+ StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));

				if (corpusFile.getName().contains(".txt")) {
					goldpipeFile = new File(goldDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));
					predictedpipeFile = new File(crfopDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));
				}

				if(!goldpipeFile.exists()) {
					System.out.println("No pipe file found = " + goldpipeFile);
					continue;
				}

				goldpipeDelimitedLines = FileUtils.readLines(goldpipeFile);
				predictedpipeDelimitedLines = FileUtils.readLines(predictedpipeFile);


				List<String> predictedSpans = new ArrayList<String>();

				for (String predictedpipeDelimitedLine : predictedpipeDelimitedLines) {
					String medicalterm = "";

					String[] slots = predictedpipeDelimitedLine.split("\\|");

					String[] spanString = slots[1].split(",");

					int[] spanCUI = new int[spanString.length * 2];
					for (int i = 0; i < spanString.length; i++) {
						predictedSpans.add(spanString[i]);
						String[] spanSplit1 = spanString[i].split("-");
						try {
							Span span = new Span(Integer.parseInt(spanSplit1[0]), Integer.parseInt(spanSplit1[1]));
							predictedSpanList.add(span);
						} catch (Exception e) {
							System.err.println(corpusFile.getName() +" \t" + spanString[i]);
							e.printStackTrace();
						}
					}
					countpredicted++;
				}


				//For each line of the piped file. Each line has a medical term with spans.
				for (String goldpipeDelimitedLine : goldpipeDelimitedLines) {

					String medicalterm = "";

					String[] slots = goldpipeDelimitedLine.split("\\|");

					String[] spanString = slots[1].split(",");

					int[] spanCUI = new int[spanString.length * 2];

					//Iterate on each span of each line.
					for (int i = 0; i < spanString.length; i++) {

						spanCUI[i * 2] = Integer.parseInt(spanString[i].split("-")[0]);
						spanCUI[i * 2 + 1] = Integer.parseInt(spanString[i].split("-")[1]);
						//System.out.println(spanCUI[i * 2] +"-"+ spanCUI[i * 2 + 1]);
						medicalterm = medicalterm + " " + StringUtils.substring(corpuslines, spanCUI[i * 2], (spanCUI[i * 2 + 1]+1));

						//Span span = new Span(spanCUI[i * 2], spanCUI[i * 2 + 1]);
						try {
							Span span = new Span(spanCUI[i * 2], spanCUI[i * 2 + 1] );
							goldSpanList.add(span);
						} catch (Exception e) {
							System.err.println(corpusFile.getName() +" \t" + spanString[i]);
							e.printStackTrace();
						}
					}

					medicalterm = medicalterm.trim();
					//					if(goldpipeDelimitedLine.endsWith("0")) {
					if(!goldpipeDelimitedLine.endsWith("0")) {
						//						System.out.println(goldpipeDelimitedLine);
						countGold++;
						Span span = new Span(spanCUI[0],spanCUI[1]);
						//						if(predictedSpanList.contains(span)) {
						if(isPresentRelaxed(predictedSpanList,span)!= null) {
							correct++;
							//							FileUtils.write(new File("predictedDiseaseTermsrelaxed_PPU"), medicalterm.trim()+"\n", true);
							//							System.out.println(medicalterm.trim());
							/*FileUtils.write(new File("predictedDiseaseTermsrelaxed_PPU_GP1"), corpusFile.getName()
									+"\tInPredictedNotInGold: \t"+medicalterm.trim()+"\t"+
									spanString[0]+"\n", true);*/
						}// print relaxed gold and predicted
						/*else if(!isPresentRelaxedText(corpuslines,predictedSpanList,span).isEmpty()) {
//							Span s = isPresentRelaxedpredictedSpanList,span);
//							String predictedRelax = StringUtils.substring(corpuslines,s.getStart(),s.getEnd());

							String predictedRelax = isPresentRelaxedText(corpuslines,predictedSpanList,span);
							countPredictedCorrect++;
							FileUtils.write(new File(outputFile), 
									"Gold:\t" +corpusFile.getName() +"|"+spanString[0]+"|"+medicalterm.replace("\n", " ").trim()
									+"\t"+
									"\t&3456#\tPredicted: \t"+ predictedRelax + "\n", true);
//							System.out.println(medicalterm.trim());
						}*/ else {
							//wrong++;
							/*FileUtils.write(new File(outputFile), 
									"InGoldNotPredicted: \t"+corpusFile.getName()+"|"+spanString[0]+"|"+
											medicalterm.trim()+"\n", true);*/
						}
					}

					/*if(!predictedSpans.contains(spanString[0])) {
						System.out.println(medicalterm.trim());
					}*/
				}

				//For each line of the piped file. Each line has a medical term with spans.
				/*for (String predictedpipeDelimitedLine : predictedpipeDelimitedLines) {

					String medicalterm = "";
//					System.out.println(pipeDelimitedLine);
					String[] slots = predictedpipeDelimitedLine.split("\\|");

					String[] spanString = slots[1].split(",");

					int[] spanCUI = new int[spanString.length * 2];

					//Iterate on each span of each line.
					for (int i = 0; i < spanString.length; i++) {

						spanCUI[i * 2] = Integer.parseInt(spanString[i].split("-")[0]);
						spanCUI[i * 2 + 1] = Integer.parseInt(spanString[i].split("-")[1]);
						//System.out.println(spanCUI[i * 2] +"-"+ spanCUI[i * 2 + 1]);
						medicalterm = medicalterm + " " + StringUtils.substring(corpuslines, spanCUI[i * 2], (spanCUI[i * 2 + 1]+1));

						//Span span = new Span(spanCUI[i * 2], spanCUI[i * 2 + 1]);
					}

					medicalterm = medicalterm.trim();
						countGold++;
						Span span = new Span(spanCUI[0],spanCUI[1]);
						if(goldSpanList.contains(span)) {
//						if(isPresentRelaxed(predictedSpanList,span)!= null) {
//							FileUtils.write(new File("predictedDiseaseTermsrelaxed_PPU"), medicalterm.trim()+"\n", true);
//							System.out.println(medicalterm.trim());
						}// print relaxed gold and predicted
						else if(!isPresentRelaxedText(corpuslines,goldSpanList,span).isEmpty()) {
//							Span s = isPresentRelaxedpredictedSpanList,span);
//							String predictedRelax = StringUtils.substring(corpuslines,s.getStart(),s.getEnd());

							String predictedRelax = isPresentRelaxedText(corpuslines,goldSpanList,span);
							countPredictedCorrect++;
						} else {
							FileUtils.write(new File(outputFile), 
									"InPredictedNotInGold: \t"+corpusFile.getName()+"|"+spanString[0]+"|"+
											medicalterm.trim()+"\n", true);
							FileUtils.write(new File(outputFile), 
									corpusFile.getName()+"|"+spanString[0]+"|"+
											medicalterm.trim()+"\n", true);
						}

					if(!predictedSpans.contains(spanString[0])) {
						System.out.println(medicalterm.trim());
					}
				}*/
			} catch (Exception e) {
				System.err.println(corpusFile.getName());
				e.printStackTrace();
			}

			//			}
		}

		double recall = (correct*1.0/countGold);
		double precision = (correct*1.0/countpredicted);
		double fscore = 2*(precision*recall)/(precision+recall);
		System.err.println("countGold = " + countGold);
		System.err.println("countPredicted = " + countpredicted);
		System.err.println("countPredictedCorrect = " + correct);
		System.err.println("precision = " + precision);
		System.err.println("recall = " + recall );
		System.err.println("f-score = " + fscore);

	}

	Span isPresentRelaxed (List<Span> predictedSpanList, Span goldspan) {
		for (Span predictedspan : predictedSpanList) {
			if(goldspan.intersects(predictedspan))
				return predictedspan;
		}
		return null;
	}


	String isPresentRelaxedText (String corpuslines, List<Span> predictedSpanList, Span goldspan) {
		StringBuffer sb = new StringBuffer();
		for (Span predictedspan : predictedSpanList) {
			if(goldspan.intersects(predictedspan)) {
				sb.append(StringUtils.substring(corpuslines,predictedspan.getStart(),predictedspan.getEnd()+1)+"\t"
						+predictedspan+" , ");
			}
		}
		return sb.toString();
	}

	/**
	 * Gold medical terms that are not predicted by out model.
	 * @param corpusDir
	 * @param goldDir
	 * @param crfopDir
	 * @throws IOException
	 */
	public void getWordsNotCaughtCLEF2013(String corpusDir, String goldDir,
			String crfopDir) throws IOException {

		File[] corpusFiles = new File(corpusDir).listFiles();
		for(File corpusFile : corpusFiles) {

			try {
				//read the piped files of the discharge summary files

				File pipeFile = new File(goldDir + "/" + corpusFile.getName());
				File predictedpipeFile = new File(crfopDir + "/" + corpusFile.getName());
				//+ StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));

				/*if (corpusFile.getName().contains(".txt")) {
					pipeFile = new File(goldDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));
					predictedpipeFile = new File(crfopDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));
				}*/

				if(!pipeFile.exists()) {
					System.out.println("No pipe file found = " + pipeFile);
					continue;
				}
				getWordsNotCaughtCLEF2013(corpusFile, pipeFile, predictedpipeFile);

				System.out.println("\n");
			} catch (IOException e) {
				e.printStackTrace();
			}


		}
	}

	public void getWordsNotCaughtCLEF2013(File corpusFile, File pipeFile,
			File predictedpipeFile) throws IOException {
		List<String> goldpipeDelimitedLines = null;
		List<String> predictedpipeDelimitedLines = null;
		String corpuslines = FileUtils.readFileToString(corpusFile);
		goldpipeDelimitedLines = FileUtils.readLines(pipeFile);
		predictedpipeDelimitedLines = FileUtils.readLines(predictedpipeFile);
		//iterate gold over predicted 
		for (String goldpipeDelimitedLine : goldpipeDelimitedLines) {
			boolean flag = false;
			int[] spanCUI = clefPipedLineParser.parseCLEF2013PipedFile(goldpipeDelimitedLine);
			for (String predictedpipeDelimitedLine : predictedpipeDelimitedLines) {
				int[] spanCUI1 = clefPipedLineParser.parseCLEF2013PipedFile(predictedpipeDelimitedLine);
				if(Arrays.equals(spanCUI, spanCUI1)) {
					flag = true;
					break;
				}
			}
			if(!flag) {
				String medicalterm = "";
				for (int i = 0; i < spanCUI.length/2; i++) {
					medicalterm = medicalterm + " " + StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]);
				}
				System.out.println("## NOTP - "+goldpipeDelimitedLine+"||"+medicalterm.trim());
			}

		}

		//iterate  predicted over gold  
		for (String predictedpipeDelimitedLine : predictedpipeDelimitedLines) {
			boolean flag = false;
			int[] spanCUI = clefPipedLineParser.parseCLEF2013PipedFile(predictedpipeDelimitedLine);
			for (String goldpipeDelimitedLine : goldpipeDelimitedLines) {
				int[] spanCUI1 = clefPipedLineParser.parseCLEF2013PipedFile(goldpipeDelimitedLine);
				if(Arrays.equals(spanCUI, spanCUI1)) {
					flag = true;
					break;
				}
			}
			if(!flag) {
				String medicalterm = "";
				for (int i = 0; i < spanCUI.length/2; i++) {
					medicalterm = medicalterm + " " + StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]);
				}
				System.out.println("WRONGLYP - "+predictedpipeDelimitedLine+"||"+medicalterm.trim());
			}
		}
	}

	/*public int[] parseCLEF2013PipedFile(String pipeDelimitedLine) {
		String[] slots = pipeDelimitedLine.split("\\|\\|");
//		System.out.println(pipeDelimitedLine);
		int[] spanCUI = new int[slots.length-3];
		for (int i=3,j=0 ; i < slots.length; i++,j++) {
			spanCUI[j]=Integer.parseInt(slots[i]);
			//System.out.println(Integer.parseInt(slots[i]));
		}

		return spanCUI;
	}*/

	/**
	 * Extract medical term word templates from the spans.
	 * Checks the count of the templates that a medical term occurs.
	 * @param corpusDir
	 * @param pipeDir
	 * @throws IOException
	 */
	void extractMedicalTermWordTemplatesFromSpans (String corpusDir, String pipeDir) throws IOException {

		File[] corpusFiles = new File(corpusDir).listFiles();
		for(File corpusFile : corpusFiles) {
			System.out.println(corpusFile.getAbsolutePath());
			List<String> pipeDelimitedLines = null;
			String corpuslines = "";
			try {
				//read the piped files of the discharge summary files
				corpuslines  = FileUtils.readFileToString(corpusFile);
				File pipeFile = new File(pipeDir + "/" + corpusFile.getName()); 
				//+ StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));

				if (corpusFile.getName().contains(".txt"))
					pipeFile = new File(pipeDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));

				if(!pipeFile.exists()) {
					System.out.println("No pipe file found = " + pipeFile);
					continue;
				}

				pipeDelimitedLines = FileUtils.readLines(pipeFile);
			} catch (IOException e) {
				e.printStackTrace();
			}

			//For each line of the piped file. Each line has a medical term with spans.
			for (String pipeDelimitedLine : pipeDelimitedLines) {
				String medicalterm = "";

				String[] slots = pipeDelimitedLine.split("\\|");

				String[] spanString = slots[1].split(",");

				int[] spanCUI = new int[spanString.length * 2];

				int start = 0;
				int end  = 0;
				//Iterate on each span of each line.
				for (int i = 0; i < spanString.length; i++) {

					spanCUI[i * 2] = Integer.parseInt(spanString[i].split("-")[0]);
					spanCUI[i * 2 + 1] = Integer.parseInt(spanString[i].split("-")[1]);
					//System.out.println(spanCUI[i * 2] +"-"+ spanCUI[i * 2 + 1]);


					if(i==0) {
						medicalterm = medicalterm + " " + StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]);
					} else {
						int intermediates = StringUtils.countMatches(StringUtils.substring(corpuslines, end, spanCUI[i * 2]), " ");
						//System.out.println(intermediates);
						for (int j=0; j < intermediates; j++)
							medicalterm = medicalterm + " X" ;

						medicalterm = medicalterm + " " + StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]);
					}
					end  = spanCUI[i * 2 + 1];
				}
				System.out.println(medicalterm.trim());
			}
		}
	}

	/**
	 * Extract medical term templates from the spans.
	 * Checks the count of the templates that a medical term occurs.
	 * @param corpusDir
	 * @param pipeDir
	 * @throws IOException
	 */
	void extractMedicalTermTemplatesFromSpans (String corpusDir, String pipeDir) throws IOException {

		File[] corpusFiles = new File(corpusDir).listFiles();
		for(File corpusFile : corpusFiles) {
			List<String> pipeDelimitedLines = null;
			String corpuslines = "";
			try {
				//read the piped files of the discharge summary files
				corpuslines  = FileUtils.readFileToString(corpusFile);
				File pipeFile = new File(pipeDir + "/" + corpusFile.getName()); 
				//+ StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));

				if (corpusFile.getName().contains(".txt"))
					pipeFile = new File(pipeDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));

				if(!pipeFile.exists()) {
					System.out.println("No pipe file found = " + pipeFile);
					continue;
				}

				pipeDelimitedLines = FileUtils.readLines(pipeFile);
			} catch (IOException e) {
				e.printStackTrace();
			}

			//For each line of the piped file. Each line has a medical term with spans.
			for (String pipeDelimitedLine : pipeDelimitedLines) {
				String medicalterm = "";

				String[] slots = pipeDelimitedLine.split("\\|");

				String[] spanString = slots[1].split(",");

				int[] spanCUI = new int[spanString.length * 2];

				int start = 0;
				int end  = 0;
				//Iterate on each span of each line.
				for (int i = 0; i < spanString.length; i++) {

					spanCUI[i * 2] = Integer.parseInt(spanString[i].split("-")[0]);
					spanCUI[i * 2 + 1] = Integer.parseInt(spanString[i].split("-")[1]);
					//System.out.println(spanCUI[i * 2] +"-"+ spanCUI[i * 2 + 1]);


					if(i==0) {
						//medicalterm = medicalterm + " " + StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]);
						int words = StringUtils.countMatches(StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]), " ");
						for(int j=0; j < words+1; j++) {
							if(j==0)
								medicalterm = medicalterm + " B";
							else 
								medicalterm = medicalterm +" I";
						}
					} else {
						int intermediates = StringUtils.countMatches(StringUtils.substring(corpuslines, end, spanCUI[i * 2]), " ");
						//System.out.println(intermediates);
						for (int j=0; j < intermediates; j++)
							medicalterm = medicalterm + " X" ;
						int words = StringUtils.countMatches(StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]), " ");
						for(int j=0; j < words+1; j++) {
							if(j==0)
								medicalterm = medicalterm +" B";
							else 
								medicalterm = medicalterm +" I";
						}
					}
					end  = spanCUI[i * 2 + 1];
				}
				System.out.println(medicalterm.trim());
			}
		}
	}

	/**
	 * Checks how many times a span occurred in a piped file.
	 * This checks the words that belong more than one medical term.
	 * @param corpusDir
	 * @param pipeDir
	 * @throws IOException
	 */
	void getSpansCountForDisjointTerms (String corpusDir, String pipeDir) throws IOException {

		File[] corpusFiles = new File(corpusDir).listFiles();
		for(File corpusFile : corpusFiles) {
			List<String> pipeDelimitedLines = null;
			String corpuslines = "";
			try {
				//read the piped files of the discharge summary files
				corpuslines  = FileUtils.readFileToString(corpusFile);
				File pipeFile = new File(pipeDir + "/" + corpusFile.getName()); 
				//+ StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));

				if (corpusFile.getName().contains(".txt"))
					pipeFile = new File(pipeDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));

				if(!pipeFile.exists()) {
					System.out.println("No pipe file found = " + pipeFile);
					continue;
				}

				pipeDelimitedLines = FileUtils.readLines(pipeFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
			TreeMap<String, Integer> map = new TreeMap<String, Integer>();
			//For each line of the piped file. Each line has a medical term with spans.
			for (String pipeDelimitedLine : pipeDelimitedLines) {
				String medicalterm = "";

				String[] slots = pipeDelimitedLine.split("\\|");

				String[] spanString = slots[1].split(",");

				int[] spanCUI = new int[spanString.length * 2];

				int start = 0;
				int end  = 0;
				//Iterate on each span of each line.
				for (int i = 0; i < spanString.length; i++) {


					HashMapUtilsCustom.incrementKeyFreqCount(map, spanString[i]);
					//					spanCUI[i * 2] = Integer.parseInt(spanString[i].split("-")[0]);
					//					spanCUI[i * 2 + 1] = Integer.parseInt(spanString[i].split("-")[1]);
					//System.out.println(spanCUI[i * 2] +"-"+ spanCUI[i * 2 + 1]);
					//					StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]);
				}
			}

			for(Entry<String, Integer> entry: map.entrySet()) {
				//				if(entry.getValue()>1) {
				System.out.println(entry.getValue());
				//				}
			}

		}
	}


	void mergePipedOutputsForce (String modelPipedDir, String ctakesPipedDir, String mergedOutputDir) {
		File[] modelPipedFiles = new File(modelPipedDir).listFiles();
		for(File modelPipedFile : modelPipedFiles) {

			List<String> modelPipeDelimitedLines = null;
			List<String> ctakesPipeDelimitedLines = null;
			try {
				File ctakesPipedFile = new File(ctakesPipedDir + "/" + modelPipedFile.getName());
				File mergedPipedFile = new File(mergedOutputDir + "/" + modelPipedFile.getName()); 

				if(!ctakesPipedFile.exists()) {
					System.out.println("No pipe file found = " + ctakesPipedFile);
					continue;
				}

				modelPipeDelimitedLines = FileUtils.readLines(modelPipedFile);
				ctakesPipeDelimitedLines = FileUtils.readLines(ctakesPipedFile);
				TreeSet<String> mergedLines = new TreeSet<String>(); 
				for (String modelPipeDelimitedLine : modelPipeDelimitedLines) {
					mergedLines.add(modelPipeDelimitedLine);
				}
				for (String ctakesPipeDelimitedLine : ctakesPipeDelimitedLines) {
					mergedLines.add(ctakesPipeDelimitedLine);
				}

				FileUtils.writeLines(mergedPipedFile, mergedLines);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	void mergePipedOutputs (String modelPipedDir, String ctakesPipedDir, String mergedOutputDir) {

		File[] modelPipedFiles = new File(modelPipedDir).listFiles();
		for(File modelPipedFile : modelPipedFiles) {

			List<String> modelPipeDelimitedLines = null;
			List<String> ctakesPipeDelimitedLines = null;
			try {
				File ctakesPipedFile = new File(ctakesPipedDir + "/" + modelPipedFile.getName());
				File mergedPipedFile = new File(mergedOutputDir + "/" + modelPipedFile.getName()); 

				if(!ctakesPipedFile.exists()) {
					System.out.println("No pipe file found = " + ctakesPipedFile);
					continue;
				}

				modelPipeDelimitedLines = FileUtils.readLines(modelPipedFile);
				ctakesPipeDelimitedLines = FileUtils.readLines(ctakesPipedFile);

				TreeSet<Span> modelSpans = new TreeSet<Span>();
				TreeSet<Span> ctakesSpans = new TreeSet<Span>();

				//model spans. write all model spans to the merged output file
				for (String modelPipeDelimitedLine : modelPipeDelimitedLines) {

					FileUtils.write(mergedPipedFile, modelPipeDelimitedLine+"\n", true);

					String[] slots = modelPipeDelimitedLine.split("\\|\\|");
					//					System.out.println(modelPipeDelimitedLine);
					int[] spanCUI = new int[slots.length-3];
					for (int i=3 ; i < slots.length; i++) {
						//						spanCUI[j]=Integer.parseInt(slots[i]);
						Span span = new Span(Integer.parseInt(slots[i++]), Integer.parseInt(slots[i]));
						modelSpans.add(span);
					}
				}

				//ctakes spans
				//For each line of the piped file. Each line has a medical term with spans.
				for (String ctakesPipeDelimitedLine : ctakesPipeDelimitedLines) {
					String[] slots = ctakesPipeDelimitedLine.split("\\|\\|");
					//					System.out.println(modelPipeDelimitedLine);
					int[] spanCUI = new int[slots.length-3];
					for (int i=3 ; i < slots.length; i++) {
						//						spanCUI[j]=Integer.parseInt(slots[i]);
						Span span = new Span(Integer.parseInt(slots[i++]), Integer.parseInt(slots[i]));
						ctakesSpans.add(span);
					}
				}

				//merge spans. write the ctakes spans that are not identified by model.
				for (Span ctakesSpan : ctakesSpans) {
					boolean intersects = false;
					for (Span modelSpan : modelSpans) {
						if(ctakesSpan.intersects(modelSpan)) { 
							intersects = true;
						}
					}
					if(!intersects) {
						FileUtils.write(mergedPipedFile, mergedPipedFile.getName()+"||Disease_Disorder||CUI-less||"+
								ctakesSpan.getStart()+"||" + ctakesSpan.getEnd()+"\n", true);
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}


	void generatedOverSampledFeaturesFile () throws IOException {
		File B_features_file  = new File("finaldata/clef2013Dataset/model_dump_os/apr21_svm_clef13_train_features.txt_B");
		File DB_features_file  = new File("finaldata/clef2013Dataset/model_dump_os/apr21_svm_clef13_train_features.txt_DB");
		File TB_features_file  = new File("finaldata/clef2013Dataset/model_dump_os/apr21_svm_clef13_train_features.txt_TB");
		FileUtils.deleteQuietly(new File("finaldata/clef2013Dataset/model_dump_os/apr21_svm_clef13_train_features.txt_final"));
		File finalFeaturesFile = new File("finaldata/clef2013Dataset/model_dump_os/apr21_svm_clef13_train_features.txt_final");

		int qid = 0 ;
		int exampleId = 0;
		List<String> lines = FileUtils.readLines(B_features_file);
		for (String line : lines ) {
			int tempid = getQid(line);
			if(qid!=tempid) {
				exampleId++;
				qid = tempid;
			} 
			FileUtils.write(finalFeaturesFile, line.replace("qid:"+tempid, "qid:"+exampleId)+"\n", true);
		}

		qid=0;//464 * 7
		for (int i =0 ; i< 5;i++) {
			lines = FileUtils.readLines(DB_features_file);
			for (String line : lines ) {
				int tempid = getQid(line);
				if(qid!=tempid) {
					exampleId++;
					qid = tempid;
				} 
				FileUtils.write(finalFeaturesFile, line.replace("qid:"+tempid, "qid:"+exampleId)+"\n", true);
			}
		}

		qid=0;//39* 88
		for (int i =0 ; i< 10;i++) {
			lines = FileUtils.readLines(TB_features_file);
			for (String line : lines ) {
				int tempid = getQid(line);
				if(qid!=tempid) {
					exampleId++;
					qid = tempid;
				} 
				FileUtils.write(finalFeaturesFile, line.replace("qid:"+tempid, "qid:"+exampleId)+"\n", true);
			}
		}
	}

	int getQid (String line) {
		int qid = Integer.parseInt(StringUtils.substringBetween(line, "qid:", " "));
		return qid;
	} 

	/**
	 * For hitachi 2015 pos and crf
	 */
	void mergePipedOutputsOfPOSAndCRF (String posPipedDir, String crfPipedDirNewCRF,  String crfPipedDirOldCRF, String mergedOutputDir) {
		File[] posPipedFiles = new File(posPipedDir).listFiles();
		new File(mergedOutputDir).mkdir();
		for(File posPipedFile : posPipedFiles) {

			List<String> posPipeDelimitedLines = new ArrayList<String>();
			List<String> oldcrfPipeDelimitedLines = new ArrayList<String>();
			List<String> newcrfPipeDelimitedLines = new ArrayList<String>();
			try {
				File oldCrfPipedFile = new File(crfPipedDirOldCRF + "/" + posPipedFile.getName().replace(".txt", ".pipe.txt"));
				File newCrfPipedFile = new File(crfPipedDirNewCRF + "/" + posPipedFile.getName().replace(".txt", ".pipe.txt"));
				File mergedPipedFile = new File(mergedOutputDir + "/" + posPipedFile.getName().replace(".txt", ".pipe.txt")); 

				if(!oldCrfPipedFile.exists()) {
					System.out.println("No pipe file found = " + oldCrfPipedFile);
					continue;
				}


				List<String> lines = FileUtils.readLines(posPipedFile);
				//pos 
				for (String line : lines) {
					String[] split = line.split("\\|");
					posPipeDelimitedLines.add(split[0]+"|"+split[1]+"|"+split[2]);
				}

				//oldcrf
				lines = FileUtils.readLines(oldCrfPipedFile);
				for (String line : lines) {
					String[] split = line.split("\\|");
					String[] span = split[1].split("-");
					//System.out.println(line);
					try {
						oldcrfPipeDelimitedLines.add(split[0]+"|"+span[0]+"-"+(Integer.parseInt(span[1])+1)+"|"+split[2]);
					} catch (Exception e) {

					}
				}

				//oldcrf
				lines = FileUtils.readLines(newCrfPipedFile);
				for (String line : lines) {
					String[] split = line.split("\\|");
					String[] span = split[1].split("-");
					try {
						newcrfPipeDelimitedLines.add(split[0]+"|"+span[0]+"-"+(Integer.parseInt(span[1])+1)+"|"+split[2]);
					} catch (Exception e) {
						//						newcrfPipeDelimitedLines.add(line);
					}
				}

				TreeSet<String> mergedLines = new TreeSet<String>(); 
				for (String posPipeDelimitedLine : posPipeDelimitedLines) {
					mergedLines.add(posPipeDelimitedLine);
				}
				for (String oldcrfPipeDelimitedLine : oldcrfPipeDelimitedLines) {
					mergedLines.add(oldcrfPipeDelimitedLine);
				}
				for (String newcrfPipeDelimitedLine : newcrfPipeDelimitedLines) {
					mergedLines.add(newcrfPipeDelimitedLine);
				}


				TreeMap<Span, String> tm = new TreeMap<Span, String>();
				for (String line : mergedLines) {
					String[] split = line.split("\\|");
					String[] span = split[1].split("-");
					tm.put(new Span(Integer.parseInt(span[0]), Integer.parseInt(span[1])), split[2]);
				}

				List<String> sortedLines = new ArrayList<String>(); 
				for (Entry<Span, String> entry : tm.entrySet()) {
					sortedLines.add(posPipedFile.getName().replace(".pipe.", ".") +"|"+
							entry.getKey().getStart()+"-"+entry.getKey().getEnd()+"|"+entry.getValue());
				}

				FileUtils.writeLines(mergedPipedFile, sortedLines);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
	
	/**
	 * 
	 * @param pipedInDir
	 * @param pipedOutputDir
	 * @param manualAnnotatedFile
	 * @throws IOException 
	 */
	public void addManualAnnotatedTermsGt5(String pipedInDir, String pipedOutputDir, String manualAnnotatedFile) throws IOException {
		HashMap<String, String> actualChangedGtTermsMap = new HashMap<>();
		actualChangedGtTermsMap = readManualAnnotatedFile(new File(manualAnnotatedFile));
		System.out.println("Manually annotated file size = " + actualChangedGtTermsMap.size());
		File[] pipedInFiles = new File(pipedInDir).listFiles();
		for(File pipedInFile : pipedInFiles) {
			
			File prunedOutputFile = new File(pipedOutputDir + File.separator + pipedInFile.getName());
			FileUtils.deleteQuietly(prunedOutputFile);
			
			TreeMap<Span, String> spanToTextMapActual = new TreeMap<>();
			TreeMap<Span, String> spanToTextMapChanged = new TreeMap<>();
			spanToTextMapActual = readSpanAndTextFromHitachiFile(pipedInFile, 0);
			
			//Iterate on actual piped output
			//<span, medical term>
			for (Entry<Span, String> entry : spanToTextMapActual.entrySet()) {
				//check medical term in gt manually annotated file
				if(actualChangedGtTermsMap.containsKey(entry.getValue())) {
					String actual = entry.getValue();
					String changed = actualChangedGtTermsMap.get(entry.getValue());
					if(changed.contains(",")) {
						String[] split = changed.split(",");
						for(String term : split) {
							if(!term.trim().isEmpty()) {
								int start = entry.getKey().getStart()+StringUtils.indexOf(actual, term.trim());
								Span span  = new Span(start,
										start+term.trim().length());
								spanToTextMapChanged.put(span, term.trim());
								System.out.println("Changed with , = " + actual+ " = " + term + " - " + pipedInFile.getName());
								System.out.println(entry.getKey() + " - "  +span);
							}
							
						}
					} else {
						int start = entry.getKey().getStart()+StringUtils.indexOf(actual, changed);
						Span span  = new Span(start,
								start+changed.length());
						spanToTextMapChanged.put(span, entry.getValue());
						System.out.println("Changed without , "+ actual+ " = " + changed + " - " + pipedInFile.getName());
						System.out.println(entry.getKey() + " - "  +span);
					}
				} else {
					spanToTextMapChanged.put(entry.getKey(), entry.getValue());
				}
			}
			
			for (Entry<Span, String> entry1 : spanToTextMapChanged.entrySet()) {
				String line = pipedInFile.getName().replace(".pipe", "")+"|"+entry1.getKey().getStart()+"-"+entry1.getKey().getEnd()
						+"|"+entry1.getValue();
				FileUtils.write(prunedOutputFile, line +"\n", true);
			}

			//clear all for each file
			spanToTextMapChanged.clear();
			spanToTextMapActual.clear();
			
		}
	}

	private HashMap<String, String> readManualAnnotatedFile(
			File manualAnnotatedFile) {
		HashMap<String, String> actualChangedGtTermsMap = new HashMap<>();
		List<String> lines = null;
		try {
			lines = FileUtils.readLines(manualAnnotatedFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (String line : lines) {
			String[] split = line.split("\t");
			if(split.length ==2) {
				String actual = StringUtils.removeStartIgnoreCase(split[0], "Actual Term = ").trim();
				String changed = StringUtils.removeStartIgnoreCase(split[1], "Changed = ").trim();
				if(!actual.equals(changed) && !changed.isEmpty()) {
					actualChangedGtTermsMap.put(actual, changed);
				} else {
					//not changed terms
				}
			}
		}
		 
		return actualChangedGtTermsMap;
	}
	
	/**
	 * Tag disease terms
	 * @param pipedInDir
	 * @param pipedOutputDir
	 * @param manualAnnotatedFile
	 * @throws IOException
	 */
	public void diseaseTermAnnotation(String pipedInDir, String pipedOutputDir) throws IOException {
		
		File[] pipedInFiles = new File(pipedInDir).listFiles();
		for(File pipedInFile : pipedInFiles) {
			
			File diseaseTaggedFile = new File(pipedOutputDir + File.separator + pipedInFile.getName().replace(" .txt", ".txt"));
			
			List<String> lines = FileUtils.readLines(pipedInFile);
			List<String> diseaseTaggedLines = new ArrayList<>();
			for (String line : lines) {
				line = line.replace(" .txt", ".txt");
				if(StringUtils.countMatches(line, "|") == 3) {
					line = StringUtils.substringBeforeLast(line, "|");
				} else if(StringUtils.countMatches(line, "|") >= 3) {
					 String[] split = line.split("\\|");
					 line = split[0] + "|" + split[1] + "|" + split[2];
				}
				String term  = line.split("\\|")[2];
				if(!ClinicalTokenizer.isGarbageTerm(term)) {

					//String umlsterms = Java2MySql.getInstance().getCUIsAndUMLSTermOfMedicalTerm(term);
					//if (StringUtils.isEmpty(umlsterms))
					diseaseTaggedLines.add(line+"|"+CancerDiseaseDictionary.getInstance().getCancerDisease(term));
					//else 
					//	diseaseTaggedLines.add(line+"|"+ umlsterms+""+CancerDiseaseDictionary.getInstance().getCancerDisease(term));
				}
			}
			FileUtils.writeLines(diseaseTaggedFile, diseaseTaggedLines);
		}
	}
	
	/**
	 * 
	 * @param POSpipedOPDir finaldata/pubmedDataset_phase4/dec20_pma_new/
	 * Brain_Tumor_Pathol_0.txt|123-143|adrenocortical tumor|false|adrenocortical tumor|neop|C0001618| A neoplasm of the adrenal cortex in     which the malignancy status has not been established.  Tumors or cancers of the ADRENAL CORTEX.  A benign or malignant (primary or     metastatic) neoplasm affecting the adrenal cortex. (NCI05) 0
	 * @param pipedInDir Brain_Tumor_Pathol_0.txt|10792-10804|CSF pathways|0
	 * @param pipedOutputDir 
	 * @throws IOException
	 */
	public void getCUIsFromPOStoFinal(String POSpipedOPDir, String pipedInDir, String pipedOutputDir) throws IOException {
		
		File[] pipedInFiles = new File(pipedInDir).listFiles();
		for(File pipedInFile : pipedInFiles) {
			
			List<String> posLines = FileUtils.readLines(new File (POSpipedOPDir + File.separator + pipedInFile.getName()
					.replace(".pipe.txt", ".txt")));
			
			HashMap<String, String> spanToCUIsPipedMap = new HashMap<>();
			for (String posline : posLines) {
				if (StringUtils.countMatches(posline, "|") == 7) {
					String[] split = posline.split("\\|");
					spanToCUIsPipedMap.put(split[1], "|"+split[6]+"|"+split[4]+"|");
				}
			}
			
			File diseaseTaggedFile = new File(pipedOutputDir + File.separator + pipedInFile.getName());
			
			
			List<String> lines = FileUtils.readLines(pipedInFile);
			List<String> diseaseTaggedLines = new ArrayList<>();
			for (String line : lines) {
				String term  = line.split("\\|")[2];
				String span = line.split("\\|")[1];
				
				String beforeDisease = StringUtils.substringBeforeLast(line, "|");
				String disease = StringUtils.substringAfterLast(line, "|");
				
				if(!ClinicalTokenizer.isGarbageTerm(term)) {
					if (spanToCUIsPipedMap.containsKey(span))
						diseaseTaggedLines.add(beforeDisease+"" + spanToCUIsPipedMap.get(span)+ ""
								+ disease);
					else 
						diseaseTaggedLines.add(beforeDisease+"|"
								+ disease);
				}
			}
			FileUtils.writeLines(diseaseTaggedFile, diseaseTaggedLines);
			spanToCUIsPipedMap.clear();
		}
	}

	public static void main (String[] args) throws IOException {
		PipedOutputProcessor pipedOutputProcessor = new PipedOutputProcessor();
		/*pipedOutputProcessor.removeLinesLT3("finaldata/DataAnnotation_phase3/test1/DSPIPED_CRFOP", 
				"finaldata/DataAnnotation_phase3/test1/DSPIPED_CRFOP_gt2");
		pipedOutputProcessor.removeLinesLT3("finaldata/DataAnnotation_phase3/test1/DSPIPED_GOLD", 
				"finaldata/DataAnnotation_phase3/test1/DSPIPED_GOLD_gt2");*/
		/*pipedOutputProcessor.getRelaxedWords("finaldata/DataAnnotation_phase3/test1/DS",
				"finaldata/DataAnnotation_phase3/test1/DSPIPED_GOLD",
				"finaldata/DataAnnotation_phase3/test1/DSPIPED_CRFOP",
				args[1]);*/


		/*pipedOutputProcessor.getWordsNotCaughtCLEF2013("finaldata/clef2013Dataset/test/DS/",
				"finaldata/clef2013Dataset/test/DSPIPED",
				"finaldata/clef2013Dataset/test/apr21_DSPIPED_SVMOP_OS");*/

		//stats
		/*pipedOutputProcessor.getStatOfDataset("finaldata/phase9/totalDS/DS",
				"finaldata/phase9/totalDS/DSPIPED");*/

		/*pipedOutputProcessor.getWordsNotCaught("finaldata/phase9/1/DS",
				"finaldata/phase9/1/DSPIPED",
				"finaldata/phase9/1/may16_DSPIPED_SVMOP",
				"op");*/

		//TESTING four fold
		/*pipedOutputProcessor.getWordsNotCaught("./finaldata/phase8a/"+args[0]+"/DS",
				"./finaldata/phase8a/"+args[0]+"/DSPIPED",
				"./finaldata/phase8a/"+args[0]+"/may12_DSPIPED_CRFOP_PPUD",
				"predictedDiseaseTermsrelaxed_phase8a_"+args[0]);*/



		/*pipedOutputProcessor.extractMedicalTermWordTemplatesFromSpans("/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/finaldata/clef2013Dataset/test1/DS"
				, "/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/finaldata/clef2013Dataset/test1/DSPIPED");
		 */
		/*pipedOutputProcessor.extractTermsOfSpan("finaldata/phase9/train_"+args[0]+"/DS"
				, "finaldata/phase9/train_"+args[0]+"/DSPIPED");*/

		/*pipedOutputProcessor.getSpansCountForDisjointTerms("/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/finaldata/clef2013Dataset/train1/DS"
		, "/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/finaldata/clef2013Dataset/train1/DSPIPED");
		 */

		/*//merge force crf
		new File("./finaldata/clef2013Dataset/test/apr9_CRF_CTAKES_Merged_FORCE").mkdir();
		pipedOutputProcessor.mergePipedOutputsForce("finaldata/clef2013Dataset/test/apr9_DSPIPED_CRFOP_WUMLS_PP_DUP",
				"finaldata/clef2013Dataset/test/apr9_CTAKES_CLEF2013_OP", "finaldata/clef2013Dataset/test/apr9_CRF_CTAKES_Merged_FORCE");

		//merge force svm
		new File("./finaldata/clef2013Dataset/test/apr9_SVM_CTAKES_Merged_FORCE").mkdir();
		pipedOutputProcessor.mergePipedOutputsForce("finaldata/clef2013Dataset/test/apr9_DSPIPED_SVMOP_WUMLS_PP_DUP",
				"finaldata/clef2013Dataset/test/apr9_CTAKES_CLEF2013_OP", "finaldata/clef2013Dataset/test/apr9_SVM_CTAKES_Merged_FORCE");

		//merge crf
		new File("./finaldata/clef2013Dataset/test/apr9_CRF_CTAKES_Merged").mkdir();
		pipedOutputProcessor.mergePipedOutputs("finaldata/clef2013Dataset/test/apr9_DSPIPED_CRFOP_WUMLS_PP_DUP",
				"finaldata/clef2013Dataset/test/apr9_CTAKES_CLEF2013_OP", "finaldata/clef2013Dataset/test/apr9_CRF_CTAKES_Merged");

		//merge svm
		new File("./finaldata/clef2013Dataset/test/apr9_SVM_CTAKES_Merged").mkdir();
		pipedOutputProcessor.mergePipedOutputs("finaldata/clef2013Dataset/test/apr9_DSPIPED_SVMOP_WUMLS_PP_DUP",
				"finaldata/clef2013Dataset/test/apr9_CTAKES_CLEF2013_OP", "finaldata/clef2013Dataset/test/apr9_SVM_CTAKES_Merged");
		 */

		//merge crf-svm and ctakes force
		/*new File("./finaldata/clef2013Dataset/test/apr9_CRF_SVM_CTAKES_Merged_FORCE").mkdir();
		pipedOutputProcessor.mergePipedOutputsForce("finaldata/clef2013Dataset/test/apr9_CRF_SVM_Merged_FORCE",
				"finaldata/clef2013Dataset/test/apr9_CTAKES_CLEF2013_OP", "finaldata/clef2013Dataset/test/apr9_CRF_SVM_CTAKES_Merged_FORCE");

		merge crf-svm and ctakes
		new File("./finaldata/clef2013Dataset/test/apr9_CRF_SVM_CTAKES_Merged").mkdir();
		pipedOutputProcessor.mergePipedOutputs("finaldata/clef2013Dataset/test/apr9_CRF_SVM_Merged",
				"finaldata/clef2013Dataset/test/apr9_CTAKES_CLEF2013_OP", "finaldata/clef2013Dataset/test/apr9_CRF_SVM_CTAKES_Merged");
		 */

		//merge crf and svm force
		/*new File("./finaldata/phase4/test/apr30_CRF_SVM_Merged_FORCE").mkdir();
		pipedOutputProcessor.mergePipedOutputsForce("finaldata/phase4/test/apr30_DSPIPED_CRFOP",
				"finaldata/phase4/test/apr30_DSPIPED_SVMOP", "finaldata/phase4/test/apr30_CRF_SVM_Merged_FORCE");

		//merge crf and svm ctakes
		new File("./finaldata/phase4/test/apr30_CRF_SVM_Merged").mkdir();
		pipedOutputProcessor.mergePipedOutputs("finaldata/phase4/test/apr30_DSPIPED_CRFOP",
				"finaldata/phase4/test/apr30_DSPIPED_SVMOP", "finaldata/phase4/test/apr30_CRF_SVM_Merged");*/

		//		pipedOutputProcessor.mergePipedOutputsOfPOSAndCRF(args[0], args[1], args[2], args[3]);

		//final features
		//		pipedOutputProcessor.generatedOverSampledFeaturesFile();

		//java -cp bin/:conf:lib/*  edu.iiit.cde.r.crf.PipedOutputProcessor 
		// /home/cde/Hitachi2015/pubmedDataset/brainCancerDocs_pipedoutput_pma/brainCancerDocs_pipedoutput_pma/ 
		// /home/cde/Hitachi2015/pubmedDataset/brainCancerDocs_pipedoutput_pma_crf/1/ 
		// /home/cde/Hitachi2015/pubmedDataset/brainCancerDocs_pipedoutput_pma_crf_h/brainCancerDocs_pipedoutput_pma_crf_h/ 
		// /home/cde/Hitachi2015/pubmedDataset/overlapped_pos_crf1_crf2014

		//pipedOutputProcessor.getOverlappedTerms(args[0], args[1], args[2], args[3]);

		//Testing on sample
		/*pipedOutputProcessor.addManualAnnotatedTermsGt5("finaldata/samplePipedDir", "finaldata/samplePipedDir_aftermanualgt5",
				"gt5TermsManualAnnotated.txt");*/
		
		/*pipedOutputProcessor.mergePmaAndCRFoldUsingGoogleApiWorkingFinal("pubmedDataset/sampleDocs",
				"pubmedDataset/sampleDocs_pma", "pubmedDataset/sampleDocs_crfh", "pubmedDataset/sampleDocs_pma_crfh_merged");
*/
		/*if(args.length==4)
			pipedOutputProcessor.mergePmaAndCRFoldUsingGoogleApiWorkingFinal
				(args[0], args[1], args[2], args[3]);
		else if(args.length==3)
			pipedOutputProcessor.addManualAnnotatedTermsGt5(args[0], args[1], args[2]);
		else if(args.length==2)
			pipedOutputProcessor.diseaseTermAnnotation(args[0], args[1]);*/
		
		pipedOutputProcessor.getCUIsFromPOStoFinal(args[0], args[1], args[2]);
	}

}