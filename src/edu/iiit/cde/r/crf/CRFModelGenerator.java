package edu.iiit.cde.r.crf;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import opennlp.tools.util.Span;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import edu.iiit.cde.anafora.util.AnaforaConverter;
import edu.iiit.cde.r.mysql.Java2MySql;
import edu.iiit.cde.r.svm.Word2Index;
import edu.iiit.cde.r.utils.ClefPipedLineParser;
import edu.iiit.cde.r.utils.ClinicalTokenizer;
import edu.iiit.nlp.Chunker;
import edu.iiit.nlp.OpenNLPTokenizer;
import edu.iiit.nlp.POSTagger;
import edu.iiit.nlp.SentenceBreaker;
import edu.iiit.nlp.WordStemmer;

/**
 * CRFModelGenerator extracts the features from unstructured data.
 * It then trains and test the CRF model using the features file.
 * 
 * @author Raghavendra
 *
 */
public class CRFModelGenerator {


	//	String crfmodel = "./finaldata/DataAnnotation_phase3/model_dump/feb27_crf_train3_features_WUMLS_PP.model";
	//					    "./finaldata/DataAnnotation_phase3/model_dump/crf_train3_features_feb14_biclass.model";

	Java2MySql java2MySql; 
	ClefPipedLineParser clefPipedLineParser = new ClefPipedLineParser();
	PipedOutputProcessor pipedOutputProcessor = new PipedOutputProcessor();
	int noOfFeatures = 19;
	private boolean includeUMLS = true;
	/**
	 * CRFModelGenerator constructor.
	 */
	public CRFModelGenerator() {
		java2MySql = Java2MySql.getInstance();
	}

	/**
	 * Extracts the corresponding medical terms from the TextFile using the spans of PipedFile.
	 */
	void getEntitiesFromFile (String corpusDir, String pipeDir){
		//		String corpusDir = "/home/raghavendra/BackUP/MyWorks/Hitachi2014/datasets/clefDS";
		//		String pipeDir = "/home/raghavendra/BackUP/MyWorks/Hitachi2014/clef_finalAnnotations_67";

		File[] corpusFiles = new File(corpusDir).listFiles();
		for(File corpusFile : corpusFiles) {
			List<String> pipeDelimitedLines = null;
			String corpuslines = null;
			try {
				corpuslines  = FileUtils.readFileToString(corpusFile);

				File pipeFile = new File(pipeDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));

				if(!pipeFile.exists())
					continue;

				pipeDelimitedLines = FileUtils.readLines(pipeFile);
			} catch (IOException e) {
				e.printStackTrace();
			}

			for (String pipeDelimitedLine : pipeDelimitedLines) {

				String[] slots = pipeDelimitedLine.split("\\|");
				String filename = slots[0];

				String[] spanString = slots[1].split(",");
				String[] CUIString = slots[2].split(",");


				int[] spanCUI = new int[spanString.length * 2];
				for (int i = 0; i < spanString.length; i++) {
					spanCUI[i * 2] = Integer.parseInt(spanString[i].split("-")[0]);
					spanCUI[i * 2 + 1] = Integer.parseInt(spanString[i].split("-")[1]);
					int j = CUIString.length;
					if (i<j) {
						j=i;
					} else {
						j = CUIString.length-1;
					}

					System.out.println(filename + "\t" 
							+ StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]+1) + "\t"
							+ CUIString[j] + "\t" + slots[3]
							);

				}
			}


		}

	}

	/**
	 * Returns the orthographic features of the token.
	 * @param token
	 * @param includeUMLS
	 * @return orthographic features.
	 * @throws IOException 
	 */
	String getTokenFeatures (String token, String[] tokens, int k, boolean includeUMLS){

		token = ClinicalTokenizer.getPrunedMedicalTerm(token);
		if(token.isEmpty() && includeUMLS) {
			return "\tX\tX\tX\tX\tX\tX\tX\tX\tX\tX\tX\tX\tX\t";
		} 
		if(token.isEmpty()) {
			return "\tX\tX\tX\tX\tX\tX\tX\tX\tX\tX\t";
		}


		StringBuffer sb = new StringBuffer();
		sb.append("\t");

		//stopword
		if(ClinicalTokenizer.getInstance().isStopword(token)) {
			sb.append("STOPWORD");
		} else {
			sb.append("X");
		}
		sb.append("\t");
		//first char CAP
		if(Character.isUpperCase(token.charAt(0))) {
			sb.append("START-CAP");
		} else { 
			sb.append("X");
		}
		sb.append("\t");

		//CAPS
		if(StringUtils.isAllUpperCase(token)) {
			sb.append("AllUpperCase");
		} else { 
			sb.append("X");
		}
		sb.append("\t");

		//Numeric
		if(StringUtils.isNumericSpace(token)) {
			sb.append("NUMERIC");
		} else { 
			sb.append("X");
		}
		sb.append("\t");

		//alphanumeric
		if(StringUtils.isAlphanumericSpace(token)) {
			sb.append("ALPHANUMERIC");
		} else { 
			sb.append("X");
		}
		sb.append("\t");

		//
		if(StringUtils.isAlphanumericSpace(token) && token.contains("-")) {
			sb.append("HasHyphen");
		} else { 
			sb.append("X");
		}
		sb.append("\t");

		//includes UMLS features (Is token in UMLS dictionary? and Semantic type of the token)
		if(includeUMLS) {
			if(java2MySql.isInUMLS(token)) {
				sb.append("isInUMLS");
				sb.append("\t");
				String st = java2MySql.getSemanticTypeOfTerm(token);
				if(st != null) {
					if(!st.isEmpty())
						sb.append(st);//X is no semantic type
					else 
						sb.append("X");
				} else {
					sb.append("X");
				}
				sb.append("\t");
			} else { 
				sb.append("X");
				sb.append("\t");
				sb.append("X");
				sb.append("\t");
			}

		}

		/*List<String> finalmedicalTerms = pipedOutputProcessor.extendMedicalTermBothSides(spanstart, spanend,
				medicalterm, tokens, spanToTokenNumberMap);*/
		if(includeUMLS) {
			if(pipedOutputProcessor.isInUMLS(token, tokens, k)) {
				sb.append("isInUMLSEx");
				sb.append("\t");
			} else { 
				sb.append("X");
				sb.append("\t");
			}
		}


		try {
			if (Word2Index.getInstance().wikiWords.contains(token)) {
				sb.append("isInWiki");
				sb.append("\t");
			} else {
				sb.append("X");
				sb.append("\t");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (Word2Index.getInstance().wikiClasses.contains(token)) {
				sb.append("isInWikiClass");
				sb.append("\t");
			} else {
				sb.append("X");
				sb.append("\t");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			//CLEF2014-hitachi
//			if (Word2Index.getInstance().hitachiTrainDict.contains(token) || 
//					Word2Index.getInstance().hitachiTrainDict.contains(token.toLowerCase())) {
				//CLEF2013
			if (Word2Index.getInstance().trainDict.contains(token)) {
				sb.append("isInTrainDict");
				sb.append("\t");
			} else {
				sb.append("X");
				sb.append("\t");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			//CLEF2014-hitachi
//			if (Word2Index.getInstance().hitachiDiseaseTrainDict.contains(token)  || 
//					Word2Index.getInstance().hitachiDiseaseTrainDict.contains(token.toLowerCase())) {
				//CLEF2013
			if (Word2Index.getInstance().trainDict.contains(token)) {
				sb.append("isInHitachiDiseaseTrainDict");
				sb.append("\t");
			} else {
				sb.append("X");
				sb.append("\t");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	/**
	 * Reads discharge summary and its piped output, and generates features file to build CRF model.
	 * 
	 * Sample PipedOP : 00098-017139-DISCHARGE_SUMMARY.txt|9-20|C0524850|Neurosurgery|Neurosurgery|topp|generic
	 *
	 * @throws IOException 
	 */
	/*void generateFeaturesAndTrain (String corpusDir, String pipeDir, String featuresFileString, String crfmodel, boolean includeUMLS ) throws IOException {

		FileUtils.deleteQuietly(new File(featuresFileString));
		File featuresFile = new File(featuresFileString);

		File[] corpusFiles = new File(corpusDir).listFiles();
		for(File corpusFile : corpusFiles) {
			System.out.println("Processing ..  " + corpusFile.getName());
			int start = 0;
			int end = 0;
			List<String> pipeDelimitedLines = null;
			String corpuslines = "";
			try {
				//read the piped files of the discharge summary files
				corpuslines  = FileUtils.readFileToString(corpusFile);
				File pipeFile = new File(pipeDir + "/" + corpusFile.getName()); 
				//+ StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));

				if (corpusFile.getName().contains(".txt"))
					pipeFile = new File(pipeDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));


				if(!pipeFile.exists()) {
					System.out.println("No pipe file found = " + pipeFile);
					continue;
				}

				pipeDelimitedLines = FileUtils.readLines(pipeFile);
			} catch (IOException e) {
				e.printStackTrace();
			}

			//For each line of the piped file. Each line has a medical term with spans.
			for (String pipeDelimitedLine : pipeDelimitedLines) {

				String[] slots = pipeDelimitedLine.split("\\|");

				String[] spanString = slots[1].split(",");
				String[] CUIString = slots[3].split(",");
				String diseaseTag = slots[slots.length-1];

				int[] spanCUI = new int[spanString.length * 2];

				//Iterate on each span of each line.
				for (int i = 0; i < spanString.length; i++) {

					spanCUI[i * 2] = Integer.parseInt(spanString[i].split("-")[0]);
					spanCUI[i * 2 + 1] = Integer.parseInt(spanString[i].split("-")[1]);
					int j = CUIString.length;
					if (i<j) {
						j=i;
					} else {
						j = CUIString.length-1;
					}

					end = spanCUI[i * 2];
					//For first line 0 < start of first span
					if(start < end) {

						//get sentence apart from medical term
						String remainingSentence = StringUtils.substring(corpuslines,start,end);
						if(remainingSentence.equals(" ")) {
							continue;
						} else if(remainingSentence.startsWith(" ")) {
							remainingSentence = StringUtils.removeStart(remainingSentence, " ");
						} else if (remainingSentence.equals("\n")) {
							FileUtils.write(featuresFile, "\n", true);
						}

 						//System.out.println("start-End = " + start + "\t" + end + remainingSentence);

 						//Get all sentences of text(other than medical terms) and append with 'O'
 						String[] sentences = SentenceBreaker.getSentenceBreaker().getSentences(remainingSentence);
 						for (String sentence : sentences) {
 							String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize(sentence);
 							String[] tags = POSTagger.getPOSTagger().tag(tokens);
 							String[] chunks = Chunker.getChunker().getChunks(tokens, tags);

 							//System.out.println("Sentence == " + sentence);
 							for (int k =0 ; k < tokens.length; k++) {
 								//nlp features
 								FileUtils.write(featuresFile, tokens[k]+
 										"\t"+ WordStemmer.getStemmer().stem(tokens[k]) +
 										"\t"+ tags[k] +
 										"\t"+ chunks[k], true);
// 								FileUtils.write(featuresFile, getTokenFeatures(tokens[k]), true);
//								FileUtils.write(featuresFile,"O\n", true);

 								if(tokens[k].equals(".")) {
 									FileUtils.write(featuresFile, getTokenFeatures(tokens[k],includeUMLS), true);
 									FileUtils.write(featuresFile,"O\n", true);

 									//. and newline
 									//FileUtils.write(featuresFile, ".\tX\tX\t", true);
 									FileUtils.write(featuresFile,"O\n\n", true);
 								} else if (tokens[k].isEmpty()) {
 									//newline
 									//System.out.println("newline");
 									FileUtils.write(featuresFile, "\n", true);
 								} else  {
// 									FileUtils.write(featuresFile, tokens[k], true);
 									//orthographic features
 									FileUtils.write(featuresFile, getTokenFeatures(tokens[k],includeUMLS), true);
 									FileUtils.write(featuresFile,"O\n", true);
 								}
 							}
 							//System.out.println();
 						}

					} 

					//Add +1 if span is one less in training data.
//					String word = StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]+1);
//					start = spanCUI[i * 2 + 1]+1;
					String word = StringUtils.substring(corpuslines, spanCUI[i * 2], spanCUI[i * 2 + 1]);
					start = spanCUI[i * 2 + 1];



					System.out.println(word + "\tterm");
					//Iterate on spans of the medical term and append the B-M or I-M label
					//Abdominal wall
					if (!word.isEmpty()) { // && !diseaseTag.equals("0")) {
						String position = "B";

						String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize(word);
						String[] tags = POSTagger.getPOSTagger().tag(tokens);
						String[] chunks = Chunker.getChunker().getChunks(tokens, tags);

						for (int k= 0; k< tokens.length; k++) {
							//nlp features
							//delete filename later. this is only to get error in piped files
							FileUtils.write(featuresFile, tokens[k]+
										"\t"+ WordStemmer.getStemmer().stem(tokens[k]) +
										"\t"+ tags[k] +
										"\t"+ chunks[k], true);
							//orthographic features
							FileUtils.write(featuresFile, getTokenFeatures(tokens[k],includeUMLS), true);
							FileUtils.write(featuresFile, position + "-M\n", true);
							position = "I";
						}
					} 
				}
			}
		}
		trainCRFModelUsingFeaturesFile(featuresFile, crfmodel);
	}*/

	/**
	 * Reads discharge summary and its piped output, and generates features file to build CRF model.
	 * 
	 * Sample PipedOP : 00098-017139-DISCHARGE_SUMMARY.txt|9-20|C0524850|Neurosurgery|Neurosurgery|topp|generic
	 *
	 * @throws IOException 
	 */
	void generateFeaturesAndTrainCRFWithBIO (String corpusDir, String pipeDir, String featuresFileString, String crfmodel, boolean includeUMLS ) throws IOException {

		FileUtils.deleteQuietly(new File(featuresFileString));
		File featuresFile = new File(featuresFileString);

		File[] corpusFiles = new File(corpusDir).listFiles();
		for(File corpusFile : corpusFiles) {
			//+ StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));
			File pipeFile = new File(pipeDir + "/" + corpusFile.getName()); 
			if (corpusFile.getName().contains(".txt")) {
				pipeFile = new File(pipeDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));
			}
			if(!pipeFile.exists()) {
				System.out.println("No pipe file found = " + pipeFile);
				continue;
			}
			generateTrainFeaturesFile (corpusFile, pipeFile, featuresFile);
		}
		trainCRFModelUsingFeaturesFile(featuresFile, crfmodel);
	}


	void generateTrainFeaturesFile (File corpusFile, File pipeFile, File featuresFile) throws IOException {

		System.out.println("Processing ..  " + corpusFile.getName());
		List<String> pipeDelimitedLines = null;
		try {
			pipeDelimitedLines = FileUtils.readLines(pipeFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		TreeMap<Integer, Integer> singleSpans = new TreeMap<Integer, Integer>();
		TreeMap<Integer, Integer> disjointFirstSpans = new TreeMap<Integer, Integer>();
		TreeMap<Integer, Integer> disjointSecondSpans = new TreeMap<Integer, Integer>();
		TreeMap<Integer, Integer> tripletFirstSpans = new TreeMap<Integer, Integer>();
		TreeMap<Integer, Integer> tripletSecondSpans = new TreeMap<Integer, Integer>();
		//For each line of the piped file. Each line has a medical term with spans.
		for (String pipeDelimitedLine : pipeDelimitedLines) {


			//CLEF2014
			String[] slots = pipeDelimitedLine.split("\\|");

			String[] spanString = slots[1].split(",");

			int[] spanCUI = new int[spanString.length * 2];

			//Iterate on each span of each line.
			for (int i = 0; i < spanString.length; i++) {

				spanCUI[i * 2] = Integer.parseInt(spanString[i].split("-")[0]);
				spanCUI[i * 2 + 1] = Integer.parseInt(spanString[i].split("-")[1]);

				if (spanString.length==1) {
					if (i==0) {
						singleSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
					}
				} else if (spanString.length==2) {
					if (i==0) {
						disjointFirstSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
					} else {
						disjointSecondSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
					}
				} else if (spanString.length==3) {
					if (i==0) {
						tripletFirstSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
					} else {
						tripletSecondSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
					}
				}
				//System.out.println(spanCUI[i * 2] +"-"+ spanCUI[i * 2 + 1]);
			}


			//CLEF 2013
			/*int[] spanCUI = clefPipedLineParser.parseCLEF2013PipedFile(pipeDelimitedLine);
			for (int i = 0; i < spanCUI.length/2; i++) {

				if (spanCUI.length/2==1) {
					if (i==0) {
						singleSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
					}
				} else if (spanCUI.length/2==2) {
					if (i==0) {
						disjointFirstSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
					} else {
						disjointSecondSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
					}
				} else if (spanCUI.length/2==3) {
					if (i==0) {
						tripletFirstSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
					} else {
						tripletSecondSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
					}
				}
				//System.out.println(spanCUI[i * 2] +"-"+ spanCUI[i * 2 + 1]);
			}*/


		}

		//new code
		List<String> lines = FileUtils.readLines(corpusFile); 
		String fileText = FileUtils.readFileToString(corpusFile);
		int currentPos = 0;
		String sectionHeader = "";
		String clefDocType = getDocumentType(corpusFile.getName());
		for (int i =0; i < lines.size(); i++) {

			String sectionHeaderSubstring = StringUtils.substringBefore(lines.get(i), ":");
			//System.out.println(sectionHeaderSubstring);
			Word2Index.getInstance();
			if (Word2Index.sectionHeadersTop.contains(sectionHeaderSubstring.toLowerCase())) {
				sectionHeader = sectionHeaderSubstring.replace(" ", "_");
				//System.out.println("sectionheader = " + sectionHeader);
			} else {
				sectionHeader = "X";
			}

			String[] sentences = SentenceBreaker.getSentenceBreaker().getSentences(lines.get(i));
			for (int j = 0; j < sentences.length; j++) {
				String sentence = sentences[j];
				//					System.out.println("sentence = " + sentence);

				String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize(sentence);
				String[] tags = POSTagger.getPOSTagger().tag(tokens);
				String[] chunks = Chunker.getChunker().getChunks(tokens, tags);

				int endMedicaltermSpan = -1;
				String medicaltag = ""; 
				for (int k =0 ; k < tokens.length; k++) {
					int noOfSpaces = StringUtils.indexOf(fileText, tokens[k]);
					//						System.out.println("No of spaces = " + noOfSpaces);
					//at medical term start
					currentPos += noOfSpaces;

					if(tokens[k].startsWith("-")) {
						//							tokens[k] = StringUtils.substring(tokens[k], 1);
						currentPos++;
					}
					//nlp features
					FileUtils.write(featuresFile, tokens[k]+
							"\t"+ WordStemmer.getStemmer().stem(tokens[k]) +
							"\t"+ tags[k] +
							"\t"+ chunks[k] + 
							"\t" + sectionHeader +
							"\t" + clefDocType , true);

					//orthographic features
					FileUtils.write(featuresFile, getTokenFeatures(tokens[k],tokens, k,includeUMLS ), true);

					if(singleSpans.containsKey(currentPos)) {
						//System.out.println(currentPos+"-"+tokens[k]);
						FileUtils.write(featuresFile, "B-M\n", true);
						medicaltag = "I-M";
						endMedicaltermSpan = singleSpans.get(currentPos);
					} else if(disjointFirstSpans.containsKey(currentPos)) {
						//System.out.println(currentPos+"-"+tokens[k]);
						FileUtils.write(featuresFile, "DB-M\n", true);
						medicaltag = "DI-M";
						endMedicaltermSpan = disjointFirstSpans.get(currentPos);
					}  else if(disjointSecondSpans.containsKey(currentPos)) {
						//System.out.println(currentPos+"-"+tokens[k]);
						FileUtils.write(featuresFile, "DHB-M\n", true);
						medicaltag = "DHI-M";
						endMedicaltermSpan = disjointSecondSpans.get(currentPos);
					} else if(tripletFirstSpans.containsKey(currentPos)) {
						//System.out.println(currentPos+"-"+tokens[k]);
						FileUtils.write(featuresFile, "TB-M\n", true);
						medicaltag = "TI-M";
						endMedicaltermSpan = tripletFirstSpans.get(currentPos);
					}  else if(tripletSecondSpans.containsKey(currentPos)) {
						//System.out.println(currentPos+"-"+tokens[k]);
						FileUtils.write(featuresFile, "THB-M\n", true);
						medicaltag = "THI-M";
						endMedicaltermSpan = tripletSecondSpans.get(currentPos);
					} else if (currentPos < endMedicaltermSpan) {
						//System.out.println(currentPos+"-"+tokens[k]);
						FileUtils.write(featuresFile, medicaltag+"\n", true);
					} else {
						FileUtils.write(featuresFile,"O\n", true);
					}
					if(tokens[k].startsWith("-")) {
						//							tokens[k] = StringUtils.substring(tokens[k], 1);
						currentPos--;
					}
					//startSentence += tokens[k].length()+1;
					//						System.out.println("tokens length = " + tokens[k].length());
					//						System.out.println(span+"-"+tokens[k]);
					fileText = StringUtils.substring(fileText, (tokens[k].length()+ noOfSpaces));
					currentPos += tokens[k].length();

				}
				FileUtils.write(featuresFile,"\n", true);
			}
			//span++;
		}


	}
	/*private int[] parseCLEF2013PipedFile(String pipeDelimitedLine) {
		String[] slots = pipeDelimitedLine.split("\\|\\|");
//		System.out.println(pipeDelimitedLine);
		int[] spanCUI = new int[slots.length-3];
		for (int i=3,j=0 ; i < slots.length; i++,j++) {
			spanCUI[j]=Integer.parseInt(slots[i]);
			//System.out.println(Integer.parseInt(slots[i]));
		}

		return spanCUI;
	}*/

	/**
	 * Returns document type. Ex : dischargeSummary, ECHO-REPORT, 
	 * @param name
	 * @return
	 */
	private String getDocumentType(String docname) {
		if(docname.contains("ECG_REPORT")) {
			return "ECG_REPORT";
		} 
		if(docname.contains("ECHO_REPORT")) {
			return "ECHO_REPORT";
		} 
		if(docname.contains("DISCHARGE_SUMMARY")) {
			return "DISCHARGE_SUMMARY";
		} 
		if(docname.contains("RADIOLOGY_REPORT")) {
			return "RADIOLOGY_REPORT";
		} 
		return "X";
	}

	public void generateFeaturesAndTrainWithOutUMLS (String corpusDir, String pipeDir, String featuresFileString, String crfmodel) throws IOException {
		//generateFeaturesAndTrain(corpusDir, pipeDir, featuresFileString, crfmodel, false);
	}

	public void generateFeaturesAndTrainWithUMLS (String corpusDir, String pipeDir, String featuresFileString, String crfmodel) throws IOException {
		generateFeaturesAndTrainCRFWithBIO(corpusDir, pipeDir, featuresFileString, crfmodel, true);
	}


	//crf_learn ./model_dump/crf++templateFile.txt ./model_dump/features.txt medicalterm.model

	/**
	 * Trains the crf model using features file.
	 * @param featuresFile
	 * @param crfmodel
	 */
	private void trainCRFModelUsingFeaturesFile(File featuresFile, String crfmodel) {
		Process p;
		try {

			ProcessBuilder builder = new ProcessBuilder("sed",
					"/^O$/d", 
					featuresFile.getAbsolutePath()  );
			builder.redirectOutput(new File("1"));
			builder.redirectError(new File("1"));
			p = builder.start();
			p.waitFor(); 

			builder = new ProcessBuilder("cat",
					"1" );
			builder.redirectOutput(featuresFile);
			p = builder.start();
			p.waitFor(); 

			///svm_hmm_learn -c 5 -e 0.5 example7/declaration_of_independence.dat declaration.model 
			builder = new ProcessBuilder("crf_learn",
					"./conf/crf++templateFile.txt", 
					featuresFile.getAbsolutePath(), crfmodel );
			p = builder.start();

			p.waitFor(); 
		} catch (IOException  | InterruptedException e) {
			e.printStackTrace();
		} 

	}

	/**
	 * Creates piped output files of discharge summaries using CRF model and does post processing on the output files.
	 * @param corpusDir
	 * @param pipedOutputDir
	 * @param featuresFileString
	 * @param includeUMLS
	 * @throws IOException
	 */
	public void crfTestBNERWithPP (String corpusDir, String pipedOutputDir, String crfmodel, boolean includeUMLS) throws IOException {

		File featuresFileWithLabel = null;
		File[] corpusFiles = new File(corpusDir).listFiles();
		System.out.println("Total Files = " + corpusFiles.length);
		for(File corpusFile : corpusFiles) {
			String featuresFileString = "tempscrf/temp.txt_"+corpusFile.getName();
			File featuresFile = new File(featuresFileString);

			//generate Features File - temp.txt. comment if you only process on piped outputs for pp
			generateTestFeaturesFile(corpusFile, featuresFile);
			// Genereate labels for the features file using CRF model - temp.txt.WithLabel
			featuresFileWithLabel = getLabelsForFeaturesFile(featuresFile, crfmodel);


			//for own dictionary for recursive tagging
			//			featuresFileWithLabel = new File(featuresFile.getAbsolutePath()+"_WithLabel");
			//			buildOwnDictionary(corpusFile, featuresFileWithLabel);
			//post processing
			//			File labelsPostProcessed = postProcessingLabels(featuresFile, featuresFileWithLabel);
			//			createPipeFileFromFeaturesFileWithPostProcessing(corpusFile, labelsPostProcessed, pipedOutputDir, includeUMLS);

			//			createPipeFileFromFeaturesFileWithPostProcessing(corpusFile, featuresFileWithLabel, pipedOutputDir, includeUMLS);
			//createPipeFileFromFeaturesFileWithPostProcessing(corpusFile, featuresFileWithLabel, pipedOutputDir, includeUMLS);
			//CLEF2014
			File pipedOutputFile = new File(pipedOutputDir + "/" + corpusFile.getName().replace(".txt", ".pipe.txt"));

			//CLEF2013
			//File pipedOutputFile = new File(pipedOutputDir + "/" + corpusFile.getName());
			createPipeFileFromFeaturesFileWithPPWithBIODTtags(corpusFile, featuresFileWithLabel, pipedOutputFile);
			//			createPipeFileFromFeaturesFileWithPPWithBIODTtags(corpusFile, labelsPostProcessed, pipedOutputDir, includeUMLS);
			//			FileUtils.deleteQuietly(new File(featuresFileString));
		}
		//		FileUtils.deleteQuietly(new File(featuresFileString));
		//		FileUtils.deleteQuietly(featuresFileWithLabel);
	}

	/**
	 * Creates piped output file from the features file.
	 * @param corpusFile
	 * @param featuresFileWithLabel
	 * @param pipedOutputDir
	 * @param includeUMLS
	 */
	private void createPipeFileFromFeaturesFileWithPPWithBIODTtags(File corpusFile, File featuresFileWithLabel,
			File pipedOutputFile) {

		try {
			String fileText = FileUtils.readFileToString(corpusFile);
			String fullText = FileUtils.readFileToString(corpusFile);
			FileUtils.deleteQuietly(pipedOutputFile);
			System.out.println("crf piped output = " + pipedOutputFile.getPath());
			List<String> lines = FileUtils.readLines(featuresFileWithLabel);
			int currentPos = 0, spanstart = 0, spanend = 0;
			List<String> tokensList = new ArrayList<String>();
			List<String> tagsList = new ArrayList<String>();
			List<Span> spansList = new ArrayList<Span>();
			for (String line : lines) {
				//			System.out.println(line);
				if(StringUtils.startsWith(line, "# "))
					continue;
				if(line.isEmpty()) {

					if(spansList.size()>0) { 
						/*int sentenceStart = spansList.get(0).getStart();
						int sentenceEnd = spansList.get(spansList.size()-1).getEnd();
						String spanSentence = StringUtils.substring(fullText, sentenceStart, sentenceEnd);*/
						TreeMap<String, String> allMedicalTermSpansOfSentence = outputSpansFromSentenceCLEF2013(tokensList, tagsList, spansList, fullText);
						//						System.out.println(" piped output = " +allMedicalTermSpansOfSentence.size());
						for(Entry<String, String> spanStringEntry : allMedicalTermSpansOfSentence.entrySet()) {
							//span-term
							//for single term prune the term
							//							System.out.println(spanStringEntry.getKey()+"\tvalue\t"+spanStringEntry.getValue());
							if(StringUtils.countMatches(StringUtils.strip(spanStringEntry.getKey(),"|"),"||")==1) {

								try {
									String prunedMedicalTerm = ClinicalTokenizer.getPrunedMedicalTerm(spanStringEntry.getValue());
									int outputspanstart = Integer.parseInt(StringUtils.substringBefore(spanStringEntry.getKey(),"||"));
									int outputspanend = Integer.parseInt(StringUtils.substringAfter(
											StringUtils.strip(spanStringEntry.getKey(),"|"),"||"));
									int startIndexOfPrunedWord = StringUtils.indexOf(spanStringEntry.getValue(), prunedMedicalTerm);
									int afterEndIndexOfPrunedWord = spanStringEntry.getValue().length() - (startIndexOfPrunedWord+prunedMedicalTerm.length());

									int startindex1 = (outputspanstart+startIndexOfPrunedWord);
									int endindex1 = (outputspanend-afterEndIndexOfPrunedWord);
									System.err.println("pruned = " + prunedMedicalTerm );
									if(!prunedMedicalTerm.contains("|") && (endindex1-startindex1)>=1) {
										//									System.out.println(startindex1+"--"+endindex1);
										FileUtils.write(pipedOutputFile, corpusFile.getName()+"|"+startindex1+
												"-"+endindex1 +"|"+prunedMedicalTerm + "\n", true);
									} 
								} catch (NumberFormatException ne) {
									ne.printStackTrace();
								}
							} else {
								/*System.out.println("piped = " + spanStringEntry.getKey() + " == " +
										spanStringEntry.getValue());*/

								String[] spanValues = StringUtils.strip(spanStringEntry.getKey().replace("||||", "||"),"|").split("\\|\\|");
								TreeSet<Integer> intValues = new TreeSet<Integer>();
								for (String val : spanValues) {
									intValues.add(Integer.parseInt(val));
								}
								StringBuffer spanValueBuffer = new StringBuffer();

								ArrayList<Integer> intValuesList = new ArrayList<Integer>();
								intValuesList.addAll(intValues);
								for (int l=0; l < intValuesList.size(); l++) {
									if(l%2==0 && l<intValuesList.size())
										spanValueBuffer.append(((l==0)?"":",")+String.valueOf(intValuesList.get(l)));
									else 
										spanValueBuffer.append("-"+String.valueOf((intValuesList.get(l))));
								}
								System.out.println("spanValueBuffer = "+spanValueBuffer.toString());
								FileUtils.write(pipedOutputFile,corpusFile.getName()+ "|"
										+ spanValueBuffer.toString()
										+ "|"+ spanStringEntry.getValue().trim() + "\n",true);
							}
						}
					}
					tokensList = new ArrayList<String>();
					tagsList = new ArrayList<String>();
					spansList = new ArrayList<Span>();
				} else if (StringUtils.countMatches(line, "\t") == noOfFeatures) {
					String[] split = line.split("\t");
					tagsList.add(split[noOfFeatures]);
					tokensList.add(split[0]);

					//no of spaces
					int noOfSpaces = StringUtils.indexOf(fileText, split[0]);
					//space + length of string

					//at medical term start
					currentPos += noOfSpaces;
					spanstart = currentPos;
					//medicalterm = split[0];
					currentPos += split[0].length();
					spanend = currentPos;

					Span span2 = new Span(spanstart, spanend);
					spansList.add(span2);
					//get remaining sentence from the end of the string
					fileText = StringUtils.substring(fileText, (split[0].length()+ noOfSpaces));
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	TreeMap<String, String> outputSpansFromSentenceCLEF2013 (List<String> tokensList, List<String> tagsList, List<Span> spansList, String fileText) {

		//		System.out.println("in output" + tokensList.size());
		TreeMap<String, String> outputSpanTermMap = new TreeMap<String, String>();
		String medicalterm = "";
		String spanString = "";
		int spanStart = 0, spanEnd = 0;
		//		int sentenceStart = spansList.get(0).getStart();
		//		int sentenceEnd = spansList.get(spansList.size()-1).getEnd();
		/*System.out.println("sentenceStart " + sentenceStart);
		System.out.println("sentenceEnd " + sentenceEnd);
		System.out.println("tagsList = " + tagsList);*/
		//single term
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals("B-M")) {
				if(!medicalterm.isEmpty()) {

					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);

						//if medical term contains '/'
						if(medicalterm.contains("/")) {
							System.out.println("In / " + spanStart +"-"+ spanEnd);
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}

							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();

							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								//System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;
									String spanStringShared = String.valueOf(spanStart)+"||"+String.valueOf(x)
											+"||"+String.valueOf(y)+"||"+String.valueOf(spanEnd);
									System.out.println("spanStringShared1 = "+ spanStringShared);
									outputSpanTermMap.put(spanStringShared, StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									String spanStringShared = String.valueOf(spanStart)+"||"+String.valueOf(x)
											+"||"+String.valueOf(y)+"||"+String.valueOf(spanEnd);
									System.out.println("spanStringShared2 = "+ spanStringShared);
									outputSpanTermMap.put(spanStringShared, StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;
									String spanStringShared = String.valueOf(spanStart)+"||"+String.valueOf(x)
											+"||"+String.valueOf(y)+"||"+String.valueOf(z)
											+"||"+String.valueOf(w)+"||"+String.valueOf(spanEnd);
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);
									System.out.println("spanStringShared3 = "+ spanStringShared);
									outputSpanTermMap.put(spanStringShared, sharedtermfinal);
								}

								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;

							}

						} else {
							outputSpanTermMap.put(spanString, medicalterm);
						}
						medicalterm="";
					}
				}


				medicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
				//				System.out.println("in B-M = " + spanStart);
			} else if (tagsList.get(i).equals("I-M")) {
				if (medicalterm.isEmpty()) {
					spanStart = spansList.get(i).getStart();
				}
				medicalterm = medicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();

			} else if(tagsList.get(i).equals("O")) {

				if(!medicalterm.isEmpty()) {

					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {


						//if medical term contains '/'
						if(medicalterm.contains("/")) {
							//System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}

							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();

							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								//System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}

									//									System.out.println("spanStringShared 0 = "+ spanStringShared.toString());
									outputSpanTermMap.put(ClinicalTokenizer.trimSpanString(spanStringShared.toString()), (StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd)).trim());
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 

									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									//									System.out.println("spanStringShared l = "+ spanStringShared.toString());
									outputSpanTermMap.put(ClinicalTokenizer.trimSpanString(spanStringShared.toString()), (StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd)).trim());
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(z)+"||");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"||"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);

									//									System.out.println("spanStringShared f = "+ spanStringShared);
									outputSpanTermMap.put(ClinicalTokenizer.trimSpanString(spanStringShared.toString()), sharedtermfinal.trim());
								}

								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;

							}

						} else {
							spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
							outputSpanTermMap.put(spanString, medicalterm);
						}
						medicalterm="";
					}
				}
			}

			if(i==tokensList.size()-1) {

				if(!medicalterm.isEmpty()) {
					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
						outputSpanTermMap.put(spanString, medicalterm);
						medicalterm="";
					}
				}
			}
		}

		List<String> disjointFirstTerms = new ArrayList<String>();
		List<String> disjointFirstspanstring = new ArrayList<String>();
		List<String> disjointSecondTerms = new ArrayList<String>();
		List<String> disjointSecondspanstring = new ArrayList<String>();
		medicalterm = "";
		spanString = "";
		spanStart = 0;
		spanEnd = 0;
		//disjoint double term first span
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals("DB-M")) {
				if(!medicalterm.isEmpty()) {

					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
						disjointFirstspanstring.add(spanString);
						disjointFirstTerms.add(medicalterm);
						medicalterm="";
					}
				}
				medicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals("DI-M")) {
				if (medicalterm.isEmpty()) {
					spanStart = spansList.get(i).getStart();
				}
				medicalterm = medicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("O")) {
				if(!medicalterm.isEmpty()) {
					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {

						if(medicalterm.contains("/")) {
							//							System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}

							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();

							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}


									disjointFirstspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 

									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									disjointFirstspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(z)+"||");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"||"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);

									disjointFirstspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(sharedtermfinal);
								}

								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;

							}
						} else {
							spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
							disjointFirstspanstring.add(spanString);
							disjointFirstTerms.add(medicalterm);
						}


						medicalterm="";
					}
				}
			}
		}

		//disjoint double term second spans
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals("DHB-M")) {
				if(!medicalterm.isEmpty()) {

					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
						disjointSecondspanstring.add(spanString);
						disjointSecondTerms.add(medicalterm);
						medicalterm="";
					}
				}
				medicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals("DHI-M")) {
				if (medicalterm.isEmpty()) {
					spanStart = spansList.get(i).getStart();
				}
				medicalterm = medicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("O")) {
				if(!medicalterm.isEmpty()) {
					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {

						if(medicalterm.contains("/")) {
							//							System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}

							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();

							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}


									disjointSecondspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 

									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									disjointSecondspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(z)+"||");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"||"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);

									disjointSecondspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(sharedtermfinal);
								}

								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;

							}
						} else {
							spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
							disjointSecondspanstring.add(spanString);
							disjointSecondTerms.add(medicalterm);
						}


						medicalterm="";
					}
				}
			}
		}



		//get all combinations of disjoint terms
		for(int i=0;i< disjointFirstspanstring.size();i++) {
			for(int j=0;j< disjointSecondspanstring.size();j++) {
				outputSpanTermMap.put(disjointFirstspanstring.get(i) +"||"+disjointSecondspanstring.get(j),
						disjointFirstTerms.get(i)+" "+disjointSecondTerms.get(j));
			}
		}


		medicalterm = "";
		String shortmedicalterm = "";
		spanString = "";
		spanStart = 0;
		spanEnd = 0;
		//triplet terms span
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals("TB-M")) {
				if(!shortmedicalterm.isEmpty()) {
					spanString = spanString +"||"+String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
				}
				medicalterm = tokensList.get(i);
				shortmedicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals("TI-M")) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				shortmedicalterm = shortmedicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("THB-M")) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				shortmedicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals("THI-M")) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				shortmedicalterm = shortmedicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("O")) {
				if(!shortmedicalterm.isEmpty()) {
					if(spanString.isEmpty())
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
					else 
						spanString = spanString +"||"+String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
					//spanString = "";
				}
				shortmedicalterm = "";
			}
		}

		if(!spanString.isEmpty()) {
			outputSpanTermMap.put(spanString, medicalterm);
		}

		return outputSpanTermMap;

	}

	TreeMap<String, String> outputSpansFromSentence (List<String> tokensList, List<String> tagsList, List<Span> spansList, String fileText) {

		//		System.out.println("in output" + tokensList.size());
		TreeMap<String, String> outputSpanTermMap = new TreeMap<String, String>();
		String medicalterm = "";
		String spanString = "";
		int spanStart = 0, spanEnd = 0;
		//		int sentenceStart = spansList.get(0).getStart();
		//		int sentenceEnd = spansList.get(spansList.size()-1).getEnd();
		/*System.out.println("sentenceStart " + sentenceStart);
		System.out.println("sentenceEnd " + sentenceEnd);
		System.out.println("tagsList = " + tagsList);*/
		//single term
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals("B-M")) {
				if(!medicalterm.isEmpty()) {

					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "-" + String.valueOf(spanEnd);

						//if medical term contains '/'
						if(medicalterm.contains("/")) {
							System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}

							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();

							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								//System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;
									String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(spanEnd);
									outputSpanTermMap.put(spanStringShared, StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(spanEnd);
									outputSpanTermMap.put(spanStringShared, StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;
									String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);
									outputSpanTermMap.put(spanStringShared, sharedtermfinal);
								}

								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;

							}



						} else {
							outputSpanTermMap.put(spanString, medicalterm);
						}
						medicalterm="";
					}
				}


				medicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
				//				System.out.println("in B-M = " + spanStart);
			} else if (tagsList.get(i).equals("I-M")) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("O")) {

				if(!medicalterm.isEmpty()) {

					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {


						//if medical term contains '/'
						if(medicalterm.contains("/")) {
							System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}

							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();

							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								//System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"-"+String.valueOf(x)+",");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"-"+String.valueOf(spanEnd));
									}


									outputSpanTermMap.put(ClinicalTokenizer.trimSpanString(spanStringShared.toString()), StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 

									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"-"+String.valueOf(x)+",");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"-"+String.valueOf(spanEnd));
									}
									outputSpanTermMap.put(ClinicalTokenizer.trimSpanString(spanStringShared.toString()), StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"-"+String.valueOf(x)+",");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"-"+String.valueOf(z)+",");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"-"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);
									outputSpanTermMap.put(ClinicalTokenizer.trimSpanString(spanStringShared.toString()), sharedtermfinal);
								}

								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;

							}

						} else {
							spanString = String.valueOf(spanStart) + "-" + String.valueOf(spanEnd);
							outputSpanTermMap.put(spanString, medicalterm);
						}
						medicalterm="";
					}
				}
			}

			if(i==tokensList.size()-1) {

				if(!medicalterm.isEmpty()) {
					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "-" + String.valueOf(spanEnd);
						outputSpanTermMap.put(spanString, medicalterm);
						medicalterm="";
					}
				}
			}
		}

		List<String> disjointFirstTerms = new ArrayList<String>();
		List<String> disjointFirstspanstring = new ArrayList<String>();
		List<String> disjointSecondTerms = new ArrayList<String>();
		List<String> disjointSecondspanstring = new ArrayList<String>();
		medicalterm = "";
		spanString = "";
		spanStart = 0;
		spanEnd = 0;
		//disjoint double term first span
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals("DB-M")) {
				if(!medicalterm.isEmpty()) {

					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "-" + String.valueOf(spanEnd);
						disjointFirstspanstring.add(spanString);
						disjointFirstTerms.add(medicalterm);
						medicalterm="";
					}
				}
				medicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals("DI-M")) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("O")) {
				if(!medicalterm.isEmpty()) {
					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {

						if(medicalterm.contains("/")) {
							//							System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}

							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();

							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								//System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"-"+String.valueOf(x)+",");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"-"+String.valueOf(spanEnd));
									}


									disjointFirstspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 

									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"-"+String.valueOf(x)+",");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"-"+String.valueOf(spanEnd));
									}
									disjointFirstspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"-"+String.valueOf(x)+",");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"-"+String.valueOf(z)+",");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"-"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);

									disjointFirstspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(sharedtermfinal);
								}

								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;

							}
						} else {
							spanString = String.valueOf(spanStart) + "-" + String.valueOf(spanEnd);
							disjointFirstspanstring.add(spanString);
							disjointFirstTerms.add(medicalterm);
						}


						medicalterm="";
					}
				}
			}
		}

		//disjoint double term second spans
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals("DHB-M")) {
				if(!medicalterm.isEmpty()) {

					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "-" + String.valueOf(spanEnd);
						disjointSecondspanstring.add(spanString);
						disjointSecondTerms.add(medicalterm);
						medicalterm="";
					}
				}
				medicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals("DHI-M")) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("O")) {
				if(!medicalterm.isEmpty()) {
					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {

						if(medicalterm.contains("/")) {
							//							System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}

							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();

							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								//System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"-"+String.valueOf(x)+",");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"-"+String.valueOf(spanEnd));
									}


									disjointSecondspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 

									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"-"+String.valueOf(x)+",");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"-"+String.valueOf(spanEnd));
									}
									disjointSecondspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"-"+String.valueOf(x)+",");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"-"+String.valueOf(z)+",");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"-"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);

									disjointSecondspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(sharedtermfinal);
								}

								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;

							}
						} else {
							spanString = String.valueOf(spanStart) + "-" + String.valueOf(spanEnd);
							disjointSecondspanstring.add(spanString);
							disjointSecondTerms.add(medicalterm);
						}


						medicalterm="";
					}
				}
			}
		}



		//get all combinations of disjoint terms
		for(int i=0;i< disjointFirstspanstring.size();i++) {
			for(int j=0;j< disjointSecondspanstring.size();j++) {
				outputSpanTermMap.put(disjointFirstspanstring.get(i) +","+disjointSecondspanstring.get(j),
						disjointFirstTerms.get(i)+" "+disjointSecondTerms.get(j));
			}
		}


		medicalterm = "";
		String shortmedicalterm = "";
		spanString = "";
		spanStart = 0;
		spanEnd = 0;
		//triplet terms span
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals("TB-M")) {
				if(!shortmedicalterm.isEmpty()) {
					spanString = spanString +","+String.valueOf(spanStart) + "-" + String.valueOf(spanEnd);
				}
				medicalterm = tokensList.get(i);
				shortmedicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals("TI-M")) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				shortmedicalterm = shortmedicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("THB-M")) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				shortmedicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals("THI-M")) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				shortmedicalterm = shortmedicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("O")) {
				if(!shortmedicalterm.isEmpty()) {
					if(spanString.isEmpty())
						spanString = String.valueOf(spanStart) + "-" + String.valueOf(spanEnd);
					else 
						spanString = spanString +","+String.valueOf(spanStart) + "-" + String.valueOf(spanEnd);
					//spanString = "";
				}
				shortmedicalterm = "";
			}
		}

		if(!spanString.isEmpty()) {
			outputSpanTermMap.put(spanString, medicalterm);
		}

		return outputSpanTermMap;

	}

	/**
	 * Creates piped output file from the features file.
	 * @param corpusFile
	 * @param featuresFileWithLabel
	 * @param pipedOutputDir
	 * @param includeUMLS
	 */
	private void buildOwnDictionary(File corpusFile, File featuresFileWithLabel) {
		try {
			String fileText = FileUtils.readFileToString(corpusFile);
			List<String> lines = FileUtils.readLines(featuresFileWithLabel);
			int span = 0, spanstart = 0, spanend = 0;
			String medicalterm = "";
			for (String line : lines) {
				//				System.out.println(line);
				if(StringUtils.startsWith(line, "# ") || line.isEmpty())
					continue;
				if (StringUtils.countMatches(line, "\t") == noOfFeatures) {

					String[] split = line.split("\t");

					if(split[noOfFeatures].equals("B-M")) {

						if(!medicalterm.isEmpty()) {

							if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
								medicalterm = "";
							} else {
								medicalterm = ClinicalTokenizer.getPrunedMedicalTerm(medicalterm);
								if(!medicalterm.isEmpty())
									Word2Index.getInstance().hitachiTrainDictTermOwnDict.add(medicalterm);
								medicalterm="";
							}
						}

						//no of spaces
						int noOfSpaces = StringUtils.indexOf(fileText, split[0]);
						//space + length of string

						//at medical term start
						span += noOfSpaces;
						spanstart = span;

						medicalterm = split[0];
						span += split[0].length();
						spanend = span;
						//get remaining sentence from the end of the string
						fileText = StringUtils.substring(fileText, (split[0].length()+ noOfSpaces));

					} else if (split[noOfFeatures].equals("I-M")) {
						int noOfSpaces = StringUtils.indexOf(fileText, split[0]);
						//at medical term start
						span += noOfSpaces;

						medicalterm = medicalterm + " " + split[0];
						span += split[0].length();
						spanend = span;
						//get remaining sentence from the end of the string
						fileText = StringUtils.substring(fileText, (split[0].length()+ noOfSpaces));
					} else if(split[noOfFeatures].equals("O")) {

						if(!medicalterm.isEmpty()) {

							if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
								medicalterm = "";
							} else {
								medicalterm = ClinicalTokenizer.getPrunedMedicalTerm(medicalterm);
								if(!medicalterm.isEmpty())
									Word2Index.getInstance().hitachiTrainDictTermOwnDict.add(medicalterm);
								medicalterm="";
							}

						}

						//System.out.println(line);
						//position from start of the fileText. (space*)text. It just calculates number of spaces. 
						int noOfSpaces = StringUtils.indexOf(fileText, split[0]);
						//space + length of string
						span += split[0].length()+ noOfSpaces;
						//get remaining sentence from the end of the string
						fileText = StringUtils.substring(fileText, (split[0].length()+ noOfSpaces));

					}

				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}


	}


	/**
	 * Creates the piped output files from the features file.
	 * @param corpusFile
	 * @param featuresFileWithLabel
	 * @param pipedOutputDir
	 * @param includeUMLS
	 */
	private void createPipeFileFromFeaturesFileWithPostProcessing(File corpusFile, File featuresFileWithLabel,
			String pipedOutputDir, boolean includeUMLS) {
		try {
			String fileText = FileUtils.readFileToString(corpusFile);
			String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize(fileText);
			TreeMap<Integer, Integer> spanToTokenNumberMap = pipedOutputProcessor.getStartSpansForTokens(corpusFile, tokens);
			File pipedOutputFile = new File(pipedOutputDir + "/" + corpusFile.getName().replace(".txt", ".pipe.txt"));
			FileUtils.deleteQuietly(pipedOutputFile);
			List<String> lines = FileUtils.readLines(featuresFileWithLabel);
			int span = 0, spanstart = 0, spanend = 0;
			String medicalterm = "";
			for (String line : lines) {
				//				System.out.println();
				//				System.out.println(line);
				if(StringUtils.startsWith(line, "# ") || line.isEmpty())
					continue;
				if (StringUtils.countMatches(line, "\t") == noOfFeatures) {

					String[] split = line.split("\t");

					if(split[noOfFeatures].equals("B-M")) {

						if(!medicalterm.isEmpty()) {

							if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
								medicalterm = "";
							} else {

								String prunedMedicalTerm = ClinicalTokenizer.getPrunedMedicalTerm(medicalterm);
								int startIndexOfPrunedWord = StringUtils.indexOf(medicalterm, prunedMedicalTerm);

								//add -1 
								if(!prunedMedicalTerm.contains("|")) {
									/*FileUtils.write(pipedOutputFile, corpusFile.getName()+"|"+(spanstart+startIndexOfPrunedWord)+
										"-"+(spanstart+startIndexOfPrunedWord+prunedMedicalTerm.length()-1) +"|"+prunedMedicalTerm + "\n", true);*/
									FileUtils.write(pipedOutputFile, corpusFile.getName()+"|"+(spanstart+startIndexOfPrunedWord)+
											"-"+(spanstart+startIndexOfPrunedWord+prunedMedicalTerm.length()) +"|"+prunedMedicalTerm + "\n", true);
								}


								List<String> finalmedicalTerms = pipedOutputProcessor.extendMedicalTermBothSides(spanstart, spanend,
										medicalterm, tokens, spanToTokenNumberMap);
								//								System.out.println("Predicted = " + prunedMedicalTerm+" \tFinal = " + finalmedicalTerms);
								//								System.out.println("Final = " + finalmedicalTerms);
								for (String t : finalmedicalTerms) {

									if(!StringUtils.substringAfterLast(t,"|").equals(medicalterm)){
										//										FileUtils.write(pipedOutputFile, corpusFile.getName()+"|"+ t + "\n", true);
										medicalterm = StringUtils.substringAfterLast(t,"|");
										prunedMedicalTerm = ClinicalTokenizer.getPrunedMedicalTerm(medicalterm);
										startIndexOfPrunedWord = StringUtils.indexOf(medicalterm, prunedMedicalTerm);
										int spanstartNew = 0; 
										try {
											spanstartNew = Integer.parseInt(StringUtils.substringBefore(t,"-"));
										} catch(Exception e) {
											System.out.println("Error : " + StringUtils.substringBefore(t,"-"));
										}
										if(!prunedMedicalTerm.contains("|") && spanstartNew != 0) {
											/*FileUtils.write(pipedOutputFile, corpusFile.getName()+"|"+(spanstartNew+startIndexOfPrunedWord)+
												"-"+(spanstartNew+startIndexOfPrunedWord+prunedMedicalTerm.length()-1) +"|"+prunedMedicalTerm + "\n", true);*/
											FileUtils.write(pipedOutputFile, corpusFile.getName()+"|"+(spanstartNew+startIndexOfPrunedWord)+
													"-"+(spanstartNew+startIndexOfPrunedWord+prunedMedicalTerm.length()) +"|"+prunedMedicalTerm + "\n", true);
										}

									}

								}

								medicalterm="";
							}
						}

						//no of spaces
						int noOfSpaces = StringUtils.indexOf(fileText, split[0]);
						//space + length of string

						//at medical term start
						span += noOfSpaces;
						spanstart = span;

						medicalterm = split[0];
						span += split[0].length();
						spanend = span;

						//						System.out.println("spanstart  = " + spanstart +" = "+ medicalterm);
						//get remaining sentence from the end of the string
						fileText = StringUtils.substring(fileText, (split[0].length()+ noOfSpaces));

					} else if (split[noOfFeatures].equals("I-M")) {
						int noOfSpaces = StringUtils.indexOf(fileText, split[0]);
						//at medical term start
						span += noOfSpaces;

						//1. Asthma. 2. Hypertension.
						if(split[0].equals(".")) {
							medicalterm = medicalterm + split[0];
						} else {
							medicalterm = medicalterm + " " + split[0];
						}
						span += split[0].length();
						spanend = span;
						//get remaining sentence from the end of the string
						fileText = StringUtils.substring(fileText, (split[0].length()+ noOfSpaces));
					} else if(split[noOfFeatures].equals("O")) {

						if(!medicalterm.isEmpty()) {

							if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
								medicalterm = "";
							} else {
								//pruning the medical term starting and ending
								String prunedMedicalTerm = ClinicalTokenizer.getPrunedMedicalTerm(medicalterm);
								int startIndexOfPrunedWord = StringUtils.indexOf(medicalterm, prunedMedicalTerm);
								//								System.out.println("Medical Term = " + spanstart + "|" + medicalterm);
								//								System.out.println("Pruned Medical Term = " + (spanstart+startIndexOfPrunedWord) + "|" + prunedMedicalTerm);

								List<String> finalmedicalTerms = pipedOutputProcessor.extendMedicalTermBothSides(spanstart, spanend,
										medicalterm, tokens, spanToTokenNumberMap);
								if(!prunedMedicalTerm.contains("|")) {
									/*FileUtils.write(pipedOutputFile, corpusFile.getName()+"|"+(spanstart+startIndexOfPrunedWord)+
										"-"+(spanstart+startIndexOfPrunedWord+prunedMedicalTerm.length()-1) +"|"+prunedMedicalTerm +"\n", true);*/
									FileUtils.write(pipedOutputFile, corpusFile.getName()+"|"+(spanstart+startIndexOfPrunedWord)+
											"-"+(spanstart+startIndexOfPrunedWord+prunedMedicalTerm.length()) +"|"+prunedMedicalTerm +"\n", true);
								}

								System.out.print("Predicted = " + (spanstart+startIndexOfPrunedWord)+
										"-"+(spanstart+startIndexOfPrunedWord+prunedMedicalTerm.length()) +"|"+prunedMedicalTerm);
								System.out.println("\tFinal = " + finalmedicalTerms);
								for (String t : finalmedicalTerms) {
									if(!StringUtils.substringAfterLast(t,"|").equals(medicalterm)){
										//										FileUtils.write(pipedOutputFile, corpusFile.getName()+"|"+ t + "\n", true);
										String medicaltermNew = StringUtils.substringAfterLast(t,"|");
										String prunedMedicalTermNew = ClinicalTokenizer.getPrunedMedicalTerm(medicaltermNew);
										startIndexOfPrunedWord = StringUtils.indexOf(medicaltermNew, prunedMedicalTermNew);
										int spanstartNew = 0; 
										try {
											spanstartNew = Integer.parseInt(StringUtils.substringBefore(t,"-"));
										} catch(Exception e) {
											System.out.println("Error : " + StringUtils.substringBefore(t,"-"));
										}
										if(!prunedMedicalTermNew.contains("|") && spanstartNew != 0) {
											if(!prunedMedicalTermNew.equals(prunedMedicalTerm)){
												/*FileUtils.write(pipedOutputFile, corpusFile.getName()+"|"+(spanstartNew+startIndexOfPrunedWord)+
														"-"+(spanstartNew+startIndexOfPrunedWord+prunedMedicalTermNew.length()-1) +"|"+prunedMedicalTermNew + "\n", true);*/
												FileUtils.write(pipedOutputFile, corpusFile.getName()+"|"+(spanstartNew+startIndexOfPrunedWord)+
														"-"+(spanstartNew+startIndexOfPrunedWord+prunedMedicalTermNew.length()) +"|"+prunedMedicalTermNew + "\n", true);
												System.out.println("final pruned = "+ (spanstartNew+startIndexOfPrunedWord)+
														"-"+(spanstartNew+startIndexOfPrunedWord+prunedMedicalTermNew.length()-1) +"|"+prunedMedicalTermNew);
											}
										}

									}
								}

								medicalterm="";
							}

						}

						//System.out.println(line);
						//position from start of the fileText. (space*)text. It just calculates number of spaces. 
						int noOfSpaces = StringUtils.indexOf(fileText, split[0]);
						//space + length of string
						span += split[0].length()+ noOfSpaces;
						//get remaining sentence from the end of the string
						fileText = StringUtils.substring(fileText, (split[0].length()+ noOfSpaces));

					}

				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}


	}



	/**
	 * Returns true if the given token is a measuring unit.
	 * @param unit
	 * @return true/false
	 */
	public boolean isUnit(String unit) {
		List<String> units = new ArrayList<String>();
		units.add("acre");units.add("acres");units.add("hectares");
		units.add("m");units.add("cms");units.add("sq mi"); units.add("mi");units.add("m2");units.add("km2");
		units.add("cm");units.add("ft");units.add("footnotes");units.add("footnote");units.add("feet");
		units.add("in");units.add("ins");units.add("inch");units.add("inches");units.add("miles");
		units.add("minutes");units.add("minute");units.add("seconds");units.add("second");
		units.add("hour");units.add("days");units.add("years");
		units.add("km");
		if(units.contains(unit.trim()))
			return true;
		return false;
	}

	/**
	 * Generate features for the test File.
	 * @param corpusFile
	 * @param featuresFile
	 * @return
	 */
	File generateTestFeaturesFile (File corpusFile, File featuresFile) {
		FileUtils.deleteQuietly(featuresFile);

		try {

			List<String> lines = FileUtils.readLines(corpusFile); 
			String sectionHeader = "";
			for (int i =0; i < lines.size(); i++) {

				String sectionHeaderSubstring = StringUtils.substringBefore(lines.get(i), ":");
				if (Word2Index.getInstance().sectionHeadersTop.contains(sectionHeaderSubstring.toLowerCase())) {
					sectionHeader = sectionHeaderSubstring.replace(" ", "_");
				} else {
					sectionHeader = "X";
				}

				String[] sentences = SentenceBreaker.getSentenceBreaker().getSentences(lines.get(i));
				for (int j = 0; j < sentences.length; j++) {
					//System.out.println("sentence = " + sentence);
					getCRFNLPFeaturesForSentence(sentences[j], featuresFile, includeUMLS, sectionHeader, corpusFile.getName());
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return featuresFile;
	}

	/**
	 * CRF model tags class label for each line of the features file.
	 * Returns the features file with class labels tagged.
	 * @param featuresFile
	 * @return featuresFile With labels
	 */
	File getLabelsForFeaturesFile(File featuresFile, String crfmodel)  {


		String inputFile = featuresFile.getAbsolutePath();
		String outputFile = featuresFile.getAbsolutePath()+"_WithLabel";
		System.out.println(inputFile +"\n"+ outputFile);
		//Scanner scan = new Scanner(System.in);
		Process p;
		try {

			ProcessBuilder builder = new ProcessBuilder("crf_test", "-m", crfmodel , inputFile);
			builder.redirectOutput(new File(outputFile));
			builder.redirectError(new File(outputFile));
			p = builder.start();

			p.waitFor(); 
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		} 

		/*try {
			System.out.println(FileUtils.readFileToString(new File(outputFile)));
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		return new File(outputFile);
	}


	/**
	 * Extracts features for all tokens of the sentence.
	 * The each line in the output features file corresponds to features of each token.
	 * @param sentence
	 * @param featuresFile
	 * @param includeUMLS
	 * @return
	 * @throws IOException
	 */
	File getCRFNLPFeaturesForSentence (String sentence, File featuresFile, boolean includeUMLS, String sectionHeader,
			String docname) throws IOException {

		String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize(sentence);
		String[] tags = POSTagger.getPOSTagger().tag(tokens);
		String[] chunks = Chunker.getChunker().getChunks(tokens, tags);

		//System.out.println("Sentence == " + sentence);
		for (int k =0 ; k < tokens.length; k++) {
			//nlp features
			FileUtils.write(featuresFile, tokens[k]+
					"\t"+ WordStemmer.getStemmer().stem(tokens[k]) +
					"\t"+ tags[k] +
					"\t"+ chunks[k] +
					"\t"+ sectionHeader+
					"\t"+ getDocumentType(docname), true);

			//orthographic features
			FileUtils.write(featuresFile, getTokenFeatures(tokens[k],tokens, k, includeUMLS), true);
			FileUtils.write(featuresFile,"\n", true);
		}
		FileUtils.write(featuresFile,"\n", true);
		//System.out.println();
		return featuresFile;
	}

	/**
	 *  Post processing the SVM model piped outputs.
	 * Outputs all occurrences of medical terms in the discharge summary into piped output.
	 * @param featuresFile
	 * @param featuresFileWithLabel
	 * @return
	 */
	public File postProcessingLabels(File featuresFile,
			File featuresFileWithLabel) {

		File labelsPostProcessed = new File(featuresFile.getAbsolutePath()+"_pp");
		ArrayList<String> postProcessedLines = new ArrayList<String>();
		/*try {
			List<String> featuresLines = FileUtils.readLines(featuresFileWithLabel);
			HashSet<String> medicalTermsBegin = new HashSet<String>();
			HashSet<String> medicalTermsIntermediate = new HashSet<String>();

			String medicalterm = "";
			// Preprocess - 1
			//get all the tokens that are tagged as medical terms
			for (String line : featuresLines) {

//				System.out.println(line);
				if(StringUtils.startsWith(line, "# ") || line.isEmpty())
					continue;
				if (StringUtils.countMatches(line, "\t") == noOfFeatures) {

					String[] split = line.split("\t");
					if(split[noOfFeatures].equals("B-M")) {
						medicalterm = split[0];
						medicalTermsBegin.add(medicalterm.toLowerCase());
//						System.err.println("add begin" + medicalterm);
					} else if (split[noOfFeatures].equals("I-M") || 
							Word2Index.getInstance().hitachiTrainDict.contains(split[0].toLowerCase())) {
//						System.err.println(medicalterm);
						medicalterm = split[0];
						medicalTermsIntermediate.add(medicalterm.toLowerCase());
					} 
				}

			}
//			System.out.println("medical terms size = " + medicalTerms.size());
			for (String line : featuresLines) {

				String[] split = line.split("\t");
//				System.err.println(split.length);
				if(split.length == noOfFeatures+1) {
					if (medicalTermsBegin.contains(split[0].toLowerCase())) {
//						System.err.println("in b-m"+split[0]);
						postProcessedLines.add(line.replaceAll("O$", "B-M"));
					} else if (medicalTermsIntermediate.contains(split[0].toLowerCase())) {
						postProcessedLines.add(line.replaceAll("O$", "I-M"));
					} else {
						postProcessedLines.add(line);
					}
				} else {
					postProcessedLines.add(line);
				} 

			}

			FileUtils.writeLines(labelsPostProcessed, postProcessedLines);

		} catch (IOException e) {
			e.printStackTrace();
		}*/

		try {
			List<String> featuresLines = FileUtils.readLines(featuresFileWithLabel);

			String medicalterm = "";
			//			System.out.println("medical terms size = " + medicalTerms.size());
			for (int k =0; k< featuresLines.size(); k++) {

				String[] split = featuresLines.get(k).split("\t");
				//				System.err.println(split.length);
				if(split.length == noOfFeatures+1 && StringUtils.endsWith(featuresLines.get(k),"O")) {

					medicalterm = split[0];
					boolean flag = false;

					if (medicalterm.length()>2 || StringUtils.isAllUpperCase(medicalterm) ) {
						StringBuffer sb = new StringBuffer();
						if (k-3>=0) {
							String[] tempsplit = featuresLines.get(k-3).split("\t");
							if(tempsplit.length == noOfFeatures+1) { 
								sb.append(tempsplit[0] + " ");
							}
						}
						if (k-2>=0) {
							String[] tempsplit = featuresLines.get(k-2).split("\t");
							if(tempsplit.length == noOfFeatures+1) { 
								sb.append(tempsplit[0] + " ");
							}
						}
						if (k-1>=0) {
							String[] tempsplit = featuresLines.get(k-1).split("\t");
							if(tempsplit.length == noOfFeatures+1) { 
								sb.append(tempsplit[0] + " ");
							}
						}
						if (k>=0) {
							String[] tempsplit = featuresLines.get(k).split("\t");
							if(tempsplit.length == noOfFeatures+1) { 
								sb.append(tempsplit[0] + " ");
							}
						}

						if (k+1<=featuresLines.size()-1) {
							String[] tempsplit = featuresLines.get(k+1).split("\t");
							if(tempsplit.length == noOfFeatures+1) { 
								sb.append(tempsplit[0] + " ");
							}
						}
						if (k+2<=featuresLines.size()-1) {
							String[] tempsplit = featuresLines.get(k+2).split("\t");
							if(tempsplit.length == noOfFeatures+1) { 
								sb.append(tempsplit[0] + " ");
							}
						}
						if (k+3<=featuresLines.size()-1) {
							String[] tempsplit = featuresLines.get(k+3).split("\t");
							if(tempsplit.length == noOfFeatures+1) { 
								sb.append(tempsplit[0] + " ");
							}
						}

						List<String> bigrams = ClinicalTokenizer.getNGrams(sb.toString(), 2); 
						for (String bigram : bigrams) {
							if (bigram.contains(medicalterm)) {
								if (Word2Index.getInstance().hitachiTrainDictTermOwnDict.contains(bigram) 
										|| java2MySql.isDiseaseDisorderSemanticTypeTerm(bigram)) {
									postProcessedLines.add(featuresLines.get(k).replaceAll("O$", "I-M"));
									flag = true;
								}
							}
						}

						if (!flag) {
							List<String> trigrams = ClinicalTokenizer.getNGrams(sb.toString(), 3); 
							for (String trigram : trigrams) {
								if (trigram.contains(medicalterm)) {
									if (Word2Index.getInstance().hitachiTrainDictTermOwnDict.contains(trigram) 
											|| java2MySql.isDiseaseDisorderSemanticTypeTerm(trigram) ) {
										postProcessedLines.add(featuresLines.get(k).replaceAll("O$", "I-M"));
										flag = true;
									}
								}
							}
						}

						if (!flag) {
							List<String> trigrams = ClinicalTokenizer.getNGrams(sb.toString(), 4); 
							for (String trigram : trigrams) {
								if (trigram.contains(medicalterm)) {
									if (Word2Index.getInstance().hitachiTrainDictTermOwnDict.contains(trigram) 
											|| java2MySql.isDiseaseDisorderSemanticTypeTerm(trigram)) {
										postProcessedLines.add(featuresLines.get(k).replaceAll("O$", "I-M"));
										flag = true;
									}
								}
							}
						}

						if (!flag) {
							if (Word2Index.getInstance().hitachiTrainDictTermOwnDict.contains(medicalterm) 
									|| java2MySql.isDiseaseDisorderSemanticTypeTerm(medicalterm)) {
								postProcessedLines.add(featuresLines.get(k).replaceAll("O$", "B-M"));
								flag = true;
							}
						}
						//						System.out.println(sb.toString()+"\t"+medicalterm +"\t"+ flag);

					}


					if (!flag) {
						postProcessedLines.add(featuresLines.get(k));
					}


				} else {
					postProcessedLines.add(featuresLines.get(k));
				} 

			}

			FileUtils.writeLines(labelsPostProcessed, postProcessedLines);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return labelsPostProcessed;
	}

	//	comparePipedOutputFiles()

	/**
	 * Generated the all feature files from corpus and piped output.
	 * @param corpusDir
	 * @param pipedDir
	 * @param deepNlOutputDir
	 * @throws IOException
	 */
	public void generateTrainFeatureFileForDeepnl (String corpusDir, String pipedDir, String deepNlOutputDir) throws IOException {

		new File(deepNlOutputDir).mkdir();
		File[] corpusFiles = new File(corpusDir).listFiles();
		for(File corpusFile : corpusFiles) {

			//piped file
			File pipeFile = new File(pipedDir + "/" + corpusFile.getName());
			if (corpusFile.getName().contains(".txt")) {
				//train 
				// comment for test
				pipeFile = new File(pipedDir + "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));
			}
			if(!pipeFile.exists()) {
				System.out.println("No pipe file found = " + pipeFile);
				continue;
			}

			//features file
			String featuresFileString = deepNlOutputDir + File.separator +corpusFile.getName();
			File featuresFile = new File(featuresFileString);
			FileUtils.deleteQuietly(featuresFile);
			generateTrainFeaturesFile(corpusFile, pipeFile, featuresFile);
			//Only for train features
			pruneFeaturesFileForRecall (deepNlOutputDir, featuresFile);
		}
	}
	
	void pruneFeaturesFileForRecall(String deepNlOutputDir, File featuresFile) {
		new File(deepNlOutputDir+"prunedForRecall").mkdir();
		try {
			List<String> featureFileLines = FileUtils.readLines(featuresFile);
		
			File prunedOutputFile = new File (deepNlOutputDir+"prunedForRecall" + File.separator 
					+ featuresFile.getName());
			FileUtils.deleteQuietly(prunedOutputFile);
			List<String> linesOfSentence = new ArrayList<>();
			int count = (int) (Math.random()*10);
			boolean hasMedicalTerm = false;
			for (String line : featureFileLines) {
				
				linesOfSentence.add(line);
				
				if(line.endsWith("-M")) {
					hasMedicalTerm = true;
				}
				//if line is empty write to the output file based on condition
				if(line.isEmpty()) {
					//if sentence has medical term write
					if(hasMedicalTerm) {
						FileUtils.writeLines(prunedOutputFile, linesOfSentence, true);
						System.out.println("hasmedicalterm");
					} else if (count>=8) 	{ 
						//if count of sentences of not having medical terms is > 0. Then write. 
						//Prob 0.1
						count = 0;
						FileUtils.writeLines(prunedOutputFile, linesOfSentence, true);
						System.out.println("count>10");
					} else {//does not has medical term
						count++;
						System.out.println("count = " + count);
					}
					
					
					linesOfSentence.clear();;
					hasMedicalTerm = false;
				}
				
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * CRF model tags the medical terms in the given document.
	 * Returns the anafora xml of the document.
	 * @param docPath
	 * @param pipedopPath
	 * @param xmlopPath
	 * @param crfmodel
	 */
	void getAnaforaXMLUsingCRF (String docPath, String pipedopPath, String xmlopPath, String crfmodel) {
		
		File corpusFile = new File(docPath);
		File pipedopFile = new File(pipedopPath);
		File xmlopFile = new File(xmlopPath);
		String featuresFileString = "temp.txt_"+corpusFile.getName();
		File featuresFile = new File(featuresFileString);

		//generate Features File - temp.txt. comment if you only process on piped outputs for pp
		generateTestFeaturesFile(corpusFile, featuresFile);
		// Genereate labels for the features file using CRF model - temp.txt.WithLabel
		File featuresFileWithLabel = getLabelsForFeaturesFile(featuresFile, crfmodel);

		createPipeFileFromFeaturesFileWithPPWithBIODTtags(corpusFile, featuresFileWithLabel, pipedopFile);
		
		AnaforaConverter anaforaConverter = new AnaforaConverter();
		anaforaConverter.writeAnaforaXML(pipedopFile, xmlopFile);
	
	}
	

	public static void main (String[] args) throws IOException{
		CRFModelGenerator crfModelGenerator = new CRFModelGenerator();
		long start = System.currentTimeMillis();
		
		
		//Annotation
		//cancer documents
		/*if(args.length==3) {
			//for each disease <train|test> <brain|breast|lung|headAndNeck|prostate>
			//System.out.println("Generate features");
			if(args[0].equals("train")) {
				System.out.println("Training");
				crfModelGenerator.generateFeaturesAndTrainWithUMLS("/home/cde/Pictures/MedicalParser/pubmedclean/"+args[1]+"CancerDocs",
					"/home/cde/Pictures/MedicalParser/pubmedclean/"+args[1]+"CancerDocs_pipedoutput_pma",
					"/home/cde/Pictures/MedicalParser/pubmedclean/model_dump/"+args[1]+"CancerDocs_features.txt",
					"/home/cde/Pictures/MedicalParser/pubmedclean/model_dump/"+args[1]+"CancerDocs_features.model");

			}
			System.out.println("training TIME2 = " + (System.currentTimeMillis()-start)/(1000*60.0));
			start = System.currentTimeMillis();

			//"/home/cde/Pictures/MedicalParser/pubmedclean/"+args[1]+"CancerDocs",
			///home/cde/Hitachi2015/pubmedDataset_phase3/Docs
			if (args[0].equals("test")) {
				System.out.println("Testing");
				new File(args[2]).mkdir();
				//				crfModelGenerator.crfTestBNERWithPP(args[0],
				crfModelGenerator.crfTestBNERWithPP(args[1],
						args[2],
						"/home/cde/Raghavendra/Hitachi2014Eclipse/finaldata/phase9/model_dump/may15_crf_train_features.model_1",
						true);
			}

			System.out.println("Testing TIME1 = " + (System.currentTimeMillis()-start)/(1000*60.0));
			start = System.currentTimeMillis();
			//System.out.println((System.currentTimeMillis()-start)/(1000*60.0));

		}*/
		
		//Pubmed Dataset
		//Training 
		if(args[0].equals("train")) {
			System.out.println("Training");
			crfModelGenerator.generateFeaturesAndTrainWithUMLS("finaldata/pubmedDataset_phase4/train/DOCS",
				"finaldata/pubmedDataset_phase4/train/PIPED",
				"finaldata/pubmedDataset_phase4/model_dump/dec8_train_features.txt",
				"finaldata/pubmedDataset_phase4/model_dump/dec8_train_features.model");

		}
		System.out.println("training TIME2 = " + (System.currentTimeMillis()-start)/(1000*60.0));
		start = System.currentTimeMillis();

		//"/home/cde/Pictures/MedicalParser/pubmedclean/"+args[1]+"CancerDocs",
		///home/cde/Hitachi2015/pubmedDataset_phase3/Docs
		if (args[0].equals("test")) {
			System.out.println("Testing");
			new File("finaldata/pubmedDataset_phase4/test/dec8_PIPED").mkdir();
			crfModelGenerator.crfTestBNERWithPP("finaldata/pubmedDataset_phase4/test/DOCS",
					"finaldata/pubmedDataset_phase4/test/dec8_PIPED",
					"finaldata/pubmedDataset_phase4/model_dump/dec8_train_features.model",
					true);
		}

		System.out.println("Testing TIME1 = " + (System.currentTimeMillis()-start)/(1000*60.0));

	}

}
