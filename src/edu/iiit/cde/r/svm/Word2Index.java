package edu.iiit.cde.r.svm;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import edu.iiit.cde.r.mysql.Java2MySql;
import edu.iiit.cde.r.utils.ClinicalTokenizer;
import edu.iiit.cde.r.utils.HashMapUtilsCustom;
import edu.iiit.cde.r.utils.LRUCacheImpl;
import edu.iiit.cde.r.utils.MetaMapApiUse;
import edu.iiit.cde.r.utils.MetaMapToken;
import edu.iiit.cde.wiki.SearchIndex;
import edu.iiit.nlp.WordStemmer;
import gov.nih.nlm.nls.metamap.Ev;
import gov.nih.nlm.nls.metamap.MetaMapApi;

/**
 * The Word2Index takes a token as input and produces the corresponding index as output. 
 * It first constructs a vocabulary and assigns index for each token. 
 * The resulting word index can be used as features in many natural language processing and machine learning applications.
 * @author raghavendra
 *
 */
public final class Word2Index {
	
	static int wordsSize;
	static int stemsSize;
	static int posSize;
	static int chunksSize;
	static int prefixesSize;
	static int suffixesSize;
	static int semanticTypesSize;
	static int wikiSize;
	static int wikiClassSize;
	static int trainDictSize;
	static int sectionHeadersSize;
	static int docTypesSize;
	static int wordClusterSize;
	
	static int totalfeatures;
	
	static List<String> words;
	static List<String> stems;
	static List<String> pos;
	static List<String> chunks;
	static List<String> prefixes;
	static List<String> suffixes;
	static List<String> semanticTypes;
	public static List<String> wikiWords;
	public static List<String> wikiClasses;
	public static List<String> trainDict;
	public static List<String> hitachiTrainDict;
	public static List<String> hitachiTrainDictTerm;
	public static List<String> hitachiDiseaseTrainDict;
	public static List<String> sectionHeadersTop;
	
	public static HashSet<String> hitachiTrainDictTermOwnDict;
	public static LRUCacheImpl<String, String> hitachiTrainDictTermLRUCache = new LRUCacheImpl<String,String>(10000000, 0.75f);
//	static List<String> sectionHeadersAll;
	static List<String> docTypes;
	public static HashMap<String, Integer> wordClustersMap = new HashMap<String, Integer>();
	
	static HashMap<String, List<String>> word2VectorsMap = new HashMap<String, List<String>>();
	static HashMap<String, String> opennlp_word2VectorsMap = new HashMap<String, String>();
	
	public static Word2Index instance = null;
	ClinicalTokenizer clinicalTokenizer = ClinicalTokenizer.getInstance();
	
	Java2MySql java2MySql; 
	
	/**
	 * Word2Index Constructor
	 */
	public Word2Index() {
		try {
			java2MySql = new Java2MySql();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the instance of Word2Index
	 * @return {@link Word2Index}
	 * @throws IOException
	 */
	public static Word2Index getInstance() throws IOException {
		if(Word2Index.instance == null) {
			Word2Index.instance = new Word2Index();
			init();
		}
		return Word2Index.instance;
	}
	
	/**
	 * Initializes the vocabulary.
	 * @throws IOException
	 */
	static void init() throws IOException {
		words = FileUtils.readLines(new File("./finaldata/word2VecFiles/vocabulary_all.txt"), "utf-8");
		stems = FileUtils.readLines(new File("./finaldata/word2VecFiles/stems_all.txt"), "utf-8");
		pos = FileUtils.readLines(new File("./finaldata/word2VecFiles/posTags.txt"), "utf-8");
		chunks = FileUtils.readLines(new File("./finaldata/word2VecFiles/chunkTags.txt"), "utf-8");
		prefixes = FileUtils.readLines(new File("./finaldata/word2VecFiles/prefixes.txt"), "utf-8");
		suffixes = FileUtils.readLines(new File("./finaldata/word2VecFiles/suffixes.txt"), "utf-8");
		semanticTypes = FileUtils.readLines(new File("./finaldata/word2VecFiles/semanticTypes.txt"), "utf-8");
		wikiWords = FileUtils.readLines(new File("./finaldata/word2VecFiles/wiki.txt"), "utf-8");
		wikiClasses = FileUtils.readLines(new File("./finaldata/word2VecFiles/wikiClass.txt"), "utf-8");
		trainDict = FileUtils.readLines(new File("./finaldata/word2VecFiles/clef201314trainDictFromFeatures.txt"), "utf-8");
		hitachiTrainDict = FileUtils.readLines(new File("./finaldata/word2VecFiles/hitachitrainDictFromFeatures.txt_1"), "utf-8");
		hitachiTrainDictTerm = FileUtils.readLines(new File("./finaldata/word2VecFiles/hitachiTrainDictDiseaseTerms.txt_1"), "utf-8");
		hitachiDiseaseTrainDict = FileUtils.readLines(new File("./finaldata/word2VecFiles/hitachiDiseasetrainDictFromFeatures.txt_1"), "utf-8");
		sectionHeadersTop = FileUtils.readLines(new File("./finaldata/word2VecFiles/sectionHeaders_top.txt"), "utf-8");
		//sectionHeadersAll = FileUtils.readLines(new File("./finaldata/word2VecFiles/sectionHeaders_all.txt"), "utf-8");
		docTypes = loadDocTypes();
		wordClustersMap = loadWordClusters();
		loadWord2Vectors();
		buildOwnDictionary();
		
		wordsSize = words.size()+1;
		stemsSize = stems.size()+1;
		posSize = pos.size()+1;
		chunksSize = chunks.size()+1;
		prefixesSize = prefixes.size()+1;
		suffixesSize = suffixes.size()+1;
		semanticTypesSize = semanticTypes.size()+1;
		wikiSize = wikiWords.size()+1;
		wikiClassSize = wikiClasses.size()+1;
		trainDictSize = trainDict.size()+1;
		sectionHeadersSize = sectionHeadersTop.size()+1;
		docTypesSize = docTypes.size()+1;
		wordClusterSize = 1000;
		
		totalfeatures = wordsSize + stemsSize + posSize + chunksSize + prefixesSize + suffixesSize + semanticTypesSize +
				wikiSize + wikiClassSize + trainDictSize + sectionHeadersSize+docTypesSize+wordClusterSize;
		
	}

	private static void buildOwnDictionary() {
//		hitachiTrainDictTermLRUCache.p
		hitachiTrainDictTermOwnDict = new HashSet<String>(hitachiTrainDictTerm);
	}
	
	public static HashMap<String, Integer> loadWordClusters() {
		try {
			List<String> lines = FileUtils.readLines(new File("./finaldata/word2VecFiles/clef_classes_cluster.sorted.txt"), "utf-8");
			int temp = 0;
			for (String line : lines) {
				String[] split = line.split(" ");
				if(split.length == 2) {
					try {
						int clusterNo = Integer.parseInt(split[1]);
						wordClustersMap.put(split[0], clusterNo);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return wordClustersMap;
	}
	
	public static HashMap<String, List<String>> loadWord2Vectors() {
		
		if (word2VectorsMap.isEmpty()) { 
			try {
				List<String> lines = FileUtils.readLines(new File("./finaldata/word2VecFiles/clef_w2v_vocab.txt"), "utf-8");
				List<String> vectors = FileUtils.readLines(new File("./finaldata/word2VecFiles/clef_w2v_vectors.txt"), "utf-8");
				for (int i = 0 ; i < lines.size(); i++) {
					word2VectorsMap.put(lines.get(i).toLowerCase(), Arrays.asList(vectors.get(i).trim().split(" ")));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (opennlp_word2VectorsMap.isEmpty()) { 
			try {
				List<String> lines = FileUtils.readLines(new File("./finaldata/word2VecFiles/opennlp_clef_w2v_vocab.txt"), "utf-8");
				List<String> vectors = FileUtils.readLines(new File("./finaldata/word2VecFiles/opennlp_clef_w2v_vectors.txt"), "utf-8");
				for (int i = 0 ; i < lines.size(); i++) {
					opennlp_word2VectorsMap.put(lines.get(i), vectors.get(i).trim());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return word2VectorsMap;
	}
	
	public String getOpenNlpClefWordVector(String word) {
		if(opennlp_word2VectorsMap.containsKey(word)) {
			return opennlp_word2VectorsMap.get(word);
		} else {
			return "0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0";
		}
	}
	
	
	/*public String getOpenNlpClefWordIndexVector(String word) {
		if(words.contains(word)) {
			StringBuffer sb = new StringBuffer();
			for (int i=0; i< words.size(); i++)
			return opennlp_word2VectorsMap.get(word);
		} else {
			return "0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0";
		}
	}*/
	
	

	private static List<String> loadDocTypes() {

		docTypes = new ArrayList<String>();
		docTypes.add("ECG_REPORT");
		docTypes.add("ECHO_REPORT");
		docTypes.add("DISCHARGE_SUMMARY");
		docTypes.add("RADIOLOGY_REPORT");
		
		return docTypes;
	}

	/**
	 * Returns the size of the words vocabulary.
	 * @return the wordsSize
	 */
	public int getWordsSize() {
		return wordsSize;
	}

	/**
	 * Returns the size of the stems vocabulary.
	 * @return the stemsSize
	 */
	public int getStemsSize() {
		return stemsSize;
	}

	/**
	 * Returns the number of POS tags.
	 * @return the posSize
	 */
	public int getPosSize() {
		return posSize;
	}

	/**
	 * Returns the number of Chunk tags.
	 * @return the chunksSize
	 */
	public int getChunksSize() {
		return chunksSize;
	}

	/**
	 * Returns the size of prefixes list.
	 * @return the prefixSize
	 */
	public int getPrefixesSize() {
		return prefixesSize;
	}

	/**
	 * Returns the size of suffixes list.
	 * @return the suffixSize
	 */
	public int getSuffixesSize() {
		return suffixesSize;
	}
	
	
	static int totalfeaturesAll = 0;
	/*public int getTotalFeatures() {
		//getOrthograhicIndexes("",new String[]{});
		return totalfeaturesAll;
	}*/
	/**
	 * Returns index of the word.
	 * @param word
	 * @return index
	 */
	public int getWordIndex (String word) {
		if(words.contains(word))
			return words.indexOf(word) + 1;
		if(words.contains(word.toLowerCase()))
			return words.indexOf(word.toLowerCase()) + 1;
		
		return -1;
	}
	
	/**
	 * Returns index of the stem.
	 * @param stem
	 * @return index
	 */
	public int getStemIndex (String stem) {
		int index =  wordsSize;
		if(stems.contains(stem)) {
			return stems.indexOf(stem) + index + 1;
		} 
		if(stems.contains(stem.toLowerCase())) {
			return stems.indexOf(stem.toLowerCase()) + index + 1;
		} 
		
		return -1;
	}
	
	/**
	 * Returns index of the pos tag.
	 * @param pos
	 * @return index
	 */
	public int getPosIndex (String pos) {
		int index =  wordsSize + stemsSize;
		if(Word2Index.pos.contains(pos))
			return Word2Index.pos.indexOf(pos) + index + 1;
		
		return -1;
	}
	
	/**
	 * Returns index of the chunk tag.
	 * @param chunk
	 * @return index
	 */
	public int getChunkIndex (String chunk) {
		int index =  wordsSize + stemsSize + posSize;
		if(chunks.contains(chunk))
			return chunks.indexOf(chunk) + index + 1;
		
		return -1;
	}
	
	/**
	 * Returns index of the prefix word.
	 * @param prefix
	 * @return index
	 */
	private int getPrefixIndex (String prefix) {
		prefix = prefix.toLowerCase();
		int index =  wordsSize + stemsSize + posSize + chunksSize;
		
		if(prefixes.contains(StringUtils.substring(prefix, 0, 4))) {
			return prefixes.indexOf(StringUtils.substring(prefix, 0, 4)) + index + 1;
		} else if(prefixes.contains(StringUtils.substring(prefix, 0, 3))) {
			return prefixes.indexOf(StringUtils.substring(prefix, 0, 3)) + index + 1;
		} else if(prefixes.contains(StringUtils.substring(prefix, 0, 2))) {
			return prefixes.indexOf(StringUtils.substring(prefix, 0, 2)) + index + 1;
		} else if(prefixes.contains(StringUtils.substring(prefix, 0, 1))) {
			return prefixes.indexOf(StringUtils.substring(prefix, 0, 1)) + index + 1;
		} else {
			return -1;
		}
		
	}

	/**
	 * Returns index of the suffix word.
	 * @param suffix
	 * @return index
	 */
	private int getSuffixIndex (String suffix) {
		suffix = suffix.toLowerCase();
		int index =  wordsSize + stemsSize + posSize + chunksSize + prefixesSize;
		
		if(suffixes.contains(StringUtils.substring(suffix, suffix.length()-4, suffix.length()))) {
			return suffixes.indexOf(StringUtils.substring(suffix, suffix.length()-4, suffix.length())) + index + 1;
		} else if(suffixes.contains(StringUtils.substring(suffix, suffix.length()-3, suffix.length()))) {
			return suffixes.indexOf(StringUtils.substring(suffix, suffix.length()-3, suffix.length())) + index + 1;
		} else if(suffixes.contains(StringUtils.substring(suffix, suffix.length()-2, suffix.length()))) {
			return suffixes.indexOf(StringUtils.substring(suffix, suffix.length()-2, suffix.length())) + index + 1;
		} else if(suffixes.contains(StringUtils.substring(suffix, suffix.length()-1, suffix.length()))) {
			return suffixes.indexOf(StringUtils.substring(suffix, suffix.length()-1, suffix.length())) + index + 1;
		} else {
			return -1;
		}		
		
	}
	
	/**
	 * Returns index of the UMLS semantic type.
	 * @param semanticType
	 * @return index
	 */
	public int getSemanticTypeIndex (String semanticType) {
		int index =  wordsSize + stemsSize + posSize + chunksSize + prefixesSize + suffixesSize;
		if(semanticTypes.contains(semanticType))
			return semanticTypes.indexOf(semanticType) + index + 1;
		
		return index + semanticTypesSize;
	}
	
	public int getWikiIndex (String word) {
		int index =  wordsSize + stemsSize + posSize + chunksSize + prefixesSize + suffixesSize + semanticTypesSize;
		if(wikiWords.contains(word.toLowerCase()))
			return wikiWords.indexOf(word.toLowerCase()) + index + 1;
		
		return -1;
	}
	
	public int getWikiClassIndex (String word) {
		int index =  wordsSize + stemsSize + posSize + chunksSize + prefixesSize + suffixesSize + semanticTypesSize
				+ wikiSize;
		if(wikiClasses.contains(word))
			return wikiClasses.indexOf(word) + index + 1;
		
		return -1;
	}
	
	public int getTrainDictIndex1 (String word) {
		int index =  wordsSize + stemsSize + posSize + chunksSize + prefixesSize + suffixesSize + semanticTypesSize
				+ wikiSize + wikiClassSize;
		
		if(HashMapUtilsCustom.indexOfIgnorecaseList(trainDict, word)>0 && word.length()>3)
			return HashMapUtilsCustom.indexOfIgnorecaseList(trainDict, word)+ index + 1;
	
		if(trainDict.contains(word))
			return trainDict.indexOf(word) + index + 1;
		
		word= StringUtils.strip(word, ".,:-?%;");
		if(HashMapUtilsCustom.indexOfIgnorecaseList(trainDict, word)>0 && word.length()>1)
			return HashMapUtilsCustom.indexOfIgnorecaseList(trainDict, word)+ index + 1;
	
		return -1;
	}
	public int getTrainDictIndex (String word, String[] tokens, int k) {
		int index =  wordsSize + stemsSize + posSize + chunksSize + prefixesSize + suffixesSize + semanticTypesSize
				+ wikiSize + wikiClassSize;
		{
			int trainidx = getTrainDictIndex1(word);
			if(trainidx != -1)
				return trainidx;
		}
		//bigram
		//one left
		/*if (k>0) {
			String wordStr = tokens[k-1] +" "+ tokens[k];
			int trainidx = getTrainDictIndex1(wordStr);
			if(trainidx != -1)
				return trainidx;
		}
		//one right
		if (k<tokens.length-1) {
			String wordStr = tokens[k] +" "+ tokens[k+1];
			int trainidx = getTrainDictIndex1(wordStr);
			if(trainidx != -1)
				return trainidx;
		}
		//trigram
		//one left and one right
		if (k>0 && k<tokens.length-1) {
			String wordStr = tokens[k-1] +" "+ tokens[k] +" "+ tokens[k+1];
			int trainidx = getTrainDictIndex1(wordStr);
			if(trainidx != -1)
				return trainidx;
		}
		//two left
		if (k>1) {
			String wordStr = tokens[k-2] +" "+ tokens[k-1] +" "+ tokens[k];
			int trainidx = getTrainDictIndex1(wordStr);
			if(trainidx != -1)
				return trainidx;
		}
		//two right
		if (k < tokens.length-2) {
			String wordStr = tokens[k] +" "+ tokens[k+1] +" "+ tokens[k+2];
			int trainidx = getTrainDictIndex1(wordStr);
			if(trainidx != -1)
				return trainidx;
		}
		//four grams
		//two left and one right
		if (k>1 && k<tokens.length-1) {
			String wordStr = tokens[k-2] +" "+ tokens[k-1] +" "+ tokens[k] +" "+ tokens[k+1];
			int trainidx = getTrainDictIndex1(wordStr);
			if(trainidx != -1)
				return trainidx;
		}
		//three left
		if (k>2) {
			String wordStr = tokens[k-3] +" "+ tokens[k-2] +" "+ tokens[k-1] +" "+ tokens[k];
			int trainidx = getTrainDictIndex1(wordStr);
			if(trainidx != -1)
				return trainidx;
		}
		//three right
		if (k < tokens.length-3) {
			String wordStr = tokens[k] +" "+ tokens[k+1] +" "+ tokens[k+2] +" "+ tokens[k+3];
			int trainidx = getTrainDictIndex1(wordStr);
			if(trainidx != -1)
				return trainidx;
		}*/
		
		return -1;
	}
	
	private boolean isTrainDictTerm (String term) {
		
		if(hitachiTrainDictTerm.contains(term)) 
			return true;
		
		return false;
	}
	
	public int getSectionHeaderIndex (String sectionHeader) {
		int index =  wordsSize + stemsSize + posSize + chunksSize + prefixesSize + suffixesSize + semanticTypesSize
				+ wikiSize + wikiClassSize + trainDictSize;
		if(sectionHeadersTop.contains(sectionHeader.toLowerCase()))
			return sectionHeadersTop.indexOf(sectionHeader.toLowerCase()) + index + 1;
		
		return -1;
	}
	
	public int getDocTypeIndex (String doctype) {
		int index =  wordsSize + stemsSize + posSize + chunksSize + prefixesSize + suffixesSize + semanticTypesSize
				+ wikiSize + wikiClassSize + trainDictSize + sectionHeadersSize;
		
		if(docTypes.contains(doctype))
			return docTypes.indexOf(doctype) + index + 1;
		
		return -1;
	}
	
	public int getWordClusterIndex (String word) {
		int index =  wordsSize + stemsSize + posSize + chunksSize + prefixesSize + suffixesSize + semanticTypesSize
				+ wikiSize + wikiClassSize + trainDictSize + sectionHeadersSize + docTypesSize;
		
		if(wordClustersMap.containsKey(word.toLowerCase())) {
			return (index+wordClustersMap.get(word.toLowerCase())+1);
		}
		
		return (index+wordClusterSize);
	}
	
		
	private int getTermFrequency(String token, String[] tokens) {
		HashMap<String, Integer> map = HashMapUtilsCustom.convertStringArrayToHashMap(tokens);
		if(map.containsKey(token)) {
			if(map.get(token)<1000) {
				return map.get(token);
			}
		}
		return 1000;
	}

	/**
	 * Returns the length of the string
	 * @param token
	 * @return
	 */
	private int getStringLength(String token) {
		if(token.length()<1000) {
			return token.length();
		}
		return 1000;
	}

	/**
	 * Returns the ratio of vowels to length of the word.
	 * @param token
	 * @return
	 */
	private int getNoOfVowelsByWordLength(String token) {
		if (token.isEmpty())
			return 1000;
		try {
			int ratio = 0;
			ratio = getNoOfVowels(token)/token.length();
			
			if(ratio<1000) {
				return ratio;
			}
		} catch(Exception e) {
			
		}
		return 1000;
	}
	
	/**
	 * Return number of vowels in the token
	 * @param str
	 * @return
	 */
	public int getNoOfVowels(String str)
	{
		int vowels = 0, digits = 0, blanks = 0;
		char ch;

//		System.out.print("Enter a String : ");

		for(int i = 0; i < str.length(); i ++)
		{
			ch = str.charAt(i);

			if(ch == 'a' || ch == 'A' || ch == 'e' || ch == 'E' || ch == 'i' || 
			ch == 'I' || ch == 'o' || ch == 'O' || ch == 'u' || ch == 'U')
				vowels ++;
			else if(Character.isDigit(ch))
				digits ++;
			else if(Character.isWhitespace(ch))
				blanks ++;
		}
		
		if(vowels<1000) {
			return vowels;
		} else {
			return 1000;
		}
	}
	
	
	/**
	 * Returns the index of the class label
	 * B-M - 1
	 * I-M - 2
	 * O - 3
	 * @param label - class labels
	 * @return index
	 */
	public int getClassLabelIndex(String label) {
		if (label.equals("B-M"))
			return 1;
		else if (label.equals("I-M"))
			return 2;
		else if (label.equals("DB-M"))
			return 3;
		else if (label.equals("DI-M"))
			return 4;
		else if (label.equals("DHB-M"))
			return 5;
		else if (label.equals("DHI-M"))
			return 6;
		else if (label.equals("TB-M"))
			return 7;
		else if (label.equals("TI-M"))
			return 8;
		else if (label.equals("THB-M"))
			return 9;
		else if (label.equals("THI-M"))
			return 10;
		else 
			return 11;
	}
	
	/**
	 * Returns the class label for the index 
	 * @param index
	 * @return class label (B-M/I-M/O)
	 */
	public String getClassLabelIndexString(String label) {
		if (label.equals("B-M"))
			return "1";
		else if (label.equals("I-M"))
			return "2";
		else if (label.equals("DB-M"))
			return "3";
		else if (label.equals("DI-M"))
			return "4";
		else if (label.equals("DHB-M"))
			return "5";
		else if (label.equals("DHI-M"))
			return "6";
		else if (label.equals("TB-M"))
			return "7";
		else if (label.equals("TI-M"))
			return "8";
		else if (label.equals("THB-M"))
			return "9";
		else if (label.equals("THI-M"))
			return "10";
		else 
			return "11";
	}
	
	/**
	 * Returns the class label for the index 
	 * @param index
	 * @return class label (B-M/I-M/O)
	 */
	public String getClassLabel(int index) {
		switch (index) {
			case 1: return "B-M";
			case 2: return "I-M";
			case 3: return "DB-M";
			case 4: return "DI-M";
			case 5: return "DHB-M";
			case 6: return "DHI-M";
			case 7: return "TB-M";
			case 8: return "TI-M";
			case 9: return "THB-M";
			case 10: return "THI-M";
			default : return "O";
		}
	}
	
	/**
	 * Returns the class label for the index 
	 * @param index
	 * @return class label (B-M/I-M/O)
	 */
	public String getClassLabel(String index) {
		switch (index) {
			case "1": return "B-M";
			case "2": return "I-M";
			case "3": return "DB-M";
			case "4": return "DI-M";
			case "5": return "DHB-M";
			case "6": return "DHI-M";
			case "7": return "TB-M";
			case "8": return "TI-M";
			case "9": return "THB-M";
			case "10": return "THI-M";
			default : return "O";
		}
	}
	
	
	/**
	 * Returns the tokens of the given index.
	 * @param index
	 * @return CorrespondingToken
	 */
	public String getValueOf(int index) {
		if (index < wordsSize) {
			return words.get(index-1);
		} else if (index < wordsSize + stemsSize ) {
			return stems.get(index - wordsSize -1);
		} else if (index < wordsSize + stemsSize + posSize) {
			return pos.get(index - wordsSize - stemsSize -1);
		} else if (index < wordsSize + stemsSize + posSize + chunksSize) {
			return chunks.get(index - wordsSize - stemsSize - posSize - 1);
		} else if (index < wordsSize + stemsSize + posSize + chunksSize + prefixesSize) {
			return prefixes.get(index - wordsSize - stemsSize - posSize - chunksSize -1);
		} else if (index < wordsSize + stemsSize + posSize + chunksSize + prefixesSize + suffixesSize) {
			return suffixes.get(index - wordsSize - stemsSize - posSize - chunksSize - prefixesSize -1);
		} else if (index < wordsSize + stemsSize + posSize + chunksSize + prefixesSize + suffixesSize + semanticTypesSize) {
			return semanticTypes.get(index - wordsSize - stemsSize - posSize - chunksSize - prefixesSize - suffixesSize -1);
		} 
		return "";
	}
	
	/**
	 * Returns a set of indexes of orthographic features for the token
	 * @param token
	 * @return set of indexes
	 */
	/*public TreeMap<Integer,Double> getOrthograhicIndexes (String token, String[] tokens) {
		TreeMap<Integer,Double> indexes = new TreeMap<Integer,Double>();
		int index = totalfeatures;
		index++;//11 new features
		
		//1. stopword 387971
		if(clinicalTokenizer.isStopword(token)) {
			indexes.put(index,1.0);
		}
		
		//2. first char CAP
		index++;//387972 21
		if(Character.isUpperCase(token.charAt(0))) {
			indexes.put(index,1.5);
		}

		//3. CAPS
		index++;//387973
		if(StringUtils.isAllUpperCase(token)) {
			indexes.put(index,1.5);
		}
		
		index++;
		//4. Numeric 387974
		if(StringUtils.isNumericSpace(token)) {
			indexes.put(index,1.0);
		}
		
		index++;
		//5. alphanumeric 387975 24
		if(StringUtils.isAlphanumericSpace(token)) {
			indexes.put(index,1.0);
		}
		
		index++;
		//6. contains hyphen 387976 25
		if(StringUtils.isAlphanumericSpace(token) && token.contains("-")) {
			indexes.put(index,1.0);
		}
		
		index++;
		//7,8
		try {
			if(java2MySql.isInUMLS(token)) {
				indexes.add(index); //387977
				String semanticType = java2MySql.getSemanticTypeOfTerm(token);
				if(semanticType != null) {
					if(!semanticType.isEmpty()) {
						if(semanticTypes.contains(semanticType)) {
							indexes.add(getSemanticTypeIndex(semanticType));
						}
					}
				}
			}
		} catch(Exception e) {
			//e.printStackTrace();
		}
		//26
		index++;
		//9. is in wiki 27
		if(wikiWords.contains(token.toLowerCase())) {
			indexes.put(index,1.0);
		}
		
		index++; 
		// 11. is in trainDict 28
		if(trainDict.contains(token)) {
			System.out.println("in train dict = " + token);
			indexes.put(index,1.5);
		} else if(HashMapUtils.containsIgnorecaseList(trainDict,token) && token.length()>3) {
			indexes.put(index,1.5);
		}
		
		getTrainDictIndex(token, tokens, k);
		
		//12. has /
		index++;//29
		if(token.contains("/")) {
			indexes.put(index,1.0);
		}
		
		//13. has punctuation
		
		int dictSize = 1000;
		index++;
		//14. length of the word //30
		//indexes.add((index+getStringLength(token)));
		
		index++;
		index += dictSize; 
		//15. no of vowels
		//indexes.add(index+getNoOfVowels(token));
		
		index++;
		index += dictSize;
		//16. average vowels
//		indexes.add(index+getNoOfVowelsByWordLength(token));
		
		index++;
		index+= dictSize;
		//17. term frequency
		indexes.put(index+getTermFrequency(token,tokens),1.0);
		index+= dictSize;
		
		
		//System.out.println("token = " + token);
		//wiki classes - within wiki indexes
		try {
			List<String> categories = SearchIndex.getInstance().getCategory(token);
			if(categories.size() > 0) {
				for (String category : categories) {
					if (!category.trim().isEmpty()) {
						if(getWikiClassIndex(category.toLowerCase())>0) {
							indexes.put(getWikiClassIndex(category.toLowerCase()),1.0);
//							System.out.println("wiki category = " + category +" - "+getWikiClassIndex(category.toLowerCase()));
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

//		System.out.println("cluster index = " + getWordClusterIndex(token));
		//topic modeling/ word clusters
		if (wordClustersMap.containsKey(token.toLowerCase()))
			indexes.put(getWordClusterIndex(token),1.0);
		
		//TODO
		//topic modeling or word2vec to get the class (cluster) of the word
		//ctakes : send sentence and get the spans of the medical terms from ctakes.
		
		
		List<MetaMapToken> metamapTokens = new ArrayList<MetaMapToken>();
		List<Ev> mappingList = new ArrayList<Ev>();
		MetaMapApiUse.getInstance().processWrapper(token, metamapTokens, mappingList);
		//System.out.println(mappingList);
		if(mappingList.size()>0) {
			Ev ev = mappingList.get(0);
			indexes.put(index,1.0);
			index++;
			List<String> gotSts;
			try {
				gotSts = ev.getSemanticTypes();
				for (String gotSt : gotSts) {
					if(semanticTypes.contains(gotSt)) {
						indexes.put(getSemanticTypeIndex(gotSt),1.0);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		index++;
		
		totalfeaturesAll = totalfeatures + index;
		//387979,387980,387981,387982,387983,387984, 387985
		//word, stem, pos, chunk, prefix, suffix, semantictype
		//prefixes and suffixes
		return indexes;
	}
*/
	
	public TreeMap<Integer,Double> getAllFeatureIndexes(String[] tokens,
			String[] tags, String[] chunks, int k, String sectionHeader,
			String clefDocType) throws IOException {
		
		TreeMap<Integer,Double> featureIndexesMap = new TreeMap<Integer, Double>();
		String token = tokens[k];
		int index = totalfeatures;
		index++;//11 new features
		
		if (Word2Index.getInstance()
				.getWordIndex(tokens[k])>0) {
			featureIndexesMap.put(Word2Index.getInstance()
					.getWordIndex(tokens[k]),1.0);
		}
		
		if (Word2Index.getInstance().getStemIndex(WordStemmer.getStemmer().stem(tokens[k])) > 0)
			featureIndexesMap.put(Word2Index.getInstance()
				.getStemIndex(WordStemmer.getStemmer().stem(tokens[k])),1.0);
		
		if(Word2Index.getInstance().getPosIndex(tags[k])>0)
			featureIndexesMap.put(Word2Index.getInstance()
				.getPosIndex(tags[k]),1.0);
		
		if(Word2Index.getInstance().getChunkIndex(chunks[k])>0)
			featureIndexesMap.put(Word2Index.getInstance()
				.getChunkIndex(chunks[k]),1.0);
		
		/*if(Word2Index.getInstance().getSuffixIndex(tokens[k])>0)
			featureIndexesMap.put(Word2Index.getInstance()
				.getSuffixIndex(tokens[k]),1.0);
		
		if(Word2Index.getInstance().getPrefixIndex(tokens[k])>0)
			featureIndexesMap.put(Word2Index.getInstance()
				.getPrefixIndex(tokens[k]),1.0);*/
		
		if(Word2Index.getInstance().getWikiIndex(tokens[k])>0)
			featureIndexesMap.put(Word2Index.getInstance()
				.getWikiIndex(tokens[k]),1.0);
		
		if(Word2Index.getInstance().getTrainDictIndex(tokens[k], tokens,k)>0) {
			featureIndexesMap.put(Word2Index.getInstance() 
				.getTrainDictIndex(tokens[k], tokens,k),1.0);
			//traindict
			featureIndexesMap.put(index, 1.5);
		}
		
		if(Word2Index.getInstance().getSectionHeaderIndex(sectionHeader)>0)
			featureIndexesMap.put(Word2Index.getInstance()
				.getSectionHeaderIndex(sectionHeader),1.0);
		
		if(Word2Index.getInstance().getDocTypeIndex(clefDocType)>0)
			featureIndexesMap.put(Word2Index.getInstance()
				.getDocTypeIndex(clefDocType),1.0);
		
		

		//1. stopword 387971
		if(clinicalTokenizer.isStopword(token)) {
			featureIndexesMap.put(index,1.0);
		}
		
		//2. first char CAP
		index++;//387972 21
		if(Character.isUpperCase(token.charAt(0))) {
			featureIndexesMap.put(index,1.0);
		}

		//3. CAPS
		index++;//387973
		if(StringUtils.isAllUpperCase(token)) {
			featureIndexesMap.put(index,1.0);
		}
		
		index++;
		//4. Numeric 387974
		if(StringUtils.isNumericSpace(token)) {
			featureIndexesMap.put(index,1.0);
		}
		
		index++;
		//5. alphanumeric 387975 24
		if(StringUtils.isAlphanumericSpace(token)) {
			featureIndexesMap.put(index,1.0);
		}
		
		index++;
		//6. contains hyphen 387976 25
		if(StringUtils.isAlphanumericSpace(token) && token.contains("-")) {
			featureIndexesMap.put(index,1.0);
		}
		
		index++;
		//7,8 UMLS
		try {
			if(java2MySql.isInUMLS(token)) {
				featureIndexesMap.put(index,1.5); //387977
				String semanticType = java2MySql.getSemanticTypeOfTerm(token);
				if(semanticType != null) {
					if(!semanticType.isEmpty()) {
						if(semanticTypes.contains(semanticType)) {
							featureIndexesMap.put(getSemanticTypeIndex(semanticType),1.5);
						}
					}
				}
			}
		} catch(Exception e) {
			//e.printStackTrace();
		}
		//26
		index++;
		//9. is in wiki 27
		if(wikiWords.contains(token.toLowerCase())) {
			featureIndexesMap.put(index,1.0);
		}
		
		index++; 
		// 11. is in trainDict 28
		/*if(trainDict.contains(token)) {
			System.out.println("in train dict = " + token);
			indexes.put(index,1.5);
		} else if(HashMapUtils.containsIgnorecaseList(trainDict,token) && token.length()>3) {
			indexes.put(index,1.5);
		}
		*/
		/*if(getTrainDictIndex(token, tokens, k)>0){
			
		}*/
		
		//12. has /
		index++;//29
		if(token.contains("/")) {
			featureIndexesMap.put(index,1.0);
		}
		
		//13. has punctuation
		
		int dictSize = 1000;
		index++;
		//14. length of the word //30
		//indexes.add((index+getStringLength(token)));
		
		index++;
		index += dictSize; 
		//15. no of vowels
		//indexes.add(index+getNoOfVowels(token));
		
		index++;
		index += dictSize;
		//16. average vowels
		featureIndexesMap.put(index+getNoOfVowelsByWordLength(token), 1.0);
		
		index++;
		index+= dictSize;
		//17. term frequency
		featureIndexesMap.put(index+getTermFrequency(token,tokens),1.0);
		index+= dictSize;
		
		
		//System.out.println("token = " + token);
		//wiki classes - within wiki indexes
		try {
			List<String> categories = SearchIndex.getInstance().getCategory(token);
			if(categories.size() > 0) {
				for (String category : categories) {
					if (!category.trim().isEmpty()) {
						if(getWikiClassIndex(category.toLowerCase())>0) {
							featureIndexesMap.put(getWikiClassIndex(category.toLowerCase()),1.0);
//								System.out.println("wiki category = " + category +" - "+getWikiClassIndex(category.toLowerCase()));
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

//			System.out.println("cluster index = " + getWordClusterIndex(token));
		//topic modeling/ word clusters. word vector
		/*if (wordClustersMap.containsKey(token.toLowerCase()))
			featureIndexesMap.put(getWordClusterIndex(token),1.0);
		*/
		//Metamap op
		/*List<MetaMapToken> metamapTokens = new ArrayList<MetaMapToken>();
		List<Ev> mappingList = new ArrayList<Ev>();
		MetaMapApiUse.getInstance().processWrapper(token, metamapTokens, mappingList);
		//System.out.println(mappingList);
		if(mappingList.size()>0) {
			Ev ev = mappingList.get(0);
			featureIndexesMap.put(index,1.0);
			index++;
			List<String> gotSts;
			try {
				gotSts = ev.getSemanticTypes();
				for (String gotSt : gotSts) {
					if(semanticTypes.contains(gotSt)) {
						featureIndexesMap.put(getSemanticTypeIndex(gotSt),1.0);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}*/
		index++;
		
		totalfeaturesAll = totalfeatures + index;
		//387979,387980,387981,387982,387983,387984, 387985
		//word, stem, pos, chunk, prefix, suffix, semantictype
		//prefixes and suffixes
			
		return featureIndexesMap;
	}

	
	public String getFeaturesVectorForTheToken(String[] tokens,
			String[] postags, String[] chunks, int k, String sectionHeader,
			String clefDocType) throws IOException {
		StringBuffer sb = new StringBuffer();
		TreeMap<Integer,Double> featureIndexesSet = getAllFeatureIndexes (tokens,postags,chunks,k, sectionHeader,
				clefDocType);

		for (Entry<Integer, Double> entry : featureIndexesSet.entrySet()) {
			sb.append(" "+ entry.getKey() + ":" + entry.getValue());
		}
		
		//adding word 2 vector
		/*HashMap<String, List<String>> word2Vectors = Word2Index.getInstance().loadWord2Vectors();
		if(word2Vectors.containsKey(tokens[k].toLowerCase())) {
			int finalidx = Word2Index.getInstance().totalfeaturesAll;
			List<String> wordVecs = word2Vectors.get(tokens[k].toLowerCase());
			for (int l = 0;l < wordVecs.size(); l++) {
				sb.append(" " + (finalidx+l+1)+ ":"+wordVecs.get(l));
			}
		}*/
		
		return sb.toString().trim();
	}
	
	public static void main (String[] args) throws IOException {
		
		Word2Index word2Index = new Word2Index();
		String tokens[] = new String[1];
		String[] postags = new String[1];
		String[] chunks = new String[1];
		postags[0]="NN";
		chunks[0] = "B-NP";
		tokens[0] = "Diabetes";
		int k =0;
		String sectionHeader ="allergies";
		String clefDocType = "";
		StringBuffer sb = new StringBuffer();
		TreeMap<Integer,Double> featureIndexesSet = word2Index.getAllFeatureIndexes (tokens,postags,chunks,k, sectionHeader,
				clefDocType);

		for (Entry<Integer, Double> entry : featureIndexesSet.entrySet()) {
			sb.append(" "+ entry.getKey() + ":" + entry.getValue());
		}
		
		//adding word 2 vector
		/*HashMap<String, List<String>> word2Vectors = Word2Index.getInstance().loadWord2Vectors();
		if(word2Vectors.containsKey(tokens[k].toLowerCase())) {
			int finalidx = Word2Index.getInstance().totalfeaturesAll;
			List<String> wordVecs = word2Vectors.get(tokens[k].toLowerCase());
			for (int l = 0;l < wordVecs.size(); l++) {
				sb.append(" " + (finalidx+l+1)+ ":"+wordVecs.get(l));
			}
		}*/
		int index = wordsSize;
		System.out.println("wordsSize = " + wordsSize);
		index += stemsSize;
		System.out.println("stemsSize = " + index);
		index += posSize;
		System.out.println("posSize = " + index);
		index += chunksSize;
		System.out.println("chunksSize = " + index);
		index += prefixesSize;
		System.out.println("prefixesSize = " + index);
		index += suffixesSize;
		System.out.println("suffixesSize = " +  index);
		index += semanticTypesSize;
		System.out.println("semanticTypesSize = " +  index);
		index += wikiSize;
		System.out.println("wikiSize = " +  index);
		index += wikiClassSize;
		System.out.println("wikiClassSize = " +  index);
		index += trainDictSize;
		System.out.println("trainDictSize = " +  index);
		index += sectionHeadersSize;
		System.out.println("sectionHeadersSize = " +  index);
		index += docTypesSize;
		System.out.println("docTypesSize = " +  index);
		index += wordClusterSize;
		System.out.println("wordClusterSize = " +  index);
		System.out.println("totalfeatures = " + totalfeatures);
		System.out.println("totalfeaturesAll = " + totalfeaturesAll);
		
		System.out.println(sb.toString());
	}
	
}
