package edu.iiit.cde.r.svm;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import opennlp.tools.util.Span;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import edu.iiit.cde.r.mysql.Java2MySql;
import edu.iiit.cde.r.utils.ClefPipedLineParser;
import edu.iiit.cde.r.utils.ClinicalTokenizer;
import edu.iiit.nlp.Chunker;
import edu.iiit.nlp.OpenNLPTokenizer;
import edu.iiit.nlp.POSTagger;
import edu.iiit.nlp.SentenceBreaker;

/**
 * Creates features file from the given discharge summaries and the
 * corresponding piped outputs. Trains a SVM model using the features files.
 * Extracts the medical terms using the trained SVM model.
 * 
 * @author raghavendra
 *
 */
public class SVMModelGenerator {

	Java2MySql java2MySql; 
	ClefPipedLineParser clefPipedLineParser = new ClefPipedLineParser();
	
	
	/**
	 * SVMModelGenerator constructor.
	 */
	public SVMModelGenerator() {
		java2MySql = Java2MySql.getInstance();
	}
	
	/**
	 * Reads discharge summary and its piped output. Generates features file to
	 * build SVM model using SVMstruct.
	 * 
	 * Sample PipedOP :
	 * 00098-016139-DISCHARGE_SUMMARY.txt|9-20|C0524850|Neurosurgery
	 * |Neurosurgery|topp|generic
	 * 
	 * @throws IOException
	 */
	void generateNLPFeaturesFileGoldStd(String corpusDir, String pipeDir,
			String featuresFileString, String svmModel) throws IOException {/*

		FileUtils.deleteQuietly(new File(featuresFileString));
		File featuresFile = new File(featuresFileString);
		int exampleNumber = 1;

		File[] corpusFiles = new File(corpusDir).listFiles();
		for (File corpusFile : corpusFiles) {
			int start = 0;
			int end = 0;
			List<String> pipeDelimitedLines = null;
			String corpuslines = "";
			try {
				// read the piped files of the discharge summary files
				corpuslines = FileUtils.readFileToString(corpusFile);
				File pipeFile = new File(pipeDir + "/" + corpusFile.getName());
				// + StringUtils.replace(corpusFile.getName(), "txt",
				// "pipe.txt"));

				if (corpusFile.getName().contains(".txt"))
					pipeFile = new File(pipeDir
							+ "/"
							+ StringUtils.replace(corpusFile.getName(), "txt",
									"pipe.txt"));

				if (!pipeFile.exists()) {
					System.out.println("No pipe file found = " + pipeFile);
					continue;
				}

				pipeDelimitedLines = FileUtils.readLines(pipeFile);
			} catch (IOException e) {
				e.printStackTrace();
			}

			// For each line of the piped file. Each line has a medical term
			// with spans.
			for (String pipeDelimitedLine : pipeDelimitedLines) {

				String[] slots = pipeDelimitedLine.split("\\|");

				String[] spanString = slots[1].split(",");
				String[] CUIString = slots[3].split(",");

				int[] spanCUI = new int[spanString.length * 2];

				// Iterate on each span of each line.
				for (int i = 0; i < spanString.length; i++) {

					spanCUI[i * 2] = Integer
							.parseInt(spanString[i].split("-")[0]);
					spanCUI[i * 2 + 1] = Integer.parseInt(spanString[i]
							.split("-")[1]);
					int j = CUIString.length;
					if (i < j) {
						j = i;
					} else {
						j = CUIString.length - 1;
					}

					end = spanCUI[i * 2];
					// For first line 0 < start of first span
					if (start < end) {

						// get sentence apart from medical term
						String remainingSentence = StringUtils.substring(
								corpuslines, start, end);
						if (remainingSentence.equals(" ")) {
							continue;
						} else if (remainingSentence.startsWith(" ")) {
							remainingSentence = StringUtils.removeStart(
									remainingSentence, " ");
						} else if (remainingSentence.equals("\n")) {
							// FileUtils.write(featuresFile, "\n", true);
						}

						System.out.println("start-End = " + start + "\t" + end
								+ remainingSentence);

						// Get all sentences of text(other than medical terms)
						// and append with 'O'
						String[] sentences = SentenceBreaker
								.getSentenceBreaker().getSentences(
										remainingSentence);
						for (String sentence : sentences) {
							
							ArrayList<StanfordToken> tokensList = StanfordUtils.processStr(sentence);
							String[] tokens = StanfordUtils.getTokens(tokensList);
							String[] tags = StanfordUtils.getPOSTags(tokensList);
							String[] lemmas = StanfordUtils.getLemmas(tokensList);
							
							String[] tokens = OpenNLPTokenizer
									.getOpenNLPTokenizer().tokenize(sentence);
							String[] tags = POSTagger.getPOSTagger()
									.tag(tokens);
							String[] chunks = Chunker.getChunker().getChunks(
									tokens, tags);

//							System.out.println("Sentence == " + sentence);
							for (int k = 0; k < tokens.length; k++) {
								// nlp features
								FileUtils.write(featuresFile, Word2Index
										.getInstance().getClassLabelIndex("O")
										+ " qid:" + exampleNumber, true);

								HashSet<Integer> featureIndexesSet = Word2Index
										.getInstance().getOrthograhicIndexes(
												tokens[k],tokens);
								featureIndexesSet.add(Word2Index.getInstance()
										.getWordIndex(tokens[k]));
								featureIndexesSet.add(Word2Index.getInstance()
										.getStemIndex(
												WordStemmer.getStemmer().stem(
														tokens[k])));
								featureIndexesSet.add(Word2Index.getInstance()
										.getStemIndex(lemmas[k]));
								featureIndexesSet.add(Word2Index.getInstance()
										.getPosIndex(tags[k]));
								featureIndexesSet.add(Word2Index.getInstance()
										.getChunkIndex(chunks[k]));
								List<Integer> featureIndexes = new ArrayList<Integer>(
										featureIndexesSet);
								Collections.sort(featureIndexes);
								for (int orthoIndex : featureIndexes) {
									FileUtils.write(featuresFile, " "
											+ orthoIndex + ":1", true);
								}
								FileUtils.write(featuresFile, " # " + tokens[k]
										+ "\n", true);

								if (tokens[k].equals(".")
										|| tokens[k].equals("\n")) {
									exampleNumber++;
								}
							}
						}

					}

					String word = StringUtils.substring(corpuslines,
							spanCUI[i * 2], spanCUI[i * 2 + 1] + 1);
					start = spanCUI[i * 2 + 1] + 1;

					System.out.println(word + "\tterm");
					// Iterate on spans of the medical term and append the B-M
					// or I-M label
					// Abdominal wall
					if (!word.isEmpty()) {
						String label = "B-M";

						String[] tokens = OpenNLPTokenizer
								.getOpenNLPTokenizer().tokenize(word);
						String[] tags = POSTagger.getPOSTagger().tag(tokens);
						String[] chunks = Chunker.getChunker().getChunks(
								tokens, tags);

						for (int k = 0; k < tokens.length; k++) {
							// nlp features

							FileUtils.write(featuresFile, Word2Index
									.getInstance().getClassLabelIndex(label)
									+ " qid:" + exampleNumber, true);

							HashSet<Integer> featureIndexesSet = Word2Index
									.getInstance().getOrthograhicIndexes(
											tokens[k],tokens);
							featureIndexesSet.add(Word2Index.getInstance()
									.getWordIndex(tokens[k]));
							featureIndexesSet.add(Word2Index.getInstance()
									.getStemIndex(
											WordStemmer.getStemmer().stem(
													tokens[k])));
							featureIndexesSet.add(Word2Index.getInstance()
									.getPosIndex(tags[k]));
							featureIndexesSet.add(Word2Index.getInstance()
									.getChunkIndex(chunks[k]));
							List<Integer> featureIndexes = new ArrayList<Integer>(
									featureIndexesSet);
							Collections.sort(featureIndexes);
							for (int orthoIndex : featureIndexes) {
								FileUtils.write(featuresFile, " " + orthoIndex
										+ ":1", true);
							}
							FileUtils.write(featuresFile, " # " + tokens[k]
									+ "\n", true);
							label = "I-M";
						}
					}
				}
			}
		}
		trainSVMModel(featuresFile, svmModel);
		// /home/raghavendra/BackUP/tools/SVM/svm_hmm
	*/}

	/**
	 * Reads discharge summary and its piped output. Generates features file to
	 * build SVM model using SVMstruct.
	 * 
	 * Sample PipedOP :
	 * 00098-016139-DISCHARGE_SUMMARY.txt|9-20|C0524850|Neurosurgery
	 * |Neurosurgery|topp|generic
	 * 
	 * @throws IOException
	 */
	/*
	 * void generateFeaturesAndTrainSVMWithBIO (String corpusDir, String
	 * pipeDir, String featuresFileString, String svmModel ) throws IOException
	 * {
	 * 
	 * FileUtils.deleteQuietly(new File(featuresFileString)); File featuresFile
	 * = new File(featuresFileString); int exampleNumber = 1; File[] corpusFiles
	 * = new File(corpusDir).listFiles(); for(File corpusFile : corpusFiles) {
	 * System.out.println("Processing ..  " + corpusFile.getName()); int start =
	 * 0; int end = 0; List<String> pipeDelimitedLines = null; String
	 * corpuslines = ""; try { //read the piped files of the discharge summary
	 * files corpuslines = FileUtils.readFileToString(corpusFile); File pipeFile
	 * = new File(pipeDir + "/" + corpusFile.getName()); //+
	 * StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));
	 * 
	 * if (corpusFile.getName().contains(".txt")) pipeFile = new File(pipeDir +
	 * "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));
	 * 
	 * 
	 * if(!pipeFile.exists()) { System.out.println("No pipe file found = " +
	 * pipeFile); continue; }
	 * 
	 * pipeDelimitedLines = FileUtils.readLines(pipeFile); } catch (IOException
	 * e) { e.printStackTrace(); }
	 * 
	 * TreeMap<Integer, Integer> firstSpans = new TreeMap<Integer, Integer>();
	 * TreeMap<Integer, Integer> secondSpans = new TreeMap<Integer, Integer>();
	 * //For each line of the piped file. Each line has a medical term with
	 * spans. for (String pipeDelimitedLine : pipeDelimitedLines) {
	 * 
	 * String[] slots = pipeDelimitedLine.split("\\|");
	 * 
	 * String[] spanString = slots[1].split(","); String[] CUIString =
	 * slots[3].split(",");
	 * 
	 * int[] spanCUI = new int[spanString.length * 2];
	 * 
	 * //Iterate on each span of each line. for (int i = 0; i <
	 * spanString.length; i++) {
	 * 
	 * spanCUI[i * 2] = Integer.parseInt(spanString[i].split("-")[0]); spanCUI[i
	 * * 2 + 1] = Integer.parseInt(spanString[i].split("-")[1]); if (i==0) {
	 * firstSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]); } else {
	 * secondSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]); }
	 * System.out.println(spanCUI[i * 2] +"-"+ spanCUI[i * 2 + 1]); } }
	 * 
	 * //new code List<String> lines = FileUtils.readLines(corpusFile); String
	 * fileText = FileUtils.readFileToString(corpusFile); int span = 0;
	 * 
	 * String sectionHeader = ""; String clefDocType =
	 * getDocumentType(corpusFile.getName()); for (int i =0; i < lines.size();
	 * i++) {
	 * 
	 * 
	 * //get section header String sectionHeaderSubstring =
	 * StringUtils.substringBefore(lines.get(i), ":"); if
	 * (Word2Index.getInstance
	 * ().sectionHeadersTop.contains(sectionHeaderSubstring.toLowerCase())) {
	 * sectionHeader = sectionHeaderSubstring;
	 * System.out.println("sectionheader = " + sectionHeader); }
	 * 
	 * String[] sentences =
	 * SentenceBreaker.getSentenceBreaker().getSentences(lines.get(i)); for (int
	 * j = 0; j < sentences.length; j++) { String sentence = sentences[j]; //
	 * System.out.println("sentence = " + sentence);
	 * 
	 * String[] tokens =
	 * OpenNLPTokenizer.getOpenNLPTokenizer().tokenize(sentence); String[] tags
	 * = POSTagger.getPOSTagger().tag(tokens); String[] chunks =
	 * Chunker.getChunker().getChunks(tokens, tags);
	 * 
	 * int endMedicaltermSpan = -1;
	 * 
	 * for (int k =0 ; k < tokens.length; k++) {
	 * 
	 * int noOfSpaces = StringUtils.indexOf(fileText, tokens[k]); //
	 * System.out.println("No of spaces = " + noOfSpaces); //at medical term
	 * start span += noOfSpaces;
	 * 
	 * if(firstSpans.containsKey(span)) {
	 * 
	 * FileUtils.write(featuresFile,
	 * Word2Index.getInstance().getClassLabelIndex("B-M") + " qid:" +
	 * exampleNumber , true);
	 * 
	 * System.out.println(span+"-"+tokens[k]); endMedicaltermSpan =
	 * firstSpans.get(span); } else if (span < endMedicaltermSpan) {
	 * System.out.println(span+"-"+tokens[k]); FileUtils.write(featuresFile,
	 * Word2Index.getInstance().getClassLabelIndex("I-M") + " qid:" +
	 * exampleNumber , true); } else if (secondSpans.containsKey(span)) {
	 * System.out.println(span+"-"+tokens[k]); FileUtils.write(featuresFile,
	 * Word2Index.getInstance().getClassLabelIndex("I-M") + " qid:" +
	 * exampleNumber , true); endMedicaltermSpan = secondSpans.get(span); } else
	 * { FileUtils.write(featuresFile,
	 * Word2Index.getInstance().getClassLabelIndex("O") + " qid:" +
	 * exampleNumber , true); }
	 * 
	 * //startSentence += tokens[k].length()+1; //
	 * System.out.println("tokens length = " + tokens[k].length()); //
	 * System.out.println(span+"-"+tokens[k]);
	 * 
	 * HashSet<Integer> featureIndexesSet =
	 * Word2Index.getInstance().getOrthograhicIndexes(tokens[k]);
	 * featureIndexesSet.add(Word2Index.getInstance().getWordIndex(tokens[k]));
	 * featureIndexesSet
	 * .add(Word2Index.getInstance().getStemIndex(WordStemmer.getStemmer
	 * ().stem(tokens[k])));
	 * featureIndexesSet.add(Word2Index.getInstance().getPosIndex(tags[k]));
	 * featureIndexesSet.add(Word2Index.getInstance().getChunkIndex(chunks[k]));
	 * featureIndexesSet.add(Word2Index.getInstance().getWikiIndex(tokens[k]));
	 * featureIndexesSet
	 * .add(Word2Index.getInstance().getWikiClassIndex(tokens[k]));
	 * featureIndexesSet
	 * .add(Word2Index.getInstance().getTrainDictIndex(tokens[k]));
	 * featureIndexesSet
	 * .add(Word2Index.getInstance().getSectionHeaderIndex(sectionHeader));
	 * featureIndexesSet
	 * .add(Word2Index.getInstance().getDocTypeIndex(clefDocType));
	 * 
	 * List<Integer> featureIndexes = new ArrayList<Integer>(featureIndexesSet);
	 * Collections.sort(featureIndexes); for (int orthoIndex : featureIndexes) {
	 * FileUtils.write(featuresFile, " " + orthoIndex + ":1" , true); }
	 * FileUtils.write(featuresFile, " # " + tokens[k]+ "\n" , true);
	 * 
	 * span += tokens[k].length(); fileText = StringUtils.substring(fileText,
	 * (tokens[k].length()+ noOfSpaces)); } exampleNumber++; } //span++; }
	 * 
	 * } trainSVMModel(featuresFile, svmModel); }
	 */

	/**
	 * Reads discharge summary and its piped output. Generates features file to
	 * build SVM model using SVMstruct.
	 * 
	 * Sample PipedOP :
	 * 00098-016139-DISCHARGE_SUMMARY.txt|9-20|C0524850|Neurosurgery
	 * |Neurosurgery|topp|generic
	 * 
	 * @throws IOException
	 */
	void generateFeaturesAndTrainSVMWithBIODTTag(String corpusDir,
			String pipeDir, String featuresFileString, String svmModel)
			throws IOException {

		FileUtils.deleteQuietly(new File(featuresFileString));
		File featuresFile = new File(featuresFileString);
		int exampleNumber = 1;
		File[] corpusFiles = new File(corpusDir).listFiles();
		for (File corpusFile : corpusFiles) {
			System.out.println("Processing ..  " + corpusFile.getName());
			int start = 0;
			int end = 0;
			List<String> pipeDelimitedLines = null;
			String corpuslines = "";
			try {
				// read the piped files of the discharge summary files
				corpuslines = FileUtils.readFileToString(corpusFile);
				File pipeFile = new File(pipeDir + "/" + corpusFile.getName());
				// + StringUtils.replace(corpusFile.getName(), "txt",
				// "pipe.txt"));
				
				
				if (corpusFile.getName().contains(".txt"))
					pipeFile = new File(pipeDir
							+ "/"
							+ StringUtils.replace(corpusFile.getName(), "txt",
									"pipe.txt"));

				if (!pipeFile.exists()) {
					System.out.println("No pipe file found = " + pipeFile);
					continue;
				}

				pipeDelimitedLines = FileUtils.readLines(pipeFile);
			} catch (IOException e) {
				e.printStackTrace();
			}

			TreeMap<Integer, Integer> singleSpans = new TreeMap<Integer, Integer>();
			TreeMap<Integer, Integer> disjointFirstSpans = new TreeMap<Integer, Integer>();
			TreeMap<Integer, Integer> disjointSecondSpans = new TreeMap<Integer, Integer>();
			TreeMap<Integer, Integer> tripletFirstSpans = new TreeMap<Integer, Integer>();
			TreeMap<Integer, Integer> tripletSecondSpans = new TreeMap<Integer, Integer>();
			// For each line of the piped file. Each line has a medical term
			// with spans.
			for (String pipeDelimitedLine : pipeDelimitedLines) {
				
				//CLEF 2014
				String[] slots = pipeDelimitedLine.split("\\|");
				String[] spanString = slots[1].split(",");
				String[] CUIString = slots[3].split(",");

				int[] spanCUI = new int[spanString.length * 2];

				// Iterate on each span of each line.
				for (int i = 0; i < spanString.length; i++) {

					spanCUI[i * 2] = Integer
							.parseInt(spanString[i].split("-")[0]);
					spanCUI[i * 2 + 1] = Integer.parseInt(spanString[i]
							.split("-")[1]);
					if (spanString.length == 1) {
						if (i == 0) {
							singleSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
						}
					} else if (spanString.length == 2) {
						if (i == 0) {
							disjointFirstSpans.put(spanCUI[i * 2],
									spanCUI[i * 2 + 1]);
						} else {
							disjointSecondSpans.put(spanCUI[i * 2],
									spanCUI[i * 2 + 1]);
						}
					} else if (spanString.length == 3) {
						if (i == 0) {
							tripletFirstSpans.put(spanCUI[i * 2],
									spanCUI[i * 2 + 1]);
						} else {
							tripletSecondSpans.put(spanCUI[i * 2],
									spanCUI[i * 2 + 1]);
						}
					}
					System.out.println(spanCUI[i * 2] + "-"
							+ spanCUI[i * 2 + 1]);
				}
			
				
				//CLEF 2013
				/*int[] spanCUI = clefPipedLineParser.parseCLEF2013PipedFile(pipeDelimitedLine);
				for (int i = 0; i < spanCUI.length/2; i++) {
					
					if (spanCUI.length/2==1) {
						if (i==0) {
							singleSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
						}
					} else if (spanCUI.length/2==2) {
						if (i==0) {
							disjointFirstSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
						} else {
							disjointSecondSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
						}
					} else if (spanCUI.length/2==3) {
						if (i==0) {
							tripletFirstSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
						} else {
							tripletSecondSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
						}
					}
					//System.out.println(spanCUI[i * 2] +"-"+ spanCUI[i * 2 + 1]);
				}*/
			}

			// new code
			List<String> lines = FileUtils.readLines(corpusFile);
			String fileText = FileUtils.readFileToString(corpusFile);
			int currentPos = 0;

			String sectionHeader = "";
			String clefDocType = getDocumentType(corpusFile.getName());
			for (int i = 0; i < lines.size(); i++) {

				// get section header
				String sectionHeaderSubstring = StringUtils.substringBefore(
						lines.get(i), ":");
				if (Word2Index.getInstance().sectionHeadersTop
						.contains(sectionHeaderSubstring.toLowerCase())) {
					sectionHeader = sectionHeaderSubstring;
					//System.out.println("sectionheader = " + sectionHeader);
				}

				String[] sentences = SentenceBreaker.getSentenceBreaker()
						.getSentences(lines.get(i));
				for (int j = 0; j < sentences.length; j++) {
					String sentence = sentences[j];
					// System.out.println("sentence = " + sentence);

					/*ArrayList<StanfordToken> tokensList = StanfordUtils.processStr(sentence);
					String[] tokens = StanfordUtils.getTokens(tokensList);
					String[] postags = StanfordUtils.getPOSTags(tokensList);
					String[] lemmas = StanfordUtils.getLemmas(tokensList);
					String[] chunks = new String[0];*/
					
					String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer()
							.tokenize(sentence);
					String[] postags = POSTagger.getPOSTagger().tag(tokens);
					String[] chunks = Chunker.getChunker().getChunks(tokens,
							postags);

					int endMedicaltermSpan = -1;
					String medicaltag = "";
					for (int k = 0; k < tokens.length; k++) {

						int noOfSpaces = StringUtils.indexOf(fileText,
								tokens[k]);
						// System.out.println("No of spaces = " + noOfSpaces);
						// at medical term start
						currentPos += noOfSpaces;

						if (tokens[k].startsWith("-")) {
							// tokens[k] = StringUtils.substring(tokens[k], 1);
							currentPos++;
						}

						if (singleSpans.containsKey(currentPos)) {
							//System.out.println(currentPos + "-" + tokens[k]);
							FileUtils.write(featuresFile, Word2Index
									.getInstance().getClassLabelIndex("B-M")
									+ " qid:" + exampleNumber, true);
							medicaltag = "I-M";
							endMedicaltermSpan = singleSpans.get(currentPos);
						} else if (disjointFirstSpans.containsKey(currentPos)) {
							//System.out.println(currentPos + "-" + tokens[k]);
							FileUtils.write(featuresFile, Word2Index
									.getInstance().getClassLabelIndex("DB-M")
									+ " qid:" + exampleNumber, true);
							medicaltag = "DI-M";
							endMedicaltermSpan = disjointFirstSpans
									.get(currentPos);
						} else if (disjointSecondSpans.containsKey(currentPos)) {
							//System.out.println(currentPos + "-" + tokens[k]);
							FileUtils.write(featuresFile, Word2Index
									.getInstance().getClassLabelIndex("DHB-M")
									+ " qid:" + exampleNumber, true);
							medicaltag = "DHI-M";
							endMedicaltermSpan = disjointSecondSpans
									.get(currentPos);
						} else if (tripletFirstSpans.containsKey(currentPos)) {
							//System.out.println(currentPos + "-" + tokens[k]);
							FileUtils.write(featuresFile, Word2Index
									.getInstance().getClassLabelIndex("TB-M")
									+ " qid:" + exampleNumber, true);
							medicaltag = "TI-M";
							endMedicaltermSpan = tripletFirstSpans
									.get(currentPos);
						} else if (tripletSecondSpans.containsKey(currentPos)) {
							//System.out.println(currentPos + "-" + tokens[k]);
							FileUtils.write(featuresFile, Word2Index
									.getInstance().getClassLabelIndex("THB-M")
									+ " qid:" + exampleNumber, true);
							medicaltag = "THI-M";
							endMedicaltermSpan = tripletSecondSpans
									.get(currentPos);
						} else if (currentPos < endMedicaltermSpan) {
							//System.out.println(currentPos + "-" + tokens[k]);
							FileUtils.write(featuresFile,
									Word2Index.getInstance()
											.getClassLabelIndex(medicaltag)
											+ " qid:" + exampleNumber, true);
						} else {
							FileUtils.write(featuresFile, Word2Index
									.getInstance().getClassLabelIndex("O")
									+ " qid:" + exampleNumber, true);
						}

						// startSentence += tokens[k].length()+1;
						// System.out.println("tokens length = " +
						// tokens[k].length());
						// System.out.println(span+"-"+tokens[k]);
						/*HashSet<Integer> featureIndexesSet = getAllFeatureIndexes (tokens,postags,chunks,k, sectionHeader,
								clefDocType);

						List<Integer> featureIndexes = new ArrayList<Integer>(
								featureIndexesSet);
						Collections.sort(featureIndexes);
						
						for (int orthoIndex : featureIndexes) {
							FileUtils.write(featuresFile, " " + orthoIndex
									+ ":1", true);
						}
						
						//adding word 2 vector
						HashMap<String, List<String>> word2Vectors = Word2Index.getInstance().loadWord2Vectors();
						if(word2Vectors.containsKey(tokens[k].toLowerCase())) {
							int finalidx = Word2Index.getInstance().totalfeaturesAll;
							List<String> wordVecs = word2Vectors.get(tokens[k].toLowerCase());
							for (int l = 0;l < wordVecs.size(); l++) {
								FileUtils.write(featuresFile, " " + (finalidx+l+1)
										+ ":"+wordVecs.get(l), true);
							}
						}*/
						
						FileUtils.write(featuresFile, " " + Word2Index.getInstance().getFeaturesVectorForTheToken
								(tokens, postags, chunks, k, sectionHeaderSubstring, clefDocType) , true);
						
						FileUtils.write(featuresFile, " # " + tokens[k] + "\n",
								true);

						if (tokens[k].startsWith("-")) {
							// tokens[k] = StringUtils.substring(tokens[k], 1);
							currentPos--;
						}

						currentPos += tokens[k].length();
						fileText = StringUtils.substring(fileText,
								(tokens[k].length() + noOfSpaces));
					}
					exampleNumber++;
				}
			}

		}
		trainSVMModel(featuresFile, svmModel);
	}
	
	
	/*private int[] parseCLEF2013PipedFile(String pipeDelimitedLine) {
		String[] slots = pipeDelimitedLine.split("\\|\\|");
		System.out.println(pipeDelimitedLine);
		int[] spanCUI = new int[slots.length-3];
		for (int i=3,j=0 ; i < slots.length; i++,j++) {
			spanCUI[j]=Integer.parseInt(slots[i]);
			System.out.println(Integer.parseInt(slots[i]));
		}
		
		return spanCUI;
	}
*/
	
	/**
	 * Returns document type. Ex : dischargeSummary, ECHO-REPORT,
	 * 
	 * @param name
	 * @return
	 */
	private String getDocumentType(String docname) {
		if (docname.contains("ECG_REPORT")) {
			return "ECG_REPORT";
		}
		if (docname.contains("ECHO_REPORT")) {
			return "ECHO_REPORT";
		}
		if (docname.contains("DISCHARGE_SUMMARY")) {
			return "DISCHARGE_SUMMARY";
		}
		if (docname.contains("RADIOLOGY_REPORT")) {
			return "RADIOLOGY_REPORT";
		}
		return "X";
	}

	int totalWithExamples = 0;
	int totalWithoutExamples = 0;

	/**
	 * Generates and trains SVM model with 3 classes B-M, I-M and O
	 * 
	 * @param corpusDir
	 * @param pipeDir
	 * @param featuresFileString
	 * @param svmModel
	 * @param triClassFlag
	 * @throws IOException
	 */
	/*
	 * public void generateFeaturesAndTrainSVM (String corpusDir, String
	 * pipeDir, String featuresFileString, String svmModel) throws IOException {
	 * 
	 * FileUtils.deleteQuietly(new File(featuresFileString)); File featuresFile
	 * = new File(featuresFileString); int exampleNumber = 1;
	 * LinkedHashMap<Integer, List<String>> exampleNumberToFeatureLines = new
	 * LinkedHashMap<Integer, List<String>>(); Set<Integer>
	 * examplesWithMedicalTerms = new HashSet<Integer>();
	 * 
	 * File[] corpusFiles = new File(corpusDir).listFiles(); for(File corpusFile
	 * : corpusFiles) { int start = 0; int end = 0; List<String>
	 * pipeDelimitedLines = null; String corpuslines = ""; try { //read the
	 * piped files of the discharge summary files corpuslines =
	 * FileUtils.readFileToString(corpusFile); File pipeFile = new File(pipeDir
	 * + "/" + corpusFile.getName()); //+
	 * StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));
	 * 
	 * if (corpusFile.getName().contains(".txt")) pipeFile = new File(pipeDir +
	 * "/" + StringUtils.replace(corpusFile.getName(), "txt", "pipe.txt"));
	 * 
	 * 
	 * if(!pipeFile.exists()) { System.out.println("No pipe file found = " +
	 * pipeFile); continue; }
	 * 
	 * pipeDelimitedLines = FileUtils.readLines(pipeFile); } catch (IOException
	 * e) { e.printStackTrace(); }
	 * 
	 * //For each line of the piped file. Each line has a medical term with
	 * spans. for (String pipeDelimitedLine : pipeDelimitedLines) {
	 * 
	 * String[] slots = pipeDelimitedLine.split("\\|");
	 * 
	 * String[] spanString = slots[1].split(","); String[] CUIString =
	 * slots[3].split(",");
	 * 
	 * int[] spanCUI = new int[spanString.length * 2];
	 * 
	 * //Iterate on each span of each line. for (int i = 0; i <
	 * spanString.length; i++) {
	 * 
	 * spanCUI[i * 2] = Integer.parseInt(spanString[i].split("-")[0]); spanCUI[i
	 * * 2 + 1] = Integer.parseInt(spanString[i].split("-")[1]); int j =
	 * CUIString.length; if (i<j) { j=i; } else { j = CUIString.length-1; }
	 * 
	 * end = spanCUI[i * 2]; //For first line 0 < start of first span if(start <
	 * end) {
	 * 
	 * //get sentence apart from medical term String remainingSentence =
	 * StringUtils.substring(corpuslines,start,end);
	 * if(remainingSentence.equals(" ")) { continue; } else
	 * if(remainingSentence.startsWith(" ")) { remainingSentence =
	 * StringUtils.removeStart(remainingSentence, " "); } else if
	 * (remainingSentence.equals("\n")) { //FileUtils.write(featuresFile, "\n",
	 * true); }
	 * 
	 * //System.out.println("start-End = " + start + "\t" + end +
	 * remainingSentence);
	 * 
	 * //Get all sentences of text(other than medical terms) and append with 'O'
	 * String[] sentences =
	 * SentenceBreaker.getSentenceBreaker().getSentences(remainingSentence); for
	 * (String sentence : sentences) { String[] tokens =
	 * OpenNLPTokenizer.getOpenNLPTokenizer().tokenize(sentence); String[] tags
	 * = POSTagger.getPOSTagger().tag(tokens); String[] chunks =
	 * Chunker.getChunker().getChunks(tokens, tags);
	 * 
	 * //System.out.println("Sentence == " + sentence); for (int k =0 ; k <
	 * tokens.length; k++) { //nlp features // FileUtils.write(featuresFile,
	 * Word2Index.getInstance().getClassLabelIndex("O") + // " qid:" +
	 * exampleNumber , true); //System.out.println(tokens[k] + "\t"+ tags[k]
	 * +"\t" + chunks[k] ); StringBuffer sb = new StringBuffer(); sb.append(
	 * Word2Index.getInstance().getClassLabelIndex("O") + " qid:" +
	 * exampleNumber);
	 * 
	 * HashSet<Integer> featureIndexesSet =
	 * Word2Index.getInstance().getOrthograhicIndexes(tokens[k]);
	 * featureIndexesSet.add(Word2Index.getInstance().getWordIndex(tokens[k]));
	 * featureIndexesSet
	 * .add(Word2Index.getInstance().getStemIndex(WordStemmer.getStemmer
	 * ().stem(tokens[k])));
	 * featureIndexesSet.add(Word2Index.getInstance().getPosIndex(tags[k]));
	 * featureIndexesSet.add(Word2Index.getInstance().getChunkIndex(chunks[k]));
	 * featureIndexesSet
	 * .add(Word2Index.getInstance().getPrefixIndex(tokens[k]));
	 * featureIndexesSet
	 * .add(Word2Index.getInstance().getSuffixIndex(tokens[k]));
	 * 
	 * List<Integer> featureIndexes = new ArrayList<Integer>(featureIndexesSet);
	 * Collections.sort(featureIndexes); for (int orthoIndex : featureIndexes) {
	 * // FileUtils.write(featuresFile, " " + orthoIndex + ":1" , true);
	 * sb.append(" " + orthoIndex + ":1"); } // FileUtils.write(featuresFile,
	 * " # " + tokens[k]+ "\n" , true); sb.append( " # " + tokens[k]+ "\n" );
	 * 
	 * HashMapUtils.addValueToListForKeyMap(exampleNumberToFeatureLines,
	 * exampleNumber, sb.toString());
	 * 
	 * if(tokens[k].equals(".") || tokens[k].equals("\n")) { exampleNumber++; }
	 * } }
	 * 
	 * }
	 * 
	 * String word = StringUtils.substring(corpuslines, spanCUI[i * 2],
	 * spanCUI[i * 2 + 1]+1); start = spanCUI[i * 2 + 1]+1;
	 * 
	 * //System.out.println(word + "\tterm"); //Iterate on spans of the medical
	 * term and append the B-M or I-M label //Abdominal wall if
	 * (!word.isEmpty()) { String label = "B-M";
	 * 
	 * String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize(word);
	 * String[] tags = POSTagger.getPOSTagger().tag(tokens); String[] chunks =
	 * Chunker.getChunker().getChunks(tokens, tags);
	 * 
	 * for (int k= 0; k< tokens.length; k++) { //nlp features
	 * 
	 * // FileUtils.write(featuresFile,
	 * Word2Index.getInstance().getClassLabelIndex(label) + // " qid:" +
	 * exampleNumber , true); StringBuffer sb = new StringBuffer();
	 * sb.append(Word2Index.getInstance().getClassLabelIndex(label) + " qid:" +
	 * exampleNumber );
	 * 
	 * HashSet<Integer> featureIndexesSet =
	 * Word2Index.getInstance().getOrthograhicIndexes(tokens[k]);
	 * featureIndexesSet.add(Word2Index.getInstance().getWordIndex(tokens[k]));
	 * featureIndexesSet
	 * .add(Word2Index.getInstance().getStemIndex(WordStemmer.getStemmer
	 * ().stem(tokens[k])));
	 * featureIndexesSet.add(Word2Index.getInstance().getPosIndex(tags[k]));
	 * featureIndexesSet.add(Word2Index.getInstance().getChunkIndex(chunks[k]));
	 * List<Integer> featureIndexes = new ArrayList<Integer>(featureIndexesSet);
	 * Collections.sort(featureIndexes); for (int orthoIndex : featureIndexes) {
	 * // FileUtils.write(featuresFile, " " + orthoIndex + ":1" , true);
	 * sb.append(" " + orthoIndex + ":1"); } // FileUtils.write(featuresFile,
	 * " # " + tokens[k]+ "\n" , true); sb.append( " # " + tokens[k]+ "\n" );
	 * //add to the list for the examplenumber
	 * HashMapUtils.addValueToListForKeyMap(exampleNumberToFeatureLines,
	 * exampleNumber, sb.toString());
	 * examplesWithMedicalTerms.add(exampleNumber);
	 * 
	 * label = "I-M"; } } } }
	 * 
	 * //for each file writeIntoFeaturesFile(featuresFile,
	 * exampleNumberToFeatureLines, examplesWithMedicalTerms);
	 * exampleNumberToFeatureLines = new LinkedHashMap<Integer, List<String>>();
	 * examplesWithMedicalTerms = new HashSet<Integer>();
	 * 
	 * }
	 * 
	 * trainSVMModel(featuresFile, svmModel);
	 * ///home/raghavendra/BackUP/tools/SVM/svm_hmm
	 * //System.out.println(totalWithExamples +"="+totalWithoutExamples); }
	 */

	/**
	 * Flushes the features in to a file.
	 * 
	 * @param featuresFile
	 *            Features file
	 * @param exampleNumberToFeatureLines
	 *            For each token, maps sentence number to features.
	 * @param examplesWithMedicalTerms
	 *            List of sentence numbers which has at least one medical term.
	 * @throws IOException
	 */
	private void writeIntoFeaturesFile(File featuresFile,
			LinkedHashMap<Integer, List<String>> exampleNumberToFeatureLines,
			Set<Integer> examplesWithMedicalTerms) throws IOException {
		int examplesSize = examplesWithMedicalTerms.size();

		for (Entry<Integer, List<String>> entry : exampleNumberToFeatureLines
				.entrySet()) {
			if (examplesWithMedicalTerms.contains(entry.getKey())) {
				totalWithExamples++;
				for (String item : entry.getValue()) {
					FileUtils.write(featuresFile, item, true);
				}
			} else if (examplesSize-- > 0) {
				totalWithoutExamples++;
				for (String item : entry.getValue()) {
					FileUtils.write(featuresFile, item, true);
				}
			}
		}

	}

	/**
	 * Trains SVM model from the given features file.
	 * 
	 * @param featuresFile
	 *            Input features file
	 * @param svmModel
	 *            output trained model
	 */
	private void trainSVMModel(File featuresFile, String svmModel) {
		// TODO Auto-generated method stub
		Process p;
		try {
			// /svm_hmm_learn -c 5 -e 0.5
			// example7/declaration_of_independence.dat declaration.model
			ProcessBuilder builder = new ProcessBuilder(
					"/home/cde/Raghavendra/tools/SVM/svm_hmm/svm_hmm_learn",
					"-c", "5000", "-e", "0.1", 
					featuresFile.getAbsolutePath(), svmModel);
			p = builder.start();

			p.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Trained SVM model " + svmModel + " for features "
				+ featuresFile);

	}

	void extractMedicalTermsFromSentence () {
		
	}
	
	/**
	 * Extracts the medical terms using the trained SVM model.
	 * 
	 * @param corpusDir
	 *            Contains discharge summaries
	 * @param pipedOutputDir
	 *            Contains piped outputs of discharge summaries
	 * @param svmModel
	 *            SVM model
	 * @param triClassFlag
	 * @throws IOException
	 */
	public void svmTestBNER(String corpusDir, String pipedOutputDir,
			String svmModel) throws IOException {

		
		File modelOutputLabels = null;
		File[] corpusFiles = new File(corpusDir).listFiles();
		for (File corpusFile : corpusFiles) {
			
			String featuresFileString = "tempssvm/temp.txt_"+corpusFile.getName();
			System.out.println("Testing = " + featuresFileString);
			File featuresFile = new File(featuresFileString);
			// generate Features File - temp.txt
			generateTestFeaturesFile(corpusFile, featuresFile);
			// Genereate labels for the features file using SVM model -
			modelOutputLabels = getLabelsForFeaturesFileUsingSVMmodel(
					featuresFile, svmModel);
			
			
			// Post Prosessing class labels
//			 File labelsPostProcessed = postProcessingLabels(featuresFile, modelOutputLabels);

			// System.out.println("PP lines = " +
			// FileUtils.readLines(labelsPostProcessed).size());
			// generate pipe file from featuresFile
			// createPipeFileFromFeaturesFile(corpusFile, labelsPostProcessed,
			// pipedOutputDir);

			// createPipeFileFromFeaturesFile(corpusFile, modelOutputLabels,
			// pipedOutputDir);
			clefPipedLineParser.createPipeFileFromFeaturesFileWithPPWithBIODTtags(corpusFile, featuresFile,
					modelOutputLabels, pipedOutputDir, "svm");
			
			/*clefPipedLineParser.createPipeFileFromFeaturesFileWithPPWithBIODTtags(corpusFile, featuresFile,
					labelsPostProcessed, pipedOutputDir, "svm");*/
			
			// FileUtils.deleteQuietly(new File(featuresFileString));
		}
		// FileUtils.deleteQuietly(new File(featuresFileString));
		// FileUtils.deleteQuietly(featuresFileWithLabel);
		System.out.println("Tested using model " + svmModel);
	}

	/**
	 * Post processing the SVM model piped outputs. Outputs all occurrences of
	 * medical terms in the discharge summary into piped output.
	 * 
	 * @param featuresFile
	 * @param modelOutputLabels
	 * @return
	 */
	public File postProcessingLabels(File featuresFile, File modelOutputLabels) {

		File labelsPostProcessed = new File(featuresFile.getAbsolutePath()
				+ ".labels_pp");
		System.out.println("PP = " + labelsPostProcessed.getName());
		
		/*try {
			
			HashSet<String> medicalTerms = new HashSet<String>();

			// Preprocess - 1
			// get all the tokens that are tagged as medical terms
			for (int i = 0; i < featuresLines.size(); i++) {
				String[] split = featuresLines.get(i).split("#");
				if (split.length == 2) {
					String token = split[1];
					if (modelLabels.get(i).equals(1)) {
						medicalTerms.add(token);
					}
				}
			}
			for (int i = 0; i < featuresLines.size(); i++) {
				String[] split = featuresLines.get(i).split("#");
				if (split.length == 2) {
					String token = split[1];
					if (medicalTerms.contains(token)) {
						modelLabels.set(i, "1");
					}
				}
			}

			FileUtils.writeLines(labelsPostProcessed, modelLabels);

		} catch (IOException e) {
			e.printStackTrace();
		}*/
		try {
			List<String> featuresLines = FileUtils.readLines(featuresFile);
			List<String> modelLabels = FileUtils.readLines(modelOutputLabels);
			String medicalterm = "";
//			System.out.println("medical terms size = " + medicalTerms.size());
			for (int k = 0; k < featuresLines.size(); k++) {
				String[] split = featuresLines.get(k).split("#");
				if (split.length == 2 && modelLabels.get(k).equals("11")) {
					
					medicalterm = split[1];
					boolean flag = false;
					
					if (medicalterm.length()>2 || StringUtils.isAllUpperCase(medicalterm) ) {
						StringBuffer sb = new StringBuffer();
						if (k-3>=0) {
							String[] tempsplit = featuresLines.get(k-3).split("#");
							if(tempsplit.length == 2) { 
								sb.append(tempsplit[1] + " ");
							}
						}
						if (k-2>=0) {
							String[] tempsplit = featuresLines.get(k-2).split("#");
							if(tempsplit.length == 2) { 
								sb.append(tempsplit[1] + " ");
							}
						}
						if (k-1>=0) {
							String[] tempsplit = featuresLines.get(k-1).split("#");
							if(tempsplit.length == 2) { 
								sb.append(tempsplit[1] + " ");
							}
						}
						if (k>=0) {
							String[] tempsplit = featuresLines.get(k).split("#");
							if(tempsplit.length == 2) { 
								sb.append(tempsplit[1] + " ");
							}
						}
						
						if (k+1<=modelLabels.size()-1) {
							String[] tempsplit = featuresLines.get(k+1).split("#");
							if(tempsplit.length == 2) { 
								sb.append(tempsplit[1] + " ");
							}
						}
						if (k+2<=modelLabels.size()-1) {
							String[] tempsplit = featuresLines.get(k+2).split("#");
							if(tempsplit.length == 2) { 
								sb.append(tempsplit[1] + " ");
							}
						}
						if (k+3<=modelLabels.size()-1) {
							String[] tempsplit = featuresLines.get(k+3).split("#");
							if(tempsplit.length == 2) { 
								sb.append(tempsplit[1] + " ");
							}
						}
						
						List<String> bigrams = ClinicalTokenizer.getNGrams(sb.toString(), 2); 
						for (String bigram : bigrams) {
							if (bigram.contains(medicalterm)) {
								if (Word2Index.getInstance().hitachiTrainDictTermOwnDict.contains(bigram) 
										|| java2MySql.isDiseaseDisorderSemanticTypeTerm(bigram)) {
									modelLabels.set(k, "2");
									flag = true;
								}
							}
						}
						
						if (!flag) {
							List<String> trigrams = ClinicalTokenizer.getNGrams(sb.toString(), 3); 
							for (String trigram : trigrams) {
								if (trigram.contains(medicalterm)) {
									if (Word2Index.getInstance().hitachiTrainDictTermOwnDict.contains(trigram) 
											|| java2MySql.isDiseaseDisorderSemanticTypeTerm(trigram) ) {
										modelLabels.set(k, "2");
										flag = true;
									}
								}
							}
						}
						
						if (!flag) {
							List<String> trigrams = ClinicalTokenizer.getNGrams(sb.toString(), 4); 
							for (String trigram : trigrams) {
								if (trigram.contains(medicalterm)) {
									if (Word2Index.getInstance().hitachiTrainDictTermOwnDict.contains(trigram) 
											|| java2MySql.isDiseaseDisorderSemanticTypeTerm(trigram)) {
										modelLabels.set(k, "2");
										flag = true;
									}
								}
							}
						}
						
						if (!flag) {
							if (Word2Index.getInstance().hitachiTrainDictTermOwnDict.contains(medicalterm) 
									|| java2MySql.isDiseaseDisorderSemanticTypeTerm(medicalterm)) {
								modelLabels.set(k, "1");
								flag = true;
							}
						}
						System.out.println(sb.toString()+"\t"+medicalterm +"\t"+ flag);
						
					}
					
					/*
					if (!flag) {
						postProcessedLines.add(modelLabels.get(k));
					}*/
						
					
				} /*else {
					postProcessedLines.add(modelLabels.get(k));
				} 
			*/
			}
			
			FileUtils.writeLines(labelsPostProcessed, modelLabels);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return labelsPostProcessed;
	}

	/**
	 * Generate features for the test File.
	 * 
	 * @param corpusFile
	 * @param featuresFile
	 * @return
	 */
	File generateTestFeaturesFile(File corpusFile, File featuresFile) {
		FileUtils.deleteQuietly(featuresFile);
		int exampleNumber = 1;
		// String corpuslines = "";
		try {
			// corpuslines = FileUtils.readFileToString(corpusFile);
			List<String> lines = FileUtils.readLines(corpusFile);

			String sectionHeader = "";
			String clefDocType = getDocumentType(corpusFile.getName());
			for (int i = 0; i < lines.size(); i++) {

				// get section header
				String sectionHeaderSubstring = StringUtils.substringBefore(
						lines.get(i), ":");
				if (Word2Index.getInstance().sectionHeadersTop
						.contains(sectionHeaderSubstring.toLowerCase())) {
					sectionHeader = sectionHeaderSubstring;
				}

				String[] sentences = SentenceBreaker.getSentenceBreaker()
						.getSentences(lines.get(i));
				for (String sentence : sentences) {
					getSVMNLPFeaturesForSentence(sentence, featuresFile,
							exampleNumber++, sectionHeader, clefDocType);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return featuresFile;
	}

	/**
	 * Get the labels for the features file.
	 * 
	 * @param featuresFile
	 * @return featuresFile With labels
	 */
	File getLabelsForFeaturesFileUsingSVMmodel(File featuresFile,
			String svmModel) {

		String inputFile = featuresFile.getAbsolutePath();
		String outputTags = featuresFile.getAbsolutePath() + "_WithLabel";
		System.out.println(inputFile + "\n" + outputTags);
		FileUtils.deleteQuietly(new File(outputTags));
		try {
			new File(outputTags).createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Process p;
		try {
			ProcessBuilder builder = new ProcessBuilder(
					"/home/cde/Raghavendra/tools/SVM/svm_hmm/svm_hmm_classify",
					inputFile, svmModel, outputTags);
			p = builder.start();

			p.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

		/*
		 * try { System.out.println(FileUtils.readFileToString(new
		 * File(outputFile))); } catch (IOException e) { e.printStackTrace(); }
		 */

		return new File(outputTags);
	}

	
	
	private Span[] getSpanArray(String fileText, String[] tokens) {
		
		int currentPos = 0;
		Span[] spanArray = new Span[tokens.length];
		for (int k=0; k< tokens.length;k++) {
		
			int noOfSpaces = StringUtils.indexOf(fileText,
					tokens[k]);
			
			currentPos += noOfSpaces;
			
			Span span = new Span(currentPos, (currentPos+tokens[k].length()));
			spanArray[k] = span;
			
			currentPos += tokens[k].length();
			fileText = StringUtils.substring(fileText,
					(tokens[k].length() + noOfSpaces));
		}
		
		return spanArray;
	}

	
	
	/**
	 * Creates piped output file from the features file.
	 * 
	 * @param corpusFile
	 * @param labelFile
	 * @param pipedOutputDir
	 */
	private void createPipeFileFromFeaturesFile(File corpusFile,
			File labelFile, String pipedOutputDir) {

		try {
			String fileText = FileUtils.readFileToString(corpusFile);
			//opennlp
			String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize(
					fileText);
			
			//stanford
			/*ArrayList<StanfordToken> stanfordtokensList = StanfordUtils.processStr(fileText);
			String[] tokens = StanfordUtils.getTokens(stanfordtokensList);*/
			
			File pipedOutputFile = new File(pipedOutputDir + "/"
					+ corpusFile.getName().replace(".txt", ".pipe.txt"));
			FileUtils.deleteQuietly(pipedOutputFile);
			System.out.println("creating piped file = "
					+ pipedOutputFile.getName());

			List<String> lines = FileUtils.readLines(labelFile);
			int span = 0, spanstart = 0, spanend = 0;
			String medicalterm = "";
			for (int i = 0; i < lines.size(); i++) {
				// System.out.println(line);
				if (StringUtils.startsWith(lines.get(i), "# ")
						|| lines.get(i).isEmpty())
					continue;
				// if (StringUtils.countMatches(line, "\t") == 10) {

				if (lines.get(i).equals("1")) {

					if (!medicalterm.isEmpty()) {
						if (!(StringUtils.countMatches(medicalterm, "|") > 0))
							FileUtils
									.write(pipedOutputFile,
											corpusFile.getName()
													+ "|"
													+ spanstart
													+ "-"
													+ (spanstart + medicalterm
															.length()) + "|"
													+ medicalterm + "\n", true);
						medicalterm = "";
					}

					// no of spaces
					int noOfSpaces = StringUtils.indexOf(fileText, tokens[i]);
					// space + length of string

					// at medical term start
					span += noOfSpaces;
					spanstart = span;

					medicalterm = tokens[i];
					span += tokens[i].length();
					spanend = span;
					// get remaining sentence from the end of the string
					fileText = StringUtils.substring(fileText,
							(tokens[i].length() + noOfSpaces));

				} else if (lines.get(i).equals("2")) {
					int noOfSpaces = StringUtils.indexOf(fileText, tokens[i]);
					// at medical term start
					span += noOfSpaces;

					if (medicalterm.isEmpty()) {
						spanstart = span;
						medicalterm = tokens[i];
					} else {
						medicalterm = medicalterm + " " + tokens[i];
						System.out.println("at 2 " + spanstart + " - "
								+ medicalterm);
					}
					span += tokens[i].length();
					spanend = span;
					// get remaining sentence from the end of the string
					fileText = StringUtils.substring(fileText,
							(tokens[i].length() + noOfSpaces));
				} else if (lines.get(i).equals("3")) {

					if (!medicalterm.isEmpty()) {
						if (!(StringUtils.countMatches(medicalterm, "|") > 0))
							FileUtils.write(pipedOutputFile,
									corpusFile.getName() + "|" + spanstart
											+ "-" + (spanend) + "|"
											+ medicalterm + "\n", true);
						medicalterm = "";
					}

					// System.out.println(line);
					// position from start of the fileText. (space*)text. It
					// just calculates number of spaces.
					int noOfSpaces = StringUtils.indexOf(fileText, tokens[i]);
					// space + length of string
					span += tokens[i].length() + noOfSpaces;
					// get remaining sentence from the end of the string
					fileText = StringUtils.substring(fileText,
							(tokens[i].length() + noOfSpaces));

				}

			}
			// }

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Extracts features for all tokens of the sentence. The each line in the
	 * output features file corresponds to features of each token.
	 * 
	 * @param sentence
	 * @param featuresFile
	 * @param exampleNumber
	 * @return
	 * @throws IOException
	 */
	File getSVMNLPFeaturesForSentence(String sentence, File featuresFile,
			int exampleNumber, String sectionHeader, String clefDocType)
			throws IOException {
		
		/*ArrayList<StanfordToken> tokensList = StanfordUtils.processStr(sentence);
		String[] tokens = StanfordUtils.getTokens(tokensList);
		String[] tags = StanfordUtils.getPOSTags(tokensList);
		String[] lemmas = StanfordUtils.getLemmas(tokensList);
		String[] chunks = new String[0];*/

		String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize(
				sentence);
		String[] postags = POSTagger.getPOSTagger().tag(tokens);
		String[] chunks = Chunker.getChunker().getChunks(tokens, postags);

		// System.out.println("Sentence == " + sentence);
		for (int k = 0; k < tokens.length; k++) {
			// nlp features
			FileUtils.write(featuresFile, Word2Index.getInstance()
					.getClassLabelIndex("O") + " qid:" + exampleNumber, true);

			/*HashSet<Integer> featureIndexesSet = getAllFeatureIndexes (tokens,tags,chunks,k, sectionHeader,
					clefDocType);

			List<Integer> featureIndexes = new ArrayList<Integer>(
					featureIndexesSet);
			Collections.sort(featureIndexes);
			for (int orthoIndex : featureIndexes) {
				FileUtils.write(featuresFile, " " + orthoIndex + ":1", true);
			}
			//adding word 2 vector
			HashMap<String, List<String>> word2Vectors = Word2Index.getInstance().loadWord2Vectors();
			if(word2Vectors.containsKey(tokens[k].toLowerCase())) {
				int finalidx = Word2Index.getInstance().totalfeaturesAll;
				List<String> wordVecs = word2Vectors.get(tokens[k].toLowerCase());
				for (int l = 0;l < wordVecs.size(); l++) {
					FileUtils.write(featuresFile, " " + (finalidx+l+1)
							+ ":"+wordVecs.get(l), true);
				}
			}
*/			
			FileUtils.write(featuresFile, " " + Word2Index.getInstance().getFeaturesVectorForTheToken
					(tokens, postags, chunks, k, sectionHeader, clefDocType) , true);
			
			FileUtils.write(featuresFile, " # " + tokens[k] + "\n", true);
		}
		// System.out.println();
		return featuresFile;
	}
	
	public static void main(String[] args) throws IOException {
		SVMModelGenerator svmFeaturesGenerator = new SVMModelGenerator();

		System.out.println(args.length);
		long start = System.currentTimeMillis();
		// pipedOutputReader.getEntitiesFromWikiFile();
		// train file

		/*
		 * if (args.length == 0)
		 * svmFeaturesGenerator.generateFeaturesAndTrainSVM
		 * ("./finaldata/clef2013Dataset/train3/DS",
		 * "./finaldata/clef2013Dataset/train3/DSPIPED",
		 * "finaldata/clef2013Dataset/model_dump/svm_train3_features_feb18.txt"
		 * , svmFeaturesGenerator.svmmodel);
		 */

		// test file with model argument multiple e and C values
		 if(args.length == 2) { 
			 System.out.println("DSPIPED OP = "+args[0]);
			 System.out.println("model = "+ args[1]);
			 new File(args[0]).mkdir();
			 svmFeaturesGenerator.svmTestBNER("./finaldata/clef2013Dataset/test/DS",
			 //"./finaldata/clef2013Dataset/test1/DSPIPED_SVMOP_WUMLS",
			 args[0], 
			 // svmFeaturesGenerator.svmmodel, true); 
			 args[1]); 
		 } else  if(args.length == 1) { 
			
		 } else {

			 
				// on hitachi whole dataset
				svmFeaturesGenerator.generateFeaturesAndTrainSVMWithBIODTTag("./finaldata/phase9/train_1/DS",
						"./finaldata/phase9/train_1/DSPIPED",
						"./finaldata/phase9/model_dump/may16_svm_train_features.txt_1",
						"./finaldata/phase9/model_dump/may16_svm_train_features.model_1"
						); // args[1], args[0]);
				 
				//  hitachi test file
				new File("./finaldata/phase9/1/may16_DSPIPED_SVMOP").mkdir();
				 svmFeaturesGenerator.svmTestBNER("./finaldata/phase9/1/DS",
					 "./finaldata/phase9/1/may16_DSPIPED_SVMOP",
					 "./finaldata/phase9/model_dump/may16_svm_train_features.model_1"
				 );
				 
		
				// on sample documents
			// sample hitachi train
				/*svmFeaturesGenerator.generateFeaturesAndTrainSVMWithBIODTTag("./finaldata/phase4/train1/DS",
						"./finaldata/phase4/train1/DSPIPED",
						"./finaldata/phase4/model_dump/apr30_svm_train1_features.txt",
						"./finaldata/phase4/model_dump/apr30_svm_train1_features.model"
						); // args[1], args[0]);
				 
				//  hitachi test file
				new File("./finaldata/phase4/test1/apr30_DSPIPED_SVMOP").mkdir();
				 svmFeaturesGenerator.svmTestBNER("./finaldata/phase4/test1/DS",
					 "./finaldata/phase4/test1/apr30_DSPIPED_SVMOP",
					 "./finaldata/phase4/model_dump/apr30_svm_train1_features.model"
				 );*/
				 
			 
			// testing training features featuresfile, model
			// on CLEF2013 whole dataset
			/*svmFeaturesGenerator.generateFeaturesAndTrainSVMWithBIODTTag("./finaldata/clef2013Dataset/train/DS",
					"./finaldata/clef2013Dataset/train/DSPIPED",
					"./finaldata/clef2013Dataset/model_dump/may9_svm_clef13_train_features.txt",
					"./finaldata/clef2013Dataset/model_dump/may9_svm_clef13_train_features.model"
					); // args[1], args[0]);
*/			 
			//  test file
			/*new File("./finaldata/clef2013Dataset/test/may9_DSPIPED_SVMOP").mkdir();
			 svmFeaturesGenerator.svmTestBNER("./finaldata/clef2013Dataset/test/DS",
				 "./finaldata/clef2013Dataset/test/may9_DSPIPED_SVMOP",
				 "./finaldata/clef2013Dataset/model_dump/may9_svm_clef13_train_features.model"
			 );
			 */
			// generate test features file
			/*svmFeaturesGenerator.generateTestFeaturesFile(new
			 File("./finaldata/test1_5/DS"), new
			 File("model_dump/svm_test1_features_equal.txt"));*/
			 
	
			// on sample documents
			// sample train
			/*svmFeaturesGenerator
					.generateFeaturesAndTrainSVMWithBIODTTag(
							"./finaldata/clef2013Dataset/train1/DS",
							"./finaldata/clef2013Dataset/train1/DSPIPED",
							"./finaldata/clef2013Dataset/model_dump/may9_svm_clef13_train1_features.txt",
							"./finaldata/clef2013Dataset/model_dump/may9_svm_clef13_train1_features.model");
			// args[1], args[0]);
	
			// sample test
			new File(
					"./finaldata/clef2013Dataset/test1/apr20_DSPIPED_SVMOP")
					.mkdir();
			svmFeaturesGenerator
					.svmTestBNER(
							"./finaldata/clef2013Dataset/test1/DS",
							"./finaldata/clef2013Dataset/test1/may9_DSPIPED_SVMOP",
						"./finaldata/clef2013Dataset/model_dump/may9_svm_clef13_train1_features.model");*/
		
			
			/*svmFeaturesGenerator.createPipeFileFromFeaturesFileWithPPWithBIODTtags
			(new File("finaldata/clef2013Dataset/test1/DS/21312-018707-DISCHARGE_SUMMARY.txt"), 
					new File("temp.txt"),
					new File("temp.txt.WithLabel"),
					"./finaldata/clef2013Dataset/test1/apr18_DSPIPED_SVMOP_WUMLS_PP_DUP");
			
			*/
				 
				/*svmFeaturesGenerator.createPyBrainFilesWithBIODTTag(
						"./finaldata/clef2013Dataset/test/DS",
						"./finaldata/clef2013Dataset/test/DSPIPED");*/
			
		}

		System.out.println("time taken = "
				+ (System.currentTimeMillis() - start) / (1000 * 60.0));
	}

}
