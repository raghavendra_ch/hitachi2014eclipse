package edu.iiit.cde.r.umls;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Contains all dictionary files created for tagging medical term with disease
 * @author raghavendra
 *
 */
public class MedicalDictionary {
	
	public List<String> cancerList = new ArrayList<String> ();
	public List<String> contextcancerList = new ArrayList<String> ();
	public List<String> notcancerList = new ArrayList<String> ();
	
	/**
	 * Constructor.
	 */
	public MedicalDictionary() {
		
		try {
			loadCancerFile () ;
			loadContextCancerFile();
			loadNotCancerFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load cancer related terms.
	 * @throws IOException
	 */
	private void loadCancerFile() throws IOException {
		cancerList = FileUtils.readLines(new File("dictionaryFiles/cancer1_file1b_pm_clef.txt"));		
	}
	
	/**
	 * Load terms related to cancer contextually.
	 * @throws IOException
	 */
	private void loadContextCancerFile() throws IOException {
		contextcancerList = FileUtils.readLines(new File("dictionaryFiles/cancer2_pm_clef.txt"));		
	}

	/**
	 * Load the terms that are not related to cancer.
	 * @throws IOException
	 */
	private void loadNotCancerFile() throws IOException {
		notcancerList = FileUtils.readLines(new File("dictionaryFiles/notCancer_pm_clef.txt"));
	}

	/**
	 * Returns true if term/cui is  a cancer.
	 * @param term
	 * @return
	 */
	public boolean isCancer (String cui) {
		
		for (String line : cancerList) {
			
			if (StringUtils.substringBefore(line,"_").equalsIgnoreCase(cui)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns true if term/cui is  a context cancer.
	 * @param term
	 * @return 
	 */
	public boolean isContextCancer (String cui) {
		
		for (String line : contextcancerList) {
			if (StringUtils.substringBefore(line,"_").equalsIgnoreCase(cui)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns true if term/cui is not a cancer.
	 * @param term
	 * @return
	 */
	public boolean isNotCancer (String cui) {
		
		for (String line : notcancerList) {
			if (StringUtils.substringBefore(line,"_").equalsIgnoreCase(cui)) {
				return true;
			}
		}
		
		return false;
	}
	

}
