package edu.iiit.cde.r.umls;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import edu.iiit.cde.r.mysql.Java2MySql;
import edu.iiit.cde.r.utils.HashMapUtilsCustom;

/**
 * Creates medicalTerm-diseases index.
 * @author Raghavendra Ch
 *
 */
public class UMLSTermDiseaseIndexer {

	Java2MySql java2MySql; 
	List<String> cancerSynonyms;
	
	public UMLSTermDiseaseIndexer() {
		try {
			java2MySql = new Java2MySql();
			cancerSynonyms = FileUtils.readLines(new File("./conf/cancerSynonyms.txt"));
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	HashSet<String> processedMedicalTermsSet = new HashSet<String>();
	
	/**
	 * Process the medical terms to get the corresponding diseases.
	 * @param diseaseTermsFile
	 * @param termDiseasesOutput
	 * @throws IOException
	 */
	void processDiseaseMedicalTerms (String diseaseTermsFile, String termDiseasesOutput) throws IOException {
		List<String> lines = FileUtils.readLines(new File(diseaseTermsFile));
		
		
		FileUtils.deleteQuietly(new File("definiteTermsWithDisease"));
		FileUtils.deleteQuietly(new File(termDiseasesOutput));
		FileUtils.deleteQuietly(new File("cancerWords_NoDisease"));
		
		int count =0;
		for (String line : lines ) {
				if(StringUtils.countMatches(line,"_") == 2 ) {
					String cui = StringUtils.substringBefore(line, "_");
					String term = StringUtils.substringBetween(line, "_", "_");
					String semanticType =  StringUtils.substringAfterLast(line, "_");
					
					if(!processedMedicalTermsSet.contains(line)) {
						
						if(!StringUtils.isEmpty(getDiseaseOfPhrase(term))) {
							FileUtils.write(new File("definiteTermsWithDisease"), line + "\t" + getDiseaseOfPhrase(term) + "\n" , true);
						}else if(StringUtils.containsIgnoreCase(term, "benign")) {
							FileUtils.write(new File("cancerWords_NoDisease"), line +"\n", true );
							processedMedicalTermsSet.add(line);
							continue;
						}else {
							HashSet<String> diseasesSet = getDiseasesDefinitionsOfCUI (cui) ;
							if(!diseasesSet.isEmpty()) {
								FileUtils.write(new File(termDiseasesOutput), line + "\t" + printHashSet (getDiseasesDefinitionsOfCUI (cui)) + "\n" , true);
							} else {
								FileUtils.write(new File("cancerWords_NoDisease"), line +"\n", true );
							}
						}
						processedMedicalTermsSet.add(line);
					}
					
					//For n-grams 
					String[] split = term.split(" ");
					int spCount = StringUtils.countMatches(term, " ");
					
					for (int i=spCount;i>=1;i--) {
						for (String token : HashMapUtilsCustom.getNGrams(split, i)) {
							cui = java2MySql.getCUIOfMedicalTerm(token);
							if(!StringUtils.isEmpty(cui)) {
								line = cui+"_"+token+"_"+java2MySql.getSemanticTypeOfCUI(cui);
								
								if(!processedMedicalTermsSet.contains(line)) {
									if(!StringUtils.isEmpty(getDiseaseOfPhrase(token))) {
										FileUtils.write(new File("definiteTermsWithDisease"), line + "\t" + getDiseaseOfPhrase(token) + "\n" , true);
									} else { 
										HashSet<String> diseasesSet = getDiseasesDefinitionsOfCUI (cui) ;
										if(diseasesSet.isEmpty() || diseasesSet.size() == 0) {
											FileUtils.write(new File("cancerWords_NoDisease"), line + "\n", true);
										} else {
											FileUtils.write(new File(termDiseasesOutput), 
													line + "\t" + printHashSet(diseasesSet) + "\n" , true);
										}
									}
									processedMedicalTermsSet.add(line);
								}
							} else {
								FileUtils.write(new File("cancerWords_NoDisease"), token +"\n", true );
							}
						}
					}
				}
				count++;
				if(count%100==0)
					System.out.println(count + "\t" + System.currentTimeMillis());
		}
		try {
			java2MySql.closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Returns disease based on the context of the phrase.
	 * @param phrase
	 * @return disease
	 */
	public String getDiseaseOfPhrase(String phrase) {
		
		/*if (StringUtils.containsIgnoreCase(phrase, "arthritis")
				||StringUtils.containsIgnoreCase(phrase, "rheumatism")) {
				return "arthritis";
		} */
		
		for (String cancerTerm : cancerSynonyms) {
			if (StringUtils.containsIgnoreCase(phrase, cancerTerm))  {
				return "cancer";
			}
		}
		/*if(phrase.contains("ALL") || phrase.contains("AML")) 
			return "cancer";
		*/
		/*if (StringUtils.contains(phrase, "AIDS")
				||StringUtils.contains(phrase, "HIV") 
				|| StringUtils.containsIgnoreCase(phrase, "human immunodeficiency virus")
				|| StringUtils.containsIgnoreCase(phrase, "acquired immuno deffeciency syndrome")) {
			return "AIDS";
		} 
		if (StringUtils.containsIgnoreCase(phrase, "diabetes")
				||StringUtils.contains(phrase, "T1D") 
				|| StringUtils.contains(phrase, "T2D"))  {
			return "diabetes";
		}
		
		if (StringUtils.containsIgnoreCase(phrase, "asthma"))  {
			return "asthma";
		}*/
		return null;
	}

	/**
	 * Prints Hashset
	 * @param diseasesOfCUI
	 * @return
	 */
	private String printHashSet(HashSet<String> diseasesOfCUI) {
		
		StringBuffer sb = new StringBuffer();
		for (String str : diseasesOfCUI) {
			sb.append("[" + str + "]\t\t\t\t");
		}
		return sb.toString();
	}

	/**
	 * Analyze how n-gram medical terms associate to the corresponding diseases.
	 * @param diseaseTermsFile
	 * @param termDiseasesOutput
	 * @throws IOException
	 */
	/*void analyzeDiseaseMedicalTerms (String diseaseTermsFile, String termDiseasesOutput) throws IOException {
		List<String> lines = FileUtils.readLines(new File(diseaseTermsFile));
		
		int count =0;
		for (String line : lines ) {
				if(StringUtils.countMatches(line,"_") == 2 ) {
					String cui = StringUtils.substringBefore(line, "_");
					String term = StringUtils.substringBetween(line, "_", "_");
					String semanticType =  StringUtils.substringAfterLast(line, "_");
					HashMap<String,String> ngramToDisease = new HashMap<String, String>();
					ngramToDisease.put(line,  getDiseasesDefinitionsOfCUI (cui).toString());
					//For n-grams 
					String[] split = term.split(" ");
					int spCount = StringUtils.countMatches(term, " ");
					
					for (int i=1;i<=spCount;i++) {
						for (String token : HashMapUtils.getNGrams(split, i)) {
							cui = java2MySql.getCUIOfMedicalTerm(token);
							if(!StringUtils.isEmpty(cui)) {
								semanticType = java2MySql.getSemanticTypeOfCUI(cui);
								ngramToDisease.put(cui+"_"+token+"_"+semanticType,  getDiseasesDefinitionsOfCUI (cui).toString());
							}
						}
					}
					
					FileUtils.write(new File(termDiseasesOutput), ngramToDisease.toString() + "\n", true);
				}
				count++;
				if(count%100==0)
					System.out.println(count + "\t" + System.currentTimeMillis());
		}
		try {
			java2MySql.closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
*/	
	

	/**
	 * Returns all diseases related to the given phrase
	 * @param phrase
	 * @return set of diseases
	 */
	public HashSet<String> getAllDiseasesInPhrase (String phrase) {
		
		HashSet<String> diseases = new HashSet<String>();
		
		if (StringUtils.containsIgnoreCase(phrase, "arthritis")
				||StringUtils.containsIgnoreCase(phrase, "rheumatism")) {
			diseases.add("arthritis");
		} 
		
		for (String cancerTerm : cancerSynonyms) {
			if (StringUtils.containsIgnoreCase(phrase, cancerTerm))  {
				diseases.add("cancer");
				break;
			}
		}
		if (StringUtils.contains(phrase, "AIDS")
				||StringUtils.contains(phrase, "HIV") 
				|| StringUtils.containsIgnoreCase(phrase, "human immunodeficiency virus")
				|| StringUtils.containsIgnoreCase(phrase, "acquired immuno deffeciency syndrome")) {
			diseases.add("AIDS");
		} 
		if (StringUtils.containsIgnoreCase(phrase, "diabetes")
				||StringUtils.contains(phrase, "T1D") 
				|| StringUtils.contains(phrase, "T2D"))  {
			diseases.add( "diabetes");
		}
		
		if (StringUtils.containsIgnoreCase(phrase, "asthma"))  {
			diseases.add( "asthma");
		}
		
		return diseases;
	}
	
	
	/**
	 * Get diseases of the given CUI from UMLS dictionary.
	 * @param cui
	 * @return Diseases set
	 */
	private HashSet<String> getDiseasesDefinitionsOfCUI(String cui) {

		HashSet<String> diseasesSet = new HashSet<String>();
		List<String> defs = java2MySql.getUMLSDefinitionsOfCUI(cui);
		for (String def : defs) {
			if(def.contains("</p>")) {
				def = StringUtils.substringBefore(def, "</p>"); 
			}
			def = def.replaceAll("(\"|\')http.*(\"|\')", "");
			def = def.replaceAll("http.*(\\)|\\[|\\])", "");
			
			if (StringUtils.containsIgnoreCase(def, "arthritis")
			||StringUtils.containsIgnoreCase(def, "rheumatism")) {
				
				diseasesSet.add("arthritis_" + def);
			} 
			

			
			//Exact rules 
			// cancer treatment drug
			for (String cancerTerm : cancerSynonyms) {
				if (StringUtils.contains(def, "A " + cancerTerm.toLowerCase()  ))  {
					diseasesSet.add("cancer1_" + def);
					return diseasesSet;
				} else if (StringUtils.startsWithIgnoreCase(def, cancerTerm.toLowerCase()))  {
					diseasesSet.add("cancer1_" + def);
					return diseasesSet;
				}
			}
			
			//Tumors or cancer
			if(def.contains("Tumors or cancer")) {
				diseasesSet.add("cancer1_" + def);
				return diseasesSet;
			}else if(StringUtils.containsIgnoreCase(def, "usually benign") | 
					StringUtils.containsIgnoreCase(def, "usually malignant")) {
						diseasesSet.add("cancer2_" + def);
						return diseasesSet;
			} else if(StringUtils.containsIgnoreCase(def,"benign") && StringUtils.containsIgnoreCase(def,"malignant")) {
				diseasesSet.add("cancer2_" + def);
				return diseasesSet;
			} 
			
			
			for (String cancerTerm : cancerSynonyms) {
				if (StringUtils.containsIgnoreCase(def, cancerTerm))  {
					
					//context cancer
					String[] defSplit = def.split("(may lead to)|(may cause)|(include)|(treatment of some types of cancer)|(Some cancer treatment)|" +
							"(may be used to treat)|(treatment of)");
					if (defSplit.length == 2) {
						HashSet<String> diseasesSetInPhrase = getAllDiseasesInPhrase(defSplit[1]);
						if(diseasesSetInPhrase.contains("cancer")) {
							diseasesSet.add("cancer2_" + def);
							return diseasesSet;
						}
					}
					
					
					
					//definite cancer
					defSplit = def.split("(progress to)|(used to treat)|(used in)");
					if (defSplit.length == 2) {
						HashSet<String> diseasesSetInPhrase = getAllDiseasesInPhrase(defSplit[1]);
						if(diseasesSetInPhrase.size() == 1 && diseasesSetInPhrase.contains("cancer")) {
							if(containsOtherDiseases(defSplit[1])) {
								diseasesSet.add("cancer2_" + def);
								return diseasesSet;
							} else {
								diseasesSet.add("cancer1_" + def);
								return diseasesSet;
							}
						} else if(diseasesSetInPhrase.size() > 1 && diseasesSetInPhrase.contains("cancer")) {
							diseasesSet.add("cancer1_" + def);
							return diseasesSet;
						}
					}
					
				}
			}
			
			
			if (def.contains("A benign") && !StringUtils.containsIgnoreCase(def,"or malignant")) {
				diseasesSet.add("notcancer_" + def);
				return diseasesSet;
			}else if (def.contains("(not cancer)") && !StringUtils.containsIgnoreCase(def,"malignant")) {
				diseasesSet.add("notcancer_" + def);
				return diseasesSet;
			} else if(def.contains("benign")) {
				diseasesSet.add("notcancer_" + def);
				return diseasesSet;
			} 
				
			if (StringUtils.contains(def, "AIDS")
					||StringUtils.contains(def, "HIV") 
					|| StringUtils.containsIgnoreCase(def, "human immunodeficiency virus")
					|| StringUtils.containsIgnoreCase(def, "acquired immuno deffeciency syndrome")) {
				diseasesSet.add("AIDS_" + def);
			} 
			if (StringUtils.containsIgnoreCase(def, "diabetes")
					||StringUtils.contains(def, "T1D") 
					|| StringUtils.contains(def, "T2D"))  {
				diseasesSet.add("diabetes_" + def);
			}
			
			if (StringUtils.containsIgnoreCase(def, "asthma"))  {
				diseasesSet.add("asthma_" + def);
			}
		}
		return diseasesSet;
	}
	

	public boolean containsOtherDiseases(String string) {
		if (StringUtils.containsIgnoreCase(string, "skin conditions") | StringUtils.containsIgnoreCase(string, "brain tumor")  | 
				StringUtils.containsIgnoreCase(string, "tuberculosis") |
				StringUtils.containsIgnoreCase(string, "PSORIASIS") |
				StringUtils.containsIgnoreCase(string, "wood") |
				StringUtils.containsIgnoreCase(string, "bone") |
				StringUtils.containsIgnoreCase(string, "medical problems") |
				StringUtils.containsIgnoreCase(string, "ACNE"))
			return true;
		else
		return false;
	}
	
	MedicalDictionary medicalDictionary = new MedicalDictionary();
	
	/**
	 * Tag medical term with disease
	 * @param cuitermsFile
	 * @param termsDiseasesOutputFile
	 * @throws IOException
	 */
	public void tagMedicalTermWithDisease (String cuitermsFile, String termsDiseasesOutputFile) throws IOException {

		List<String> lines = FileUtils.readLines(new File(cuitermsFile));
		
		FileUtils.deleteQuietly(new File(termsDiseasesOutputFile));
		
		int count =0;
		for (String line : lines ) {
			if(StringUtils.countMatches(line,"_") == 2 ) {
				String disease = getDiseaseOfMedicalTerm(line);
				FileUtils.write(new File(termsDiseasesOutputFile), line + "\t "+disease+"\n", true);
			}
				count++;
				if(count%100==0)
					System.out.println(count + "\t" + System.currentTimeMillis());
		}
		try {
			java2MySql.closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	}
	
	/**
	 * Return disease of the medical term
	 * @param line
	 * @return
	 */
	public String getDiseaseOfMedicalTerm (String line) {
		
			String cui = StringUtils.substringBefore(line, "_");
			String term = StringUtils.substringBetween(line, "_", "_");
			
			if(medicalDictionary.isCancer(cui)) {
				return "cancer1";
			} else if(medicalDictionary.isContextCancer(cui)) {
				return "cancer2";
			} else if(medicalDictionary.isNotCancer(cui)) {
				return "notcancer";
			} else {
				return getDiseaseByApplyingCancerRules (cui,term);
			}
			
	}
	
	/**
	 * Get disease of the medical term using specified rules.
	 * Returns cancer1 if superphrase containing cancersynonym
	 * 		cancer1_def or cancer2_def for defs rules
	 * 		""	no cancer terms
	 * @param cui
	 * @param term
	 * @return
	 */
	public String getDiseaseByApplyingCancerRules(String cui, String term) {
		
		//if term contains disease
		if(!StringUtils.isEmpty(getDiseaseOfPhrase(term)) && StringUtils.contains(getDiseaseOfPhrase(term), "cancer")) {
			return getDiseaseOfPhrase(term)+"1";
		} 
		
		List<String> defs = java2MySql.getUMLSDefinitionsOfCUI(cui);
		for (String def : defs) {
			if(def.contains("</p>")) {
				def = StringUtils.substringBefore(def, "</p>"); 
			}
			def = def.replaceAll("(\"|\')http.*(\"|\')", "");
			def = def.replaceAll("http.*(\\)|\\[|\\])", "");
			def = def.replace("<p>", " ").trim();
			//Exact rules 
			// cancer treatment drug
			for (String cancerTerm : cancerSynonyms) {
				if (StringUtils.contains(def, "A " + cancerTerm.toLowerCase()  ))  {
					return  "cancer1_" + def;
				}else if (StringUtils.startsWithIgnoreCase(def, cancerTerm.toLowerCase()))  {
					return  "cancer1_" + def;
				}
			}
			
			//Tumors or cancer
			if(def.contains("Tumors or cancer")) {
				return  "cancer2_" + def;
			}else if(StringUtils.containsIgnoreCase(def, "usually benign") | 
					StringUtils.containsIgnoreCase(def, "usually malignant")) {
				return  "cancer2_" + def;
			} else if(StringUtils.containsIgnoreCase(def,"benign") && StringUtils.containsIgnoreCase(def,"malignant")) {
				return  "cancer2_" + def;
			} else if (def.contains("A benign") && !StringUtils.containsIgnoreCase(def,"or malignant")) {
				return "notcancer_" + def;
			}else if (def.contains("(not cancer)") && !StringUtils.containsIgnoreCase(def,"malignant")) {
				return "notcancer_" + def;
			} else if(def.contains("benign")) {
				return "notcancer_" + def;
			} 
				
			
			for (String cancerTerm : cancerSynonyms) {
				if (StringUtils.containsIgnoreCase(def, cancerTerm))  {
					
					//definite cancer
					String[] defSplit = def.split("(progress to)|(used to treat)|(used in)");
					if (defSplit.length == 2) {
						HashSet<String> diseasesSetInPhrase = getAllDiseasesInPhrase(
								StringUtils.substringBefore(defSplit[1], "."));
						if(diseasesSetInPhrase.size() == 1 && diseasesSetInPhrase.contains("cancer")) {
							if(containsOtherDiseases(defSplit[1])) {
								return  "cancer2_" + def;
							} else {
								return  "cancer1_" + def;
							}
						} else if(diseasesSetInPhrase.size() > 1 && diseasesSetInPhrase.contains("cancer")) {
							return  "cancer2_" + def;
						}
					}
					
					//context cancer
					defSplit = def.split("(may lead to)|(may cause)|(include)|(treatment of some types of cancer)|(Some cancer treatment)|" +
							"(may be used to treat)|(treatment of)");
					if (defSplit.length == 2) {
						HashSet<String> diseasesSetInPhrase = getAllDiseasesInPhrase(StringUtils.substringBefore(defSplit[1], "."));
						if(diseasesSetInPhrase.contains("cancer")) {
							return  "cancer2_" + def;
						}
					}
					
				}
			}
			
		}
		
		return "";
	}



	/**
	 * Outputs the definitions of the CUIs of cui_term_file to outputFile
	 * @param cui_term_File
	 * @param outputFile
	 */
	private void getDefiniteCancerOrdefs(String cui_term_File, String outputFile) {
		
		
		try {
			List<String> lines  = FileUtils.readLines(new File(cui_term_File));
			FileUtils.deleteQuietly(new File("dec3_definiteCancer_actualTerm.txt"));
			FileUtils.deleteQuietly(new File("dec3_definiteCancer_prefTerm.txt"));
			
			for (String cui_term_concTerm : lines) {
					
				if(StringUtils.countMatches(cui_term_concTerm, "_")==4) {
					
					System.out.println(cui_term_concTerm);
					String cui = StringUtils.substringBefore(cui_term_concTerm, "_");
					String term = StringUtils.substringAfter(cui_term_concTerm, "_");
					
					String[] slot = cui_term_concTerm.split("_");
					String actualTerm = slot[1];
					
					String conceptPrefName = slot[3] +" "+ slot[4];
					
					String op = getDiseaseByApplyingCancerRules(cui, actualTerm);
					
					if(op.contains("cancer1")) {
						FileUtils.write(new File("dec3_definiteCancer_actualTerm.txt"), cui_term_concTerm
								+ "\t" +op+ "\n", true);
					} else {
						op = getDiseaseByApplyingCancerRules(cui, conceptPrefName);
						if (op.contains("cancer1")) {
							FileUtils.write(new File("dec3_definiteCancer_prefTerm.txt"), cui_term_concTerm
									+ "\t" +op+ "\n", true);
						}
						
					}
					FileUtils.write(new File(outputFile), cui_term_concTerm + "\t" + 
							java2MySql.getUMLSDefinitionsOfCUI(cui) +"\n", true);
				
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	
	/**
	 * Usage : java -cp bin/:lib/* org.iiit.umls.UMLSTermDiseaseIndexer cancerAllTermsUnique.txt cancerAllDiseaseIndex.txt
	 * Input : File having each line "C0000726_abdomens_blor"
	 * Output : File having C0000726_abdomens_blor	[cancer]
	 * @param args
	 * @throws IOException
	 */
	public static void main (String[] args) throws IOException {
		UMLSTermDiseaseIndexer umlsTermDiseaseIndexer = new UMLSTermDiseaseIndexer();
//		umlsTermDiseaseIndexer.processDiseaseMedicalTerms(args[0],args[1]);
//		umlsTermDiseaseIndexer.analyzeDiseaseMedicalTerms(args[0],args[1]);
		if(args.length != 2) {
			System.err.println("Usage :  <termsFile> <termsDiseasesOutputFile>");
			System.exit(1);
		}
		FileUtils.deleteQuietly(new File(args[1]));
//		umlsTermDiseaseIndexer.tagMedicalTermWithDisease(args[0],args[1]);
		
		umlsTermDiseaseIndexer.getDefiniteCancerOrdefs (args[0],args[1]);
	}



	
}
