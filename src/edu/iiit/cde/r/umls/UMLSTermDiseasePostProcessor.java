package edu.iiit.cde.r.umls;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import edu.iiit.cde.r.mysql.Java2MySql;

/**
 * UMLSTermDiseasePostProcessor is used to create a dictionary for cancer disease.
 * @author raghavendra
 *
 */
public class UMLSTermDiseasePostProcessor {

	UMLSTermDiseaseIndexer umlsTermDiseaseIndexer = new UMLSTermDiseaseIndexer();
	Java2MySql java2MySql; 
	List<String> cancerSynonyms ;
	
	public UMLSTermDiseasePostProcessor() {
		try {
		java2MySql = new Java2MySql();
		cancerSynonyms = FileUtils.readLines(new File("./conf/cancerSynonyms.txt"));
	} catch (InstantiationException | IllegalAccessException
			| ClassNotFoundException | SQLException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
		}
	
	/**
	 * Splits the given list of words into definite cancer and context cancer.
	 * @param input File containing all input words
	 * @param contextOutput File containing the words that are contextually related to cancer. 
	 * @param definiteOutput File containing the words that are directly related to cancer.
	 * @throws IOException
	 */
	void pruneLinesForContextAndDefinite (String input, String contextOutput, String definiteOutput) throws IOException {
		List<String> inputLines = FileUtils.readLines(new File(input));
		
		
		for (String line : inputLines) {
			String[] term_defs = line.split("\t");
			String[] cui_term_sem = term_defs[0].split("_");
//			System.out.println(line);
			if (cui_term_sem.length == 3) {
				
				String cui = StringUtils.substringBefore(line, "_");
				String term = StringUtils.substringBetween(line, "_", "_");
				
				List<String> defs = java2MySql.getUMLSDefinitionsOfCUI(cui);
				for (String def : defs) {
					
					boolean flag = false;
					if(def.contains("</p>")) {
						def = StringUtils.substringBefore(def, "</p>"); 
					}
					def = def.replaceAll("(\"|\')http.*(\"|\')", "");
					def = def.replaceAll("http.*(\\)|\\[|\\])", "");
					def = def.replace("<p>", " ").trim();
					//Exact rules 
					// cancer treatment drug
					for (String cancerTerm : cancerSynonyms) {
						if (StringUtils.contains(def, "A " + cancerTerm.toLowerCase())  && !flag )  {
							FileUtils.write(new File(definiteOutput), term_defs[0]+"\t"+def+"\n" , true);
							flag = true;
						} else if (StringUtils.startsWithIgnoreCase(def, cancerTerm.toLowerCase()) && !flag)  {
							FileUtils.write(new File(definiteOutput), term_defs[0]+"\t"+def+"\n" , true);
							flag = true;
						} else if (StringUtils.startsWithIgnoreCase(def, "An anticancer drug") && !flag)  {
							FileUtils.write(new File(definiteOutput), term_defs[0]+"\t"+def+"\n" , true);
							flag = true;
						} 
					}
					
					//Tumors or cancer
					if(def.contains("Tumors or cancer") && !flag) {
						FileUtils.write(new File(contextOutput), term_defs[0]+"\t"+def+"\n" , true);
						flag = true;
					}else if(StringUtils.containsIgnoreCase(def, "usually benign") | 
							StringUtils.containsIgnoreCase(def, "usually malignant")  && !flag) {
						FileUtils.write(new File(contextOutput), term_defs[0]+"\t"+def+"\n" , true);
						flag = true;
					} else if(StringUtils.containsIgnoreCase(def,"benign") && StringUtils.containsIgnoreCase(def,"malignant")
							 && !flag) {
						FileUtils.write(new File(contextOutput), term_defs[0]+"\t"+def+"\n" , true);
						flag = true;
					} else {
						
					
						for (String cancerTerm : cancerSynonyms) {
							if (StringUtils.containsIgnoreCase(def, cancerTerm))  {
								
								//definite cancer
								String[] defSplit = def.split("(progress to)|(used to treat)|(used in)");
								if (defSplit.length == 2 && !flag) {
									HashSet<String> diseasesSetInPhrase = umlsTermDiseaseIndexer.getAllDiseasesInPhrase
											(StringUtils.substringBefore(defSplit[1], "."));
									//has cancer synonym
									if(diseasesSetInPhrase.size() == 1 && diseasesSetInPhrase.contains("cancer")) {
										
										if(umlsTermDiseaseIndexer.containsOtherDiseases(defSplit[1])) {
											//contains other diseases means context
											FileUtils.write(new File(contextOutput), term_defs[0]+"\t"+def+"\n" , true);
											flag = true;
										} else {
											//contains only cancer implies definite cancer term
											FileUtils.write(new File(definiteOutput), term_defs[0]+"\t"+def+"\n" , true);
											flag = true;
										}
									} else if(diseasesSetInPhrase.size() > 1 && diseasesSetInPhrase.contains("cancer")) {
										//any of 4 diseases and one cancer
										FileUtils.write(new File(contextOutput), term_defs[0]+"\t"+def+"\n" , true);
										flag = true;
									}
								}
								
								//context cancer
								defSplit = def.split("(may lead to)|(may cause)|(include)|(treatment of some types of )|(Some cancer treatment)|" +
										"(may be used to treat)|(treatment of)");
								if (defSplit.length == 2 && !flag) {
									HashSet<String> diseasesSetInPhrase = umlsTermDiseaseIndexer.getAllDiseasesInPhrase
											(StringUtils.substringBefore(defSplit[1], "."));
									if(diseasesSetInPhrase.contains("cancer")) {
										FileUtils.write(new File(contextOutput), term_defs[0]+"\t"+def+"\n" , true);
										flag = true;
									}
								}
								
								
								
							}
						}
					}
					if(flag)
						break;
				}
			}
		}
		
		
	}
	
	
	/**
	 * java -cp bin/:lib/* org.iiit.umls.UMLSTermDiseasePostProcessor  cancerAllDiseaseIndex_ngrams_withDef_removeDefiniteAndContext.txt    contextFile definiteFile
	 * Input : C0001480_atp_null	[cancer_NCI	as
	 * @param args
	 * @throws IOException
	 */
	public static void main (String[] args) throws IOException {
		UMLSTermDiseasePostProcessor umlsTermDiseasePostProcessor = new UMLSTermDiseasePostProcessor();
		
		if(args.length != 3) {
			System.err.println("Usage: <input file> <contextFile> <definiteFile>");
			System.exit(0);
		}
			
		FileUtils.deleteQuietly(new File(args[1]));
		FileUtils.deleteQuietly(new File(args[2]));
//		umlsTermDiseasePostProcessor.pruneLinesForContextAndDefinite(args[0], args[1], args[2]);
	}
}
