package edu.iiit.cde.r.mysql;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.cde.mihir.GlobalVariable;

import edu.iiit.cde.r.svm.Word2Index;
import edu.iiit.cde.r.utils.HashMapUtilsCustom;

/**
 * Java2MySql is a java class used to query MySQL database.
 * @author raghavendra
 */
public class Java2MySql { 
	String url;
	String dbName;
	String driver;
	String userName;
	String password;
	Connection conn;
	static Statement st;
	
	public static Java2MySql instance = null;
	
	/**
	 * Java2MySql Constructor
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IOException
	 */
	public Java2MySql() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, IOException {
		url = GlobalVariable.url;
		dbName = GlobalVariable.dbName;
		driver = GlobalVariable.driver;
		userName = GlobalVariable.userName;
		password = GlobalVariable.password;
		Class.forName(driver).newInstance();
		conn = DriverManager.getConnection(url+dbName,userName,password);
		st = conn.createStatement();
		readSemanticTypes();
	}
	
	/**
	 * Returns the instance of Java2MySql object.
	 * @return Java2MySql 
	 * @throws IOException
	 */
	public static Java2MySql getInstance(){
		if(Java2MySql.instance == null) {
			try {
				Java2MySql.instance = new Java2MySql();
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return Java2MySql.instance;
	}
	
	/**
	 * Closes the connection with MySQL database.
	 * @throws SQLException
	 */
	public void closeConnection() throws SQLException {
		conn.close();
	}
	
	
	HashMap<String,String> semanticTypesMap = new HashMap<String,String> ();
	HashSet<String> goldSemanticTypes = new HashSet<String>();
	/**
	 * Loads the UMLS semantic types from the configuration files.
	 * @throws IOException
	 */
	void readSemanticTypes() throws IOException {
		List<String> lines = FileUtils.readLines(new File("conf/clefSemanticTypes.txt"));
		for (String line : lines) {
			semanticTypesMap.put(StringUtils.substringBetween(line, "|").toLowerCase(),StringUtils.substringBefore(line, "|"));
		}
		lines = FileUtils.readLines(new File("conf/GoldSemanticTypes"));
		for (String line : lines) {
			goldSemanticTypes.add(StringUtils.substringBefore(line, "|"));
		}
	}
	
	/**
	 * Returns the UMLS definitions of the CUI from MRDEF table.
	 * @param cui Concept Unique Identifier
	 * @return definitions UMLS definitions of CUI
	 */
	public List<String> getUMLSDefinitionsOfCUI (String cui) {
		List<String> definitions = new ArrayList<String>();
		try { 
			
			ResultSet res = st.executeQuery("SELECT * FROM MRDEF WHERE CUI=\"" +cui+ "\"");
			while (res.next()) { 
//				String id = res.getString("CUI");
//				String msg = res.getString("DEF");
				//System.out.println(id + "\t" + msg);
				definitions.add(res.getString("DEF"));
//				System.out.println(res.getString("SAB"));
			} 
			
		} catch (Exception e) { 
			e.printStackTrace();
		} 
		return definitions;
	} 
	
	private void getKeysWithDefContainingToken(String token, String outputFile) {
		// TODO Auto-generated method stub
		try { 
			
			ResultSet res = st.executeQuery("SELECT CUI,DEF FROM MRDEF WHERE DEF LIKE \"%" +token+ "%\"");
			while (res.next()) { 
				String cui = res.getString("CUI");
				String msg = res.getString("DEF");
				System.out.println(cui + "\t" + msg);
				//definitions.add(res.getString("DEF"));
				//FileUtils.write(new File(outputFile), cui  + "_" + getTermsOfCUI(cui.trim() + "\n", true );
				FileUtils.write(new File(outputFile), cui +"_\t"+ msg   + "\n", true );
//				System.out.println(res.getString("SAB"));
			} 
			
		} catch (Exception e) { 
			e.printStackTrace();
		} 
	}
	
	public List<String> getCUIsDefContainingToken (String token) {
		List<String> list = new ArrayList<String> ();
		if(token.length()>4) {
		try { 
			ResultSet res = st.executeQuery("SELECT CUI,DEF FROM MRDEF WHERE DEF LIKE \"%" +token+ "%\"");
			while (res.next()) { 
				String cui = res.getString("CUI");
				list.add(cui);
			} 
			
		} catch (Exception e) { 
			e.printStackTrace();
		} 
		}
		return list;
	}
	
	
	/**
	 * Returns the cui of the medical term, if the medical term is present in UMLS.
	 * Else returns null.
	 * @param term Medical term
	 * @return CUI Concept Unique Identifier
	 */
	public String getCUIOfMedicalTerm (String term) {
		String cui = searchCUIForTerm(term.trim());
		if(cui != null)
		if(cui.isEmpty()) {
			if(term.endsWith("s")) {
				cui = searchCUIForTerm(term.replaceAll("s$", "").trim());
			}
		}
		if(cui != null) {
			if(cui.isEmpty()) {
				cui = searchCUIForTerm(term.replaceAll("s$", "").replaceAll("^(\\d|,|:|\\.|;|-|\\*)*", "").replaceAll("(\\d|,|:|\\.|;|-|\\*)*$", "").trim());
			}
		}
		
		return cui;
	}
	
	/**
	 * Returns the all cuis of the medical term, if the medical term is present in UMLS.
	 * Else returns null.
	 * @param term Medical term
	 * @return CUI Concept Unique Identifier
	 */
	public String getCUIsOfMedicalTerm (String term) {
		String cui = searchCUIForTerm(term.trim());
		if(cui != null)
		if(cui.isEmpty()) {
			if(term.endsWith("s")) {
				cui = searchCUIForTerm(term.replaceAll("s$", "").trim());
			}
		}
		if(cui != null) {
			if(cui.isEmpty()) {
				cui = searchCUIForTerm(term.replaceAll("s$", "").replaceAll("^(\\d|,|:|\\.|;|-|\\*)*", "").replaceAll("(\\d|,|:|\\.|;|-|\\*)*$", "").trim());
			}
		}
		
		return cui;
	}
	
	/**
	 * Returns the all cuis of the medical term, if the medical term is present in UMLS.
	 * Else returns null.
	 * @param term Medical term
	 * @return CUI Concept Unique Identifier
	 */
	public String getCUIsAndUMLSTermOfMedicalTerm (String term) {
		String cuiTermPipedOutput = searchCUIsAndTermsForTerm(term.trim());
		if(cuiTermPipedOutput != null)
		if(cuiTermPipedOutput.isEmpty()) {
			if(term.endsWith("s")) {
				cuiTermPipedOutput = searchCUIsAndTermsForTerm(term.replaceAll("s$", "").trim());
			}
		}
		if(cuiTermPipedOutput != null) {
			if(cuiTermPipedOutput.isEmpty()) {
				cuiTermPipedOutput = searchCUIsAndTermsForTerm(term.replaceAll("s$", "").replaceAll("^(\\d|,|:|\\.|;|-|\\*)*", "").replaceAll("(\\d|,|:|\\.|;|-|\\*)*$", "").trim());
			}
		}
		
		return cuiTermPipedOutput;
	}
	
	/**
	 * Searches the term in UMLS and return its CUI.
	 * @param term
	 * @return CUI
	 */
	public String searchCUIForTerm (String term) {
		TreeMap<String, Integer> CUICountMap = new TreeMap<String, Integer>();
		try { 
			ResultSet res = st.executeQuery("SELECT * FROM MRCONSO WHERE STR=\"" + term+ "\"");
			while (res.next()) {
				HashMapUtilsCustom.incrementKeyFreqCount(CUICountMap, res.getString("CUI"));
			} 
		} catch (Exception e) { 
//			e.printStackTrace();
		} 

		/*for (Entry<String, Integer> entry : CUICountMap.entrySet()) {
			System.out.println(entry.getKey() + "\t" + entry.getValue() + "\tterm = " + getTermsOfCUI(entry.getKey()));
		}*/
		String predictedCui = HashMapUtilsCustom.getKeyWithTopFreq(CUICountMap);
		String predictedTerm = getTermsOfCUI(predictedCui).toString();
		
//		System.out.println("predicted = " + predictedCui + "\t predicted term = " + predictedTerm);
		if(predictedTerm.contains(term.toLowerCase()))
			return predictedCui;
		else 
			return "";
	}
	
	/**
	 * Searches the term in UMLS and return its CUI.
	 * @param term
	 * @return CUI
	 */
	public String searchCUIsAndTermsForTerm (String term) {
		TreeMap<String, String> CUITermMap = new TreeMap<String, String>();
		try { 
			ResultSet res = st.executeQuery("SELECT * FROM MRCONSO WHERE STR=\"" + term+ "\"");
			while (res.next()) {
				//HashMapUtilsCustom.incrementKeyFreqCount(CUICountMap, res.getString("CUI"));
				CUITermMap.put(res.getString("CUI"), res.getString("STR"));
			} 
		} catch (Exception e) { 
//			e.printStackTrace();
		} 
		StringBuffer pipeoutput = new StringBuffer();
		for (Entry<String, String> entry : CUITermMap.entrySet()) {
			pipeoutput.append(entry.getKey()+"|"+entry.getValue()+"|");
		}

		return pipeoutput.toString();
	}
	
	/**
	 * Searches the term in UMLS and return its all CUI.
	 * @param term
	 * @return CUI
	 */
	public HashSet<String> searchCUIsForTerm (String term) {
		HashSet<String> cuis = new HashSet<>();
		try { 
			ResultSet res = st.executeQuery("SELECT * FROM MRCONSO WHERE STR=\"" + term+ "\"");
			while (res.next()) {
				cuis.add(res.getString("CUI"));
			} 
		} catch (Exception e) { 
//			e.printStackTrace();
		} 
		return cuis;
	}
	
	/**
	 * Get semantic type of medical term. 
	 * @param term
	 * @return semantic Type of the medical term
	 */
	public String getSemanticTypeOfTerm (String term) {
		
		String cui = getCUIOfMedicalTerm(term);
//		System.out.println("2term and cui  =" + term  + " \t "+ cui);
		if(cui !=null) {
			if(!cui.isEmpty()) {
				return getMedicalTermSemanticType(cui);
			}
		}
		
		return "";
	}
	
	
	//
	/**
	 * Get gold semantic type (from predefined list) of the medical term.  
	 * @param cui
	 * @return
	 */
	String getMedicalTermSemanticType(String cui) {
		String semanticType = getSemanticTypeOfCUI(cui);
		if (semanticType != null)
		if(!semanticType.isEmpty())
			if(goldSemanticTypes.contains(semanticType))
				return semanticType;
		return "";
	}
	
	/**
	 * Get all the terms related to the cui.
	 * @param cui
	 * @return list of terms
	 */
	public HashSet<String> getTermsOfCUI (String cui) {
		HashSet<String> list = new HashSet<String>();
		try { 
			ResultSet res = st.executeQuery("SELECT * FROM MRCONSO WHERE CUI=\"" + cui+ "\"");
			while (res.next()) {
				 list.add(res.getString("STR").toLowerCase());
			} 
		} catch (Exception e) { 
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * Returns the UMLS semantic type of CUI
	 * @param cui
	 * @return SemanticType UMLS semantic tyoe
	 */
	public String getSemanticTypeOfCUI (String cui) {
		try { 
			ResultSet res = st.executeQuery("SELECT * FROM MRSTY WHERE CUI=\"" +cui+ "\"");
			
			while (res.next()) { 
				return semanticTypesMap.get(res.getString("TUI").toLowerCase());
			} 
			
		} catch (Exception e) { 
			e.printStackTrace();
		} 
		return "";
	}
	
	/**
	 * Returns all UMLS semantic types of CUI
	 * @param cui
	 * @return SemanticType UMLS semantic tyoe
	 */
	public HashSet<String> getAllSemanticTypesOfCUI (String cui) {
		HashSet<String> semanticTypes = new HashSet<>();
		try { 
			String query = "SELECT * FROM MRSTY WHERE CUI= ?";
			//ResultSet res = st.executeQuery("SELECT * FROM MRSTY WHERE CUI= ? ");
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, cui);
			ResultSet res = stmt.executeQuery();
			while (res.next()) { 
				//System.out.println(res.getString("TUI").toLowerCase());
				semanticTypes.add(semanticTypesMap.get(res.getString("TUI").toLowerCase()));
			} 
			
		} catch (Exception e) { 
			e.printStackTrace();
		} 
		return semanticTypes;
	}
	

	/**
	 * Checks whether the token is in UMLS dictionary.
	 * Returns true if the token is in UMLS, else false.
	 * @param token
	 * @return true/false
	 */
	public boolean isInUMLS(String token) {
		if(token.length()<3)
			return false;
		String cui = getCUIOfMedicalTerm(StringUtils.strip(token,"(\\d|,|:|\\.|;|-|\\*)*"));
		//System.out.println("1term and cui  =" + token + " \t "+ cui);
		if(cui !=null) {
			if(!cui.isEmpty()) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isDiseaseDisorderSemanticTypeTerm (String term) {
		if(!getSemanticTypeOfTerm(StringUtils.strip(term,"(\\d|,|:|\\.|;|-|\\*)*")).isEmpty()) {
			return true;
		}
		return false;
	}
	
	public void printAllDiseaseUMLStermsAndCUI () {
		try { 
			System.out.println("executing query1");
			ResultSet res = st.executeQuery("SELECT * FROM MRCONSO");
			System.out.println("executing query2");
			HashMap<String, String> diseasetermCUIMap = new HashMap<String, String>();
			while (res.next()) {
				System.out.println(res.getString("CUI"));
				if (goldSemanticTypes.contains(getSemanticTypeOfCUI(res.getString("CUI")))) {
//					diseasetermCUIMap.put(res.getString("STR"), res.getString("CUI"));
					FileUtils.write(new File("AllUMLSdiseaseTermNCUI.txt"), res.getString("STR")+"_"+ res.getString("CUI")
							, true);
				}
			} 
//			HashMapUtils.dumpHashMap(diseasetermCUIMap, "AllUMLSdiseaseTermNCUI.txt");
		} catch (Exception e) { 
//			e.printStackTrace();
		} 
	}
	
	
	public static void main(String[] args) throws IOException { 
		Java2MySql java2MySql;
		try {
			java2MySql = new Java2MySql();
			
			if (args.length == 1) {
				if(args[0].equals("AllTerms")) {
					java2MySql.printAllDiseaseUMLStermsAndCUI();
				} 
				System.exit(0);
			}else if(args.length != 3) {
				System.err.println("usage: <CUIToTerms|TermToCUI|CUIToDefs|TermToDefs|CUI_TermToDefs|AllTerms|SemanticTypesOfCUI|<CUIDefContainingToken,Token>> <inputFile> <outputFile> " );
				System.exit(0);
			} 

			FileUtils.deleteQuietly(new File(args[2]));
			
			
			//only cuis - defs
			if(args[0].equals("CUIToDefs")) {
				List<String> lines = FileUtils.readLines(new File(args[1]));
				for (String cui : lines) {
					List<String> defs = java2MySql.getUMLSDefinitionsOfCUI(cui.trim());
					FileUtils.write(new File(args[2]), cui , true );
					for (int i = 0 ; i < defs.size(); i++) {
						if(i!= 0)
							FileUtils.write(new File(args[2]), "\t\t,\t\t", true);
						FileUtils.write(new File(args[2]), "\t"+ defs.get(i), true );
					}
					FileUtils.write(new File(args[2]), "\n" , true );
				}
			} else if(args[0].equals("TermToDefs")) {
				//only Terms - defs
				List<String> lines = FileUtils.readLines(new File(args[1]));
				for (String term : lines) {
					
					String cui = java2MySql.getCUIOfMedicalTerm(term.trim());
					if(!StringUtils.isEmpty(cui)) {
						List<String> defs = java2MySql.getUMLSDefinitionsOfCUI(cui);
						FileUtils.write(new File(args[2]), term +"_"+ cui + "#$$$#" , true );
						for (int i = 0 ; i < defs.size(); i++) {
							if(i!= 0)
								FileUtils.write(new File(args[2]), "\t\t,\t\t", true);
							FileUtils.write(new File(args[2]), "\t"+ defs.get(i), true );
						}
						FileUtils.write(new File(args[2]), "\n" , true );
					}
				}
			} else if(args[0].equals("CUI_TermToDefs")) {
				//cui_terms_st - defs
				List<String> lines = FileUtils.readLines(new File(args[1]));
				for (String line : lines) {
					String cui = StringUtils.substringBefore(line, "_").trim();
					List<String> defs = java2MySql.getUMLSDefinitionsOfCUI(cui);
					FileUtils.write(new File(args[2]), line , true );
					for (int i = 0 ; i < defs.size(); i++) {
						if(i!= 0)
							FileUtils.write(new File(args[2]), "\t\t,\t\t", true);
						FileUtils.write(new File(args[2]), "\t"+ defs.get(i), true );
					}
					FileUtils.write(new File(args[2]), "\n" , true );
				}
			} else if(args[0].equals("TermToCUI")) {
				//term - cui
				List<String> lines = FileUtils.readLines(new File(args[1]));
				for (String term : lines) {
					FileUtils.write(new File(args[2]), term  + "\t" + java2MySql.getCUIOfMedicalTerm(term.trim()) + "\n", true );
				}
			} else if(args[0].equals("CUIToTerms")) {
				// cui - terms
				List<String> lines = FileUtils.readLines(new File(args[1]));
				for (String cui : lines) {
					for (String term : java2MySql.getTermsOfCUI(cui.trim()))
						//FileUtils.write(new File(args[2]), cui  + "\t" + term + "\n", true );
						FileUtils.write(new File(args[2]),cui  + "_" +  term + "\n", true );
				}
			} else if (args[0].equals("CUIDefContainingToken")) {
				//search returns the keys with def containing the token
				java2MySql.getKeysWithDefContainingToken (args[1], args[2]);
			} else if (args[0].equals("SemanticTypesOfCUI")) {
				//search returns the keys with def containing the token
				//System.out.println("In SemanticTypesOfCUI");
				List<String> lines = FileUtils.readLines(new File(args[1]));
				for (String cui : lines) {
					FileUtils.write(new File(args[2]),cui  + "_" + java2MySql.getAllSemanticTypesOfCUI(cui)  + "\n", true );
				}	
			} else if (args[0].equals("SemanticTypeOfCUI")) {
				//search returns the keys with def containing the token
				//System.out.println("In SemanticTypeOfCUI");
				List<String> lines = FileUtils.readLines(new File(args[1]));
				for (String cui : lines) {
					FileUtils.write(new File(args[2]),cui  + "_" + java2MySql.getSemanticTypeOfCUI(cui)  + "\n", true );
				}	
			}
			
			
//			System.out.println(java2MySql.getUMLSDefinitionsOfCUI(args[0]));
//			System.out.println(java2MySql.getCUIOfMedicalTerm(args[0]));
			java2MySql.closeConnection();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}


	
	
} 

