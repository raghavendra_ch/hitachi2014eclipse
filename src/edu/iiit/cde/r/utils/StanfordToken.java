package edu.iiit.cde.r.utils;


public class StanfordToken {
	public String pos ;
	public String lemma ;
	public String word ;
	public String ne ; 
	
	@Override
	public String toString() {
		return "[" + word + "_" + lemma + "_" + pos + "_" + ne + "]";
	}
	
	public String getToken() {
		return word;
	}
	
	public String getLemma() {
		return lemma;
	}
	
	public String getpos() {
		return pos;
	}
	
	
	public String getNE() {
		return ne;
	}
	
}