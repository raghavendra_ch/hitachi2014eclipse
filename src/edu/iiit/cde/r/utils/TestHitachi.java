package edu.iiit.cde.r.utils;

import java.io.IOException;

import edu.iiit.cde.r.svm.Word2Index;

public class TestHitachi {
	
	
	void testWord2Index () throws IOException {
		//stopword 387971
		//first char CAP 387972
		//CAPS 387973
		//Numeric 387974
		//alphanumeric 387975
		// contains hyphen 387976
		// is UMLS 387977
		//387979,387980,387981,387982,387983,387984, 387985
		//word, stem, pos, chunk, prefix, suffix, semantictype
		
		
		int arr[] = {16928,210472,387118,387156,387212,387851,387972,387975,387977};
		for (int i : arr ) {
			System.out.println(Word2Index.getInstance().getValueOf(i));
		}
		
//		System.out.println("--");
		//System.out.println(Word2Index.getInstance().getSemanticTypeIndex("neop"));
//		System.out.println(Word2Index.getInstance().getOrthograhicIndexes("EMC"));
	}
	
	public static void main (String[] args) throws IOException {
		TestHitachi test = new TestHitachi();
		test.testWord2Index();
	}

}
