package edu.iiit.cde.r.utils;

import gov.nih.nlm.nls.metamap.AcronymsAbbrevs;
import gov.nih.nlm.nls.metamap.ConceptPair;
import gov.nih.nlm.nls.metamap.Ev;
import gov.nih.nlm.nls.metamap.Mapping;
import gov.nih.nlm.nls.metamap.MetaMapApi;
import gov.nih.nlm.nls.metamap.MetaMapApiImpl;
import gov.nih.nlm.nls.metamap.Negation;
import gov.nih.nlm.nls.metamap.PCM;
import gov.nih.nlm.nls.metamap.Phrase;
import gov.nih.nlm.nls.metamap.Position;
import gov.nih.nlm.nls.metamap.Result;
import gov.nih.nlm.nls.metamap.Utterance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MetaMapApiUse {

	MetaMapApi api;
	static MetaMapApiUse instance;

	Pattern inputExp = Pattern.compile("(tokens|lexmatch|inputmatch|tag)\\(\\[?(.*?)\\]?\\)",Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
	
	
	public MetaMapApiUse() {
		super();
		this.api = new MetaMapApiImpl();
	}

	public MetaMapApiUse(String serverHostname, int serverPort) {
		this.api = new MetaMapApiImpl();
		this.api.setHost(serverHostname);
		this.api.setPort(serverPort);
	}
	
	public static MetaMapApiUse getInstance () {
		if(instance == null) {
			String serverhost = "10.3.1.91";
			int serverport = MetaMapApi.DEFAULT_SERVER_PORT; // default port
			instance = new MetaMapApiUse(serverhost, serverport);
			
		}
		return instance;
	}

	void setTimeout(int interval) {
		this.api.setTimeout(interval);
	}

	void close () {
		this.api.disconnect();
	}
	/**
	 * @param inFile
	 *            File class referencing input file.
	 */
	/*static String readInputFile(File inFile)
			throws java.io.FileNotFoundException, java.io.IOException {
		BufferedReader ib = new BufferedReader(new FileReader(inFile));
		StringBuffer inputBuf = new StringBuffer();
		String line = "";
		while ((line = ib.readLine()) != null) {
			inputBuf.append(line).append('\n');
		}
		ib.close();
		return inputBuf.toString();
	}*/

	List<Result> process(String terms, PrintStream out, List<String> serverOptions)
			throws Exception {
		if (serverOptions == null || serverOptions.size() > 0) {
			serverOptions = new ArrayList<String>();
			serverOptions.add("-a");serverOptions.add("-K");
			serverOptions.add("-R MSH");
		}
		List<Result> resultList = api.processCitationsFromString(terms);
		for (Result result : resultList) {
			if (result != null) {
				out.println("input text: ");
				out.println(" " + result.getInputText());
				List<AcronymsAbbrevs> aaList = result.getAcronymsAbbrevsList();
				//System.out.println("ACRO ::: " + aaList.size());
				if (aaList.size() > 0) {
					out.println("Acronyms and Abbreviations:");
					for (AcronymsAbbrevs e : aaList) {
						out.println("Acronym: " + e.getAcronym());
						out.println("Expansion: " + e.getExpansion());
						out.println("Count list: " + e.getCountList());
						out.println("CUI list: " + e.getCUIList());
					}
				}
				List<Negation> negList = result.getNegationList();
				if (negList.size() > 0) {
					out.println("Negations:");
					for (Negation e : negList) {
						out.println("type: " + e.getType());
						out.print("Trigger: " + e.getTrigger() + ": [");
						for (Position pos : e.getTriggerPositionList()) {
							out.print(pos + ",");
						}
						out.println("]");
						out.print("ConceptPairs: [");
						for (ConceptPair pair : e.getConceptPairList()) {
							out.print(pair + ",");
						}
						out.println("]");
						out.print("ConceptPositionList: [");
						for (Position pos : e.getConceptPositionList()) {
							out.print(pos + ",");
						}
						out.println("]");
					}
				}
				for (Utterance utterance : result.getUtteranceList()) {
					out.println("Utterance:");
					out.println(" Id: " + utterance.getId());
					out.println(" Utterance text: " + utterance.getString());
					out.println(" Position: " + utterance.getPosition());

					for (PCM pcm : utterance.getPCMList()) {
						out.println("Phrase:");
						out.println(" text: " + pcm.getPhrase().getPhraseText());
						out.println(" Minimal Commitment Parse: "
								+ pcm.getPhrase().getMincoManAsString());
						out.println("Candidates:");
						out.println("Candidates size : " + pcm.getCandidateList().size());
						for (Ev ev : pcm.getCandidateList()) {
							out.println(" Candidate:");
							out.println("  Score: " + ev.getScore());
							out.println("  Concept Id: " + ev.getConceptId());
							out.println("  Concept Name: "
									+ ev.getConceptName());
							out.println("  Preferred Name: "
									+ ev.getPreferredName());
							out.println("  Matched Words: "
									+ ev.getMatchedWords());
							out.println("  Semantic Types: "
									+ ev.getSemanticTypes());
							out.println("  MatchMap: " + ev.getMatchMap());
							out.println("  MatchMap alt. repr.: "
									+ ev.getMatchMapList());
							out.println("  is Head?: " + ev.isHead());
							out.println("  is Overmatch?: " + ev.isOvermatch());
							out.println("  Sources: " + ev.getSources());
							out.println("  Positional Info: "
									+ ev.getPositionalInfo());
							out.println("  Pruning Status: "
									+ ev.getPruningStatus());
							out.println("  Negation Status: "
									+ ev.getNegationStatus());
						}

						out.println("Mappings:");
						for (Mapping map : pcm.getMappingList()) {
							out.println(" Map Score: " + map.getScore());
							for (Ev mapEv : map.getEvList()) {
								out.println("   Score: " + mapEv.getScore());
								out.println("   Concept Id: "
										+ mapEv.getConceptId());
								out.println("   Concept Name: "
										+ mapEv.getConceptName());
								out.println("   Preferred Name: "
										+ mapEv.getPreferredName());
								out.println("   Matched Words: "
										+ mapEv.getMatchedWords());
								out.println("   Semantic Types: "
										+ mapEv.getSemanticTypes());
								out.println("   MatchMap: "
										+ mapEv.getMatchMap());
								out.println("   MatchMap alt. repr.: "
										+ mapEv.getMatchMapList());
								out.println("   is Head?: " + mapEv.isHead());
								out.println("   is Overmatch?: "
										+ mapEv.isOvermatch());
								out.println("   Sources: " + mapEv.getSources());
								out.println("   Positional Info: "
										+ mapEv.getPositionalInfo());
								out.println("   Pruning Status: "
										+ mapEv.getPruningStatus());
								out.println("   Negation Status: "
										+ mapEv.getNegationStatus());
							}
						}
					}
				}
			} else {
				out.println("NULL result instance! ");
			}
		}
		//this.api.resetOptions();
		return resultList;
	}

	public static void main(String[] args) throws Exception {

		/*if(args.length == 0) {
			System.err.println("Usage: MetaMapApiUse <input-json-file>");
			System.exit(0);
		}*/
		
		//String serverhost = MetaMapApi.DEFAULT_SERVER_HOST;
		String serverhost = "10.3.1.91";
		int serverport = MetaMapApi.DEFAULT_SERVER_PORT; // default port
//		String inFilename = args[0];

		
		MetaMapApiUse frontEnd = new MetaMapApiUse(serverhost, serverport);
		/*File inFile = new File(inFilename.trim());
		String text = readInputFile(inFile);*/
		String text = "cancer";
		try {
//			System.out.println(MetaMapApiUse.getInstance().processToken(text));
			
			List<MetaMapToken> tokens = new ArrayList<MetaMapToken>();
			List<Ev> mappingList = new ArrayList<Ev>();
			frontEnd.processWrapper(text, tokens, mappingList);
			System.out.println(mappingList);
			System.out.println("Token list size : " + tokens.size());
			System.out.println(tokens);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(text);
		}

		frontEnd.api.disconnect();
		MetaMapApiUse.getInstance().close();
	}
	
	
	public static MetaMapToken processToken (String token) {
//		List<MetaMapToken> tokens = instance.processWrapper(token);
		List<MetaMapToken> tokens = new ArrayList<MetaMapToken>();
		List<Ev> mappingList = new ArrayList<Ev>();
		/*if(tokens.size() > 0) {
			System.out.println(tokens.get(0));
			return tokens.get(0);
		}*/
		System.out.println(mappingList);
		
		return tokens.get(0);
	}
	
	
	public List<MetaMapToken> processWrapper(String text, List<MetaMapToken> tokens, List<Ev> mappingList) { 
		
		ByteArrayOutputStream byo = new ByteArrayOutputStream();
		try {
		List<Result> resultList = process(text,new PrintStream(byo),null);
		//List<Token> tokens = new ArrayList<Token>();
		if(tokens == null || mappingList == null) {
			System.err.println("Tokens list and mappingList should not be null ");
			return null;
		}
		for(Result result : resultList) {
			//System.out.println("======++RESULT ");
			for(Utterance utterance : result.getUtteranceList()) {
				//System.out.println("======++UTTERANCE ");
				//TODO
				//System.out.println(utter);
				for(PCM pcm : utterance.getPCMList()) {
					//System.out.println("==========PCM");
					 Phrase phrase = pcm.getPhrase();
					 //System.out.println("=====POS===" + getPOSTags(phrase));
					 tokens.addAll(getPOSTags(phrase));
					 
					 for(Mapping map : pcm.getMappingList()) {
						 for(Ev ev : map.getEvList()) {
							 mappingList.add(ev);
						 }
					 }
				}
				
			}
		}
		
		for(MetaMapToken token : tokens) {
			if(token.posTag.equalsIgnoreCase("noun")) {
				//System.out.println(token.inputText + "_" + token.isHead);
			}
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return tokens;
		
		//System.out.println("OUTPUT: " + byo.toString());
	}

	private List<MetaMapToken> getPOSTags(Phrase phrase) throws Exception {
		String str = phrase.getMincoManAsString();
		List<MetaMapToken> tokens = new ArrayList<MetaMapToken>();
		//System.out.println(str);
		for(String s : str.split("\\]\\)\\]\\),")) {
			s = s + "])])";
			MetaMapToken token = new MetaMapToken();
			//System.out.println(s);
			s = s.replaceAll("^\\[", "");
			if(s.contains("head(")) {
				//System.out.println("======================================" + s);
				token.isHead = true;
			}
			Matcher m = inputExp.matcher(s);
			//System.out.println(s);
			while(m.find()) {
				switch(m.group(1)) {
				case "lexmatch" : 
					//System.out.println("Lex match : "  + m.group(2));
					token.lexEntry = m.group(2);
					break;
				case "inputmatch":
					//System.out.println("Input Txt : "  + m.group(2));
					token.inputText = m.group(2);
					break;
				case "tag" : 
					//System.out.println("Tag : "  + m.group(2));
					token.posTag = m.group(2);
					break;
				case "tokens" :
					//System.out.println("Tokens : "  + m.group(2));
					token.tokens = m.group(2);
					break;
				}
				
			}
			tokens.add(token);
			s = "";
		}
		
		/*System.out.println(phrase.getPhraseText());
		System.out.println(phrase.getMincoManAsString());
		*/
		return tokens;
	}
	
}

