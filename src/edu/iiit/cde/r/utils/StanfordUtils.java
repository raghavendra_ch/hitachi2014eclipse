package edu.iiit.cde.r.utils;

import java.util.List;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.ChunkAnnotationUtils;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.CoreMap;

public class StanfordUtils {

	static StanfordCoreNLP stanfordNLP = null;
	
	static {
		Properties props = new Properties();
		// props.put("annotators", "tokenize, ssplit, pos, lemma,ner");
		props.put("annotators", "tokenize, ssplit, pos,lemma");//),ner,parse");
		// props.put("dcoref.score", true);
		stanfordNLP = new StanfordCoreNLP(props);
	}
	
	public static void main(String[] args) {
		StanfordUtils pos = new StanfordUtils();
		String line = "Who taught cname1 about safety ?";
		System.out.println(StanfordUtils.processStr(line));
		
		/*line = "How many students registered for course cname1?";
		System.out.println(StanfordUtils.processStr(line));
		
		line = "List all students and courses taken?";
		System.out.println(StanfordUtils.processStr(line));*/
	}

	public static ArrayList<StanfordToken> processStr(String str) {
		ArrayList<StanfordToken> tokens = new ArrayList<StanfordToken>();
		String taggedRes = "";
		Annotation annotation = new Annotation(str);
		stanfordNLP.annotate(annotation);
		List<CoreMap> sentences = annotation.get(SentencesAnnotation.class);
		
//		CoreMap sent = annotation.get(CoreAnnotations.SentencesAnnotation.class).get(0);
		
		//this is for chunk
		/*Tree tree = sent.get(TreeAnnotation.class);
		ArrayList<Entry<String, String>> chunks = new ArrayList<Entry<String,String>>();
		tree.indentedListPrint();
		extractChunks(tree, chunks);
		System.out.println(chunks);
		System.out.println(tree.constituents());*/
		//System.out.println(tree.yieldWords().get(0));
		//tree.indentedListPrint();
		/*Map<String,String> map = new HashMap<String,String>();
		ChunkAnnotationUtils.annotateChunk(sent, map);
		System.out.println(map);*/
		
		for (CoreMap sent : sentences) {
			for (CoreLabel token : sent.get(TokensAnnotation.class)) {
				token.get(TextAnnotation.class);
				
				StanfordToken tokenObj = new StanfordToken();
				
				// this is the text of the token
				String word = token.get(TextAnnotation.class);
				tokenObj.word = word;
				
				// this is the POS tag of the token
				String pos = token.get(PartOfSpeechAnnotation.class);
				tokenObj.pos = pos;
				
				// this is the LEMMA label of the token
				String lemma = token.get(LemmaAnnotation.class); 
				tokenObj.lemma = lemma;
				
				//this is the NE label of the token
				String ne = token.get(NamedEntityTagAnnotation.class);
				tokenObj.ne = ne;
				
				//ChunkAnnotationUtils.annotateChunk
				
//				System.out.println(tokenObj);
				tokens.add(tokenObj);
				
			}
		}
			//System.out.println("POS=" + allPos);
			return tokens;
	}
	
	
	/*public static void extractChunks(Tree tree, ArrayList<Entry<String,String>> chunks) {
		for(Tree child : tree.children()) {
			String tag = child.value();
			String phrase = "";
			if(child.isPhrasal() && hasOnlyLeaves(child)) {
				for(Tree leaf : child.getLeaves()) {
					for(Word word : leaf.yieldWords()) {
						phrase += " " + word.word();
					}
				}
				chunks.add(new AbstractMap.SimpleEntry<String,String>(phrase,tag));
			}else {
				extractChunks(child, chunks);
			}
		}
	}*/
	
/*	public static void extractChunks(Tree tree, ArrayList<Entry<String,String>> chunks) {
		if(tree.children().length ==  0) {
			return;
		}
		for(Tree child : tree.children()) {
			
			String tag = child.value();
			String phrase = "";
			if(child.isPhrasal() ) {
				System.out.println(tag + "=" + child.getLeaves());
				for(Tree leaf : child.getLeaves()) {
					for(Word word : leaf.yieldWords()) {
						phrase += " " + word.word();
					}
				}
				chunks.add(new AbstractMap.SimpleEntry<String,String>(phrase,tag));
			}else {
				extractChunks(child, chunks);
			}
		}
	}

	private static boolean hasOnlyLeaves(Tree p) {
		for(Tree child : p.children()) {
			if(child.isPhrasal()) {
				if(child.size() != 0) {
					return false;
				}
			}
		}
		return true;
	}
	*/
	public static String[] getTokens (ArrayList<StanfordToken> tokensList) {
		String[] tokens = new String[tokensList.size()];
		for (int i=0; i< tokensList.size(); i++) {
			tokens[i]=tokensList.get(i).getToken();
		}
		return tokens;
	}
	
	
	public static String[] getLemmas (ArrayList<StanfordToken> tokensList) {
		String[] lemmas = new String[tokensList.size()];
		for (int i=0; i< tokensList.size(); i++) {
			lemmas[i]=tokensList.get(i).getLemma();
		}
		return lemmas;
	}
	
	
	public static String[] getPOSTags (ArrayList<StanfordToken> tokensList) {
		String[] postags = new String[tokensList.size()];
		for (int i=0; i< tokensList.size(); i++) {
			postags[i]=tokensList.get(i).getpos();
		}
		return postags;
	}
	
}


