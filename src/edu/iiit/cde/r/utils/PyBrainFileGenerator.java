package edu.iiit.cde.r.utils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import edu.iiit.cde.r.svm.Word2Index;
import edu.iiit.nlp.OpenNLPTokenizer;
import edu.iiit.nlp.SentenceBreaker;

public class PyBrainFileGenerator {
	
	ClefPipedLineParser clefPipedLineParser = new ClefPipedLineParser();
	
	public File createPyBrainFileDefault (File corpusFile, File outputFile) throws IOException {
		List<String> lines = FileUtils.readLines(corpusFile);
		int exampleNumber = 1;
		for (int i = 0; i < lines.size(); i++) {

			String[] sentences = SentenceBreaker.getSentenceBreaker()
					.getSentences(lines.get(i));
			for (int j = 0; j < sentences.length; j++) {
				String sentence = sentences[j];
				// System.out.println("sentence = " + sentence);
				String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer()
						.tokenize(sentence);

				for (int k = 0; k < tokens.length; k++) {
					FileUtils.write(outputFile, Word2Index.getInstance().getOpenNlpClefWordVector(tokens[k])+
							" "+tokens[k] +" " + Word2Index.getInstance().getClassLabelIndex("O")
							+ " " + exampleNumber +"\n", true);
				}
				exampleNumber++;
			}
		}
		return outputFile;
	}
	
	/**
	 * Reads discharge summary and its piped output. Generates features file to
	 * build SVM model using SVMstruct.
	 * 
	 * Sample PipedOP :
	 * 00098-016139-DISCHARGE_SUMMARY.txt|9-20|C0524850|Neurosurgery
	 * |Neurosurgery|topp|generic
	 * 
	 * @throws IOException
	 */
	void createPyBrainFilesWithBIODTTag(String corpusDir,
			String pipeDir)
			throws IOException {

		/*FileUtils.deleteQuietly(new File("clef_train1_words.txt"));
		FileUtils.deleteQuietly(new File("clef_train1_vectors.txt"));
		FileUtils.deleteQuietly(new File("clef_train1_tags.txt"));
		File wordsFile = new File("clef_train_words1.txt");
		File vectorsFile = new File("clef_train1_vectors.txt");
		File tagsFile = new File("clef_train1_tags.txt");
		*/

		FileUtils.deleteQuietly(new File("clef_test_words.txt"));
		FileUtils.deleteQuietly(new File("clef_test_vectors.txt"));
		FileUtils.deleteQuietly(new File("clef_test_tags.txt"));
		File wordsFile = new File("clef_test_words.txt");
		File vectorsFile = new File("clef_test_vectors.txt");
		File tagsFile = new File("clef_test_tags.txt");
		
		int exampleNumber = 1;
		File[] corpusFiles = new File(corpusDir).listFiles();
		for (File corpusFile : corpusFiles) {
			System.out.println("Processing ..  " + corpusFile.getName());
			int start = 0;
			int end = 0;
			List<String> pipeDelimitedLines = null;
			String corpuslines = "";
			try {
				// read the piped files of the discharge summary files
				corpuslines = FileUtils.readFileToString(corpusFile);
				File pipeFile = new File(pipeDir + "/" + corpusFile.getName());
				// + StringUtils.replace(corpusFile.getName(), "txt",
				// "pipe.txt"));
				
				//uncomment for test
				/*if (corpusFile.getName().contains(".txt"))
					pipeFile = new File(pipeDir
							+ "/"
							+ StringUtils.replace(corpusFile.getName(), "txt",
									"pipe.txt"));
*/
				if (!pipeFile.exists()) {
					System.out.println("No pipe file found = " + pipeFile);
					continue;
				}

				pipeDelimitedLines = FileUtils.readLines(pipeFile);
			} catch (IOException e) {
				e.printStackTrace();
			}

			TreeMap<Integer, Integer> singleSpans = new TreeMap<Integer, Integer>();
			TreeMap<Integer, Integer> disjointFirstSpans = new TreeMap<Integer, Integer>();
			TreeMap<Integer, Integer> disjointSecondSpans = new TreeMap<Integer, Integer>();
			TreeMap<Integer, Integer> tripletFirstSpans = new TreeMap<Integer, Integer>();
			TreeMap<Integer, Integer> tripletSecondSpans = new TreeMap<Integer, Integer>();
			// For each line of the piped file. Each line has a medical term
			// with spans.
			for (String pipeDelimitedLine : pipeDelimitedLines) {
				
				//CLEF 2013
				int[] spanCUI = clefPipedLineParser.parseCLEF2013PipedFile(pipeDelimitedLine);
				for (int i = 0; i < spanCUI.length/2; i++) {
					
					if (spanCUI.length/2==1) {
						if (i==0) {
							singleSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
						}
					} else if (spanCUI.length/2==2) {
						if (i==0) {
							disjointFirstSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
						} else {
							disjointSecondSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
						}
					} else if (spanCUI.length/2==3) {
						if (i==0) {
							tripletFirstSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
						} else {
							tripletSecondSpans.put(spanCUI[i * 2], spanCUI[i * 2 + 1]);
						}
					}
					//System.out.println(spanCUI[i * 2] +"-"+ spanCUI[i * 2 + 1]);
				}
			}

			// new code
			List<String> lines = FileUtils.readLines(corpusFile);
			String fileText = FileUtils.readFileToString(corpusFile);
			int currentPos = 0;

			String sectionHeader = "";
			for (int i = 0; i < lines.size(); i++) {

				String[] sentences = SentenceBreaker.getSentenceBreaker()
						.getSentences(lines.get(i));
				for (int j = 0; j < sentences.length; j++) {
					String sentence = sentences[j];
					// System.out.println("sentence = " + sentence);

					String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer()
							.tokenize(sentence);

					int endMedicaltermSpan = -1;
					String medicaltag = "";
					for (int k = 0; k < tokens.length; k++) {

						FileUtils.write(wordsFile, tokens[k] ,
								true);
						FileUtils.write(wordsFile, "\n", true);
						FileUtils.write(vectorsFile, Word2Index.getInstance().getOpenNlpClefWordVector(tokens[k])+
								" "+tokens[k] +" ", true);
						
						
						int noOfSpaces = StringUtils.indexOf(fileText,
								tokens[k]);
						// System.out.println("No of spaces = " + noOfSpaces);
						// at medical term start
						currentPos += noOfSpaces;

						if (tokens[k].startsWith("-")) {
							// tokens[k] = StringUtils.substring(tokens[k], 1);
							currentPos++;
						}

						if (singleSpans.containsKey(currentPos)) {
							//System.out.println(currentPos + "-" + tokens[k]);
							FileUtils.write(vectorsFile, Word2Index
									.getInstance().getClassLabelIndex("B-M")
									+ " " + exampleNumber, true);
							medicaltag = "I-M";
							endMedicaltermSpan = singleSpans.get(currentPos);
						} else if (disjointFirstSpans.containsKey(currentPos)) {
							//System.out.println(currentPos + "-" + tokens[k]);
							FileUtils.write(vectorsFile, Word2Index
									.getInstance().getClassLabelIndex("DB-M")
									+ " " + exampleNumber, true);
							medicaltag = "DI-M";
							endMedicaltermSpan = disjointFirstSpans
									.get(currentPos);
						} else if (disjointSecondSpans.containsKey(currentPos)) {
							//System.out.println(currentPos + "-" + tokens[k]);
							FileUtils.write(vectorsFile, Word2Index
									.getInstance().getClassLabelIndex("DHB-M")
									+ " " + exampleNumber, true);
							medicaltag = "DHI-M";
							endMedicaltermSpan = disjointSecondSpans
									.get(currentPos);
						} else if (tripletFirstSpans.containsKey(currentPos)) {
							//System.out.println(currentPos + "-" + tokens[k]);
							FileUtils.write(vectorsFile, Word2Index
									.getInstance().getClassLabelIndex("TB-M")
									+ " " + exampleNumber, true);
							medicaltag = "TI-M";
							endMedicaltermSpan = tripletFirstSpans
									.get(currentPos);
						} else if (tripletSecondSpans.containsKey(currentPos)) {
							//System.out.println(currentPos + "-" + tokens[k]);
							FileUtils.write(vectorsFile, Word2Index
									.getInstance().getClassLabelIndex("THB-M")
									+ " " + exampleNumber, true);
							medicaltag = "THI-M";
							endMedicaltermSpan = tripletSecondSpans
									.get(currentPos);
						} else if (currentPos < endMedicaltermSpan) {
							//System.out.println(currentPos + "-" + tokens[k]);
							FileUtils.write(vectorsFile,
									Word2Index.getInstance()
											.getClassLabelIndex(medicaltag)
											+ " " + exampleNumber, true);
						} else {
							FileUtils.write(vectorsFile, Word2Index
									.getInstance().getClassLabelIndex("O")
									+ " " + exampleNumber, true);
						}
						
						FileUtils.write(vectorsFile, "\n", true);

						if (tokens[k].startsWith("-")) {
							// tokens[k] = StringUtils.substring(tokens[k], 1);
							currentPos--;
						}

						currentPos += tokens[k].length();
						fileText = StringUtils.substring(fileText,
								(tokens[k].length() + noOfSpaces));
					}
					exampleNumber++;
				}
			}

		}
	}
	
	public static void main (String args[]) {
		 
		/*svmFeaturesGenerator.createPyBrainFilesWithBIODTTag(
				"./finaldata/clef2013Dataset/test/DS",
				"./finaldata/clef2013Dataset/test/DSPIPED");*/
	}
}
