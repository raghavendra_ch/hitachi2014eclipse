package edu.iiit.cde.r.utils;

import java.util.LinkedHashMap;
import java.util.Map;
 
public class LRUCacheImpl<K, V> extends LinkedHashMap<K, V> {
   private static final long serialVersionUID = 1L;
   private int capacity;
    
   public LRUCacheImpl(int capacity, float loadFactor){
      super(capacity, loadFactor, true);
      this.capacity = capacity;
   }
    
   /**
    * removeEldestEntry() should be overridden by the user, otherwise it will not 
    * remove the oldest object from the Map.
    */
   @Override
   protected boolean removeEldestEntry(Map.Entry<K, V> eldest){
      return size() > this.capacity;
   }
    
}