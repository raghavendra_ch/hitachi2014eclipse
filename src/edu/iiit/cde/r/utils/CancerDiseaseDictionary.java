package edu.iiit.cde.r.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class CancerDiseaseDictionary {

	static HashSet<String> brainCancerTerms;
	static HashSet<String> breastCancerTerms;
	static HashSet<String> headAndNeckCancerTerms;
	static HashSet<String> lungCancerTerms;
	static HashSet<String> prostateCancerTerms;
	static HashSet<String> genericCancerTerms;
	static HashSet<String> brainCancerKeyWords;
	static HashSet<String> breastCancerKeyWords;
	static HashSet<String> headAndNeckCancerKeyWords;
	static HashSet<String> lungCancerKeyWords;
	static HashSet<String> prostateCancerKeyWords;
	static HashSet<String> genericCancerKeyWords;
	
	static CancerDiseaseDictionary instance = null;
	public static CancerDiseaseDictionary getInstance() {
		if(instance == null) {
			instance = new CancerDiseaseDictionary();
			try {
				loadCancerDictionaries();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}
	
	static void loadCancerDictionaries() throws IOException {
		brainCancerTerms = new HashSet<>(FileUtils.readLines(new File("cancerdictionaries/brainCancer.txt")));
		breastCancerTerms = new HashSet<>(FileUtils.readLines(new File("cancerdictionaries/breastCancer.txt")));
		headAndNeckCancerTerms = new HashSet<>(FileUtils.readLines(new File("cancerdictionaries/headandneckCancer.txt")));
		lungCancerTerms = new HashSet<>(FileUtils.readLines(new File("cancerdictionaries/lungCancer.txt")));
		prostateCancerTerms = new HashSet<>(FileUtils.readLines(new File("cancerdictionaries/prostateCancer.txt")));
		genericCancerTerms = new HashSet<>(FileUtils.readLines(new File("cancerdictionaries/cancer.txt")));
		
		brainCancerKeyWords = new HashSet<>(FileUtils.readLines(new File("cancerkeywords/braincancerSheet1.csv")));
		breastCancerKeyWords = new HashSet<>(FileUtils.readLines(new File("cancerkeywords/breastcancerSheet1.csv")));
		headAndNeckCancerKeyWords = new HashSet<>(FileUtils.readLines(new File("cancerkeywords/headandneckcanceSheet1.csv")));
		lungCancerKeyWords = new HashSet<>(FileUtils.readLines(new File("cancerkeywords/lungcancerSheet1.csv")));
		prostateCancerKeyWords = new HashSet<>(FileUtils.readLines(new File("cancerkeywords/prostatecancerSheet1.csv")));
		genericCancerKeyWords = new HashSet<>(FileUtils.readLines(new File("cancerkeywords/genericcancerSheet1.csv")));
	}
	
	public String getCancerDisease(String term) {
		if (brainCancerTerms.contains(term)) {
			return "braincancer";
		} else  if (breastCancerTerms.contains(term)) {
			return "breastcancer";
		} else  if (headAndNeckCancerTerms.contains(term)) {
			return "headandneckcancer";
		} else  if (lungCancerTerms.contains(term)) {
			return "lungcancer";
		} else  if (prostateCancerTerms.contains(term)) {
			return "prostatecancer";
		} else if (brainCancerTerms.contains(term.toLowerCase())) {
			return "braincancer";
		} else  if (breastCancerTerms.contains(term.toLowerCase())) {
			return "breastcancer";
		} else  if (headAndNeckCancerTerms.contains(term.toLowerCase())) {
			return "headandneckcancer";
		} else  if (lungCancerTerms.contains(term.toLowerCase())) {
			return "lungcancer";
		} else  if (prostateCancerTerms.contains(term.toLowerCase())) {
			return "prostatecancer";
		} else if (checkForKeyword(term.toLowerCase(), genericCancerKeyWords)) {
			if (checkForKeyword(term, brainCancerKeyWords))
				return "braincancer";
			else if (checkForKeyword(term, breastCancerKeyWords))
				return "breastcancer";
			else if (checkForKeyword(term, headAndNeckCancerKeyWords))
				return "headandneckcancer";
			else if (checkForKeyword(term, lungCancerKeyWords))
				return "lungcancer";
			else if (checkForKeyword(term, prostateCancerKeyWords))
				return "prostatecancer";
			else 
				return "genericcancer";
		} 
		
		if (genericCancerTerms.contains(term)) {
			return "genericcancer";
		}  else  if (genericCancerTerms.contains(term.toLowerCase())) {
			return "genericcancer";
		}
			
		return "0";
	}

	private boolean checkForKeyword (String term, HashSet<String> keywords) {
		for(String keyword : keywords) {
			if(StringUtils.containsIgnoreCase(term, keyword))
				return true;
		}
		return false;
	}
	
	public static void main (String[] args) {
		String cancerTerm = CancerDiseaseDictionary.getInstance().getCancerDisease("breast cancer");
		System.out.println(cancerTerm);
	}
}
