package edu.iiit.cde.r.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import opennlp.tools.util.Span;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import edu.iiit.nlp.OpenNLPTokenizer;
import edu.iiit.nlp.SentenceBreaker;

/**
 * Custom tokenizer specific to medical domain.
 * @author Raghavendra Ch
 *
 */
public class ClinicalTokenizer {

	static List<String> stopwords = new ArrayList<>();
	static HashSet<String> junkwords = new HashSet<>();
	static ClinicalTokenizer instance = null;

	/**
	 * Constructor
	 */
	public ClinicalTokenizer() {
		loadJunkwords();
		System.err.println("JunkWords loaded = " + junkwords.size());
		getStopWords();
	} 

	/**
	 * Returns the instance of the Clinical Tokenizer.
	 * @return
	 */
	public static ClinicalTokenizer getInstance () {
		if(instance == null) {
			instance = new ClinicalTokenizer();
		}
		return instance;
	}

	/**
	 * Returns the ngrams from the given text. 
	 * @param text Input text
	 * @param len 1 for unigram, 2 for bigram, 3 for trigram
	 * @return ngrams
	 */
	public static List<String> getNGrams(String text, int len) {
		String[] tokens = tokenize(text);
		return getNGrams(tokens, len);
	}

	/**
	 * Returns the list of stopwords from the configuration file.
	 * @return stopwords
	 */
	public static List<String> getStopWords () {
		try {
			stopwords = FileUtils.readLines(new File("conf/clinicalstopwords.txt"));
			//System.out.println(stopwords.size());
			return stopwords;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static HashSet<String> loadJunkwords () {
		try {
			junkwords = new HashSet<>(FileUtils.readLines(new File("conf/clinicaljunkwords.txt")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return junkwords;
	}


	/**
	 * Checks whether the given word is a stop word or not.
	 * Returns true if stopword, else false.
	 * @param word
	 * @return true/false
	 */
	public boolean isStopword(String word) {
		if(stopwords.contains(word))
			return true;
		return false;
	}

	/**
	 * Tokenizes the text by splitting on space.
	 * Retains ., from the text.
	 * @param text
	 * @return tokens
	 */
	public static String[] tokenize(String text) {

		//returns newlines
		return text.split("\\s");

		//does not return newline and spaces 
		/*StringTokenizer tokens = new StringTokenizer(text);
		List<String> list = new ArrayList<String>();
	    while (tokens.hasMoreTokens()) {
	        String token = tokens.nextToken();
	        list.add(token);
	    }
	    return list.toArray(new String[0]);*/



		/*text = text.replace("voices - a blog about what is important to the people","");
		text = text.replace("voices - a blog approximately that what is important to people","");
		text = text.replace("to break off itself: the emergency to gastech quarters the construction [modernizations]","");
		text = text.replace("breaking: emergency at gastech headquarters building [updates]","");
		text = text.replace("on the scene blog","");
		text = text.replace("on scene blog","");

		text = text.replaceAll("[,\":;.()-]", "");
		text = text.replaceAll("[0-9]{4}", "");
		List<String> list = new ArrayList<String>(Arrays.asList(StringUtils.split(text.toLowerCase())));
		list.removeAll(getStopWords());
		return list.toArray(new String[0]);*/
	}

	/**
	 * Returns the ngrams from the given text. 
	 * @param tokens
	 * @param len 1 for unigram, 2 for bigram, 3 for trigram
	 * @return
	 */
	public static List<String> getNGrams(String[] tokens, int len) {
		List<String> ngrams = new ArrayList<String>();

		for(int i = 0 ; i < tokens.length - len + 1; i++) {
			ngrams.add(concat(tokens, i, i+len));
		}

		return ngrams;
	}

	/**
	 * Concatenates the given character array from start index to end index to form a string.  
	 * @param chars
	 * @param start
	 * @param end
	 * @return
	 */
	public static String concat(char[] chars, int start, int end) {
		StringBuilder sb = new StringBuilder();
		for(int i = start; i < end; i++)
			sb.append((i > start ? "" : "") + chars[i]);
		return sb.toString();
	}

	/**
	 * Concatenates the given string array from start index to end index to form a single string.
	 * @param tokens
	 * @param start
	 * @param end
	 * @return
	 */
	public static String concat(String[] tokens, int start, int end) {
		StringBuilder sb = new StringBuilder();
		for (int i = start; i < end; i++)
			sb.append((i > start ? "_" : "") + tokens[i]);
		return sb.toString();
	}

	/*public void getStems () throws IOException {
		List<String> lines = FileUtils.readLines(new File("/home/raghavendra/BackUP/tools/SVM/word2VecFiles/vocabulary_all.txt"));
		for (String line : lines){
			FileUtils.write(new File("/home/raghavendra/BackUP/tools/SVM/word2VecFiles/stems_all.txt"), WordStemmer.getStemmer().stem(line) + "\n", true);
		}
	}*/

	public static void main (String[] args) throws IOException {
		/*String inputText = FileUtils.readFileToString(new File(args[0]));
		String[] tokens = ClinicalTokenizer.tokenize(inputText);
		for (int i = 0 ; i < tokens.length; i++) {
			FileUtils.write(new File(args[1]), tokens[i] + (i==tokens.length-1?"":"\n"), "UTF-8", true);
		}*/
		//		ClinicalTokenizer.getInstance().getStems();

		File dir = new File("finaldata/clef2013Dataset/train/DS");
		File[] files = dir.listFiles();
		FileUtils.deleteQuietly(new File("openNLP_clef2013_Words.txt"));
		for (File file: files) {
			List<String> lines = FileUtils.readLines(file);
			for (int i = 0; i < lines.size(); i++) {

				String[] sentences = SentenceBreaker.getSentenceBreaker()
						.getSentences(lines.get(i));
				for (int j = 0; j < sentences.length; j++) {
					String sentence = sentences[j];

					String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer()
							.tokenize(sentence);
					for (int k = 0 ; k < tokens.length; k++) {
						FileUtils.write(new File("openNLP_clef2013_Words.txt"), tokens[k] +"\n", "UTF-8", true);
					}
				}
			}
		}
		dir = new File("finaldata/clef2013Dataset/test/DS");
		files = dir.listFiles();

		for (File file: files) {
			List<String> lines = FileUtils.readLines(file);
			for (int i = 0; i < lines.size(); i++) {

				String[] sentences = SentenceBreaker.getSentenceBreaker()
						.getSentences(lines.get(i));
				for (int j = 0; j < sentences.length; j++) {
					String sentence = sentences[j];

					String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer()
							.tokenize(sentence);
					for (int k = 0 ; k < tokens.length; k++) {
						FileUtils.write(new File("openNLP_clef2013_Words.txt"), tokens[k] +"\n", "UTF-8", true);
					}
				}
			}
		}
		System.out.println("Done");
	}


	public static String trimSpanString(String spanstring) {
		return StringUtils.strip(spanstring, ",");
	}

	/**
	 * Returns true if the token is a garbage term i.e., contains only numerics, only punctuation symbols or measuring units.
	 * @param medicalterm
	 * @return
	 */
	public static boolean isGarbageTerm(String medicalterm) {
		boolean flag = false;
		
		medicalterm = remove_punct_digits(medicalterm).toLowerCase();
		
		if(StringUtils.isNumericSpace(medicalterm)) {
			flag = true;
		} else if (medicalterm.matches("(\\d|\\s|,|:|\\.|;|-|=|\\+|!|<|>|≥|×|\\*)*")) {
			flag = true;
		} /*else if (isUnit(medicalterm)) {
			flag = true;
		}*/else if(isJunkTerm(medicalterm)) {
			flag = true;
		} else if (medicalterm.length() <= 2) {
			flag = true;
		} else if (StringUtils.countMatches(medicalterm," ") > 15 ) {
			flag = true;
		} else if (checkForInstituteTerms(medicalterm)) {
			flag = true;
		}

		return flag;
	}
	
	public static boolean checkForInstituteTerms(String term) {
		if(StringUtils.containsIgnoreCase(term, "institut") ||
				StringUtils.containsIgnoreCase(term, "industry") ||
				StringUtils.startsWith(term, "limited") ||
				StringUtils.startsWith(term, "manufactur") ||
				StringUtils.startsWith(term, "measure") ||
				StringUtils.startsWith(term, "mechanical") ||
				StringUtils.startsWith(term, "nationwide") ||
				StringUtils.startsWith(term, "northern") ||
				StringUtils.startsWith(term, "north") ||
				StringUtils.startsWith(term, "novartis") ||
				StringUtils.startsWith(term, "nottingham") ||
				StringUtils.startsWith(term, "notable") ||
				StringUtils.startsWith(term, "older") ||
				StringUtils.startsWith(term, "oldest") ||
				StringUtils.startsWith(term, "patient") ||
				StringUtils.startsWith(term, "participant") ||
				StringUtils.startsWith(term, "peak ") ||
				StringUtils.startsWith(term, "pennsylvania") ||
				StringUtils.startsWith(term, "perfect") ||
				StringUtils.startsWith(term, "perform") ||
				StringUtils.startsWith(term, "phenomen") ||
				StringUtils.startsWith(term, "process") ||
				StringUtils.startsWith(term, "program") ||
				StringUtils.startsWith(term, "posses") ||
				StringUtils.startsWith(term, "progress ") ||
				StringUtils.startsWith(term, "question") ||
				StringUtils.startsWith(term, "ratio") ||
				StringUtils.startsWith(term, "reconstruct") ||
				StringUtils.startsWith(term, "recruit") ||
				StringUtils.startsWith(term, "report") ||
				StringUtils.startsWith(term, "result") ||
				StringUtils.startsWith(term, "review") ||
				StringUtils.startsWith(term, "revolut") ||
				StringUtils.startsWith(term, "rich ") ||
				StringUtils.startsWith(term, "richard") ||
				StringUtils.startsWith(term, "schematic") ||
				StringUtils.startsWith(term, "schedule") ||
				StringUtils.startsWith(term, "science") ||
				StringUtils.startsWith(term, "scientific") ||
				StringUtils.startsWith(term, "south") ||
				StringUtils.startsWith(term, "student") ||
				StringUtils.startsWith(term, "studied") ||
				StringUtils.startsWith(term, "studies") ||
				StringUtils.startsWith(term, "subject") ||
				StringUtils.startsWith(term, "suffer") ||
				StringUtils.startsWith(term, "sufficient") ||
				StringUtils.startsWith(term, "suitable") ||
				StringUtils.startsWith(term, "table") ||
				
				StringUtils.containsIgnoreCase(term, "software") ||
				StringUtils.containsIgnoreCase(term, "portal") ||
				StringUtils.containsIgnoreCase(term, "panel") ||
				StringUtils.containsIgnoreCase(term, "system") ||
				StringUtils.containsIgnoreCase(term, "national") ||
				StringUtils.containsIgnoreCase(term, "http") ||
				StringUtils.containsIgnoreCase(term, "fig. ") ||
				StringUtils.containsIgnoreCase(term, "figure ") ||
				StringUtils.containsIgnoreCase(term, "interest") ||
				StringUtils.containsIgnoreCase(term, "japanese") ||
				StringUtils.containsIgnoreCase(term, "universit") ||
				StringUtils.containsIgnoreCase(term, "united") ||
				StringUtils.containsIgnoreCase(term, "academic") ||
				StringUtils.containsIgnoreCase(term, "accurate") ||
				StringUtils.containsIgnoreCase(term, "achieve") ||
				StringUtils.containsIgnoreCase(term, "actual") ||
				StringUtils.containsIgnoreCase(term, "agency ") ||
				StringUtils.containsIgnoreCase(term, "faculty") ||
				StringUtils.containsIgnoreCase(term, "financial") ||
				StringUtils.containsIgnoreCase(term, "geographic") ||
				StringUtils.containsIgnoreCase(term, "found") ||
				StringUtils.containsIgnoreCase(term, "fourth") ||
				StringUtils.containsIgnoreCase(term, "fifth") ||
				StringUtils.containsIgnoreCase(term, "georg") ||
				StringUtils.containsIgnoreCase(term, "government") ||
				StringUtils.containsIgnoreCase(term, "google") ||
				StringUtils.containsIgnoreCase(term, "graduate") ||
				StringUtils.containsIgnoreCase(term, "gradual") ||
				StringUtils.containsIgnoreCase(term, "google") ||
				StringUtils.containsIgnoreCase(term, "hospital") ||
				StringUtils.containsIgnoreCase(term, "household ") ||
				StringUtils.containsIgnoreCase(term, "society ") ||
				StringUtils.containsIgnoreCase(term, "house ") ||
				StringUtils.containsIgnoreCase(term, "housing ") ||
				StringUtils.containsIgnoreCase(term, "howard ") ||
				StringUtils.containsIgnoreCase(term, "image ") ||
				StringUtils.containsIgnoreCase(term, "imaging ") ||
				StringUtils.containsIgnoreCase(term, "school") ||
				StringUtils.startsWith(term, "immigrant ") ||
				StringUtils.containsIgnoreCase(term, "year ") ||
				StringUtils.containsIgnoreCase(term, "germany") ||
				StringUtils.containsIgnoreCase(term, "statistical analysis") ||
				StringUtils.containsIgnoreCase(term, "active surveillance") ||
				StringUtils.containsIgnoreCase(term, "analysis") ||
				StringUtils.containsIgnoreCase(term, "study") ||
				StringUtils.containsIgnoreCase(term, "design") ||
				StringUtils.startsWith(term, "implement") ||
				StringUtils.startsWith(term, "independent") ||
				StringUtils.startsWith(term, "india") ||
				StringUtils.startsWith(term, "indirect") ||
				StringUtils.startsWith(term, "gov ") ||
				StringUtils.startsWith(term, "ms. ") ||
				StringUtils.startsWith(term, "al. ") ||
				StringUtils.startsWith(term, "st. ") ||
				StringUtils.startsWith(term, "st ") ||
				StringUtils.startsWith(term, "prof. ") ||
				StringUtils.startsWith(term, "professor ") ||
				StringUtils.startsWith(term, "indian ") ||
				StringUtils.startsWith(term, "american ") ||
				StringUtils.startsWith(term, "age ") ||
				StringUtils.startsWith(term, "younger ") ||
				StringUtils.containsIgnoreCase(term, ".gov"))
		return true;
		else 
			return false;
	}

	private static boolean isJunkTerm(String junkterm) {
		if(junkwords.contains(junkterm))
			return true;
		return false;
	}
	
	public static String remove_punct_digits(String medicalterm) {
		medicalterm = StringUtils.strip(medicalterm, " 0123456789|,|:|\\.|;|-|* \"\'=+!><≥×~Δ#™−–“”®()[]");
		return medicalterm;
	}

	//Removes numerics, puntuations from the medical term starting and ending
	/**
	 * Trims the medical term by removing the numerics, punctuation from starting and ending of the term.
	 * @param medicalterm
	 * @return pruned Medical term
	 */
	public static String getPrunedMedicalTerm(String medicalterm) {
		medicalterm = StringUtils.strip(medicalterm, " |,|:|\\.|;|-|* \"\'=+!><≥×~Δ#™−–“”®");
		return medicalterm;
	}

	public static Span getPrunedSpan(Span correctSpan, String term2,
			String prunedTerm) {
		int startIndexOfPrunedWord = correctSpan.getStart() + StringUtils.indexOf(term2, prunedTerm);
		//StringUtils.lastIndexOf(term2, prunedTerm);
		int endIndexOfPrunedWord = startIndexOfPrunedWord + prunedTerm.length();
		
		return new Span(startIndexOfPrunedWord, endIndexOfPrunedWord);
	}

}