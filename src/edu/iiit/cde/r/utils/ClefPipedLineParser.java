package edu.iiit.cde.r.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;

import opennlp.tools.util.Span;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import edu.iiit.cde.r.svm.Word2Index;
import edu.iiit.nlp.OpenNLPTokenizer;

public class ClefPipedLineParser {

	public int[] parseCLEF2013PipedFile(String pipeDelimitedLine) {
		String[] slots = pipeDelimitedLine.split("\\|\\|");
//		System.out.println(pipeDelimitedLine);
		int[] spanCUI = new int[slots.length-3];
		for (int i=3,j=0 ; i < slots.length; i++,j++) {
			spanCUI[j]=Integer.parseInt(slots[i]);
			//System.out.println(Integer.parseInt(slots[i]));
		}
		
		return spanCUI;
	}
	
	
	/**
	 * Creates piped output file from the features file.
	 * 
	 * @param corpusFile
	 * @param labelFile
	 * @param pipedOutputDir
	 */
	public void createPipeFileFromFeaturesFileWithPPWithBIODTtags(
			File corpusFile, File featuresFile, File labelFile, String pipedOutputDir, String svmORnn) {

		try {
			System.out.println("Writing into " + pipedOutputDir+"/"+corpusFile.getName());
			String fileText = FileUtils.readFileToString(corpusFile);
			String fullText = FileUtils.readFileToString(corpusFile);
			//CLEF 2014
			File pipedOutputFile = new File(pipedOutputDir + "/" + corpusFile.getName().replace(".txt", ".pipe.txt"));

			//CLEF 2013
//			File pipedOutputFile = new File(pipedOutputDir + "/" + corpusFile.getName());
//			FileUtils.deleteQuietly(pipedOutputFile);
			FileUtils.deleteQuietly(pipedOutputFile);
			List<String> lines = FileUtils.readLines(featuresFile);
			int currentPos = 0, spanstart = 0, spanend = 0;
			
			//opennlp
			String[] tokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize(
			fileText);
			
			
			List<String> totaltokensList = Arrays.asList(tokens);
			List<String> totaltagsList = FileUtils.readLines(labelFile);
			
			//opennlp
			Span[] spanArray = OpenNLPTokenizer.getOpenNLPTokenizer()
					.tokenizePos(fileText);
			
			List<Span> totalspansList = Arrays.asList(spanArray);

			List<String> tokensList = new ArrayList<String>();
			List<String> tagsList = new ArrayList<String>();
			List<Span> spansList = new ArrayList<Span>();
			int exampleNumber = 1;
			for (int i = 0; i < lines.size(); i++) {
				//System.out.println(lines.get(i));
				int currentExampleNumber = 0;
				if(StringUtils.equals(svmORnn, "svm")){
					currentExampleNumber = getExampleNumber(lines.get(i));
				} else if(StringUtils.equals(svmORnn, "nn")) {
					currentExampleNumber = Integer.parseInt(lines.get(i).split(" ")[27]);
				}

				if (exampleNumber < currentExampleNumber) {
					exampleNumber = currentExampleNumber;
					if (spansList.size() > 0) {
						/*
						 * int sentenceStart = spansList.get(0).getStart(); int
						 * sentenceEnd =
						 * spansList.get(spansList.size()-1).getEnd(); String
						 * spanSentence = StringUtils.substring(fullText,
						 * sentenceStart, sentenceEnd);
						 */
						TreeMap<String, String> allMedicalTermSpansOfSentence = outputSpansFromSentenceCLEF2013(
								tokensList, tagsList, spansList, fullText);
						// System.out.println(" piped output = "
						// +allMedicalTermSpansOfSentence.size());
						for (Entry<String, String> spanStringEntry : allMedicalTermSpansOfSentence
								.entrySet()) {
							// span-term
							System.out.println("piped = "
									+ spanStringEntry.getKey() + " == "
									+ spanStringEntry.getValue());
						
							
							String[] spanValues = StringUtils.strip(spanStringEntry.getKey().replace("||||", "||"),"|").split("\\|\\|");
							TreeSet<Integer> intValues = new TreeSet<Integer>();
							for (String val : spanValues) {
								intValues.add(Integer.parseInt(val));
							}
							StringBuffer spanValueBuffer = new StringBuffer();
							
							//CLEF 2013
							
							/*for (int v : intValues) {
								spanValueBuffer.append("||"+String.valueOf(v));
							}
							FileUtils.write(pipedOutputFile, corpusFile.getName()+"||Disease_Disorder||CUI-less"+ 
							spanValueBuffer.toString() +"\n", true);*/
							// }
							
							//CLEF 2014
							ArrayList<Integer> intValuesList = new ArrayList<Integer>();
							intValuesList.addAll(intValues);
							for (int l=0; l < intValuesList.size(); l++) {
								if(l%2==0)
									spanValueBuffer.append(((l==0)?"":",")+String.valueOf(intValuesList.get(l)));
								else 
									spanValueBuffer.append("-"+String.valueOf((intValuesList.get(l)-1)));
							}
							FileUtils.write(pipedOutputFile,corpusFile.getName()+ "|"
													+ spanValueBuffer.toString()
													+ "|"+ spanStringEntry.getValue().trim() + "\n",true);
							
						}
					}
					tokensList = new ArrayList<String>();
					tagsList = new ArrayList<String>();
					spansList = new ArrayList<Span>();
				}

				tagsList.add(totaltagsList.get(i));
				tokensList.add(totaltokensList.get(i));
				spansList.add(totalspansList.get(i));

			}

		} catch (Exception e) {
			System.out.println("Exception with corpus file " + corpusFile.getPath());
			e.printStackTrace();
		}

	}
	
	
	
	TreeMap<String, String> outputSpansFromSentenceCLEF2013 (List<String> tokensList, List<String> tagsList, List<Span> spansList, String fileText) throws IOException {

//		System.out.println("in output" + tokensList.size());
		TreeMap<String, String> outputSpanTermMap = new TreeMap<String, String>();
		String medicalterm = "";
		String spanString = "";
		int spanStart = 0, spanEnd = 0;
//		int sentenceStart = spansList.get(0).getStart();
//		int sentenceEnd = spansList.get(spansList.size()-1).getEnd();
		/*System.out.println("sentenceStart " + sentenceStart);
		System.out.println("sentenceEnd " + sentenceEnd);
		System.out.println("tagsList = " + tagsList);*/
		//single term
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("B-M"))) {
				if(!medicalterm.isEmpty()) {
					
					if(isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
						String medicaltermTrimmed = StringUtils.strip(medicalterm, ".,:-?%");
						spanStart += StringUtils.indexOf(medicalterm, medicaltermTrimmed);
						spanEnd = spanStart + medicaltermTrimmed.length();
						medicalterm = medicaltermTrimmed;
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
						

						//if medical term contains '/'
						if(medicalterm.contains("/")) {
							System.out.println("In / ");
							
							//opennlp
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							
							//stanford
							/*ArrayList<StanfordToken> stanfordtokensList = StanfordUtils.processStr(fileText);
							String[] medicalTermTokens = StanfordUtils.getTokens(stanfordtokensList);*/
							
							
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}
							
							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();
							
							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;
									String spanStringShared = String.valueOf(spanStart)+"||"+String.valueOf(x)
											+"||"+String.valueOf(y)+"||"+String.valueOf(spanEnd);
									outputSpanTermMap.put(spanStringShared, StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									String spanStringShared = String.valueOf(spanStart)+"||"+String.valueOf(x)
											+"||"+String.valueOf(y)+"||"+String.valueOf(spanEnd);
									outputSpanTermMap.put(spanStringShared, StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;
									String spanStringShared = String.valueOf(spanStart)+"||"+String.valueOf(x)
											+"||"+String.valueOf(y)+"||"+String.valueOf(z)
											+"||"+String.valueOf(w)+"||"+String.valueOf(spanEnd);
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);
									outputSpanTermMap.put(spanStringShared, sharedtermfinal);
								}
								
								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;
								
							}
							
							
							
						} else {
							outputSpanTermMap.put(spanString, medicalterm);
						}
						
					}
					medicalterm="";
				}
				
				
				medicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
				System.out.println("in B-M = " + spanStart + " - " + medicalterm);
			} else if (tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("I-M"))) {
				
				if(medicalterm.isEmpty()) {
					spanStart = spansList.get(i).getStart();
				}
				
				medicalterm = medicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
					
			} else if(tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("O"))
					|| !medicalterm.isEmpty()) {
				
				if(!medicalterm.isEmpty()) {
//					System.out.println("O medical term not empty = " + medicalterm);
					if(isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
						String medicaltermTrimmed = StringUtils.strip(medicalterm, ".,:-?%");
						spanStart += StringUtils.indexOf(medicalterm, medicaltermTrimmed);
						spanEnd = spanStart + medicaltermTrimmed.length();
						medicalterm = medicaltermTrimmed;
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
						
						//if medical term contains '/'
						if(medicalterm.contains("/")) {
							System.out.println("In / ");
							
							//opennlp
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							
							//stanford
							/*ArrayList<StanfordToken> stanfordtokensList = StanfordUtils.processStr(fileText);
							String[] medicalTermTokens = StanfordUtils.getTokens(stanfordtokensList);*/
							
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}
							
							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();
							
							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;
									
									StringBuffer spanStringShared = new StringBuffer(); 
											
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									
									
									outputSpanTermMap.put(trimSpanString(spanStringShared.toString()), StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									outputSpanTermMap.put(trimSpanString(spanStringShared.toString()), StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;
									
									StringBuffer spanStringShared = new StringBuffer(); 
											
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(z)+"||");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"||"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);
									outputSpanTermMap.put(trimSpanString(spanStringShared.toString()), sharedtermfinal);
								}
								
								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;
								
							}
							
						} else {
							spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
							outputSpanTermMap.put(spanString, medicalterm);
						}
						
					}
					medicalterm="";
				}
			}
			
			if(i==tokensList.size()-1) {
				
				if(!medicalterm.isEmpty()) {
//					System.out.println("O medical term not empty = " + medicalterm);
					if(isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
						String medicaltermTrimmed = StringUtils.strip(medicalterm, ".,:-?%");
						spanStart += StringUtils.indexOf(medicalterm, medicaltermTrimmed);
						spanEnd = spanStart + medicaltermTrimmed.length();
						medicalterm = medicaltermTrimmed;
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
						
						//if medical term contains '/'
						if(medicalterm.contains("/")) {
							System.out.println("In / ");
							
							//opennlp
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							
							//stanford
							/*ArrayList<StanfordToken> stanfordtokensList = StanfordUtils.processStr(fileText);
							String[] medicalTermTokens = StanfordUtils.getTokens(stanfordtokensList);*/
							
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}
							
							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();
							
							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;
									
									StringBuffer spanStringShared = new StringBuffer(); 
											
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									
									
									outputSpanTermMap.put(trimSpanString(spanStringShared.toString()), StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									outputSpanTermMap.put(trimSpanString(spanStringShared.toString()), StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;
									
									StringBuffer spanStringShared = new StringBuffer(); 
											
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(z)+"||");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"||"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);
									outputSpanTermMap.put(trimSpanString(spanStringShared.toString()), sharedtermfinal);
								}
								
								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;
								
							}
							
						} else {
							spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
							outputSpanTermMap.put(spanString, medicalterm);
						}
						
					}
					medicalterm="";
				}
			}
		}
		
		List<String> disjointFirstTerms = new ArrayList<String>();
		List<String> disjointFirstspanstring = new ArrayList<String>();
		List<String> disjointSecondTerms = new ArrayList<String>();
		List<String> disjointSecondspanstring = new ArrayList<String>();
		medicalterm = "";
		spanString = "";
		spanStart = 0;
		spanEnd = 0;
		//disjoint double term first span
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("DB-M"))) {
				if(!medicalterm.isEmpty()) {
					
					if(isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
						disjointFirstspanstring.add(spanString);
						disjointFirstTerms.add(medicalterm);
						medicalterm="";
					}
				}
				medicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("DI-M"))) {
				if(medicalterm.isEmpty()) {
					spanStart = spansList.get(i).getStart();
				}
				medicalterm = medicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("O"))
					|| !medicalterm.isEmpty()) {
				if(!medicalterm.isEmpty()) {
					if(isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						
						if(medicalterm.contains("/")) {
//							System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							
							//opennlp
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							
							//stanford
							/*ArrayList<StanfordToken> stanfordtokensList = StanfordUtils.processStr(fileText);
							String[] medicalTermTokens = StanfordUtils.getTokens(stanfordtokensList);*/
							
							
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}
							
							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();
							
							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;
									
									StringBuffer spanStringShared = new StringBuffer(); 
											
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									
									
									disjointFirstspanstring.add(trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									disjointFirstspanstring.add(trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;
									
									StringBuffer spanStringShared = new StringBuffer(); 
											
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(z)+"||");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"||"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);
									
									disjointFirstspanstring.add(trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(sharedtermfinal);
								}
								
								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;
								
							}
						} else {
							spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
							disjointFirstspanstring.add(spanString);
							disjointFirstTerms.add(medicalterm);
						}
						
						
						
					}
					medicalterm="";
				}
			}
			if(i==tokensList.size()-1) {

				if(!medicalterm.isEmpty()) {
					if(isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						
						if(medicalterm.contains("/")) {
//							System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							
							//opennlp
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							
							//stanford
							/*ArrayList<StanfordToken> stanfordtokensList = StanfordUtils.processStr(fileText);
							String[] medicalTermTokens = StanfordUtils.getTokens(stanfordtokensList);*/
							
							
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}
							
							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();
							
							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;
									
									StringBuffer spanStringShared = new StringBuffer(); 
											
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									
									
									disjointFirstspanstring.add(trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									disjointFirstspanstring.add(trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;
									
									StringBuffer spanStringShared = new StringBuffer(); 
											
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(z)+"||");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"||"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);
									
									disjointFirstspanstring.add(trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(sharedtermfinal);
								}
								
								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;
								
							}
						} else {
							spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
							disjointFirstspanstring.add(spanString);
							disjointFirstTerms.add(medicalterm);
						}
						
						
						
					}
					medicalterm="";
				}
			
			}
		}
		
		//disjoint double term second spans
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("DHB-M"))) {
				if(!medicalterm.isEmpty()) {
					
					if(isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
						disjointSecondspanstring.add(spanString);
						disjointSecondTerms.add(medicalterm);
						medicalterm="";
					}
				}
				medicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("DHI-M"))) {
				if(medicalterm.isEmpty()) {
					spanStart = spansList.get(i).getStart();
				}
				medicalterm = medicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("O"))
					|| !medicalterm.isEmpty()) {
				if(!medicalterm.isEmpty()) {
					if(isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						
						if(medicalterm.contains("/")) {
//							System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							//opennlp
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							
							//stanford
							/*ArrayList<StanfordToken> stanfordtokensList = StanfordUtils.processStr(fileText);
							String[] medicalTermTokens = StanfordUtils.getTokens(stanfordtokensList);*/
							
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}
							
							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();
							
							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;
									
									StringBuffer spanStringShared = new StringBuffer(); 
											
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									
									
									disjointSecondspanstring.add(trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									disjointSecondspanstring.add(trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;
									
									StringBuffer spanStringShared = new StringBuffer(); 
											
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(z)+"||");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"||"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);
									
									disjointSecondspanstring.add(trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(sharedtermfinal);
								}
								
								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;
								
							}
						} else {
							spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
							disjointSecondspanstring.add(spanString);
							disjointSecondTerms.add(medicalterm);
						}
						
						
						
					}
					medicalterm="";
				}
			}
			
			if(i==tokensList.size()-1) {

				if(!medicalterm.isEmpty()) {
					if(isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						
						if(medicalterm.contains("/")) {
//							System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							//opennlp
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							
							//stanford
							/*ArrayList<StanfordToken> stanfordtokensList = StanfordUtils.processStr(fileText);
							String[] medicalTermTokens = StanfordUtils.getTokens(stanfordtokensList);*/
							
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}
							
							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();
							
							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;
									
									StringBuffer spanStringShared = new StringBuffer(); 
											
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									
									
									disjointSecondspanstring.add(trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									disjointSecondspanstring.add(trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;
									
									StringBuffer spanStringShared = new StringBuffer(); 
											
									
									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(z)+"||");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"||"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);
									
									disjointSecondspanstring.add(trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(sharedtermfinal);
								}
								
								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;
								
							}
						} else {
							if(spanStart==0) {
								spanStart = spanEnd - medicalterm.length(); 
							}
							spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
							disjointSecondspanstring.add(spanString);
							disjointSecondTerms.add(medicalterm);
						}
						
						
						
					}
					medicalterm="";
				}
			
			}

		}
		
		
		
		//get all combinations of disjoint terms
		for(int i=0;i< disjointFirstspanstring.size();i++) {
			for(int j=0;j< disjointSecondspanstring.size();j++) {
				outputSpanTermMap.put(disjointFirstspanstring.get(i) +"||"+disjointSecondspanstring.get(j),
						disjointFirstTerms.get(i)+" "+disjointSecondTerms.get(j));
			}
		}
		
		
		medicalterm = "";
		String shortmedicalterm = "";
		spanString = "";
		spanStart = 0;
		spanEnd = 0;
		//triplet terms span
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("TB-M"))) {
				if(!shortmedicalterm.isEmpty()) {
					spanString = spanString +"||"+String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
				}
				medicalterm = tokensList.get(i);
				shortmedicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("TI-M"))) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				shortmedicalterm = shortmedicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("THB-M"))) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				shortmedicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("THI-M"))) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				shortmedicalterm = shortmedicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals(Word2Index.getInstance().getClassLabelIndexString("O")) 
					|| (!shortmedicalterm.isEmpty() || !medicalterm.isEmpty())) {
				if(!shortmedicalterm.isEmpty() || !medicalterm.isEmpty()) {
					if(spanStart==0) {
						spanStart = spanEnd - shortmedicalterm.length(); 
						
					}
					if(spanString.isEmpty()) {
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
					} else { 
						spanString = spanString +"||"+String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
					}
					//spanString = "";
				}
				shortmedicalterm = "";
				medicalterm = "";
			}
		}
		
		if(!spanString.isEmpty()) {
			outputSpanTermMap.put(spanString, medicalterm);
		}
		
		return outputSpanTermMap;
		
	}
	


	private int getExampleNumber(String line) {
		
		int exampleNo = 0;
		try {
			exampleNo = Integer.parseInt(StringUtils.substringBetween(line, "qid:"," "));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return exampleNo;
	}
	
	

	private String trimSpanString(String medicalterm, int spanStart, int spanEnd) {
		
		String medicaltermTrimmed = StringUtils.strip(medicalterm, ".,:");
		spanStart += StringUtils.indexOf(medicalterm, medicaltermTrimmed);
		spanEnd = spanStart + medicaltermTrimmed.length();
		
		return medicaltermTrimmed;
	}
	
	
	private String trimSpanString(String spanstring) {
		return StringUtils.strip(spanstring, ",");
	}
	

	/**
	 * Returns true if the token is a garbage term i.e., contains only numerics, only punctuation symbols or measuring units.
	 * @param medicalterm
	 * @return
	 */
	private boolean isGarbageTerm(String medicalterm) {
		boolean flag = false;
		
		if(StringUtils.isNumericSpace(medicalterm)) {
			flag = true;
		} else if (medicalterm.matches("(\\d|\\s|,|:|\\.|;|-|\\*)*")) {
			flag = true;
		} /*else if (isUnit(medicalterm)) {
			flag = true;
		}*/
		
		return flag;
	}
	


	
}
