package edu.iiit.cde.r.utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class FeaturesOverSampler {

	
	public static void main (String[] args) throws IOException {
		List<String> lines = FileUtils.readLines(new File(args[0]));
		StringBuffer sb = new StringBuffer();
		boolean hasMedicalterm = false;
		FileUtils.write(new File(args[1]), "\n", true);
		for (String line : lines) {
			sb.append(line + "\n");
			
			if(line.endsWith("B-M")) {
				hasMedicalterm  = true;
			}
			
			if(line.isEmpty()) {
				sb.append("\n");
				if(hasMedicalterm)  {
					FileUtils.write(new File(args[1]), sb.toString(), true);
				}
				hasMedicalterm = false;
				sb = new StringBuffer();
			}
				
		}
	}
}
 