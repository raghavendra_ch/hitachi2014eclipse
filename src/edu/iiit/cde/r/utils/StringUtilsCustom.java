package edu.iiit.cde.r.utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class StringUtilsCustom {

	void printDictionaryTermsInFile (String dictionary, String pubmedDir, String outputFile) throws IOException {
		List<String> lines = FileUtils.readLines(new File(dictionary));
		File[] files = new File(pubmedDir).listFiles();
		for (File file : files) {
			String fileContent = FileUtils.readFileToString(file).toLowerCase();
			StringBuffer sb = new StringBuffer();
			for (String term : lines) {
				if (fileContent.contains(term.toLowerCase())) {
					sb.append(term + " , ");
				}
			}
			System.out.println(file.getName() + ":\t" + sb.toString());
		}
	}
	
	void getTermBasedOnWordCount (String inputfile, int wordCount, String outputFile) throws IOException {
		List<String> lines = FileUtils.readLines(new File(inputfile));
		FileUtils.deleteQuietly(new File(outputFile));
		for (String line : lines) {
			//words above wordcount. trigram has 2 spaces
			if(StringUtils.countMatches(line, " ")>=wordCount-1) {
				//System.out.println(line);
				FileUtils.write(new File(outputFile), line +"\n", true);
			}
		}
	}
	
	public static void main (String args[]) throws IOException {
		StringUtilsCustom grepUtils = new StringUtilsCustom();
		
		
		//grepUtils.printDictionaryTermsInFile(args[0], args[1], args[2]);
		
		grepUtils.getTermBasedOnWordCount(args[0], Integer.parseInt(args[1]), args[2]);
	}
}
