package edu.iiit.cde.r.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.io.FileUtils;

import edu.iiit.cde.r.mysql.Java2MySql;

/**
 * Extract the UMLS terms related to specific cancer diseases.
 * @author raghavendra
 *
 */
public class UMLSCancerTermsExtractor {

	HashSet<String> genericCancerKeyWords  = null;
	HashSet<String> brainCancerKeyWords  = new HashSet<>();
	HashSet<String> breastCancerKeyWords  = new HashSet<>();
	HashSet<String> headandneckCancerKeyWords  = new HashSet<>();
	HashSet<String> lungCancerKeyWords  = new HashSet<>();
	HashSet<String> prostateCancerKeyWords  = new HashSet<>();
	
	Java2MySql java2MySql; 
	public UMLSCancerTermsExtractor() {
		try {
			loadCancerDictionaries();
			java2MySql = Java2MySql.getInstance();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	void loadCancerDictionaries () throws IOException {
		brainCancerKeyWords = new HashSet<>(FileUtils.readLines(new File(
				"/home/cde/Hitachi2015/MedicalTermsDictionary/brainterms_1_withDefs_grepBrain_uniq_phase1_lc"))); 
		breastCancerKeyWords = new HashSet<>(FileUtils.readLines(new File(
				"/home/cde/Hitachi2015/MedicalTermsDictionary/breastterms_1_withDefs_grepBreastDefs_phase1_lc"))); 
		headandneckCancerKeyWords = new HashSet<>(FileUtils.readLines(new File(
				"/home/cde/Hitachi2015/MedicalTermsDictionary/headandneckcancer_CUIs_Defs_checkedCUIs_terms_phase1_lc"))); 
		lungCancerKeyWords = new HashSet<>(FileUtils.readLines(new File(
				"/home/cde/Hitachi2015/MedicalTermsDictionary/lungterms_1_withDefs_greplungDefs_phase1_lc"))); 
		prostateCancerKeyWords = new HashSet<>(FileUtils.readLines(new File(
				"/home/cde/Hitachi2015/MedicalTermsDictionary/prostateterms_1_withDefs_grepprostateDefs_phase1_lc"))); 
		genericCancerKeyWords = new HashSet<>(FileUtils.readLines(new File(
				"/home/cde/Hitachi2015/cancerKeywords/genericcancer.txt")));
	}
	
	//CUI_term maps synonym
	HashMap<String, String> brainTermsMap = new HashMap();
	HashMap<String, String> breastTermsMap = new HashMap();
	HashMap<String, String> headandneckTermsMap = new HashMap();
	HashMap<String, String> lungTermsMap = new HashMap();
	HashMap<String, String> prostateTermsMap = new HashMap();
	HashMap<String, String> genericTermsMap = new HashMap();
	/**
	 * Extract cui of each cancer term synonym and store it in HashMaps for each disease.
	 */
	void extractTermsSpecificToCancerDiseases () {
		HashSet<String> cuiListTotal = new HashSet<>();
		//brain cancer
		for (String braintermSynonym : brainCancerKeyWords) {
			// gets the CUIs with definitions containing the term.
			List<String> cuiList = java2MySql.getCUIsDefContainingToken(braintermSynonym.trim());
			//get terms of each CUI
			for (String cui : cuiList) {
				if (!cuiListTotal.contains(cui)) {
					HashSet<String> termsOfCUIList = java2MySql.getTermsOfCUI(cui); 
					for (String termOfCUI : termsOfCUIList){
						brainTermsMap.put(cui+"_"+termOfCUI, braintermSynonym.trim());
//						System.out.println(cui+"_"+termOfCUI +" \t- "+ braintermSynonym.trim());
					}
					cuiListTotal.add(cui);
				}
			}
		}
		System.out.println("brainTerms size = " + brainTermsMap.size());
		HashMapUtilsCustom.dumpHashMap(brainTermsMap, 
				"/home/cde/Hitachi2015/MedicalTermsDictionary/UMLS_brainterms_usingsynonyms");
		brainTermsMap.clear();
		
		//breast cancer
		for (String breasttermSynonym : breastCancerKeyWords) {
			// gets the CUIs with definitions containing the term.
			List<String> cuiList = java2MySql.getCUIsDefContainingToken(breasttermSynonym.trim());
			//get terms of each CUI
			for (String cui : cuiList) {
				if (!cuiListTotal.contains(cui)) {
					HashSet<String> termsOfCUIList = java2MySql.getTermsOfCUI(cui); 
					for (String termOfCUI : termsOfCUIList){
						breastTermsMap.put(cui+"_"+termOfCUI, breasttermSynonym.trim());
					}
					cuiListTotal.add(cui);
				}
			}
		}
		HashMapUtilsCustom.dumpHashMap(breastTermsMap, 
				"/home/cde/Hitachi2015/MedicalTermsDictionary/UMLS_breastterms_usingsynonyms");
		breastTermsMap.clear();
		
		
		//headandneck
		for (String headandnecktermSynonym : headandneckCancerKeyWords) {
			// gets the CUIs with definitions containing the term.
			List<String> cuiList = java2MySql.getCUIsDefContainingToken(headandnecktermSynonym.trim());
			//get terms of each CUI
			for (String cui : cuiList) {
				if (!cuiListTotal.contains(cui)) {
					HashSet<String> termsOfCUIList = java2MySql.getTermsOfCUI(cui); 
					for (String termOfCUI : termsOfCUIList){
						headandneckTermsMap.put(cui+"_"+termOfCUI, headandnecktermSynonym.trim());
					}
					cuiListTotal.add(cui);
				}
			}
		}
		System.out.println("headandneck cancer size = " + headandneckTermsMap.size());
		HashMapUtilsCustom.dumpHashMap(headandneckTermsMap, 
				"/home/cde/Hitachi2015/MedicalTermsDictionary/UMLS_headandneckterms_usingsynonyms");
		headandneckTermsMap.clear();
		
		//lung cancer
		for (String lungtermSynonym : lungCancerKeyWords) {
			// gets the CUIs with definitions containing the term.
			List<String> cuiList = java2MySql.getCUIsDefContainingToken(lungtermSynonym.trim());
			//get terms of each CUI
			for (String cui : cuiList) {
				if (!cuiListTotal.contains(cui)) {
					HashSet<String> termsOfCUIList = java2MySql.getTermsOfCUI(cui); 
					for (String termOfCUI : termsOfCUIList){
						lungTermsMap.put(cui+"_"+termOfCUI, lungtermSynonym.trim());
					}
					cuiListTotal.add(cui);
				}
			}
		}
		
		System.out.println("Lung terms size = " + lungTermsMap.size());
	
		HashMapUtilsCustom.dumpHashMap(lungTermsMap, 
				"/home/cde/Hitachi2015/MedicalTermsDictionary/UMLS_lungterms_usingsynonyms");
		lungTermsMap.size();
		
		
		//prostate cancer
		for (String prostatetermSynonym : prostateCancerKeyWords) {
			// gets the CUIs with definitions containing the term.
			List<String> cuiList = java2MySql.getCUIsDefContainingToken(prostatetermSynonym.trim());
			//get terms of each CUI
			for (String cui : cuiList) {
				if (!cuiListTotal.contains(cui)) {
					HashSet<String> termsOfCUIList = java2MySql.getTermsOfCUI(cui); 
					for (String termOfCUI : termsOfCUIList){
						prostateTermsMap.put(cui+"_"+termOfCUI, prostatetermSynonym.trim());
					}
					cuiListTotal.add(cui);
				}
			}
		}
		System.out.println("prostate terms size = " + prostateTermsMap.size());
		HashMapUtilsCustom.dumpHashMap(prostateTermsMap, 
				"/home/cde/Hitachi2015/MedicalTermsDictionary/UMLS_prostateterms_usingsynonyms");
		prostateTermsMap.size();
		//generic
		for (String generictermSynonym : genericCancerKeyWords) {
			// gets the CUIs with definitions containing the term.
			List<String> cuiList = java2MySql.getCUIsDefContainingToken(generictermSynonym.trim());
			//System.out.println(cuiList);
			//get terms of each CUI
			for (String cui : cuiList) {
				if (!cuiListTotal.contains(cui)) {
					HashSet<String> termsOfCUIList = java2MySql.getTermsOfCUI(cui); 
					for (String termOfCUI : termsOfCUIList){
						genericTermsMap.put(cui+"_"+termOfCUI, generictermSynonym.trim());
					}
					cuiListTotal.add(cui);
				}
			}
		}
		
		System.out.println("generic terms size = " + genericTermsMap.size());
		HashMapUtilsCustom.dumpHashMap(genericTermsMap, 
				"/home/cde/Hitachi2015/MedicalTermsDictionary/UMLS_genericterms_usingsynonyms");
		
		
	}
	
	public static void main(String[] args) {
		UMLSCancerTermsExtractor umlsCancerTermsExtractor = new UMLSCancerTermsExtractor();
		umlsCancerTermsExtractor.extractTermsSpecificToCancerDiseases();
	}

}
