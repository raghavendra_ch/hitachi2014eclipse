package edu.iiit.cde.r.utils;

public class MetaMapToken {
	public String lexEntry = "";
	public String inputText = "";
	public String posTag = "";
	public boolean isHead = false;
	public String tokens = "";
	
	@Override
	public String toString() {
		return lexEntry + "_" + inputText + "_" + posTag + "_" + isHead + "_" + tokens;
	}
}
