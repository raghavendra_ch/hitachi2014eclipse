package edu.iiit.cde.r.nn;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;

import opennlp.tools.util.Span;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import edu.iiit.cde.r.utils.ClinicalTokenizer;
import edu.iiit.nlp.OpenNLPTokenizer;

public class DeepNlConvertor {

	private int noOfFeatures = 1;

	/**
	 * Creates piped output file from the features file.
	 * @param corpusFile
	 * @param featuresFileWithLabel
	 * @param pipedOutputDir
	 * @param includeUMLS
	 */
	private void createPipeFileFromDeepnlOutput(File corpusFile, File featuresFileWithLabel,
			String pipedOutputDir) {

		try {
			String fileText = FileUtils.readFileToString(corpusFile);
			String fullText = FileUtils.readFileToString(corpusFile);
			//CLEF2014
			//			File pipedOutputFile = new File(pipedOutputDir + "/" + corpusFile.getName().replace(".txt", ".pipe.txt"));
			//			FileUtils.deleteQuietly(pipedOutputFile);

			//CLEF2013
			File pipedOutputFile = new File(pipedOutputDir + "/" + corpusFile.getName());
			System.out.println("Generating = " + pipedOutputFile.getName());
			FileUtils.deleteQuietly(pipedOutputFile);
			List<String> lines = FileUtils.readLines(featuresFileWithLabel);
			int currentPos = 0, spanstart = 0, spanend = 0;
			List<String> tokensList = new ArrayList<String>();
			List<String> tagsList = new ArrayList<String>();
			List<Span> spansList = new ArrayList<Span>();
			for (String line : lines) {

//				102920	CD	B-NP	O
				if(noOfFeatures==1) {
					if (StringUtils.countMatches(line, "\t")>0) {
						noOfFeatures = StringUtils.countMatches(line, "\t");
					}
				}
				//if line is empty
				if(line.isEmpty()) {

					if(spansList.size()>0) { 
						/*int sentenceStart = spansList.get(0).getStart();
						int sentenceEnd = spansList.get(spansList.size()-1).getEnd();
						String spanSentence = StringUtils.substring(fullText, sentenceStart, sentenceEnd);*/
						////span-term
						TreeMap<String, String> allMedicalTermSpansOfSentence = outputSpansFromSentenceCLEF2013(tokensList, tagsList, spansList, fullText);
						//						System.out.println(" piped output = " +allMedicalTermSpansOfSentence.size());
						for(Entry<String, String> spanStringEntry : allMedicalTermSpansOfSentence.entrySet()) {
							//span-term
							//if garbage term. skip
							if(ClinicalTokenizer.isGarbageTerm(spanStringEntry.getValue()))
								continue;
							
							//for single term prune the term
							//System.out.println(spanStringEntry.getKey()+"\tvalue\t"+spanStringEntry.getValue());
							if(StringUtils.countMatches(StringUtils.strip(spanStringEntry.getKey(),"|"),"||")==1) {

								try {
									String prunedMedicalTerm = ClinicalTokenizer.getPrunedMedicalTerm(spanStringEntry.getValue());
									int outputspanstart = Integer.parseInt(StringUtils.substringBefore(spanStringEntry.getKey(),"||"));
									int outputspanend = Integer.parseInt(StringUtils.substringAfter(
											StringUtils.strip(spanStringEntry.getKey(),"|"),"||"));
									int startIndexOfPrunedWord = StringUtils.indexOf(spanStringEntry.getValue(), prunedMedicalTerm);
									int afterEndIndexOfPrunedWord = spanStringEntry.getValue().length() - (startIndexOfPrunedWord+prunedMedicalTerm.length());
									//CLEF2013
									if(!prunedMedicalTerm.contains("|") && !prunedMedicalTerm.isEmpty()) {
										FileUtils.write(pipedOutputFile, corpusFile.getName()+"||Disease_Disorder||CUI-less||"+(outputspanstart+startIndexOfPrunedWord)+
												"||"+(outputspanend-afterEndIndexOfPrunedWord) + "\n", true);
										System.out.println(prunedMedicalTerm);
									} 
									//CLEF2014
									//System.out.println(outputspanstart+"+"+startIndexOfPrunedWord +"-"+spanStringEntry.getValue());
									/*int startindex1 = (outputspanstart+startIndexOfPrunedWord);
									int endindex1 = (outputspanend-afterEndIndexOfPrunedWord-1);
									System.err.println("pruned = " + prunedMedicalTerm );
									if(!prunedMedicalTerm.contains("|") && (endindex1-startindex1)>=1) {
										//									System.out.println(startindex1+"--"+endindex1);
										FileUtils.write(pipedOutputFile, corpusFile.getName()+"|"+startindex1+
												"-"+endindex1 +"|"+prunedMedicalTerm + "\n", true);
									} */
								} catch (NumberFormatException ne) {
									ne.printStackTrace();
								}
							} else {
								/*System.out.println("piped = " + spanStringEntry.getKey() + " == " +
										spanStringEntry.getValue());*/
								System.out.println(spanStringEntry.getValue());
								String[] spanValues = StringUtils.strip(spanStringEntry.getKey().replace("||||", "||"),"|").split("\\|\\|");
								TreeSet<Integer> intValues = new TreeSet<Integer>();
								for (String val : spanValues) {
									intValues.add(Integer.parseInt(val));
								}
								StringBuffer spanValueBuffer = new StringBuffer();

								//CLEF2013
								if (intValues.size()%2==0) {
									for (int v : intValues) {
										//System.out.println("||"+String.valueOf(v));
										spanValueBuffer.append("||"+String.valueOf(v));
									}
								} else {
									spanValueBuffer.append("||"+intValues.first()+"||"+intValues.last());
									//System.out.println("||"+intValues.first()+"||"+intValues.last());
								}
								FileUtils.write(pipedOutputFile, corpusFile.getName()+"||Disease_Disorder||CUI-less"+ 
										spanValueBuffer.toString() +"\n", true);
								
								//CLEF2014
								/*ArrayList<Integer> intValuesList = new ArrayList<Integer>();
								intValuesList.addAll(intValues);
								for (int l=0; l < intValuesList.size(); l++) {
									if(l%2==0 && l<intValuesList.size())
										spanValueBuffer.append(((l==0)?"":",")+String.valueOf(intValuesList.get(l)));
									else 
										spanValueBuffer.append("-"+String.valueOf((intValuesList.get(l)-1)));
								}
								System.out.println("spanValueBuffer = "+spanValueBuffer.toString());
								FileUtils.write(pipedOutputFile,corpusFile.getName()+ "|"
														+ spanValueBuffer.toString()
														+ "|"+ spanStringEntry.getValue().trim() + "\n",true);*/
							}
						}
					}
					tokensList = new ArrayList<String>();
					tagsList = new ArrayList<String>();
					spansList = new ArrayList<Span>();
				} else if (StringUtils.countMatches(line, "\t") == noOfFeatures ) {
					String[] split = line.split("\t");
					tagsList.add(split[noOfFeatures]);
					tokensList.add(split[0]);

					//no of spaces
					int noOfSpaces = StringUtils.indexOf(fileText, split[0]);
					//space + length of string

					//at medical term start
					currentPos += noOfSpaces;
					spanstart = currentPos;
					//medicalterm = split[0];
					currentPos += split[0].length();
					spanend = currentPos;

					Span span2 = new Span(spanstart, spanend);
					spansList.add(span2);
					//get remaining sentence from the end of the string
					fileText = StringUtils.substring(fileText, (split[0].length()+ noOfSpaces));
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	/**
	 * 
	 * @param tokensList
	 * @param tagsList
	 * @param spansList
	 * @param fileText
	 * @return
	 */
	TreeMap<String, String> outputSpansFromSentenceCLEF2013 (List<String> tokensList, List<String> tagsList, List<Span> spansList, String fileText) {

		//		System.out.println("in output" + tokensList.size());
		TreeMap<String, String> outputSpanTermMap = new TreeMap<String, String>();
		String medicalterm = "";
		String spanString = "";
		int spanStart = 0, spanEnd = 0;
		//		int sentenceStart = spansList.get(0).getStart();
		//		int sentenceEnd = spansList.get(spansList.size()-1).getEnd();
		/*System.out.println("sentenceStart " + sentenceStart);
		System.out.println("sentenceEnd " + sentenceEnd);
		System.out.println("tagsList = " + tagsList);*/
		//single term
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals("B-M")) {
				if(!medicalterm.isEmpty()) {

					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);

						//if medical term contains '/'
						if(medicalterm.contains("/")) {
							System.out.println("In / " + spanStart +"-"+ spanEnd);
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}

							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();

							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								//System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;
									String spanStringShared = String.valueOf(spanStart)+"||"+String.valueOf(x)
											+"||"+String.valueOf(y)+"||"+String.valueOf(spanEnd);
									System.out.println("spanStringShared1 = "+ spanStringShared);
									outputSpanTermMap.put(spanStringShared, StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									String spanStringShared = String.valueOf(spanStart)+"||"+String.valueOf(x)
											+"||"+String.valueOf(y)+"||"+String.valueOf(spanEnd);
									System.out.println("spanStringShared2 = "+ spanStringShared);
									outputSpanTermMap.put(spanStringShared, StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;
									String spanStringShared = String.valueOf(spanStart)+"||"+String.valueOf(x)
											+"||"+String.valueOf(y)+"||"+String.valueOf(z)
											+"||"+String.valueOf(w)+"||"+String.valueOf(spanEnd);
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);
									System.out.println("spanStringShared3 = "+ spanStringShared);
									outputSpanTermMap.put(spanStringShared, sharedtermfinal);
								}

								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;

							}

						} else {
							outputSpanTermMap.put(spanString, medicalterm);
						}
						medicalterm="";
					}
				}


				medicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
				//				System.out.println("in B-M = " + spanStart);
			} else if (tagsList.get(i).equals("I-M")) {
				if (medicalterm.isEmpty()) {
					spanStart = spansList.get(i).getStart();
				}
				medicalterm = medicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();

			} else if(tagsList.get(i).equals("O")) {

				if(!medicalterm.isEmpty()) {

					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {


						//if medical term contains '/'
						if(medicalterm.contains("/")) {
							//System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}

							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();

							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								//System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}

									//									System.out.println("spanStringShared 0 = "+ spanStringShared.toString());
									outputSpanTermMap.put(ClinicalTokenizer.trimSpanString(spanStringShared.toString()), (StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd)).trim());
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 

									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									//									System.out.println("spanStringShared l = "+ spanStringShared.toString());
									outputSpanTermMap.put(ClinicalTokenizer.trimSpanString(spanStringShared.toString()), (StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd)).trim());
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(z)+"||");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"||"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);

									//									System.out.println("spanStringShared f = "+ spanStringShared);
									outputSpanTermMap.put(ClinicalTokenizer.trimSpanString(spanStringShared.toString()), sharedtermfinal.trim());
								}

								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;

							}

						} else {
							spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
							outputSpanTermMap.put(spanString, medicalterm);
						}
						medicalterm="";
					}
				}
			}

			if(i==tokensList.size()-1) {

				if(!medicalterm.isEmpty()) {
					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
						outputSpanTermMap.put(spanString, medicalterm);
						medicalterm="";
					}
				}
			}
		}

		List<String> disjointFirstTerms = new ArrayList<String>();
		List<String> disjointFirstspanstring = new ArrayList<String>();
		List<String> disjointSecondTerms = new ArrayList<String>();
		List<String> disjointSecondspanstring = new ArrayList<String>();
		medicalterm = "";
		spanString = "";
		spanStart = 0;
		spanEnd = 0;
		//disjoint double term first span
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals("DB-M")) {
				if(!medicalterm.isEmpty()) {

					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
						disjointFirstspanstring.add(spanString);
						disjointFirstTerms.add(medicalterm);
						medicalterm="";
					}
				}
				medicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals("DI-M")) {
				if (medicalterm.isEmpty()) {
					spanStart = spansList.get(i).getStart();
				}
				medicalterm = medicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("O")) {
				if(!medicalterm.isEmpty()) {
					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {

						if(medicalterm.contains("/")) {
							//							System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}

							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();

							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}


									disjointFirstspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 

									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									disjointFirstspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(z)+"||");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"||"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);

									disjointFirstspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointFirstTerms.add(sharedtermfinal);
								}

								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;

							}
						} else {
							spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
							disjointFirstspanstring.add(spanString);
							disjointFirstTerms.add(medicalterm);
						}


						medicalterm="";
					}
				}
			}
		}

		//disjoint double term second spans
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals("DHB-M")) {
				if(!medicalterm.isEmpty()) {

					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
						disjointSecondspanstring.add(spanString);
						disjointSecondTerms.add(medicalterm);
						medicalterm="";
					}
				}
				medicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals("DHI-M")) {
				if (medicalterm.isEmpty()) {
					spanStart = spansList.get(i).getStart();
				}
				medicalterm = medicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("O")) {
				if(!medicalterm.isEmpty()) {
					if(ClinicalTokenizer.isGarbageTerm(medicalterm)) {
						medicalterm = "";
					} else {

						if(medicalterm.contains("/")) {
							//							System.out.println("In / ");
							medicalterm = StringUtils.substring(fileText,spanStart, spanEnd);
							String[] medicalTermTokens = OpenNLPTokenizer.getOpenNLPTokenizer().tokenize
									(medicalterm);
							int k=0;
							String sharedToken = "";
							for (k=0; k < medicalTermTokens.length; k++) {
								if(medicalTermTokens[k].contains("/")) {
									sharedToken = medicalTermTokens[k];
									break;
								}
							}

							//span of start of shared token 
							int sharedTokenStartIndex  = spanStart + medicalterm.indexOf(sharedToken);
							int sharedTokenEndIndex = sharedTokenStartIndex + sharedToken.length();

							String[] sharedTokenSplit = sharedToken.split("/");
							int sharedTokenCurrentPOS = sharedTokenStartIndex;
							for (k=0; k< sharedTokenSplit.length;k++) {
								System.out.println("shared token = " + sharedTokenSplit[k]);
								if(k==0) {
									int x = sharedTokenStartIndex + sharedTokenSplit[k].length();
									int y = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}


									disjointSecondspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else if(k==sharedTokenSplit.length-1) {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenEndIndex-sharedTokenSplit[k].length();
									StringBuffer spanStringShared = new StringBuffer(); 

									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if(y<=spanEnd) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(spanEnd));
									}
									disjointSecondspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,spanEnd));
								} else {
									int x = sharedTokenStartIndex-1;
									int y = sharedTokenCurrentPOS;
									int z = sharedTokenCurrentPOS + sharedTokenSplit[k].length();
									int w = sharedTokenEndIndex+1;

									StringBuffer spanStringShared = new StringBuffer(); 


									if (spanStart<=x) {
										spanStringShared.append(String.valueOf(spanStart)+"||"+String.valueOf(x)+"||");
									} 
									if (y<=z) {
										spanStringShared.append(String.valueOf(y)+"||"+String.valueOf(z)+"||");
									} 
									if(w<=spanEnd) {
										spanStringShared.append(String.valueOf(w)+"||"+String.valueOf(spanEnd));
									}
									/*String spanStringShared = String.valueOf(spanStart)+"-"+String.valueOf(x)
											+","+String.valueOf(y)+"-"+String.valueOf(z)
											+","+String.valueOf(w)+"-"+String.valueOf(spanEnd);*/
									String sharedtermfinal =  StringUtils.substring(fileText, spanStart,x)+" "+
											StringUtils.substring(fileText, y,z) +  " "+
											StringUtils.substring(fileText, w,spanEnd);

									disjointSecondspanstring.add(ClinicalTokenizer.trimSpanString(spanStringShared.toString()));
									disjointSecondTerms.add(sharedtermfinal);
								}

								sharedTokenCurrentPOS += sharedTokenSplit[k].length()+1;

							}
						} else {
							spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
							disjointSecondspanstring.add(spanString);
							disjointSecondTerms.add(medicalterm);
						}


						medicalterm="";
					}
				}
			}
		}



		//get all combinations of disjoint terms
		for(int i=0;i< disjointFirstspanstring.size();i++) {
			for(int j=0;j< disjointSecondspanstring.size();j++) {
				outputSpanTermMap.put(disjointFirstspanstring.get(i) +"||"+disjointSecondspanstring.get(j),
						disjointFirstTerms.get(i)+" "+disjointSecondTerms.get(j));
			}
		}


		medicalterm = "";
		String shortmedicalterm = "";
		spanString = "";
		spanStart = 0;
		spanEnd = 0;
		//triplet terms span
		for (int i = 0; i < tokensList.size(); i++) {
			if(tagsList.get(i).equals("TB-M")) {
				if(!shortmedicalterm.isEmpty()) {
					spanString = spanString +"||"+String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
				}
				medicalterm = tokensList.get(i);
				shortmedicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals("TI-M")) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				shortmedicalterm = shortmedicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("THB-M")) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				shortmedicalterm = tokensList.get(i);
				spanStart = spansList.get(i).getStart();
				spanEnd = spansList.get(i).getEnd();
			} else if (tagsList.get(i).equals("THI-M")) {
				medicalterm = medicalterm + " " + tokensList.get(i);
				shortmedicalterm = shortmedicalterm + " " + tokensList.get(i);
				spanEnd = spansList.get(i).getEnd();
			} else if(tagsList.get(i).equals("O")) {
				if(!shortmedicalterm.isEmpty()) {
					if(spanString.isEmpty())
						spanString = String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
					else 
						spanString = spanString +"||"+String.valueOf(spanStart) + "||" + String.valueOf(spanEnd);
					//spanString = "";
				}
				shortmedicalterm = "";
			}
		}

		if(!spanString.isEmpty()) {
			outputSpanTermMap.put(spanString, medicalterm);
		}

		return outputSpanTermMap;

	}

	void createGazetterForGenia (File inputFile, File gazetterFile) throws IOException {
		FileUtils.deleteQuietly(gazetterFile);
		List<String> lines = FileUtils.readLines(inputFile);
		String tag = "";
		String term = "";
		for (String line : lines) {
			String[] split = line.split("\t");
			if (split.length == 2) {
				if(split[1].startsWith("B")) {
					if(!term.isEmpty()) {
						FileUtils.write(gazetterFile, tag + "\t" + term + "\n", true);
						term = "";
					}
					tag = split[1].split("-")[1];
					term = split[0];
				} else if(split[1].startsWith("I")) {
					term = term + " " + split[0];
				} else {
					if(!term.isEmpty()) {
						FileUtils.write(gazetterFile, tag + "\t" + term + "\n", true);
						term = "";
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param args
	 * @throws IOException 
	 */
	public static void main (String[] args) throws IOException {
//		args[0] = "/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/finaldata/clef2013Dataset/test/DS";
//		args[1] = "/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/finaldata/dnn/deepnlTestFeatureFiles_OP";
//		args[2] = "/home/raghavendra/BackUP/MyWorks/workspace/Hitachi2014Eclipse/finaldata/dnn/deepnlTestFeatureFiles_OP_Piped";
		DeepNlConvertor deepNlConvertor = new DeepNlConvertor();
		File corpusDir = new File (args[0]);
		File deepnlOPDir = new File (args[1]);
		File featuresFileWithLabel = null;
		//args[2] pipedoutput dir
		for (File corpusFile : corpusDir.listFiles()) {
			featuresFileWithLabel = new File(deepnlOPDir + File.separator + corpusFile.getName());
			deepNlConvertor.createPipeFileFromDeepnlOutput(corpusFile, featuresFileWithLabel, args[2]);
		}
		
		
		/*deepNlConvertor.createPipeFileFromDeepnlOutput
			(new File("temp/21312-018707-DISCHARGE_SUMMARY.txt"), 
					new File("temp/21312-018707-DISCHARGE_SUMMARY.txt_dnnop"), "temp/1");*/
		
		//
		//deepNlConvertor.createGazetterForGenia(new File(args[0]), new File(args[1]));
	}
}
