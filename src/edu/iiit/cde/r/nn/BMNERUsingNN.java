package edu.iiit.cde.r.nn;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import edu.iiit.cde.r.utils.ClefPipedLineParser;
import edu.iiit.cde.r.utils.PyBrainFileGenerator;

public class BMNERUsingNN {
	PyBrainFileGenerator pyBrainFileGenerator = new PyBrainFileGenerator();
	ClefPipedLineParser clefPipedLineParser = new ClefPipedLineParser();
	
	public void testBNER(String corpusDir, String pipedOutputDir) {

		try {
			FileUtils.deleteDirectory(new File("features"));
			
			FileUtils.deleteDirectory(new File("labels"));
			new File("features").mkdir();
			new File("labels").mkdir();
			File featuresDir = new File("features");
			File labelsDir = new File("labels");
			File[] corpusFiles = new File(corpusDir).listFiles();
			for (File corpusFile : corpusFiles) {
				
				String featuresFileString = featuresDir.getPath()+"/"+corpusFile.getName();
				System.out.println("Testing = " + featuresFileString);
				File featuresFile = new File(featuresFileString);
				// generate Features File - temp.txt
				pyBrainFileGenerator.createPyBrainFileDefault(corpusFile, featuresFile);
			}
	
			getLabelsForFeaturesFileUsingNN(featuresDir, labelsDir);
			
			File[] featuresFiles = featuresDir.listFiles();
			for (File featuresFile : featuresFiles) {
				
				File corpusFile = new File(corpusDir+"/"+featuresFile.getName());
				File modelOutputLabels = new File(labelsDir.getPath()+"/"+featuresFile.getName());
				
				clefPipedLineParser.createPipeFileFromFeaturesFileWithPPWithBIODTtags(corpusFile, featuresFile,
						modelOutputLabels, pipedOutputDir,"nn");
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void getLabelsForFeaturesFileUsingNN(File featuresDir, File labelsDir) {

		Process p;
		try {
			System.out.println("featuresDir" + featuresDir.getPath());
			ProcessBuilder builder = new ProcessBuilder(
					"python", "/home/raghavendra/BackUP/tools/DNN/NNPOS/clef2013_tagger_args.py",
					featuresDir.getAbsolutePath(), labelsDir.getAbsolutePath());
			p = builder.start();

			p.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

	}
	
	
	public static void main (String args[]) throws IOException {
		long start = System.currentTimeMillis();
		BMNERUsingNN bmnerUsingNN = new BMNERUsingNN();
		new File("./finaldata/clef2013Dataset/test1/apr20_DSPIPED_NNOP").mkdir();
		bmnerUsingNN.testBNER("./finaldata/clef2013Dataset/test1/DS",
				 "./finaldata/clef2013Dataset/test1/apr20_DSPIPED_NNOP");
		System.out.println("time taken = "
				+ (System.currentTimeMillis() - start) / (1000 * 60.0));

	}


}
