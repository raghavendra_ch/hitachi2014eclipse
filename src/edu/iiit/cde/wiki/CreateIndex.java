package edu.iiit.cde.wiki;
import java.util.Vector;

import edu.jhu.nlp.wikipedia.*;
import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class CreateIndex {

	    static Analyzer analyzer; 
		static Directory directory;
		static IndexWriterConfig config;
		static IndexWriter iwriter;
	//	static String path;
		static int count =0;
		public CreateIndex(String path){
			analyzer = new StandardAnalyzer(Version.LUCENE_42);
			try {
				directory = FSDirectory.open(new File(path));
				config =new IndexWriterConfig(Version.LUCENE_42, analyzer);
				iwriter =new IndexWriter(directory, config);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	public static void main(String[] args) throws IOException {
		   
		if(args.length==2)
		{
		        String dumpPath =  args[0];
		        String path = args[1];
	        	CreateIndex c= new CreateIndex(path);

		        WikiXMLParser wxsp = WikiXMLParserFactory.getSAXParser(dumpPath);
		                
		        try {
		                  
		            wxsp.setPageCallback(new PageCallbackHandler() { 
		                           public void process(WikiPage page) {
		                        	   
		                        	      String category = "";
		                                  String title = page.getTitle();
	                                      String text = page.getWikiText();
	                                      String link = "";
	                                      
		                        	   if(page.getCategories()!=null)
		                        	   {
		                        		    category = "";
		                        		   Vector <String>c = page.getCategories();
		                        		   for(int i=0;i<c.size();i++)
		                        		   {
		                        			   category+=c.get(i)+"\n";
		                        		   }
		                        		   
		                        		   
		                        	   }
	                                  if(page.getLinks()!=null)
	                                  {
	                                	  
		                        		   Vector <String>c = page.getLinks();
		                        		   for(int i=0;i<c.size();i++)
		                        		   {
		                        			   link+=c.get(i)+"\n";
		                        		   }
	                                  }
	                                  try {
										CreateIndex.AddIndex(title,text,category,link);
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
		                                  
		                           }
		            });
		                
		           wxsp.parse();
		        }catch(Exception e) {
		                e.printStackTrace();
		        }
		        iwriter.commit();
				iwriter.close();       
		}
		else
		{
			System.out.println("WikiPedia Dump Path, Folder where Index is created");
		}

	}
	public static void AddIndex(String title, String text, String category, String link) throws IOException
	{
		try {
			
		
		Document doc = new Document();
		TextField Title = new TextField("title",title, Field.Store.YES);
		TextField Text = new TextField("text",text, Field.Store.YES);
		TextField Category = new TextField("category",category, Field.Store.YES);
		TextField Link = new TextField("link",link, Field.Store.YES);
		doc.add(Title);
		doc.add(Text);
		doc.add(Category);
		doc.add(Link);
		iwriter.addDocument(doc);
		} catch (Exception e) {
		System.out.println("Unable to do "+title+"   "+category);
		}
		if(++count%10000==0)
		System.out.println(count);

	}

}
