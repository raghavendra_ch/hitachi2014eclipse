package edu.iiit.cde.wiki;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import edu.iiit.cde.r.utils.LRUCacheImpl;

public class SearchIndex {
	static String path="/home/cde/Wikipedia/wikiindex";
//	static String path="wikiresources/wikiindex";
	static String outputPath="wikiSearchresult.txt";
	static int required = 1;
	IndexSearcher searcher;
	Analyzer analyzer;
	QueryParser parser;
	static SearchIndex instance= null;
	
	
//	static LinkedHashMap<String, List<String>> queryToCategoriesCache = new 
	LRUCacheImpl<String, List<String>> wikiCategorylruCache = new LRUCacheImpl<String, List<String>>(1000000, 0.75f);
	
	@SuppressWarnings("deprecation")
	public SearchIndex() throws IOException
	{
		DirectoryReader reader = DirectoryReader.open(FSDirectory.open(new File(path)));
		searcher = new IndexSearcher(reader);
		analyzer = new StandardAnalyzer(Version.LUCENE_40);
		parser = new QueryParser(Version.LUCENE_40, "text", analyzer);
	}
	
	public static SearchIndex getInstance () throws IOException {
		if (instance == null) {
			instance = new SearchIndex();
		}
		return instance;
	}
	
	public void searching(String userquery) throws IOException, ParseException
	{
		/*System.out.println(path);
		System.out.println(new File(path).exists());*/
		
		userquery = "title:("+userquery+")";
		Query query = parser.parse(userquery);

		TopDocs results = searcher.search(query, 5);
		ScoreDoc[] hits = results.scoreDocs;
		int numTotalHits = results.totalHits;
		if(numTotalHits > 0)
			hits = searcher.search(query, numTotalHits).scoreDocs;
		System.out.println(numTotalHits + " total matching documents");
		int count=0;
		BufferedWriter bw= new BufferedWriter(new FileWriter(outputPath));		
		for(int i=0;i<numTotalHits && i<10;i++)
		{
			bw.write("---------------------------------------------------");
			bw.newLine();
			Document doc = searcher.doc(hits[i].doc);
			String title= doc.get("title");
			String text=doc.get("text");
			String category=doc.get("category");
			String link=doc.get("link");

			bw.write("<<<<<Title>>>>>");
			bw.newLine();
			bw.write(title);
			bw.newLine();

			bw.write("<<<<<Text>>>>>");
			bw.newLine();
			bw.write(text);
			bw.newLine();

			bw.write("<<<<<Category>>>>>");
			bw.newLine();
			bw.write(category);
			bw.newLine();
			bw.write("<<<<<link>>>>");
			bw.newLine();
			bw.write(link);
			bw.newLine();

		}
		bw.close();
	}
	
	void hasWikiPage(String userquery) throws IOException, ParseException {
		
		userquery = "title:("+userquery+")";
		Query query = parser.parse(userquery);

		TopDocs results = searcher.search(query, 5);
		ScoreDoc[] hits = results.scoreDocs;
		int numTotalHits = results.totalHits;
		if(numTotalHits > 0)
			hits = searcher.search(query, numTotalHits).scoreDocs;
		
		System.out.println(userquery + " has " + numTotalHits + " total matching documents");
		for(int i=0;i<numTotalHits && i<5;i++)
		{
			Document doc = searcher.doc(hits[i].doc);
			String title= doc.get("title");
			System.out.println(title);
		}
	}
	
	public List<String> getCategory(String userquery) throws IOException {
		
		if(wikiCategorylruCache.containsKey(userquery)) {
			return wikiCategorylruCache.get(userquery);
		} else {
			
			List<String> categories=new ArrayList<String>();
			userquery = "title:("+userquery+")";
			Query query = null;
			try {
				query = parser.parse(userquery);
				TopDocs results = searcher.search(query, 5);
				ScoreDoc[] hits = results.scoreDocs;
				int numTotalHits = results.totalHits;
				if(numTotalHits > 0)
					hits = searcher.search(query, numTotalHits).scoreDocs;
				
				//System.out.println(numTotalHits + " total matching documents");
				
				for(int i=0;i<numTotalHits && i<10;i++) {
					Document doc = searcher.doc(hits[i].doc);
					String title= doc.get("title");
//					System.out.println("title = " + title);
					String text=doc.get("text");
					String category = doc.get("category");
					
//					System.out.println("category = " + category);
					String[] split = StringUtils.split(category, "\n");
					for (String sp : split) {
						if(!sp.isEmpty())
							categories.add(sp.trim());
					}
					wikiCategorylruCache.put(userquery, categories);
					return categories;
				}
			} catch (ParseException e) {
	//			e.printStackTrace();
			}
			wikiCategorylruCache.put(userquery, categories);
			return categories;
		}
	}
	
	public String get_first_paragraph(String userquery) throws IOException, ParseException
	{
		userquery = "title:("+userquery+")";
		Query query = parser.parse(userquery);
		TopDocs results = searcher.search(query, 5);
		ScoreDoc[] hits = results.scoreDocs;
		int numTotalHits = results.totalHits;
		if(numTotalHits > 0) { 
			hits = searcher.search(query, numTotalHits).scoreDocs;
		int i=0;
		Document doc = searcher.doc(hits[i].doc);
		String text=doc.get("text");
		if(text.startsWith("{{Infobox"))
		{
			String paragraph = extract_paragraph(text);
			return paragraph;
		}
		else if (text.startsWith("#REDIRECT"))
		{
			int begin_index=0,end_index=0;
			for (begin_index=0;begin_index<text.length();begin_index++)
			{
				if(text.substring(begin_index).startsWith("[["))
				{
					end_index=begin_index;
					while(!text.substring(begin_index,end_index).endsWith("]]"))
						end_index++;
					break;
				}
			}
			//	System.out.println(text.substring(k+2,j-2));
			userquery = "title:("+text.substring(begin_index+2,end_index-2)+")";
			try
			{
			Query query_redirect = parser.parse(userquery);
			TopDocs results_redirect = searcher.search(query_redirect, 5);
			ScoreDoc[] hits_redirect = results_redirect.scoreDocs;
			int numTotalHits_redirect = results_redirect.totalHits;
			if(numTotalHits_redirect > 0) hits = searcher.search(query, numTotalHits).scoreDocs;
			int z=0;
			
			Document doc_redirect = searcher.doc(hits_redirect[z].doc);
			String text_redirect=doc_redirect.get("text");
			String paragraph = extract_paragraph(text_redirect);
			return paragraph;
			}
			catch(Exception e)
			{
				System.out.println("Error");
			}
			
		}
		}
		return null;
	}


	private String extract_paragraph(String text) {
		String para="";
		String []lines = text.split("\\n");
		int i=0;
		while(i<lines.length && !lines[i].equals("}}")) i++;
		i++;
		while(i<lines.length)
		{
			para=para+"\n"+lines[i];
			i++;
			if(lines[i].startsWith("==") && lines[i].endsWith("=="))
				break;

		}
		return para;
	}
	
	public static void main(String [] args) throws IOException 
	{
		String term = ".";
		if(args.length==1) {
			term = args[0];
			SearchIndex sc=new SearchIndex();
			try {
				sc.searching("title:("+term+")");
				System.out.println();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		if (args.length ==2 && args[0].equals("haswiki")) {
			SearchIndex sc=new SearchIndex();
			for (String term1 : FileUtils.readLines(new File (args[1])))
				try {
					sc.hasWikiPage(term1);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		if (args.length ==2 && args[0].equals("firstpara")) {
			SearchIndex sc=new SearchIndex();
			for (String term1 : FileUtils.readLines(new File (args[1])))
				try {
					System.out.println(term1 + " ==> " + sc.get_first_paragraph(term1));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		
//		System.out.println("="  + sc.getCategory(term));
		
		
		
	}
	

}
