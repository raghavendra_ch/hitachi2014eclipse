package edu.iiit.cde.anafora.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import edu.iiit.cde.r.utils.HashMapUtilsCustom;

public class AnaforaConverter {
        
        public AnaforaConverter() {
        }
	/**
	 * Usage: corpus - corpus/patientDir/patientxmlFiles
	 * 			pipedoutputs - dirPipedFiles
	 * @param corpus
	 * @param pipedOutputs
	 */
	void convertAnaforaXMLToPipedOutputs (String corpus, String pipedOutputs) {

		File corpusDir = new File(corpus);
		File pipedDir = new File(pipedOutputs);

		for(File patientDir : corpusDir.listFiles()) {
			String patientName = patientDir.getName();
			File patientAnaforaXML = new File(corpusDir.getAbsolutePath()+"/"+patientName+"/"+patientName+"ID004_clinic_010.DiseaseEntity.anafora.completed.xml");
			if(!patientAnaforaXML.exists()) {
				patientAnaforaXML = new File(corpusDir.getAbsolutePath()+"/"+patientName+"/"+patientName+"ID004_clinic_010.DiseaseEntity.anafora.inprogress.xml");
			}
			readAnaforaXML(patientAnaforaXML, new File(pipedDir.getAbsolutePath()+"/"+patientName.replace("txt", "pipe.txt")));
		}
	}

	File readAnaforaXML (File fXmlFile, File pipedOutput) {	
		try {

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("entity");

			List<String> pipedLines = new ArrayList<String>();
			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					/*Current Element :entity
					First Name : 12@e@ID004_clinic_010@anafora
					Last Name : 557,567;586,591
					Nick Name : heart
					Salary : DiseaseEntities*/
					//				System.out.println("Staff id : " + eElement.getAttribute("id"));
					/*System.out.println("First Name : " + eElement.getElementsByTagName("id").item(0).getTextContent());
					System.out.println("Last Name : " + eElement.getElementsByTagName("span").item(0).getTextContent());
					System.out.println("Nick Name : " + eElement.getElementsByTagName("type").item(0).getTextContent());
					System.out.println("Salary : " + eElement.getElementsByTagName("parentsType").item(0).getTextContent());
					System.out.println("Salary : " + eElement.getElementsByTagName("properties").item(0).getTextContent());*/

					
					pipedLines.add(pipedOutput.getName().replace(".pipe.", ".")+"|" 
							+ eElement.getElementsByTagName("span").item(0).getTextContent().replace(",","-").replace(";",",") +"|"
							+ getIndexOfDisease(eElement.getElementsByTagName("type").item(0).getTextContent()));

				}
			}
			FileUtils.writeLines(pipedOutput, pipedLines);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pipedOutput;
	}


	public void writeAnaforaXML (File pipedOutput, File fXmlFile) {

		/*3 <data>
		  4 <info>
		  5   <savetime>11:33:11 06-06-2015</savetime>
		  6   <progress>completed</progress>
		  7 </info>
		  8 
		  9 <schema path="./" protocol="file">temporal.schema.xml</schema>*/

		try {

			List<String> pipedLines = FileUtils.readLines(pipedOutput);

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element dataElement = doc.createElement("data");
			doc.appendChild(dataElement);

			//info block
			Element infoElement = doc.createElement("info");
			dataElement.appendChild(infoElement);

			Element savetimeElement = doc.createElement("savetime");
			savetimeElement.appendChild(doc.createTextNode(new Date(System.currentTimeMillis()).toString()));
			infoElement.appendChild(savetimeElement);

			Element progressElement = doc.createElement("progress");
			progressElement.appendChild(doc.createTextNode("inprogress"));
			infoElement.appendChild(progressElement);


			//annotations block
			Element annotationsElement = doc.createElement("annotations");
			dataElement.appendChild(annotationsElement);
			int id = 1;
			for (String line : pipedLines) {

				//System.out.println(line);
				String[] split = line.split("\\|");
				//System.out.println("split length = " + line +split.length);
				String filename = split[0];
				String spanText = split[1].replace(",",";").replace("-",",");
				String diseaseIndex = split[split.length-1];
				/*if(diseaseIndex!="0")
					System.out.println(pipedOutput);*/
				String diseaseType =  getDiseaseFromIndex(diseaseIndex);

				// entity elements
				Element entityElement = doc.createElement("entity");
				annotationsElement.appendChild(entityElement);

				// set attribute to staff element
				/*Attr attr = doc.createAttribute("id");
				attr.setValue("1");
				staff.setAttributeNode(attr);*/

				// shorten way
				// staff.setAttribute("id", "1");

				// firstname elements
				Element idElement = doc.createElement("id");
				//>3@e@ID004_clinic_010@anafora
				idElement.appendChild(doc.createTextNode(id+"@e@"+filename/*.replace("(", "").replace(")", "")*/+"@anafora"));			
				entityElement.appendChild(idElement);
				id++;

				// span elements
				Element spanElement = doc.createElement("span");
				spanElement.appendChild(doc.createTextNode(spanText));
				entityElement.appendChild(spanElement);

				// type elements
				Element diseaseTypeElement = doc.createElement("type");
				diseaseTypeElement.appendChild(doc.createTextNode(diseaseType));
				entityElement.appendChild(diseaseTypeElement);

				// salary elements
				Element parentsTypeElement = doc.createElement("parentsType");
				parentsTypeElement.appendChild(doc.createTextNode("DiseaseEntities"));
				entityElement.appendChild(parentsTypeElement);

				Element propertiesElement = doc.createElement("properties");
				entityElement.appendChild(propertiesElement);
			}

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(fXmlFile);

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);

			System.out.println("File saved!");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void convertPipedOutputsToAnaforaXMLs (String pipedOutputsDirString, String corpus) {

		File corpusDir = new File(corpus);
		File pipedDir = new File(pipedOutputsDirString);

		for(File pipedFile : pipedDir.listFiles()) {
			String patientName = pipedFile.getName().replace(".pipe.", "");
			File patientAnaforaXML = new File(corpusDir.getAbsolutePath()+"/"+patientName+"/"+patientName+"ID004_clinic_010.DiseaseEntity.anafora.inprogress.xml");
			//patientAnaforaXML = new File(corpusDir.getAbsolutePath()+"/"+patientName+"/"+patientName+"ID004_clinic_010.DiseaseEntity.anafora.completed.xml");

			writeAnaforaXML(pipedFile, patientAnaforaXML);
		}
	}

	String getIndexOfDisease(String disease){
		String index="0";
		switch (disease) {
		case "AIDS":
			index = "1";
			break;
		case "arthritis":
			index = "2";
			break;
		case "cancer":
			index = "3";
			break;
		case "diabetes":
			index = "4";
			break;
		case "asthma":
			index = "5";
			break;
		case "braincancer":
			index = "6";
			break;
		case "breastcancer":
			index = "7";
			break;
		case "headandneckcancer":
			index = "8";
			break;
		case "lungcancer":
			index = "9";
			break;
		case "prostatecancer":
			index = "10";
			break;
		default:
			index = "0";
		}

		return index;
	}

	String getDiseaseFromIndex (String index){
		String disease = "";
		switch (index) {
		case "1":
			disease = "AIDS";
			break;
		case "2":
			disease = "arthritis";
			break;
		case "3":
			disease = "cancer";
			break;
		case "4":
			disease = "diabetes";
			break;
		case "5":
			disease = "asthma";
			break;
		case "genericcancer":
			disease = "cancer";
			break;
		case "braincancer":
			disease = "braincancer";
			break;
		case "breastcancer":
			disease = "breastcancer";
			break;
		case "headandneckcancer":
			disease = "headandneckcancer";
			break;
		case "lungcancer":
			disease = "lungcancer";
			break;
		case "prostatecancer":
			disease = "prostatecancer";
			break;
		default:
			disease = "others";
		}

		return disease;
	}

	/**
	 * Create a json file containing the word count of all medical terms present in the project.
	 * @param projectPath
	 * @return
	 */
	File getWordCountJsonOfProject (String projectPath, int mincount) {
		File project = new File (projectPath);
		String projectName = getProjectName (projectPath);
		String jsonFileName = "wordCount_"+projectName;
		File jsonFile = new File(jsonFileName);
		FileUtils.deleteQuietly(jsonFile);
		HashMap<String, Integer> medicaltermCountMap_all = new HashMap<>();
//		FileFilterUtils.nameFileFilter("DiseaseEntity")
		Collection<File> xmlFiles = FileUtils.listFiles(project, 
//				FileFilterUtils.nameFileFilter("inprogress.xml"), TrueFileFilter.INSTANCE);
				TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		//System.out.println(xmlFiles.size());
		
		for (File xmlFile : xmlFiles) {
			if(!xmlFile.getName().contains("anafora")) {
				continue;
			}
			//System.out.println(xmlFile.getAbsolutePath());
			File textFile = new File(StringUtils.substringBeforeLast(xmlFile.getAbsolutePath(), ".txt") + ".txt");
			HashMapUtilsCustom.mergeHashMapKeyFreqCount(medicaltermCountMap_all, getMedicaltermsCountFromAnaforaXML(xmlFile, textFile));
			//System.out.println("Counting for file = " + xmlFile.getName() + " \nMedical terms size = " +medicaltermCountMap_all.size());
		}

		String json = createJsonFromMap(medicaltermCountMap_all, mincount);
		System.out.println(json);
		try {
			FileUtils.write(jsonFile, json);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonFile;
	}

	/**
	 * Create a json from the wordCount map.
	 * @param medicaltermCountMap_all
	 * @return
	 */
	private String createJsonFromMap(
			HashMap<String, Integer> medicaltermCountMap_all, int min_count) {

		JsonArray jsonArray = new JsonArray();
		for (Entry<String, Integer> entry : medicaltermCountMap_all.entrySet()) {
				if(entry.getValue() > min_count) {
					JsonObject jsonObj=new JsonObject();
					jsonObj.addProperty("text", entry.getKey());
					jsonObj.addProperty("size", entry.getValue());
					jsonArray.add(jsonObj);
				}
		}
		
		return jsonArray.toString();
	}

	/**
	 * 
	 * @param fXmlFile
	 * @param textFile
	 * @return
	 */
	@SuppressWarnings("resource")
	HashMap<String, Integer> getMedicaltermsCountFromAnaforaXML (File fXmlFile, File textFile) {
		HashMap<String, Integer> medicaltermCountMap = new HashMap<>();
		try {

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new BOMInputStream(new FileInputStream(fXmlFile), true));
			String text = FileUtils.readFileToString(textFile);

			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

//			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("entity");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
				//System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String medicalTerm = getTermFromSpan (eElement.getElementsByTagName("span").item(0).getTextContent(), text);
					HashMapUtilsCustom.incrementKeyFreqCount(medicaltermCountMap, medicalTerm);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return medicaltermCountMap;
	}

	/**
	 * Extract term from the text using span value.
	 * @param span
	 * @param text
	 * @return
	 */
	private String getTermFromSpan(String span, String text) {
		String[] spanString = span.split(";");
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < spanString.length; i++) {
			sb.append(StringUtils.substring(text, Integer.parseInt(spanString[i].split(",")[0]), Integer.parseInt(spanString[i].split(",")[1])) + " ");
		}
		return sb.toString().trim();
	}

	/**
	 * Get the project name from the given project path.
	 * @param projectPath
	 * @return
	 */
	private String getProjectName(String projectPath) {
		String projectName = projectPath;
		if(projectPath.endsWith("/")) {
			if(StringUtils.countMatches(projectPath, "/") > 1) {
				String[] terms = StringUtils.substringsBetween(projectPath, "/", "/");
				if(terms.length > 0)
					projectName = terms[terms.length-1]; 
			} 
		} else {
			projectName = StringUtils.substringAfterLast(projectPath, "/");
		}
		return projectName;
	}

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		AnaforaConverter anaforaConverter = new AnaforaConverter();
		/*anaforaConverter.readAnaforaXML(new File("/home/raghavendra/BackUP/MyWorks/workspace/AnaforaJavaInterface/ID004_clinic_010.DiseaseEntity.anafora.completed.xml")
		,new File("op"));*/
		/*anaforaConverter.readAnaforaXML(new File("pubmedDataset/breastCancerDocs_anafora/"
				+ "Breast_Cancer_(Dove_Med_Press)_0.txt/Breast_Cancer_(Dove_Med_Press)_0.txt.DiseaseEntity.anafora.inprogress.xml")
		,new File("op_breast"));*/
		
		/*anaforaConverter.writeAnaforaXML(new File("pubmedDataset/sampleDocs_pma_crfh_merged_diseasetagged/Brain_Tumor_Pathol_0.pipe.txt"), 
				new File("xmlop"));*/
		

		if(args.length!=3) {
			System.err.println("AnaforaConverter <corpusDir> <pipedoutputDir> <OPDir>");
			System.exit(0);
		} else if(args.length==3) {
			System.out.println("Convert Piped outputs to Anafora Xmls" + args[0]);
			new File(args[2]).mkdir();
			 CopyOption[] options = new CopyOption[]{
				      StandardCopyOption.REPLACE_EXISTING,
				      StandardCopyOption.COPY_ATTRIBUTES
				    }; 
			File pipedOutputDir = new File(args[1]);
			File[] pipedOutputDirFiles = pipedOutputDir.listFiles();
			for (File pipedOutputDirFile : pipedOutputDirFiles ) {
				String docName = pipedOutputDirFile.getName().replace(".pipe.txt", ".txt");
	
				new File(args[2]+"/"+docName).mkdir();
	
				// ID004_clinic_010/ID004_clinic_010
				//Corpus/Brain_Tumor_Pathol_0.txt/Brain_Tumor_Pathol_0.txt
				//Corpus/Brain_Tumor_Pathol_0.txt/Brain_Tumor_Pathol_0.txt.DiseaseEntity.anafora.completed.xml
				//copying corpus File to the anafora Dir where is patient record is a directory
				Path FROM = Paths.get(args[0]+"/"+docName);
				Path TO = Paths.get(args[2]+"/"+docName+
						"/"+docName);
				Files.copy(FROM, TO, options);
				anaforaConverter.writeAnaforaXML(pipedOutputDirFile, 
						new File(args[2]+"/"+docName+"/"+docName+".DiseaseEntity.anafora.completed.xml"));
			}
		}

		//wordcount of medical terms
//		anaforaConverter.getWordCountJsonOfProject("pubmedDataset/",5);
		/*if(args.length != 2) {
			System.err.println("Usage: java -cp lib/*:jar edu.iiit.cde.anafora.util.AnaforaConverter <projectPath> <min_count>");
			System.exit(0);
		} */
		//anaforaConverter.getWordCountJsonOfProject(args[0], Integer.parseInt(args[1]));
	}

}
